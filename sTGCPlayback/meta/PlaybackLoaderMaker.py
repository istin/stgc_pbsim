#!/usr/bin/env python
import platform
import os
import sys
import glob


if(len(sys.argv)!=2):
    print("Arg Error in "+sys.argv[0]+" !!!")
    os.sys.exit()

INSTALLATION=sys.argv[1]
INSTALLATION=os.path.abspath(INSTALLATION)

if not os.path.isdir(INSTALLATION) :
    print(INSTALLATION+" does not exist!")
    os.sys.exit()
print("Generating Playback Loader from "+INSTALLATION)


pfm=platform.system()
shext=""


if pfm == "Linux":
    shext="*.so"
elif pfm == "Darwin":
    shext="*.dylib"
else :
    print("Platform "+pfm+" not supported")
    os.sys.exit()
PLAYBACK_LIBS=INSTALLATION+"/playback/lib/"

liblist=glob.glob(PLAYBACK_LIBS+"/"+shext)

loadees=[]

for lib in liblist:
    tok=lib.split('/')
    n=len(tok)
    libname=tok[n-1]
    loadees.append(libname)


#create Loaders
f= open(INSTALLATION+"/modules/"+"/__init__.py", "wr+")
f.write("import ROOT\n")
f.write("ROOT.PyConfig.IgnoreCommandLineOptions=True\n")
f.write("from ROOT import gSystem as gs\n")
for lib in loadees :
    f.write("gs.Load('"+PLAYBACK_LIBS+lib+"')\n")       

for subdir, dirs, files in os.walk(INSTALLATION+"/modules/"):
    for dir in dirs:
        fulldir=os.path.abspath(dir)
        print os.path.join(subdir)+"/"+dir+"/__init__.py"
        f= open(os.path.join(subdir)+"/"+dir+"/__init__.py", "a+")
        f.write("import ROOT\n")
        f.write("ROOT.PyConfig.IgnoreCommandLineOptions=True\n")
        f.write("from ROOT import gSystem as gs\n")
        for lib in loadees :
            f.write("gs.Load('"+PLAYBACK_LIBS+lib+"')\n")
