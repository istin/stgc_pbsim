#include "Detector/DetSvc.h"
#include "GeometryModel/QuadrupletConfigurationParser.h"
#include <sstream>
#include <numeric>
#include <algorithm>
#include <Utilities/LoggingSvc.h>


DetSvc* DetSvc::m_inst=0;

DetSvc* DetSvc::instance(){
    if (m_inst == 0){
        m_inst = new DetSvc();
    }
    return m_inst;
}

DetSvc::~DetSvc(){
    delete m_inst;
}

DetSvc::DetSvc(){
    _ReadMaster();
    STGC_GEOMETRY::QuadrupletConfigurationParser qcp;
    qcp.SetConfigurationFile(std::string(std::getenv("PBDATAPATH"))+"/"+m_config.geomodelfile);
    qcp.SetXMLRoot();
    qcp.Parse();
    m_quadlookup=qcp.GetQuadLookup();
    
    m_sideA=Side(0,m_config.ActiveSectorsSideA);
    m_sideC=Side(1,m_config.ActiveSectorsSideC);
    for(const auto & sector : m_sideA){
        for(const auto& quad: *sector){
            auto qgeoconfig=m_quadlookup[quad->quadNameAGDD()];
            quad->AddLayer(qgeoconfig.nStrips);
            quad->AddLayer(qgeoconfig.nStrips);
            quad->AddLayer(qgeoconfig.nStrips);
            quad->AddLayer(qgeoconfig.nStrips);
        }
    }
    
    for(const auto & sector : m_sideC){
        for(const auto& quad: *sector){
            auto qgeoconfig=m_quadlookup[quad->quadNameAGDD()];
            quad->AddLayer(qgeoconfig.nStrips);
            quad->AddLayer(qgeoconfig.nStrips);
            quad->AddLayer(qgeoconfig.nStrips);
            quad->AddLayer(qgeoconfig.nStrips);
        }
    }
    
    //Now has all detector components
    
    for(  auto& sector: m_sideA){
        m_sectorHash.insert( std::make_pair(hashSector(*sector),sector) );
        for( auto& quad : *sector){
            m_quadHash.insert( std::make_pair(hashQuad(*quad),quad) );
            for( auto& strlayer : *quad){
                m_stripLayerHash.insert(std::make_pair(hashStripLayer(*strlayer),strlayer) );
                for(auto& strip : * strlayer){
                    m_stripHash.insert( std::make_pair(hashStrip(*strip),strip) );
                }
            }
        }
        
    }
    
    for(  auto& sector: m_sideC){
        m_sectorHash.insert( std::make_pair(hashSector(*sector),sector) );
        for( auto& quad : *sector){
            m_quadHash.insert( std::make_pair(hashQuad(*quad),quad) );
            for( auto& strlayer : *quad){
                m_stripLayerHash.insert(std::make_pair(hashStripLayer(*strlayer),strlayer) );
                for(auto& strip : * strlayer){
                    m_stripHash.insert( std::make_pair(hashStrip(*strip),strip) );
                }
            }
        }
        
    }   
    
    LOG_INFO("Hashed "<<m_stripHash.size()<<" Strips");
    LOG_INFO("Hashed "<<m_stripLayerHash.size()<<" StripLayers");
    LOG_INFO("Hashed "<<m_quadHash.size()<<" Quadruplets");
    LOG_INFO("Hashed "<<m_sectorHash.size()<<" Sectors");
    
}//end of ctor


void DetSvc::_ReadMaster(const std::string& filename){
   
   TXMLEngine* xml = new TXMLEngine;
   XMLDocPointer_t xmldoc = xml->ParseFile(filename.c_str());
   if (xmldoc==0) {
      delete xml;
      return;
   }
   XMLNodePointer_t mainnode = xml->DocGetRootElement(xmldoc);
   _Walk(xml, mainnode, 1);
   xml->FreeDoc(xmldoc);
   delete xml;
}

void DetSvc::_Walk(TXMLEngine* xml, XMLNodePointer_t node, Int_t level){
   std::string curr_nodename(xml->GetNodeName(node));

   XMLAttrPointer_t attr = xml->GetFirstAttr(node);
   while (attr!=0) {
       std::string curr_attrname(xml->GetAttrName(attr));
       if(curr_nodename=="Playback" && curr_attrname=="geomodel"){
           m_config.geomodelfile=std::string(xml->GetAttrValue(attr));
       }
       
       else if (curr_nodename=="Playback" && curr_attrname=="cabling"){
           m_config.cablingfile=std::string(xml->GetAttrValue(attr));
           
       } 
       
       else if( curr_nodename=="sideA" && curr_attrname=="ActiveSectors"){
           std::string content(xml->GetAttrValue(attr));
           std::istringstream ss(content);
           std::string tok;
           while(std::getline(ss, tok, ';')) {
               m_config.ActiveSectorsSideA.push_back(std::stoi(tok));
           }
           
       }
       
       else if( curr_nodename=="sideC" && curr_attrname=="ActiveSectors"){
           std::string content(xml->GetAttrValue(attr));
           std::istringstream ss(content);
           std::string tok;
           while(std::getline(ss, tok, ';')) {
                m_config.ActiveSectorsSideC.push_back(std::stoi(tok));
           }           
           
       }       
       
       else{
            LOG_INFO("Badly Structured XML!");
            exit(8);
       }
       
       attr = xml->GetNextAttr(attr);
   }

   XMLNodePointer_t child = xml->GetChild(node);
   while (child!=0) {
      _Walk(xml, child, level+2);
      child = xml->GetNext(child);
   }
}


void DetSvc::DumpConfig(){
    LOG_INFO("****** DetSvc Configuration ****** ");
    LOG_INFO("GeoModel File : "<<m_config.geomodelfile);
    LOG_INFO("Cabling File : "<<m_config.cablingfile);
    LOG_INFO("Active Sectors at SideA : ");
    std::for_each(
              m_config.ActiveSectorsSideA.cbegin(),
              m_config.ActiveSectorsSideA.cend(),
              [] (int c) {std::cout << c << " ";} 
              );
   LOG_INFO("\n");
    LOG_INFO("Active Sectors at SideC : ");
    std::for_each(
              m_config.ActiveSectorsSideC.cbegin(),
              m_config.ActiveSectorsSideC.cend(),
              [] (int c) {std::cout << c << " ";} 
              );
   LOG_INFO("\n");
   LOG_INFO("****** ****************** ****** ");
}


std::size_t DetSvc::hashElement(const std::vector<int> & vec) {
  std::size_t seed = vec.size();
  for(auto& i : vec) {
    seed ^= i + 0x9e3779b9 + (seed << 6) + (seed >> 2);
  }
  return seed;
}



std::size_t DetSvc::hashSide(const Side& side){
    return hashElement({side.sideId()});
}

std::size_t DetSvc::hashSector(const Sector& sector){
    return hashElement({sector.sideId(),sector.sectorId()});
}

std::size_t DetSvc::hashQuad(const Quad& quad){
    return hashElement({quad.sideId(),quad.sectorId(),quad.moduleId(),quad.multilayerId()});
}

std::size_t DetSvc::hashStripLayer(const StripLayer& sl){
    return hashElement({sl.sideId(),sl.sectorId(),sl.moduleId(),sl.multilayerId(),sl.layerId()});
}


std::size_t DetSvc::hashStrip(const STGC::Strip& str){
    return hashElement({str.sideId(),str.sectorId(),str.moduleId(),str.multilayerId(),str.layerId(),str.channel()});
}


const std::shared_ptr<STGC::Strip>& DetSvc::getStrip(uint32_t identifier){
    return m_stripHash.at(identifier);
}


const std::shared_ptr<STGC::Strip>& DetSvc::getStrip(int side,int sector,int module,int multilayer,int layer,int channel){
    try{
        return m_stripHash.at(hashElement({ side, sector, module, multilayer, layer, channel}));
    }
    
    catch(...){
        LOG_FATAL("Unable to retrieve strip@ "<<Identifiable::quadNameAGDD(sector,module,multilayer)<<" ch#"<<channel<<" at sector "<<sector<<" side "<<side);
        exit(7);
    }
}


const std::shared_ptr<Quad>& DetSvc::getQuad(uint32_t identifier){
    return m_quadHash.at(identifier);
}


const std::shared_ptr<Quad>& DetSvc::getQuad(int side,int sector,int module,int multilayer){
    try{
        return m_quadHash.at(hashElement({side,sector,module,multilayer}));
    }
    catch(std::exception& e){
        LOG_FATAL("Unable to retrieve quad@ "<<Identifiable::quadNameAGDD(sector,module,multilayer)<<" at sector "<<sector<<" side "<<side);
        exit(8);
    }
}


const STGC_GEOMETRY::QuadrupletConfiguration& DetSvc::quadLUTfromAGDD(const TString& name){

    return m_quadlookup.at(name);

}



void DetSvc::dumpStripTable()const{
    
    for(const auto& kv : m_stripHash){
        LOG_INFO(kv.first);
    }

    return;
}
