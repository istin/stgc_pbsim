#ifndef SECTOR_H
#define SECTOR_H
#include <vector>
#include <memory>
#include <string>
#include <Detector/Quad.h>



class Sector : public Identifiable,public std::vector<std::shared_ptr<Quad>>{
public:
    Sector(int,int);
    Sector()=delete;
    int layerId()=delete;
    int multilayerId()=delete;
    std::string quadNameAGDD()const=delete;
private:
    int m_nmaxquads;
    
};



#endif

