#ifndef IDENTIFIABLE_H
#define IDENTIFIABLE_H

#include <string>

class Identifiable{
    protected:  
        int m_sideId;
        int m_sectorId;
        int m_moduleId;
        int m_multilayerId;
        int m_layerId;
    public:
        Identifiable();
        Identifiable( int sideID,int sectorId,int moduleId,int multilayerId);
        static std::string quadNameAGDD(int,int,int);
        std::string quadNameAGDD()const;
        void dump();
        int sideId() const {return m_sideId;}
        int sectorId() const {return m_sectorId;}
        int moduleId() const {return m_moduleId;}
        int multilayerId() const{ return m_multilayerId;}
        int layerId() const{ return m_layerId;}
        void setLayerID(int i){m_layerId=i;}
        void setMultilayerId(int i){m_multilayerId=i;}
        void setModuleId(int i){m_moduleId=i;}
        void setSectorId(int i){m_sectorId=i;}
        void setSideId(int i){m_sideId=i;}
    
};

#endif
