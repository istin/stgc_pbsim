#include <Detector/SectorBands.h>
#include <list>

SectorBands::SectorBands(){
}


DetSvc* SectorBands::detSvc(){
    return DetSvc::instance();
}

void SectorBands::buildBandsFromAGDD(){


    Sector large(1,1);
    std::list<std::string> quad_names_large={"sTG1-QL1P","sTG1-QL2P","sTG1-QL3P","sTG1-QL1C","sTG1-QL2C","sTG1-QL3C"};
    for(const std::string& name : quad_names_large){
        STGC_GEOMETRY::QuadrupletConfiguration qcfg=detSvc()->quadLUTfromAGDD(name);
        std::vector<int> firstStripInTrigger=qcfg.firstStripInTrigger;
        std::vector<int> StripsInBandsLayer1=qcfg.StripsInBandsLayer1;
        std::vector<int> StripsInBandsLayer2=qcfg.StripsInBandsLayer1;
        std::vector<int> StripsInBandsLayer3=qcfg.StripsInBandsLayer1;
        std::vector<int> StripsInBandsLayer4=qcfg.StripsInBandsLayer1;
        int nStrips=qcfg.nStrips;
    }//quad loop


    return;
}


