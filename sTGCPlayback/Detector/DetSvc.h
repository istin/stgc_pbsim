#ifndef DETSVC_H
#define DETSVC_H
#include <string>
#include <TXMLEngine.h>
#include <GeometryModel/QuadrupletConfiguration.h>
#include <map>
#include <TString.h>
#include "Detector/Side.h"
#include "Detector/Sector.h"
#include "Detector/Quad.h"
#include "Detector/StripLayer.h"
#include "Detector/Strip.h"

#include <unordered_map>//for detector hash


struct MasterConfig{  
    std::string geomodelfile;
    std::string cablingfile;
    std::vector<int> ActiveSectorsSideA;
    std::vector<int> ActiveSectorsSideC;
};

class DetSvc{
     
    DetSvc(const DetSvc& )=delete;
    DetSvc& operator=(const DetSvc&) = delete;
    DetSvc(DetSvc&&) = delete;
    DetSvc& operator=(DetSvc&&) = delete;

public:
    static DetSvc* instance();
    const MasterConfig& getConfig() const{ return m_config; }
    void DumpConfig();
    const Side& SideA() const{ return m_sideA;}
    const Side& SideC() const { return m_sideC;}
    
    const std::shared_ptr<STGC::Strip>& getStrip(uint32_t);
    const std::shared_ptr<STGC::Strip>& getStrip(int,int,int,int,int,int);
    
    const std::shared_ptr<Quad>& getQuad(uint32_t);
    const std::shared_ptr<Quad>& getQuad(int,int,int,int);
    
    void dumpStripTable()const;
    
    const STGC_GEOMETRY::QuadrupletConfiguration& quadLUTfromAGDD(const TString&);

    ~DetSvc();
    static std::size_t hashElement(const std::vector<int>&);

private:
    DetSvc();
    static DetSvc* m_inst;    
    void _Walk(TXMLEngine* xml, XMLNodePointer_t node, Int_t level);
    void _ReadMaster(const std::string& filename =std::string(std::getenv("PBDATAPATH"))+"/PlaybackConfiguration.xml");
    MasterConfig m_config;
    std::map<TString, STGC_GEOMETRY::QuadrupletConfiguration> m_quadlookup;
    
    Side m_sideA;
    Side m_sideC;
    
    std::unordered_map<std::size_t,std::shared_ptr<STGC::Strip>> m_stripHash;
    std::unordered_map<std::size_t,std::shared_ptr<StripLayer>> m_stripLayerHash;
    std::unordered_map<std::size_t,std::shared_ptr<Quad>> m_quadHash;
    std::unordered_map<std::size_t,std::shared_ptr<Sector>> m_sectorHash;
    
    std::size_t hashSide(const Side&);
    std::size_t hashSector(const Sector&);
    std::size_t hashQuad(const Quad&);
    std::size_t hashStripLayer(const StripLayer&);
    std::size_t hashStrip(const STGC::Strip&);
    
    
    
    
    
};



#endif
