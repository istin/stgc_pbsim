#include <Detector/Strip.h>



namespace STGC{

    Strip::Strip(int chno):
    	                  m_charge(0),m_time(0),
    	                  m_channel(chno),m_state(READY),
                          m_notifiable(0)
    {
        
    }
    
    void Strip::setCharge(float q){
        m_charge=q;
        if (m_notifiable != nullptr)
        	m_notifiable->Notify();
    }

    void Strip::setState(int s){
        m_state=s;
    }

    int Strip::channel()const{
        return m_channel;
    }

    float Strip::charge()const{
        return m_charge;
    }

    float Strip::time() const{
        return m_time;
        
    }

    std::string Strip::sABName()const{
        std::string res=sABType()+="-";
        
        switch(m_sideId){
            case(0):
                res+="C_";
                break;
            case(1):
                res+="A_";
                break;
                
            default:
                break;
        }
        if(m_sectorId<=9) res+="0";
        res+=std::to_string(m_sectorId);
        return res;
    }    

    void Strip::Reset(){
        this->setCharge(0);
        this->setNotifiable(nullptr);
        this->setState(STGC::READY);
    }
    
    
}

