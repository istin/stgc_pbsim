#ifndef QUAD_H
#define QUAD_H

#include <vector>
#include <memory>
#include "StripLayer.h"

class Quad : public Identifiable, public std::vector<std::shared_ptr<StripLayer> >{
        
        public:
            Quad()=delete;
            Quad(int ,int ,int ,int );
            
            void AddLayer(int);
            void dump()const;
            int layerId()=delete;
        private:
            int m_currAddedLayer;
            int m_nmaxlayers;
        
};


#endif
