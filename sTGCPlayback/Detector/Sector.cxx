#include <Detector/Sector.h>

Sector::Sector(int sideId, int sectorId):m_nmaxquads(6){
    m_sideId=sideId;
    m_sectorId=sectorId;
    for(int iwedge=1;iwedge<3;iwedge++){  //1,2
    for(int imodule=1;imodule<4;imodule++){//1,2,3
        emplace_back(std::make_shared<Quad>(sideId,sectorId,imodule,iwedge));
    }
  }

}