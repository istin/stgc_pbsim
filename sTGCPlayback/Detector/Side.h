#ifndef ASIDE_H
#define ASIDE_H
#include <vector>
#include <memory>
#include <Detector/Sector.h>



class Side : public Identifiable,public std::vector<std::shared_ptr<Sector>>{
public:
    Side(int,std::vector<int> activeSectors={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16});
    Side(){};
    int layerId()=delete;
    int multilayerId()=delete;
    int sectorId()=delete;
    std::string quadNameAGDD()const=delete;

private:
    int m_nmaxsectors;
    
};



#endif

