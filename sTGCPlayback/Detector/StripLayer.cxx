#include <Detector/StripLayer.h>
#include <iostream>

StripLayer::StripLayer(int nstrips){
    for(int i=1;i<=nstrips;i++){
        AddStrip(i);//for a  given striplayer channels are all consecutive.
    }
}

void StripLayer::AddStrip(int ich){
    auto str=std::make_shared<STGC::Strip>(ich); 
    push_back(std::move(str));
}

void  StripLayer::dump() const{
    for (const auto& s : *this){
        std::cout<<"Strip Ch="<<s->channel()<<std::endl;
    }
}


std::string StripLayer::name(){
    std::string pfx="sTG1-Q";
    if(m_sectorId%2 == 0 ){
        pfx+="S";
    }
    else{
        pfx+="L";
    }
    pfx+=std::to_string(m_moduleId);
    
    switch(m_multilayerId){
        case 1:
            pfx+="P";
            break;
        case 2:
            pfx+="C";
            break;
        default:
            std::cout<<"Nonsense module Id"<<m_moduleId<<std::endl;
            break;
    }
    pfx+=std::to_string(m_layerId);
    return pfx;
}
