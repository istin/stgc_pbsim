#include "Detector/DetSvc.h"
#include "Detector/StripLayer.h"
#include "Detector/Quad.h"
#include "Detector/Side.h"
#include "Detector/Sector.h"
#include "Detector/Identifiable.h"
#include "Detector/Strip.h"
#include "GeometryModel/QuadrupletConfiguration.h"
#include <map>
#include <TString.h>



#include <vector>
#include <memory>
#ifdef __ROOTCLING__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

//#pragma link C++ class std::vector<std::unique_ptr<Strip>+;
//#pragma link C++ class std::vector<std::unique_ptr<StripLayer> >+;
//#pragma link C++ class std::vector<std::unique_ptr<Quad>>+;



#pragma link C++ class std::map<TString, STGC_GEOMETRY::QuadrupletConfiguration>+;
#pragma link C++ class DetSvc+;
#pragma link C++ class Identifiable+;
#pragma link C++ class STGC::Strip+;
#pragma link C++ class StripLayer+;
#pragma link C++ class Side+;
#pragma link C++ class Quad+;
#pragma link C++ class Sector+;


//#pragma link C++ class std::unique_ptr<Strip>+;



#endif
