#include "Quad.h"
#include <iostream>

Quad::Quad(int sideID,int sectorId,int moduleId,int multilayerId)
    :Identifiable(sideID,sectorId,moduleId,multilayerId){

     m_nmaxlayers=4;
     m_currAddedLayer=0;
}

void  Quad::AddLayer(int n){
    auto strlayer=std::make_shared<StripLayer>(n);
    if( m_currAddedLayer<m_nmaxlayers){
        strlayer->setSideId(this->sideId());
        strlayer->setSectorId(this->sectorId());
        strlayer->setModuleId(this->moduleId());
        strlayer->setMultilayerId(this->multilayerId());
        strlayer->setLayerID(m_currAddedLayer+1);
        for(const auto & str : *strlayer){
            str->setSideId(this->sideId());
            str->setSectorId(this->sectorId());
            str->setModuleId(this->moduleId());
            str->setMultilayerId(this->multilayerId());
            str->setLayerID(m_currAddedLayer+1);
        }
        strlayer->setLayerID(m_currAddedLayer+1);
        push_back(std::move(strlayer));
        m_currAddedLayer++;
    }
    else{
        std::cout<<"Quad "<<quadNameAGDD()<<" already has "<<m_nmaxlayers<<"layers not adding layer ... "<<std::endl;
    }
}

void Quad::dump() const{
    std::cout<<"******* Quad:"<<quadNameAGDD()<<"****"<<std::endl;
    for(const auto& sl : *this){
        
        //std::cout<<" Quad : "<<name()<<std::endl;
        std::cout<<"Strip Layer #"<<sl->layerId()<<std::endl;
        sl->dump();
    }
    std::cout<<"****************************"<<std::endl;
}
