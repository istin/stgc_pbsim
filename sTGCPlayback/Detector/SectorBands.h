#ifndef SECTOR_BANDS_H
#define SECTOR_BANDS_H

#include <map>
#include <Detector/DetSvc.h>
class SectorBands{

    public:
        SectorBands();
        void buildBandsFromAGDD();
        DetSvc* detSvc();

    private:
        //key = <sector/bandId>
        //value <1st channel/last channel> X8layers
        std::map<std::pair<int,int>,std::vector< std::pair<int,int>> > m_bandLUTSmall;
        std::map<std::pair<int,int>,std::vector< std::pair<int,int>> > m_bandLUTLarge;

        //Given a band Id and a sector make it possible to lookup the 1st and the last strip channel on the band / layer
        DetSvc* m_detsvc;
};



#endif