#ifndef STRIP_H
#define STRIP_H

#include <vector>

#include <Detector/Identifiable.h>
#include "Components/INotifiable.h"

namespace STGC{
    
    enum {READY,BUSY};
    
    class Strip : public Identifiable{
        public :
            Strip(int chno);
            Strip()=delete;
            int channel() const;
            float charge() const;
            float time() const;
            void setCharge(float);
            void setState(int);
            void setNotifiable(INotifiable* notifiable){
            	m_notifiable=notifiable;
            }
            
            void Reset();
            std::string sABType()const{ return quadNameAGDD()+std::to_string(m_layerId);}
            std::string sABName()const;
        private:
            float m_charge;
            float m_time;
            int m_channel;
            int m_state;
            INotifiable* m_notifiable;
    };
}

#endif
