#include <Detector/Side.h>

Side::Side(int sideId,std::vector<int> activesectors):m_nmaxsectors(16){
    m_sideId=sideId;
    for(const auto& secNo : activesectors){
        emplace_back(std::make_shared<Sector>(sideId,secNo));
     }
    
}
