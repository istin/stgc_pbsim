#include <Detector/Identifiable.h>

#include <iostream>

Identifiable::Identifiable():m_sideId(-1),m_sectorId(-1),m_moduleId(-1),m_multilayerId(-1),m_layerId(-1){
}


Identifiable::Identifiable(int sideID, int sectorId, int moduleId, int multilayerId){
    m_sideId=sideID;
    m_sectorId=sectorId;
    m_moduleId=moduleId;
    m_multilayerId=multilayerId;
}


std::string Identifiable::quadNameAGDD(int sec, int module, int multilayer){
    std::string pfx="sTG1-Q";
    if(sec%2 == 0 ){
        pfx+="S";
    }
    else{
        pfx+="L";
    }
    pfx+=std::to_string(module);
    
    switch(multilayer){
        case 1:
            pfx+="P";
            break;
        case 2:
            pfx+="C";
            break;
        default:
            std::cout<<"Nonsense wedge Id"<<multilayer<<std::endl;
            exit(9);
            break;
    }
    return pfx;
}

std::string Identifiable::quadNameAGDD()const{
    return quadNameAGDD(m_sectorId, m_moduleId, m_multilayerId);
}

void Identifiable::dump(){
    std::cout<<"Side="<<sideId()<<" sector="<<sectorId()<<" module="<<moduleId()<<" multilayer="<<multilayerId()<<" layer="<<layerId()<<std::endl;
}
