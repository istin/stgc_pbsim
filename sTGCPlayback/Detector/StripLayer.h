#ifndef STRIPLAYER_H
#define STRIPLAYER_H
#include <vector>
#include <memory>
#include <Detector/Strip.h>

class StripLayer : public Identifiable, public std::vector<std::shared_ptr<STGC::Strip> >{
public:  
    StripLayer(int nstrips);
    StripLayer()=delete;
    
    
    void AddStrip(int);
    void dump() const;
    std::string name();
private :
    int m_layerNo;
};

#endif
