/*
 * SimulationAlgorithmFactory.h
 *
 *  Created on: Feb 5, 2019
 *      Author: ekajomov
 */

#ifndef SIMULATIONALGORITHMFACTORY_H_
#define SIMULATIONALGORITHMFACTORY_H_

class SimulationAlgorithmFactory {
public:
	SimulationAlgorithmFactory();
	virtual ~SimulationAlgorithmFactory();
};

#endif /* SIMULATIONALGORITHMFACTORY_H_ */
