include_directories(${CMAKE_CURRENT_SOURCE_DIR}/..)
FILE(GLOB SimuSources *.cxx)
FILE(GLOB SimuHeaders *.h)
list(FILTER SimuHeaders EXCLUDE REGEX "Linkdef.h$")

set(LIBNAME Simulation)

ROOT_GENERATE_DICTIONARY(G__${LIBNAME} ${SimuHeaders} LINKDEF Linkdef.h MODULE)

add_library( ${LIBNAME} SHARED ${SimuSources} G__${LIBNAME}.cxx)

target_link_libraries(${LIBNAME}  ${ROOT_LIBRARIES})
install(TARGETS ${LIBNAME}
    LIBRARY DESTINATION ${PLAYBACK_LIBS}
)

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/lib${LIBNAME}_rdict.pcm DESTINATION ${PLAYBACK_LIBS}) 
