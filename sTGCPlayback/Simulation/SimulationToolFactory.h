/*
 * SimulationToolFactory.h
 *
 *  Created on: Feb 5, 2019
 *      Author: ekajomov
 */

#ifndef SIMULATIONTOOLFACTORY_H_
#define SIMULATIONTOOLFACTORY_H_

class SimulationToolFactory {
public:
	SimulationToolFactory();
	virtual ~SimulationToolFactory();
};

#endif /* SIMULATIONTOOLFACTORY_H_ */
