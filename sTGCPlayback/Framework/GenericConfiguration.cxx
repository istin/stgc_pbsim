/*
 * GenericConfiguration.cpp
 *
 *  Created on: Feb 6, 2019
 *      Author: ekajomov
 */

#include <Framework/GenericConfiguration.h>
#include "TObjArray.h"
#include "TList.h"
#include "TObjString.h"

GenericConfiguration::GenericConfiguration() {

}

GenericConfiguration::~GenericConfiguration() {
}

std::vector<int> GenericConfiguration::ValueAsIntList(const TString& varname) {


	TString delims = ";";
	TString value = varname_to_value[varname];
	TObjArray* value_array = value.Tokenize(delims);
	TIter valsIter(value_array);

	std::vector<int> result;
	TString token;
	while ((token = ((TObjString*) valsIter())->String())) {
		int trimIdx = 0;
		while (trimIdx < value.Length()) {
			char c = token(trimIdx);
			if (c == ' ' || c == '\t' || c == '\r' || c == '\n')
				trimIdx++;
			else break;
		}
		result.push_back(TString(value(trimIdx, token.Length() - trimIdx)).Atoi());
	}
	delete value_array;
	return result;


}

std::vector<double> GenericConfiguration::ValueAsDoubleList(
		const TString& varname) {

	TString delims = ";";
	TString value = varname_to_value[varname];
	TObjArray* value_array = value.Tokenize(delims);
	TIter valsIter(value_array);

	std::vector<double> result;

	TString token;
	while ((token = ((TObjString*) valsIter())->String())) {
		int trimIdx = 0;
		while (trimIdx < value.Length()) {
			char c = token(trimIdx);
			if (c == ' ' || c == '\t' || c == '\r' || c == '\n')
				trimIdx++;
			else break;
		}
		result.push_back(TString(value(trimIdx, token.Length() - trimIdx)).Atof());
	}
	delete value_array;
	return result;
}

std::vector<TString> GenericConfiguration::ValueAsStringList(
		const TString& varname) {


	TString delims = ";";
	TString value = varname_to_value[varname];
	TObjArray* value_array = value.Tokenize(delims);
	TIter valsIter(value_array);

	std::vector<TString> result;
	TString token;
	while ((token = ((TObjString*) valsIter())->String())) {
		result.push_back(token);
	}
	delete value_array;
	return result;
}

int GenericConfiguration::ValueAsInt(const TString& varname) {
	return varname_to_value[varname].Atoi();
}

double GenericConfiguration::ValueAsDouble(const TString& varname) {
	return varname_to_value[varname].Atof();
}

TString GenericConfiguration::ValueAsString(const TString& varname) {
	return varname_to_value[varname];
}

void GenericConfiguration::Add(TString varname, TString value) {
	varname_to_value[varname] = value;
}
