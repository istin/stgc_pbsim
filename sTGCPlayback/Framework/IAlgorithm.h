/*
 * IAlgorithm.h
 *
 *  Created on: Feb 5, 2019
 *      Author: Enrique.Kajomovitz@cern.ch
 */

#ifndef IALGORITHM_H_
#define IALGORITHM_H_

//#include "TString.h"
#include <string>
#include <Trigger/TrigTree.h>
#include <Detector/DetSvc.h>
#include <Components/ComponentStore.h>

class IAlgorithm
{
public:
	IAlgorithm()=default;
	virtual ~IAlgorithm()=default;

    void bindData(TChain* ch) { m_chain=ch;}
    virtual bool initialize() = 0;
	virtual bool execute() = 0;
	virtual bool finalize() = 0;    
    static std::string name() { return m_name;}

    
protected:
    static std::string m_name;
    TChain * m_chain;
    DetSvc* detSvc()const{ return DetSvc::instance();}
    ComponentStore * compStore()const{ return ComponentStore::Instance();}    
    
};



#endif /* IALGORITHM_H_ */
