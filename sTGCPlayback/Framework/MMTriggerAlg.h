#ifndef MMTRIGGERALG_H
#define MMTRIGGERALG_H
#include <Framework/IAlgorithm.h>
#include <Trigger/MMARTData.h>
#include <Trigger/TrigTreePRD.h>
#include <Components/ComponentStore.h>
#include <MMTriggerSystemComponents/MMARTASIC.h>
#include <TChain.h>
#include <string>

class MMTriggerAlg : public IAlgorithm {

public:
    MMTriggerAlg();
    ~MMTriggerAlg();
    bool initialize();
    bool execute();
    bool finalize();
    MMARTData*   MMData() { return m_mmdata; }
    TrigTreePRD* Tree()   { return m_tree; }
    // std::map< std::pair<int,int>, MMARTASIC* >* MMARTASICs() { return m_mmartasics; }
    std::vector< MMARTASIC* >* MMARTASICs() { return m_mmartasics; }

private:
    void Reset();
    TrigTreePRD* m_tree;
    MMARTData* m_mmdata;
    // std::map< std::pair<int,int>, MMARTASIC* >* m_mmartasics;
    std::vector< MMARTASIC* >* m_mmartasics;

};


#endif
