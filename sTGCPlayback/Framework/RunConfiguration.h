/*
 * RunConfiguration.h
 *
 *  Created on: Feb 5, 2019
 *      Author: Enrique.Kajomovitz@cern.ch
 */

#ifndef RUNCONFIGURATION_H_
#define RUNCONFIGURATION_H_

#include <vector>
#include "TString.h"

struct KeyTargetPair
{
	TString key;
	TString target;
};

struct RunConfiguration
{

	long maxEvents;
	long skipEvents;
	std::vector<KeyTargetPair> toolList;
	std::vector<KeyTargetPair> algList;
};



#endif /* RUNCONFIGURATION_H_ */
