/*
 * RunConfigurationParser.cpp
 *
 *  Created on: Feb 5, 2019
 *      Author: ekajomov
 */

#include <Framework/RunConfigurationParser.h>
#include "TDOMParser.h"
#include <Utilities/LoggingSvc.h>

RunConfigurationParser::RunConfigurationParser():
configFile(""),
docRoot(NULL),
ownDocRoot(false)
{
}

RunConfigurationParser::~RunConfigurationParser() {
	if (ownDocRoot)
		delete docRoot;
}

void RunConfigurationParser::SetXMLRoot(TXMLNode* in_docRoot) {
	if (in_docRoot != NULL)
		docRoot = in_docRoot;
	TDOMParser domParser;
	if (configFile == ""){
		LOG_FATAL("here");
		throw std::runtime_error("Trying to setup an empty XML file for configuration");
	}
	domParser.SetValidate(false); // do not validate with DTD
	int parsecode = domParser.ParseFile(configFile);

	if (parsecode < 0) {
		LOG_FATAL(" here");
		throw std::runtime_error(domParser.GetParseCodeMessage(parsecode) );
	}

	docRoot = domParser.GetXMLDocument()->GetRootNode();
	ownDocRoot = true;

}

void RunConfigurationParser::SetConfigurationFile(TString in_configFile) {
	configFile = in_configFile;
}

void RunConfigurationParser::Parse() {
	ProcessDefaultNode(docRoot);
}

std::map<TString, ToolConfig> RunConfigurationParser::GetToolLookup() {
	return toolLookup;
}

std::map<TString, AlgorithmConfig> RunConfigurationParser::GetAlgortihmLookup() {
	return algLookup;
}

std::vector<TString> RunConfigurationParser::GetAlgorithmList() {
	return algList;
}

std::vector<TString> RunConfigurationParser::GetToolList() {
	return toolList;
}

void RunConfigurationParser::ProcessDefaultNode(TXMLNode* docRoot)
{
	TXMLNode* child = docRoot->GetChildren();
	while (child != NULL) {
		parser.Clear();
		TString nodeName = child->GetName();

		if (nodeName.EqualTo("algorithm"))
			ProcessAlgorithmNode(child);
		else if (nodeName.EqualTo("tool"))
			ProcessToolNode(child);
		else if (nodeName.EqualTo("flow"))
			ProcessFlowNode(child);
		else ProcessDefaultNode(child);
		child = child->GetNextNode();
	}

}


void RunConfigurationParser::ProcessAlgorithmNode(TXMLNode* node){

}

void RunConfigurationParser::ProcessToolNode(TXMLNode* node){

}

void RunConfigurationParser::ProcessFlowNode(TXMLNode* node){

}

