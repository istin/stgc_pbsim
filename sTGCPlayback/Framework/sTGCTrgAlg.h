#ifndef STGC_TRGALG
#define STGC_TRGALG
#include <Framework/IAlgorithm.h>
#include <sTGCTriggerSystemComponents/ComponentHandle.h>
#include <Trigger/sTGCTrigger.h>
#include <Trigger/TrigTree.h>
#include <TChain.h>
#include <string>


class sTGCTrgAlg : public IAlgorithm{
    
public:
    sTGCTrgAlg();
    ~sTGCTrgAlg();
    virtual bool initialize();
    virtual bool execute();
    virtual bool finalize();
    const std::vector<sTGCTrigger> & trgBuffer()const{ return m_trgbuffer;} 
    const std::vector<sTGCTrigger> & strBuffer()const{ return m_trgbuffer;} 
    
private:
    std::vector<sTGCTrigger> m_trgbuffer;
    std::vector<std::shared_ptr<STGC::Strip> > m_stripbuffer;
    bool FillTrigBuffer();
    bool FillTriggerStripInfo();
    bool FillStripBuffer();
    bool NotifyStrips();
    bool DoTriggerProcessor();
    bool SendPadTriggerMessages();
    //These components can all be resolved / event
    ComponentHandle<sTGCsAB> m_connected_sAB;
    ComponentHandle<sTGCsFE> m_connected_sFE;
    ComponentHandle<sTGCPadTrigger> m_connected_padtrigger;
    ComponentHandle<sTGCRouter> m_connected_router;
    ComponentHandle<sTGCTriggerProcessor> m_connected_triggerProcessor;
    std::vector<int> m_sectorsInSideA;
    std::vector<int> m_sectorsInSideC;    
    bool isTriggerWithinAcc(const sTGCTrigger&);
    void Reset();
    TrigTree* m_trgtree;
    unsigned int m_nevtsmax;
    unsigned int m_currevt; 
};


#endif
