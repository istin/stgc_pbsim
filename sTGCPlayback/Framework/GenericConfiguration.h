/*
 * GenericConfiguration.h
 *
 *  Created on: Feb 6, 2019
 *      Author: Enrique.Kajomovitz@cern.ch
 */

#ifndef GENERICCONFIGURATION_H_
#define GENERICCONFIGURATION_H_

#include <vector>
#include <map>
#include "TString.h"

class GenericConfiguration {
public:
	GenericConfiguration();
	virtual ~GenericConfiguration();


	std::vector<int> ValueAsIntList(const TString& varname);
	std::vector<double> ValueAsDoubleList(const TString& varname);
	std::vector<TString> ValueAsStringList(const TString& varname);
	int ValueAsInt(const TString& varname);
	double ValueAsDouble(const TString& varname);
	TString ValueAsString(const TString& varname);
	void Add(TString varname, TString value);

protected:

	std::map<TString, TString> varname_to_value;


};

typedef GenericConfiguration ToolConfig;
typedef GenericConfiguration AlgorithmConfig;


#endif /* GENERICCONFIGURATION_H_ */
