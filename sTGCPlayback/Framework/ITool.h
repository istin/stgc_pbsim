/*
 * ITool.h
 *
 *  Created on: Feb 5, 2019
 *      Author: Enrique.Kajomovitz@cern.ch
 */

#ifndef ITOOL_H_
#define ITOOL_H_

#include "TString.h"

class ITool
{
public:
	ITool(){}
	virtual ~ITool(){}
	virtual void Initialize() = 0;
	virtual void Finalize() = 0;
	virtual TString Name() = 0;

};


#endif /* ITOOL_H_ */
