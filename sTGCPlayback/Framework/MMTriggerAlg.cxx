#include <Components/ComponentStore.h>
#include <Framework/MMTriggerAlg.h>
#include <Detector/DetSvc.h>

MMTriggerAlg::MMTriggerAlg(){
  m_name = "MMTriggerAlg";
}

MMTriggerAlg::~MMTriggerAlg(){
}

bool MMTriggerAlg::initialize(){
  m_tree       = new TrigTreePRD(m_chain);
  m_mmdata     = new MMARTData();
  m_mmartasics = new std::vector< MMARTASIC* >();
  // m_mmartasics = new std::map< std::pair<int,int>, MMARTASIC* >();
  return true;
}

bool MMTriggerAlg::execute(){

  MMData()->fetchARTData(Tree());
  if (MMData()->GetEventNumber() > 2)
    return 0;
  for (auto asic: *MMARTASICs())
    asic->ClearHits();
  MMARTASICs()->clear();

  auto cs = ComponentStore::Instance();

  // create list of MM ART ASICs
  // fiber == ASIC + 4*layer, for now
  for (int i = 0; i < MMData()->LayersPerSector(); i++) {
    for (int j = 0; j < MMData()->ARTASICsPerLayer(); j++) {
      MMARTASICs() -> push_back( new MMARTASIC() );
      MMARTASICs() -> back() -> ClearHits();
      MMARTASICs() -> back() -> SetLayer(i);
      MMARTASICs() -> back() -> SetASIC (j);
      MMARTASICs() -> back() -> SetFiber(MMARTASICs()->size() - 1);
      for (auto hit: MMData()->ARTHits())
        if (hit->layer==i && hit->artasic==j)
          MMARTASICs() -> back() -> AddHit(hit);
    }
  }


  // < STD::MAP EDITION >
  // (*MMARTASICs())[ int_pair(i, j) ] = new MMARTASIC();
  // set fiber = ASIC + 4*layer
  // for (auto kv: *MMARTASICs()) {
  //   int layer   = kv.first.first;
  //   int asic    = kv.first.second;
  //   int a_per_l = MMData()->ARTASICsPerLayer();
  //   kv.second -> SetLayer(layer);
  //   kv.second -> SetASIC (asic);
  //   kv.second -> SetFiber(layer*a_per_l + asic);
  //   kv.second -> ClearHits();
  // }
  // assign ART hits to ART ASICs
  // for (auto hit: MMData()->ARTHits())
  //  MMARTASICs() -> at(int_pair(hit.layer, hit.artasic)) -> AddHit(hit);
  //  for (auto kv: *MMARTASICs())
  //    std::cout << Form("ASIC :: %i %i %02i %02i",
  //                      kv.first.first, 
  //                      kv.first.second, 
  //                      kv.second->GetFiber(),
  //                      (int)(kv.second->GetHits().size())
  //                      ) << std::endl;
  // for (auto kv: *MMARTASICs())
  //  std::cout << kv.second -> GBTMessage() << std::endl;
  // < /STD::MAP EDITION >

  // create MMFE8s and ARTs here, instead of using the factory
  // ENRIQUE WILL SURELY CODE THIS SOON
  // for layer in layers:
  //     for mmfe8 in mmfe8s:
  //         cs->Add(mmfe8)
  //     for art in arts:
  //         cs->Add(art)

  //for (auto hit: MMData()->ARTHits()){
  //  auto mmfe8 = cs->Retrieve("MMFE8", "MMFE8", hit.NameForStore);
  //}

  // checks
  std::cout << "Event " << MMData()->GetEventNumber() << std::endl;
  for (auto hit: MMData()->ARTHits())
    std::cout << Form("Hit  :: %i %03i %i", hit->layer, hit->channel_global, hit->bcid) << std::endl;
  for (auto asic: *MMARTASICs())
    std::cout << Form("ASIC :: %i %i %02i %02i",
                      asic->GetLayer(),
                      asic->GetASIC(),
                      asic->GetFiber(),
                      (int)(asic->GetHits()->size())
                      ) << std::endl;
  std::cout << "FirstBCID - LastBCID: "
            << MMData()->FirstBCID() << " - " << MMData()->LastBCID() << std::endl;


  // write GBT msg
  for (int bcid = MMData()->FirstBCID(); bcid <= MMData()->LastBCID(); bcid++)
    for (auto asic: *MMARTASICs())
      if (asic->size() > 0 && asic->HasHit(bcid))
        // std::cout << Form("Fiber: %02i, BCID: %04i", asic->GetFiber(), bcid) << std::endl;
        //std::cout << asic->GBTMessage(bcid) << std::endl;
        auto tmp = asic->GBTMessage(bcid, MMData()->GetEventNumber());

  return 1;
}

void MMTriggerAlg::Reset(){
}

bool MMTriggerAlg::finalize(){
  return true;
}
