/*
 * ToolManager.h
 *
 *  Created on: Feb 5, 2019
 *      Author: ekajomov
 */

#ifndef TOOLMANAGER_H_
#define TOOLMANAGER_H_

#include "Framework/ITool.h"
#include "TString.h"
#include <vector>
#include <map>


class ToolManager {
private:

	ToolManager();
	static ToolManager* s_instance;
	std::vector<ITool*> tools;
	std::map<TString, ITool*> name_to_tool;


public:
	static ToolManager* Instance();
	void Add(TString name, ITool* tool);
	ITool* GetTool(TString toolName);
	void InitializeTools();
	void ProcessTools();
	void FinalizeTools();
	void DeleteTools();

};

#endif /* TOOLMANAGER_H_ */
