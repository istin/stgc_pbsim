#include <Framework/sTGCTrgAlg.h>
#include <sTGCTriggerSystemComponents/sTGCsAB.h>
#include <sTGCTriggerSystemComponents/ComponentHandle.h>
#include <Utilities/LoggingSvc.h>
#include <unordered_map>

sTGCTrgAlg::sTGCTrgAlg(){
    m_name="sTGCTrgAlg";
}

sTGCTrgAlg::~sTGCTrgAlg(){
    
}

bool sTGCTrgAlg::initialize(){
    m_trgtree=new TrigTree(m_chain);//m_chain must be set by an outsider a.k.a PlayBackLoop?
    m_nevtsmax=m_chain->GetEntries ();
    m_currevt=0;
    m_sectorsInSideA=detSvc()->getConfig().ActiveSectorsSideA;
    m_sectorsInSideC=detSvc()->getConfig().ActiveSectorsSideC;

    return true;
}

bool sTGCTrgAlg::isTriggerWithinAcc(const sTGCTrigger& atrg){
     bool sideAisActive=m_sectorsInSideA.size()>0;
     bool sideCisActive=m_sectorsInSideC.size()>0; 
     int this_side=atrg.side();
     int this_sector=atrg.sector();
        if(this_side==1){
                if(!sideAisActive ){
                    LOG_WARNING("SideA is not configured. Skipping trigger candidate");
                    return false;
                }
                auto posA=std::find(m_sectorsInSideA.begin(),m_sectorsInSideA.end(),this_sector);
                if(posA==m_sectorsInSideA.end()){
                    LOG_WARNING("Sector "<<this_sector<<" at side "<<this_side<<" is not configured. Skipping  trigger candidate");
                    return false;
                }
        }
        else if(this_side==0){//C
                if(!sideCisActive){
                    LOG_WARNING("SideC is not configured. Skipping trigger candidate");
                    return false;
                }
                auto posC=std::find(m_sectorsInSideC.begin(),m_sectorsInSideC.end(),this_sector);
                if(posC==m_sectorsInSideC.end()){
                    LOG_WARNING("Sector "<<this_sector<<" at side "<<this_side<<" is not configured. Skipping  trigger candidate");
                    return false;
                }                
        }
        else{
                LOG_FATAL("Unknown detector side "<<this_side);
                exit(8);
        }
        return true;        
}


bool sTGCTrgAlg::FillTrigBuffer(){

    Reset();
    
    //Fill the trigger buffer
    for (unsigned int itrig=0;itrig<m_trgtree->nPadTriggers;itrig++){//pad trigger loop
        sTGCTrigger trig;
        trig.fetchPadTrigger(m_trgtree,itrig);
        bool accept=isTriggerWithinAcc(trig);
        if(!accept) continue;
            
        m_trgbuffer.push_back(trig);
    }
    return true;
}


bool sTGCTrgAlg::SendPadTriggerMessages(){

    std::unordered_map<size_t,std::vector<sTGCTrigger> > triggersBySector;
    for( const sTGCTrigger& trg : m_trgbuffer){
        triggersBySector[trg.key()].push_back(trg);
    }

    LOG_DEBUG(" Triggers in "<<triggersBySector.size()<<" sectors!");

    for(const auto& kv : triggersBySector ){
        std::vector<sTGCTrigger> triggers=kv.second;//trgiggers is now the set of triggers organized /side/sector
        sTGCPadTriggerMsg msg;
        unsigned int side=triggers.at(0).side();
        unsigned int sector=triggers.at(0).sector();
        //Fill four pad trigger message slots (at max) for this sector
        for(unsigned int itrg=0;itrg<triggers.size();itrg++){
            sTGCTrigger thisTrigger=triggers.at(itrg);
            switch(itrg){
                case(0):
                    msg.bandId0=thisTrigger.bandID();
                    msg.phiId0=thisTrigger.phiId();
                    break;
                case(1):
                    msg.bandId1=thisTrigger.bandID();
                    msg.phiId1=thisTrigger.phiId();
                    break;

                case(2):
                    msg.bandId2=thisTrigger.bandID();
                    msg.phiId2=thisTrigger.phiId();
                    break;
                case(3):
                    msg.bandId3=thisTrigger.bandID();
                    msg.phiId3=thisTrigger.phiId();
                    break;
                default:
                    break;                
            } 
            if(itrg==3) break;//max four
        }
        LOG_TRACE(" Side="<<side<<" Sector="<<sector);
        m_connected_padtrigger=ComponentHandle<sTGCPadTrigger>(side,sector);
        m_connected_padtrigger.resolve()->SetPadTriggerDecision(msg);

    }


   return true;

}

bool sTGCTrgAlg::NotifyStrips(){
    //nypass this for now
    return false;

    for(const auto& trig : m_trgbuffer){
        int side=trig.side();
        int sector=trig.sector();

        for(const auto& swt : trig.wedgeTriggers()){
            auto all_lyr_str_channels=swt->selectedStripChannels();
            int module=swt->moduleId();
            int multilayer=swt->wedgeId();
            int idx=0;
            for(const auto& layer : swt->selectedLayers()){
                auto thislyr_channels=all_lyr_str_channels.at(idx);
                for(const auto& strch : thislyr_channels ){
                    int channel=strch.first;
                    int charge=strch.second;
                    auto strip=detSvc()->getStrip(side,sector,module,multilayer,layer,channel);
                    m_connected_sAB=ComponentHandle<sTGCsAB>(side,sector,module,layer);
                    strip->setNotifiable(m_connected_sAB.resolve()->GetNotifiableForStrip(channel));
                    strip->setCharge(charge);
                    strip->setState(STGC::BUSY);
                    m_stripbuffer.push_back(strip);
                }
                idx++;
            }
        }//wts
    }//trg buffer loop
    
    return true;
}


bool  sTGCTrgAlg::DoTriggerProcessor(){
    if(m_trgbuffer.size()==0 ) return false;
    int side=m_trgbuffer.at(0).side();
    int sector=m_trgbuffer.at(0).sector();    
    m_connected_triggerProcessor=ComponentHandle<sTGCTriggerProcessor>(side,sector);
    
    if(m_currevt==m_nevtsmax-1){
       m_connected_triggerProcessor.resolve()->SetLastEvent(true); 
    }
    m_connected_triggerProcessor.resolve()->setTriggersFromAth(m_trgbuffer); 
    m_connected_triggerProcessor.resolve()->DoProcess();
    return true;
    
}



bool sTGCTrgAlg::execute(){
    IScheduler* scheduler=ComponentStore::Instance()->GetScheduler();
    scheduler->Initialize();
    FillTrigBuffer(); // Fills in ATHENA triggers within the configured acceptance
    auto removeDupes=[](const sTGCTrigger& tr1,const sTGCTrigger& tr2){
         return tr1.bandID()==tr2.bandID() && tr1.phiId()==tr2.phiId() && tr1.sector()==tr2.sector();
    };

    m_trgbuffer.erase(std::unique(m_trgbuffer.begin(), m_trgbuffer.end(), removeDupes), m_trgbuffer.end());
    SendPadTriggerMessages();
    NotifyStrips();
    while(ComponentStore::Instance()->GetScheduler()->Next());
    DoTriggerProcessor();
    
    m_currevt++;
   return true;
   
}

void sTGCTrgAlg::Reset(){
    //DetSvc
    for( const auto& str : m_stripbuffer){
        str->Reset();
    }
    //AthTriggers
    m_stripbuffer.clear();
    m_trgbuffer.clear();
}

bool sTGCTrgAlg::finalize(){
    return true;
}
