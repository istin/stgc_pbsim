/*
 * RunConfigurationParser.h
 *
 *  Created on: Feb 5, 2019
 *      Author: Enrique.Kajomovitz@cern.ch
 */

#ifndef RUNCONFIGURATIONPARSER_H_
#define RUNCONFIGURATIONPARSER_H_


#include <map>
#include <vector>
#include "TXMLNode.h"
#include "XMLUtils/XMLNodeParser.h"
#include "GenericConfiguration.h"

class RunConfigurationParser {
public:

	RunConfigurationParser();
	virtual ~RunConfigurationParser();

	void SetXMLRoot(TXMLNode* in_docRoot = NULL);
	void SetConfigurationFile(TString in_configFile);

	void Parse();


	std::map<TString, ToolConfig> GetToolLookup();
	std::map<TString, AlgorithmConfig> GetAlgortihmLookup();

	std::vector<TString> GetAlgorithmList();
	std::vector<TString> GetToolList();

	void ProcessDefaultNode(TXMLNode* docRoot);


private:

	void ProcessAlgorithmNode(TXMLNode* node);
	void ProcessToolNode(TXMLNode* node);
	void ProcessFlowNode(TXMLNode* node);

	std::map<TString, ToolConfig> toolLookup;
	std::map<TString, AlgorithmConfig> algLookup;
	std::vector<TString> toolList;
	std::vector<TString> algList;

	TString configFile;
	TXMLNode* docRoot;
	bool ownDocRoot;
	XML_UTILS::XMLNodeParser parser;


};

#endif /* RUNCONFIGURATIONPARSER_H_ */
