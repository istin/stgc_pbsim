#include "Framework/AlgorithmManager.h"
#include "Framework/IAlgorithm.h"
#include "Framework/ITool.h"
#include "Framework/RunConfiguration.h"
#include "Framework/RunConfigurationParser.h"
#include "Framework/ToolManager.h"
#include "Framework/PlaybackLoop.h"


#include <vector>

#ifdef __ROOTCLING__


#pragma link C++ class RunConfiguration+;
#pragma link C++ class AlgorithmManager+;
#pragma link C++ class IAlgorithm+;
#pragma link C++ class PlaybackLoop+;
#pragma link C++ class RunConfigurationParser+;
#pragma link C++ class ToolManager+;


#endif
