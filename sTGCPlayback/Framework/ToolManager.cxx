/*
 * ToolManager.cpp
 *
 *  Created on: Feb 5, 2019
 *      Author: Enrique.Kajomovitz@cern.ch
 */

#include <Framework/ToolManager.h>


ToolManager* ToolManager::s_instance = 0;

ToolManager::ToolManager() {
}

ToolManager* ToolManager::Instance() {
	if (!s_instance)
		s_instance = new ToolManager();
	return s_instance;
}

void ToolManager::Add(TString name, ITool* tool) {

	if (tool == NULL)
		throw "Trying to add a null algorithm";
	tools.push_back(tool);
	name_to_tool[name] = tool;
}

void ToolManager::InitializeTools() {
	for (unsigned int i_tool = 0; i_tool < tools.size(); i_tool++)
		tools[i_tool]->Initialize();
}

void ToolManager::FinalizeTools() {
	for (unsigned int i_tool = 0; i_tool < tools.size(); i_tool++)
		tools[i_tool]->Finalize();
}

void ToolManager::DeleteTools() {
	for (unsigned int i_tool = 0; i_tool < tools.size(); i_tool++)
		delete (tools[i_tool]);
}

ITool* ToolManager::GetTool(TString name){
	return name_to_tool[name];
}
