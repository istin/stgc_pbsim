/*
 * AlgorithmManager.h
 *
 *  Created on: Feb 5, 2019
 *      Author: Enrique.Kajomovitz@cern.ch
 */

#ifndef ALGORITHMMANAGER_H_
#define ALGORITHMMANAGER_H_

#include <Framework/IAlgorithm.h>
//#include <vector>
#include <list>
#include <algorithm>
class AlgorithmManager {
private:
	AlgorithmManager();
	static AlgorithmManager* s_instance;
     std::list<std::pair<std::string, IAlgorithm*> > m_algorithms;
    IAlgorithm* _getAlg_(const std::string&);
    
    
    std::vector<std::string > m_algKeys;

public:

	static AlgorithmManager* Instance();
    
    const std::list<std::pair<std::string, IAlgorithm*> >& algorithms() const{ return m_algorithms;}

    template<typename Alg>  Alg* Handle(){
        auto pos = std::find_if(m_algorithms.begin(), m_algorithms.end(),
                        [](std::pair<std::string, IAlgorithm*> const &b) { 
                            return b.first == Alg::name(); 
                        });
        
        return static_cast<Alg*>( pos->second );
    }    
	bool Add(IAlgorithm* alg);
	bool InitializeAlgorithms();
	bool ExecuteAlgorithms();
	bool FinalizeAlgorithms();
	bool DeleteAlgorithms();

};

#endif
