#ifndef PLAYBACKLOOP_H
#define PLAYBACKLOOP_H

#include <Trigger/TrigTree.h>
#include <TChain.h>
#include <Detector/DetSvc.h>
#include <Framework/AlgorithmManager.h>
#include <memory>
#include <TTree.h>


class PlaybackLoop {
public:
    PlaybackLoop();
    ~PlaybackLoop();
    
    
     bool SetDirectory(const std::string&);
     bool SetTree(const std::string&);
     bool SetNEvents( int );
     bool SetNSkipEvents( int );
     
     
     bool Configure();
     bool Run();
     bool Finalize();

     DetSvc* detSvc() const{ return DetSvc::instance();}
     AlgorithmManager* algMgr() const{ return AlgorithmManager::Instance();}
     
    
private:
    /*s.I Someone else might need these methods. We might make them static or move them inside a kind of utility
     An algorithm is a very abstract entity, so lets keep them specific to this class only for now. 
     */
    std::vector<std::string> _read_directory(const std::string& name,const std::string& pattern="(.*)(NSWL1Simulation.root)(.*)");
    std::vector<std::string> _read_txt(const std::string& filelist,const std::string& p="(.*)(NSWL1Simulation.root)(.*)");    
    TChain* _buildChain(const std::vector<std::string>& files,const std::string& treename);    
    
    //No  need to have a member  like  TrigTree* or DogTree* or WhatsoEverTree*. Just have a pointer to a raw TChain*
    //And pass it  to the Algorithms
    
    //This TChain is going to be constructed through a key(a string) during instantiation of PlaybackLoop
    // ***** You cannot run PlaybackLoop on multiple datasources like DogTree* or WhatsoEverTree* ath the same time ****     
    TChain* m_chain;
    
    std::string m_datadir;    
    std::string m_treename;
    unsigned int m_nskipevents;
    unsigned int m_nmaxevts;
};



#endif
