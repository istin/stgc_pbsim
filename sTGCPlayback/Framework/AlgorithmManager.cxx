/*
 * AlgorithmManager.cpp
 *
 *  Created on: Feb 5, 2019
 *      Author: Enrique.Kajomovitz@cern.ch
 */

#include <Framework/AlgorithmManager.h>
#include <algorithm>
#include <stdexcept>
#include <iostream>
#include <Utilities/LoggingSvc.h>

AlgorithmManager* AlgorithmManager::s_instance = 0;

AlgorithmManager::AlgorithmManager():m_algorithms{} {
}

AlgorithmManager* AlgorithmManager::Instance() {
	if (!s_instance)
		s_instance = new AlgorithmManager();
	return s_instance;
}

bool AlgorithmManager::Add(IAlgorithm* alg) {
    

    if(alg==nullptr){
        return false;
    }
    
    LOG_TRACE("AlgorithmManager ===> Adding algorithm: "<<alg->name());
    
    auto checkpos=std::find_if(m_algorithms.begin(), m_algorithms.end(),
                        [&alg](std::pair<std::string, IAlgorithm*> const &b) { 
                            return b.first == alg->name(); 
                        });
    if(checkpos!=m_algorithms.end()) {
        LOG_WARNING("AlgorithmManager ===> Algorithm name :"<<alg->name()<<" already added. Skipping");
        return false;
    }
    
    
    try{

        m_algorithms.emplace_back( std::make_pair(alg->name(),alg) );
        m_algKeys.push_back(alg->name());

    }
    catch (std::exception e) {
        LOG_ERROR("Add "<< e.what());
        return false;
    }
    
    return true;
}
 IAlgorithm* AlgorithmManager:: _getAlg_(const std::string& name){
        auto pos = std::find_if(m_algorithms.begin(), m_algorithms.end(),
                        [&name](std::pair<std::string, IAlgorithm*> const &b) { 
                            return b.first == name; 
                        });
        return pos->second;
}



bool AlgorithmManager::InitializeAlgorithms() {

    try{
        std::for_each(m_algorithms.rbegin(),m_algorithms.rend(),[](std::pair<std::string,IAlgorithm*> al){al.second->initialize();});
    }
    catch (std::exception e) {
        LOG_ERROR("Initialize "<< e.what());
        return false;
    }
    return true;
}

bool AlgorithmManager::ExecuteAlgorithms() {

    //execute algorithms in the order as they were added
       for(const auto& key : m_algKeys){
            for(const auto& kv : m_algorithms){
                if(key==kv.first){
                    kv.second->execute();
                }
            }
       }
    return true;
}

bool AlgorithmManager::FinalizeAlgorithms() {

    LOG_TRACE("AlgorithmManager ===> Finalizing "<<m_algorithms.size()<<" algorithms");
    try{
        std::for_each(m_algorithms.begin(),m_algorithms.end(),[](std::pair<std::string,IAlgorithm*> al){al.second->finalize();});
    }
    catch (std::exception e) {
        LOG_ERROR("Finalize "<<e.what());
        return false;
    }
    
   return true;
}

bool AlgorithmManager::DeleteAlgorithms() {

    try{
        std::for_each(m_algorithms.begin(),m_algorithms.end(),[](std::pair<std::string,IAlgorithm*> al){delete(al.second);});
    }
    catch (std::exception e) {
        LOG_ERROR("Delete "<<e.what());
        return false;
    }
    m_algorithms.clear();
    
    return true;
    
}
