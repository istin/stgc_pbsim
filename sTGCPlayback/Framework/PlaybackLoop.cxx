#include <Framework/PlaybackLoop.h>
#include <regex>
#include <fstream>
#include <dirent.h>
#include <iostream>
#include <Framework/sTGCTrgAlg.h>
#include <Utilities/LoggingSvc.h>


PlaybackLoop::PlaybackLoop(): m_datadir(std::string(std::getenv("PBDATAPATH"))), m_treename("NSWL1SimulationTree"),m_nskipevents(0),m_nmaxevts(0){
    
    
}


bool PlaybackLoop::SetDirectory(const std::string& d){
    
    m_datadir=d;
    return true;
}

bool PlaybackLoop::SetTree(const std::string& t){
    m_treename=t;
    
    return true;
}


bool PlaybackLoop::Configure(){
     
    m_chain=_buildChain(_read_directory(m_datadir), m_treename );
    if(m_chain==nullptr){
        std::cerr<<"Unable to build chain via trename "<<m_treename<<std::endl;
        exit(7);
    }
    
    
    m_nmaxevts=m_chain->GetEntries();
    LOG_TRACE("PlaybackLoop ===> will run on "<<m_nmaxevts<<" events");
    
    
    std::for_each(algMgr()->algorithms().begin(),algMgr()->algorithms().end(),
                  [&](const std::pair<std::string,IAlgorithm*> kv){
                        kv.second->bindData(m_chain);  
                         kv.second->initialize();
                });
    return true;
}

PlaybackLoop::~PlaybackLoop(){
    
}

//posix approach
std::vector<std::string> PlaybackLoop::_read_directory(const std::string& name, const std::string& pattern){
    std::regex patt(pattern);
    std::vector<std::string> v;
    DIR* dirp = opendir(name.c_str());
    struct dirent * dp;
    while ((dp = readdir(dirp)) != NULL){
        std::string fname(dp->d_name);
        if(!std::regex_match(fname,patt)) continue;
        v.push_back(name+"/"+std::string(dp->d_name));
    }
    closedir(dirp);
    return v;
}


std::vector<std::string> PlaybackLoop::_read_txt(const std::string& filelist,const std::string& pattern){
    std::vector<std::string> v;
    std::regex patt(pattern);
    std::ifstream infile;
    infile.open(filelist);
    std::string currfile;
    while(infile>>currfile){
        if(!std::regex_match(currfile,patt)) continue;
        v.push_back(currfile);
    }
    return v;    
    
}



TChain* PlaybackLoop::_buildChain(const std::vector<std::string>& files,const std::string& treename){
    TChain *c=new TChain(treename.c_str());//doesnt own
    for(auto F : files){
        LOG_TRACE("PlaybackLoop  ===> picked up file: "<<F);
        std::ifstream ifs(F);
        if(! ifs) continue;
        c->Add(TString(F));
    }
    return c;
}


bool PlaybackLoop::Run(){
        
    for(unsigned int i=0;i<m_nmaxevts;i++){ //main event loop
        if(i%100==0){
            LOG_TRACE("Procesing entry "<<i);
        }
        m_chain->GetEntry(i);
        algMgr()->ExecuteAlgorithms();//Eventwise execution of the algorithms in the order they were appended
    }
    return true;
}
    
bool PlaybackLoop::Finalize(){
    LOG_TRACE("PlaybackLoop ===> Finalizing");
    
    
    return true;
}
