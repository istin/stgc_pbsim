include_directories(${CMAKE_CURRENT_SOURCE_DIR}/..)
FILE(GLOB GeoModelSources *.cxx)
FILE(GLOB GeoModelHeaders *.h)
list(FILTER GeoModelHeaders EXCLUDE REGEX "Linkdef.h$")

set(LIBNAME GeoModel)
ROOT_GENERATE_DICTIONARY(G__${LIBNAME} ${GeoModelHeaders} LINKDEF Linkdef.h MODULE)




add_library( ${LIBNAME} SHARED ${GeoModelSources} G__${LIBNAME}.cxx)



target_link_libraries(${LIBNAME} ${ROOT_LIBRARIES} XMLUtils XMLParser)
install(TARGETS ${LIBNAME}
    LIBRARY DESTINATION ${PLAYBACK_LIBS}
)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/lib${LIBNAME}_rdict.pcm DESTINATION ${PLAYBACK_LIBS}) 
