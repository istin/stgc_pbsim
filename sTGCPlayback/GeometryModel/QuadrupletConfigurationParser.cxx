/*
 * sTGCGeometryParser.cpp
 *
 *  Created on: Feb 3, 2019
 *      Author: Enrique.Kajomovitz@cern.ch
 *
 */
#include "TDOMParser.h"
#include <GeometryModel/QuadrupletConfigurationParser.h>
#include <Utilities/LoggingSvc.h>

 #define  DBG std::cout << __LINE__ << " " << __FILE__ << std::endl;

namespace STGC_GEOMETRY {

    QuadrupletConfigurationParser::QuadrupletConfigurationParser(TXMLNode* in_docRoot):
        m_docRoot(in_docRoot),
        m_configFile(""),
        m_ownDocRoot(false){
            
    }


    
    QuadrupletConfigurationParser::~QuadrupletConfigurationParser() {
        if (m_ownDocRoot){
            delete m_docRoot;
        }
        
    }


    void QuadrupletConfigurationParser::ProcessDefaultNode(TXMLNode* xmlNode) {
        // assumption: xmlNode is NOT a named node for which we have a specific handling
        // therefore, we don't care about its attributes, but we do have to go over all its children
        TXMLNode* child = xmlNode->GetChildren();

       
        while (child != NULL) {
            m_parser.Clear();
            TString nodeName = child->GetNodeName();
            if (nodeName.EqualTo("var"))
                ProcessVarNode(child);
            else if (nodeName.EqualTo("array"))
                ProcessArrayNode(child);
            else if (nodeName.EqualTo("sTGC"))
                ProcessSTGCNode(child);
            else if (nodeName.EqualTo("chamberPosition"))
                ProcessChamberPositionNode(child);
            else ProcessDefaultNode(child);

            child = child->GetNextNode();
        }
        

    }

    void QuadrupletConfigurationParser::ProcessVarNode(TXMLNode* xmlNode) {
        if (!m_parser.ParseNode(xmlNode, "var", "name:value")) {
            xmlNode->Dump();
            LOG_FATAL(" here");
            throw std::runtime_error("Not all attributes present in var xmlNode");
        }
        varLookup[m_parser.ValueOf("name")] = m_parser.ValueOf("value");
    }

    void QuadrupletConfigurationParser::ProcessArrayNode(TXMLNode* xmlNode) {
        if (!m_parser.ParseNode(xmlNode, "array", "name:values")) {
            xmlNode->Dump();
            LOG_FATAL(" here");
            throw std::runtime_error("Not all attributes present in array xmlNode");
        }
        arrayLookup[m_parser.ValueOf("name")] = m_parser.ValueOf("values");
    }

    void QuadrupletConfigurationParser::ProcessSTGCNode(TXMLNode* xmlNode) {
        if (!m_parser.ParseNode(xmlNode, "sTGC", "type:tech:subType:sWidth:lWidth:Length:Tck:xFrame:ysFrame:ylFrame:yCutout")) {
            xmlNode->Dump();
            LOG_FATAL("here");
            throw std::runtime_error("Not all attributes present in sTGC xmlNode");
        }

        QuadrupletConfiguration& quadConfig = Upsert(m_parser.ValueOf("type"));
        
        quadConfig.tech = ValueAsString("tech");
        quadConfig.subType = ValueAsString("subType");
        quadConfig.sWidth = ValueAsDouble("sWidth");
        quadConfig.lWidth = ValueAsDouble("lWidth");
        quadConfig.Length = ValueAsDouble("Length");

        TXMLNode* child = xmlNode->GetChildren();
        if (child == NULL){
            LOG_FATAL("here");
            throw std::runtime_error("Expecting node with name: sTGC_readout but none found");

        }
        TString nodeName = child->GetNodeName();
        while (child != NULL) {
                if (strcmp(child->GetNodeName(), "text") == 0 || strcmp(child->GetNodeName(), "comment") == 0 ){
                child = child->GetNextNode();
                continue;
                }
        
        
            m_parser.Clear();
            TString attributesToParse = "firstTriggerBand:nTriggerBands:firstStripInTrigger:firstStripWidth:nStrips:";
            attributesToParse += "StripsInBandsLayer1:StripsInBandsLayer2:StripsInBandsLayer3:StripsInBandsLayer4";
            
            //bool succeeds = m_parser.ParseNode(child, "sTGC_readout", attributesToParse);
            if (m_parser.ParseNode(child, "sTGC_readout", attributesToParse))
                break;
            
            //child->Dump();
            LOG_FATAL("here");
            throw std::runtime_error("Not all attributes present in sTGC_readout xmlNode");
        }
        
        quadConfig.firstTriggerBand = ValueAsIntList("firstTriggerBand"); 
        quadConfig.nTriggerBands =  ValueAsIntList("nTriggerBands");
        quadConfig.nStrips = ValueAsInt("nStrips"); ;
        quadConfig.firstStripWidth = ValueAsDoubleList("firstStripWidth"); ;
        quadConfig.firstStripInTrigger = ValueAsIntList("firstStripInTrigger"); 
        quadConfig.StripsInBandsLayer1 = ValueAsIntList("StripsInBandsLayer1"); 
        quadConfig.StripsInBandsLayer2 = ValueAsIntList("StripsInBandsLayer2"); 
        quadConfig.StripsInBandsLayer3 = ValueAsIntList("StripsInBandsLayer3"); 
        quadConfig.StripsInBandsLayer4 = ValueAsIntList("StripsInBandsLayer4"); 

        m_parser.Clear();
        
    }

    void QuadrupletConfigurationParser::ProcessChamberPositionNode(TXMLNode* xmlNode) {
        if (!m_parser.ParseNode(xmlNode, "chamberPosition", "volume:radius:zPos:phi0:chamberType")) {
            xmlNode->Dump();
            LOG_FATAL("here");
            throw std::runtime_error("Not all attributes present in chamberPosition xmlNode");
        }

        QuadrupletConfiguration& quadConfig = Upsert(m_parser.ValueOf("volume"));
        quadConfig.radius = ValueAsDouble("radius");
        quadConfig.zPos = ValueAsDouble("zPos");
        quadConfig.phi0 = ValueAsDouble("phi0");

        TString chamberType = ValueAsString("chamberType");
        chamberType.ToLower();
        if (chamberType.EqualTo("stgl"))
            quadConfig.sectorType = SECTOR_TYPE::LARGE;
        else if (chamberType.EqualTo("stgs"))
            quadConfig.sectorType = SECTOR_TYPE::SMALL;
        else throw std::runtime_error("Unknown sector type: " + chamberType);
    }


    std::vector<int> QuadrupletConfigurationParser::ValueAsIntList(const TString& attribute) {
        std::vector<TString>* tokens = XMLNodeParser::Tokenize(ArrayValueOf(m_parser.ValueOf(attribute)), ";");
        std::vector<int> result;
        for (std::vector<TString>::iterator it = tokens->begin(); it != tokens->end(); ++it) {
            TString& value = *it;
            int trimIdx = 0;
            while (trimIdx < value.Length()) {
                char c = value(trimIdx);
                if (c == ' ' || c == '\t' || c == '\r' || c == '\n')
                    trimIdx++;
                else break;
            }
            result.push_back(TString(value(trimIdx, value.Length() - trimIdx)).Atoi());
        }
        delete tokens;
        return result;
    }

    std::vector<double> QuadrupletConfigurationParser::ValueAsDoubleList(const TString& attribute) {
        std::vector<TString>* tokens = XMLNodeParser::Tokenize(ArrayValueOf(m_parser.ValueOf(attribute)), ";");

        std::vector<double> result;
        for (std::vector<TString>::iterator it = tokens->begin(); it != tokens->end(); ++it) {
            TString& value = *it;
            int trimIdx = 0;
            while (trimIdx < value.Length()) {
                char c = value(trimIdx);
                if (c == ' ' || c == '\t' || c == '\r' || c == '\n')
                    trimIdx++;
                else break;
            }
            result.push_back(TString(value(trimIdx, value.Length() - trimIdx)).Atof());
        }
        delete tokens;
        return result;
    }

    int QuadrupletConfigurationParser::ValueAsInt(const TString& attribute) {
        return ValueAsString(attribute).Atoi();
    }


    double QuadrupletConfigurationParser::ValueAsDouble(const TString& attribute) {
        return ValueAsString(attribute).Atof();
    }

    TString QuadrupletConfigurationParser::ValueAsString(const TString& attribute) {
        return VarValueOf(m_parser.ValueOf(attribute));
    }

    QuadrupletConfiguration& QuadrupletConfigurationParser::Upsert(const TString& name) {
        std::map<TString,QuadrupletConfiguration>::iterator it = quadLookup.find(name);
        if (it == quadLookup.end()) {
            QuadrupletConfiguration& result = quadLookup[name];
            result.type = name;
            return result;
        }
        return it->second;
    }

    TString QuadrupletConfigurationParser::VarValueOf(const TString& rawValue) {
        std::map<TString,TString>::iterator it = varLookup.find(rawValue);
        if (it == varLookup.end())
            return rawValue;
        return it->second;
    }

    TString QuadrupletConfigurationParser::ArrayValueOf(const TString& rawValue) {
        std::map<TString,TString>::iterator it = arrayLookup.find(rawValue);
        if (it == arrayLookup.end())
            return rawValue;
        return it->second;
    }

    void  QuadrupletConfigurationParser::Parse(){
        ProcessDefaultNode(m_docRoot);
    }

    void QuadrupletConfigurationParser::SetConfigurationFile(TString in_configFile){
        m_configFile = in_configFile;
     
    }

    void QuadrupletConfigurationParser::SetXMLRoot(TXMLNode* in_docRoot){
        if (in_docRoot != NULL)
            m_docRoot = in_docRoot;
        m_domParser=new TDOMParser();
        if (m_configFile == ""){
            LOG_FATAL("here");
            throw std::runtime_error("Trying to setup an empty XML file for configuration");
        }

        m_domParser->SetValidate(false); // do not validate with DTD
        int parsecode = m_domParser->ParseFile(m_configFile);

        if (parsecode < 0) {
            throw std::runtime_error(m_domParser->GetParseCodeMessage(parsecode));
        }

        m_docRoot = m_domParser->GetXMLDocument()->GetRootNode();
        m_ownDocRoot = true;
    }
}

