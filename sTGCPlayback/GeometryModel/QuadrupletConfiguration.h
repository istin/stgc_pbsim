/*
 * QuadrupletConfiguration.h
 *
 *  Created on: Feb 3, 2019
 *      Author: ekajomov
 */

#ifndef QUADRUPLETCONFIGURATION_H_
#define QUADRUPLETCONFIGURATION_H_

#include <vector>
#include "TString.h"

#include "GeometryModel/sTGCGeometry.h"
#include <iostream>

namespace STGC_GEOMETRY
{
	struct QuadrupletConfiguration
	{
		TString type;
		TString tech;
		TString subType;
		double sWidth;
		double lWidth;
		double Length;
		int nStrips;
		//all of below must be size of 8
		std::vector<int> firstTriggerBand;
		std::vector<int> nTriggerBands;
		std::vector<int> firstStripInTrigger;
		std::vector<double> firstStripWidth;
		std::vector<int> StripsInBandsLayer1;
		std::vector<int> StripsInBandsLayer2;
		std::vector<int> StripsInBandsLayer3;
		std::vector<int> StripsInBandsLayer4;

		// information that comes from chamberPosition
		double zPos;
		double radius;

		SECTOR_TYPE sectorType;
		QUAD quad;
		WEDGE wedge;

		double phi0;
		void Dump()
		{
			std::cout << "===========>>>> Quadruplet configuration Dump <<<<=========" << std::endl;
			std::cout << "type: " << type << " tech: " << tech << "subType: " << subType << std::endl;
			std::cout << "sWidth: " << sWidth << " lWidth: " << lWidth << " Length: " << Length << std::endl;
			std::cout << "nStrips: " << nStrips << std::endl;
			for (int i_layer = 0; i_layer < 4; i_layer++)
				std::cout << "firstStripInTrigger (layer =  "<<  i_layer << ") = " << firstStripInTrigger.at(i_layer) << std::endl;
			for (int i_layer = 0; i_layer < 4; i_layer++)
				std::cout << "firstStripWidth (layer =  "<<  i_layer << ") = " << firstStripWidth.at(i_layer) << std::endl;

			std::cout << "StripsInBandsLayer1: ";
			for (unsigned int i_band = 0 ; i_band < StripsInBandsLayer1.size(); i_band++)
				std::cout << StripsInBandsLayer1.at(i_band) << "; ";
			std::cout << std::endl;

			std::cout << "StripsInBandsLayer2: ";
			for (unsigned int i_band = 0 ; i_band < StripsInBandsLayer2.size(); i_band++)
				std::cout << StripsInBandsLayer1.at(i_band) << "; ";
			std::cout << std::endl;

			std::cout << "StripsInBandsLayer3: ";
			for (unsigned int i_band = 0 ; i_band < StripsInBandsLayer3.size(); i_band++)
				std::cout << StripsInBandsLayer3.at(i_band) << "; ";
			std::cout << std::endl;

			std::cout << "StripsInBandsLayer4: ";
			for (unsigned int i_band = 0 ; i_band < StripsInBandsLayer4.size(); i_band++)
				std::cout << StripsInBandsLayer4.at(i_band) << "; ";
			std::cout << std::endl;
			std::cout << "===========>>>><<<<=========" << std::endl;

		}

	};
}

#endif /* QUADRUPLETCONFIGURATION_H_ */
