/*
 * sTGCGeometry.h
 *
 *  Created on: Jan 30, 2019
 *      Author: ekajomov
 */

#ifndef STGCGEOMETRY_H_
#define STGCGEOMETRY_H_

namespace STGC_GEOMETRY
{
	enum class WEDGE{
		PIVOT,
		CONFIRM,
		UNDEFINED
	};

	enum class QUAD{
		Q1,
		Q2,
		Q3,
		UNDEFINED
	};

	enum class LAYER{
		L1,
		L2,
		L3,
		L4,
		UNDEFINED
	};

	enum class SECTOR_TYPE
	{
		LARGE,
		SMALL,
		UNDEFINED
	};

	enum class SECTOR_NUMBER{
		S01,
		S02,
		S03,
		S04,
		S05,
		S06,
		S07,
		S08,
		S09,
		S10,
		S11,
		S12,
		S14,
		S15,
		S16,
		UNDEFINED
	};

	enum class SIDE{
		A,
		C,
		UNDEFINED
	};
}




#endif /* STGCGEOMETRY_H_ */
