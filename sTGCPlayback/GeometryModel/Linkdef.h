

#include "GeometryModel/QuadrupletConfiguration.h"
#include "GeometryModel/SectorConfiguration.h"
#include "GeometryModel/sTGCGeometry.h"
#include "GeometryModel/QuadrupletConfigurationParser.h"
#include "GeometryModel/SectorConfigurationParser.h"


#include <vector>

#ifdef __ROOTCLING__

#pragma link C++ class STGC_GEOMETRY::QuadrupletConfiguration+;
#pragma link C++ class STGC_GEOMETRY::SectorConfiguration+;
#pragma link C++ enum class STGC_GEOMETRY::sTGCGeometry+;
#pragma link C++ class STGC_GEOMETRY::QuadrupletConfigurationParser+;
#pragma link C++ class STGC_GEOMETRY::SectorConfigurationParser+;


#endif
