/*
 * sTGCGeometryParser.h
 *
 *  Created on: Feb 3, 2019
 *      Author: Enrique.Kajomovitz@cern.ch
 */

#ifndef QUADRUPLETCONFIGURATIONPARSER_H_
#define QUADRUPLETCONFIGURATIONPARSER_H_

#include <map>
#include <vector>
#include "TXMLNode.h"
#include <TDOMParser.h>


#include "GeometryModel/QuadrupletConfiguration.h"
#include "XMLUtils/XMLNodeParser.h"

using namespace XML_UTILS;

namespace STGC_GEOMETRY {

/**
 * This is used to parse an XML document and extract all the quadruplet configuration information
 * It doesn't recreate exactly the XML structure, but rather flattens the results. For example,
 * it does not care where variables are declared, nor what section a quadruplet configuration is defined in.
 */
class QuadrupletConfigurationParser {
public:
	QuadrupletConfigurationParser(TXMLNode* in_docRoot = NULL);
    
    void SetXMLRoot(TXMLNode* in_docRoot = NULL);
	void SetConfigurationFile(TString in_configFile);

	void Parse();

	virtual ~QuadrupletConfigurationParser();

	std::map<TString, QuadrupletConfiguration> GetQuadLookup() const {return quadLookup;}

protected:
	TString ValueAsString(const TString& attribute);

private:

	void ProcessDefaultNode(TXMLNode* xmlNode);
	void ProcessVarNode(TXMLNode* xmlNode);
	void ProcessArrayNode(TXMLNode* xmlNode);
	void ProcessSTGCNode(TXMLNode* xmlNode);
	void ProcessChamberPositionNode(TXMLNode* xmlNode);

	std::vector<int> ValueAsIntList(const TString& attribute);
	std::vector<double> ValueAsDoubleList(const TString& attribute);
	int ValueAsInt(const TString& attribute);
	double ValueAsDouble(const TString& attribute);

	TString VarValueOf(const TString& rawValue);
	TString ArrayValueOf(const TString& rawValue);

	QuadrupletConfiguration& Upsert(const TString& name);

	TXMLNode* m_docRoot;
    
    TDOMParser *m_domParser;
    
	TString m_configFile;
	bool m_ownDocRoot;
	XMLNodeParser m_parser;
	std::map<TString, QuadrupletConfiguration> quadLookup;
	std::map<TString, TString> varLookup; // variable types will be resolved by specific attributes; wasteful, but simplest
	std::map<TString, TString> arrayLookup; // variable types will be resolved by specific attributes; wasteful, but simplest

};


}

#endif /* QUADRUPLETCONFIGURATIONPARSER_H_ */
