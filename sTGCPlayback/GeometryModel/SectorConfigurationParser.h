/*
 * SectorParser.h
 *
 *  Created on: Feb 5, 2019
 *      Author: ekajomov
 */

#ifndef SECTORCONFIGURATIONPARSER_H_
#define SECTORCONFIGURATIONPARSER_H_

#include <map>
#include <vector>
#include "TXMLNode.h"

#include "GeometryModel/SectorConfiguration.h"
#include "XMLUtils/XMLNodeParser.h"


namespace STGC_GEOMETRY {

class SectorConfigurationParser {
public:
	SectorConfigurationParser(TXMLNode* in_docRoot = NULL);
	virtual ~SectorConfigurationParser();
	void SetXMLRoot(TXMLNode* in_docRoot = NULL);
	void SetConfigurationFile(TString in_configFile);

	void Parse();
	std::map<TString, SectorConfiguration> GetSectorLookup();



private:

	SectorConfiguration& Upsert(const TString& name);

	TXMLNode* docRoot;
	TString configFile;
	bool ownDocRoot;
	XML_UTILS::XMLNodeParser parser;



};

} /* namespace STGC_GEOMETRY */

#endif /* SECTORCONFIGURATIONPARSER_H_ */
