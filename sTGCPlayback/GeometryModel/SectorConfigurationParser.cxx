/*
 * SectorParser.cpp
 *
 *  Created on: Feb 5, 2019
 *      Author: Enrique.Kajomovitz@cern.ch
 *
 */

#include "GeometryModel/SectorConfigurationParser.h"

namespace STGC_GEOMETRY {

SectorConfigurationParser::SectorConfigurationParser(TXMLNode* in_docRoot):
docRoot(in_docRoot),
configFile(""),
ownDocRoot(false)
{

}

SectorConfigurationParser::~SectorConfigurationParser() {
	// TODO Auto-generated destructor stub
}

} /* namespace STGC_GEOMETRY */
