/*
 * SectorConfiguration.h
 *
 *  Created on: Feb 5, 2019
 *      Author: Enrique.Kajomovitz@cern.ch
 */

#ifndef SECTORCONFIGURATION_H_
#define SECTORCONFIGURATION_H_


#include <vector>
#include "TString.h"

#include "GeometryModel/sTGCGeometry.h"

namespace STGC_GEOMETRY
{
	struct SectorConfiguration
	{
		SECTOR_NUMBER sector_number;
		SECTOR_TYPE sector_type;
		SIDE side;
		double phi;
		std::vector<TString> types;

	};


}



#endif /* SECTORCONFIGURATION_H_ */
