/*
 * XMLParserHelper.cpp
 *
 *  Created on: 31 Jul 2018
 *      Author: ekajomov
 */

#include "TXMLAttr.h"
#include "TObjArray.h"
#include "TList.h"
#include "TObjString.h"
#include <Utilities/LoggingSvc.h>
#include <XMLUtils/XMLNodeParser.h>
namespace XML_UTILS
{

XMLNodeParser::XMLNodeParser() {
	// TODO Auto-generated constructor stub

}

XMLNodeParser::~XMLNodeParser() {
	// TODO Auto-generated destructor stub
}

bool XMLNodeParser::ParseNode(TXMLNode* &node, const TString& name, const TString& attributes, bool& is_comment_or_text) {
	m_attributes.clear();
	TObjArray* ats_array = attributes.Tokenize(":");
	unsigned int n_atts = ats_array->GetEntries();
	is_comment_or_text = true;

	if (node->GetNodeType() != TXMLNode::kXMLElementNode )
		return false;
	if (strcmp(node->GetNodeName(), "text") == 0 || strcmp(node->GetNodeName(), "comment") == 0 )
		return false;
	is_comment_or_text = false;
	if (strcmp(node->GetNodeName(), name) != 0) {
			std::cout << "ERROR XMLNodeParser: expected a node of name: " << name << " but got " << node->GetNodeName()
																  << " - cannot proceed" << std::endl;
			return false;
		}

	TList *attrList = node->GetAttributes();
	TXMLAttr *attr = 0;
	TIter next(attrList);
	while ((attr=(TXMLAttr*)next())){
		TString attribute_name;
		TIter next_attribute_name(ats_array);
		while ( TObjString* strptr = (TObjString*) next_attribute_name()){  
            attribute_name = strptr->String();
			if (strcmp(attr->GetName(), attribute_name ) != 0)
				continue;
			m_attributes[attribute_name] = attr->GetValue();
			break;
		}
	}
	if (m_attributes.size() != n_atts)
	{
		std::cout << "ERROR XMLNodeParser: not all attributes could be parsed" << std::endl;
		delete ats_array;
		return false;
	}
	delete ats_array;
	return true;

	delete ats_array;
	return false;
}

TString XMLNodeParser::ValueOf(const TString& attribute) {
	std::map<TString, TString>::iterator itr = m_attributes.find(attribute);
	if (itr == m_attributes.end()){
		LOG_FATAL("here");
		throw std::runtime_error("Failed to find attribute " + attribute);
	}
	return itr->second;
}

double XMLNodeParser::DoubleAttributeValue(const TString& attribute){
    return double(this->ValueOf(attribute).Atof() );
}
int XMLNodeParser::IntAttributeValue(const TString& attribute){
    return int(this->ValueOf( attribute).Atoi() );
}




std::vector<TString>* XMLNodeParser::Tokenize(const TString& value, const TString& delims) {
	TObjArray* value_array = value.Tokenize(delims);
	TIter valsIter(value_array);

	std::vector<TString>* result = new std::vector<TString>();
	TString token;
	while (TObjString*  strptr = ((TObjString*) valsIter())) {
        token = strptr->String();
		result->push_back(token);
	}

	delete value_array;
	return result;
}

void XMLNodeParser::Clear(){
	m_attributes.clear();
}
}
