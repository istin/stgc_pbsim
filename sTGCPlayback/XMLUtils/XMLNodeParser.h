/*
 * XMLNODEPARSER_H_.h
 *
 *  Created on: 31 Jul 2018
 *      Author: ekajomov
 */
#include "TString.h"
#include "TXMLNode.h"
#include <map>
#include <vector>

#ifndef XMLNODEPARSER_H_
#define XMLNODEPARSER_H_

namespace XML_UTILS
{
class XMLNodeParser {
public:
	XMLNodeParser();
	virtual ~XMLNodeParser();

	bool ParseNode(TXMLNode* & node, const TString& name, const TString& attributes) {
		bool ignored;
		return ParseNode(node, name, attributes, ignored);
	}

	/**
	 * the attributes are separated by colons - returns false if any attributes are missing. Only the passed in attribute names can be
	 * passed to the appropriate attribute value method
	 */
	bool ParseNode(TXMLNode* & node, const TString& name, const TString& attributes, bool& is_comment_or_text);

	TString ValueOf(const TString& attribute);
	double DoubleAttributeValue(const TString& attribute);
	int IntAttributeValue(const TString& attribute);

	static std::vector<TString>* Tokenize(const TString& value, const TString& delims);


	void Clear();

protected:

	std::map<TString, TString> m_attributes;


};
}

#endif /* XMLNODEPARSER_H_ */
