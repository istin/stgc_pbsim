

#include "sTGCConfigGeneration/BandsConfigurationGeneration.h"
#include "sTGCConfigGeneration/sTGCsFEBandConfigGen.h"

#include <vector>

#ifdef __ROOTCLING__

#pragma link C++ class BandsConfigurationGeneration+;
#pragma link C++ class sTGCsFEBandConfigGen+;



#endif
