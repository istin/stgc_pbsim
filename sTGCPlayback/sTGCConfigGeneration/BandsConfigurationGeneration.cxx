/*
 * BandsConfigurationGeneration.cpp
 *
 *  Created on: Feb 24, 2019
 *      Author: ekajomov
 */

#include "sTGCConfigGeneration/BandsConfigurationGeneration.h"


#define N_CH_AB 512
#define N_CH_TDS 128

//#define  DBG std::cout << "E123: " << __LINE__ << " " << __FILE__ <<  std::endl;

BandsConfigurationGeneration::BandsConfigurationGeneration() {

}

BandsConfigurationGeneration::~BandsConfigurationGeneration() {
}

void BandsConfigurationGeneration::SetQuadLookup(
		std::map<TString, STGC_GEOMETRY::QuadrupletConfiguration> config) {
	quadLookup = config;
}

void BandsConfigurationGeneration::SetSABLookup(
		std::map<TString, sTGCsABConfiguration> config) {
	sABLookup = config;
}

void BandsConfigurationGeneration::SetSFELookup(
		std::map<TString, sTGCsFEConfiguration> config) {
	sFELookup = config;
}

void BandsConfigurationGeneration::ComputeBandConfig(TString sfeb) {

	// 1. Get Quad
	// 2. Extract which strips participate in the band
	// 3. for each AB and FE
	//        find AB channels matching strips
	//        find TDS channels matching AB channels

	//std::cout << sfeb << std::endl;

	fe_configGen.Reset();

	TString quad = sfeb(0, sfeb.Length() - 1);
	TString s_layer = sfeb(sfeb.Length() - 1, 1);
	int layer = s_layer.Atoi();
	int i_layer = layer - 1;
	STGC_GEOMETRY::QuadrupletConfiguration qc = quadLookup.at(quad);

	int firstTriggerBand = qc.firstTriggerBand.at(i_layer);
	int nTriggerBands = qc.nTriggerBands.at(i_layer);
	int firstStripInTrigger = qc.firstStripInTrigger.at(i_layer);

	std::vector<int> stripsInBand;


	if (i_layer == 0)
		stripsInBand = qc.StripsInBandsLayer1;
	else if (i_layer == 1)
		stripsInBand = qc.StripsInBandsLayer2;
	else if (i_layer == 2)
		stripsInBand = qc.StripsInBandsLayer3;
	else
		stripsInBand = qc.StripsInBandsLayer4;

	// now get the AB and FE
	sTGCsABConfiguration sAB = sABLookup.at(sfeb);
	sTGCsFEConfiguration sFE = sFELookup.at(sfeb);
	int activeChannels = sAB.nActiveChannels;


	fe_configGen.SetFEType(sfeb);
	fe_configGen.SetDirection(sAB.direction);

	//map bands to channels
	int i_begin_ch = (sAB.direction == DIR_WRT_DET::PARALLEL) ? 0 : N_CH_AB - 1;
	int ch_step = (sAB.direction == DIR_WRT_DET::PARALLEL) ? 1 : -1;

	int current_band = firstTriggerBand-1;
	int innermostStripInBand = 0;
	int outermostStripInBand = 0;

	// get the index of the first band

	int stripSum = 0;
	current_band = firstTriggerBand-1;
	for (unsigned int i_band = 0;
			i_band < stripsInBand.size();
			i_band++) {

		int nStripsInBand = stripsInBand.at(i_band);
		int nStripsInBandInTrigger = nStripsInBand;
		stripSum += nStripsInBand;

		innermostStripInBand = outermostStripInBand + 1;
		outermostStripInBand = stripSum;

		if (stripSum < firstStripInTrigger)
		{
			continue;
		}

		if (firstStripInTrigger > innermostStripInBand) // fix for trigger acceptance
		{
			innermostStripInBand = firstStripInTrigger;
			nStripsInBandInTrigger = (outermostStripInBand - innermostStripInBand + 1);
		}
		current_band++;

		//std::cout << "current_band: "  << current_band << " innermostStripInBand: " << innermostStripInBand << " outermostStripInBand: " << outermostStripInBand << std::endl;
		bool firstIsInner = sAB.direction == DIR_WRT_DET::PARALLEL;
		int stripForFirstChannel = (firstIsInner) ? innermostStripInBand : outermostStripInBand;
		int stripForLastChannel =  (!firstIsInner) ? innermostStripInBand : outermostStripInBand;
		//std::cout << "stripForFirstChannel: " << stripForFirstChannel << " stripForLastChannel: " << stripForLastChannel << std::endl;
		int firstChannel = sAB.GetChannelForStrip(stripForFirstChannel);
		int lastChannel = sAB.GetChannelForStrip(stripForLastChannel);

		if (firstChannel > 511 || lastChannel > 511 || firstChannel < 0 || lastChannel < 0)
		{
			std::cout << "ERROR: channels out of range for current band (" << current_band << ") first and last: " << firstChannel << ", " << lastChannel << std::endl;
			std::cout << sAB.type << std::endl;
			std::cout << "stripSum: " << stripSum << std::endl;
			std::cout << "stripsInBand.size(): " << stripsInBand.size() << std::endl;
			std::cout << "innermostStripInBand: " <<  innermostStripInBand << " outermostStripInBand: " << outermostStripInBand << std::endl;
			std::cout << "stripForFirstChannel: " << stripForFirstChannel << " stripForLastChannel: " << stripForLastChannel << std::endl;
			std::cout << "firstTriggerBand: " << firstTriggerBand << " nTriggerBands: " << nTriggerBands << " firstStripInTrigger: " << firstStripInTrigger << std::endl;
			continue;
		}


		int inconsistent_tds = false;

		for (int ch = firstChannel; ch <= lastChannel; ch++)
		{
			int connection = sFE.ChannelConnectedToTDS(ch);

			if (connection == 1)
				continue;
			std::cout << "=======================WARNING: channels not connected to TDS in band: " << current_band << " ch: " << ch << std::endl;
			inconsistent_tds = true;
			;

		}

		if (inconsistent_tds)
			continue;

		//std::cout << "current_band: " << current_band << " firstChannel: " << firstChannel << " nStripsInBandInTrigger: " << nStripsInBandInTrigger << std::endl;
		fe_configGen.AddBand(current_band, firstChannel, nStripsInBandInTrigger, 0);
	}


	sTGCsFEBandConfig& feb_config = Upsert(sfeb);

	feb_config.fe_type = sfeb;
	feb_config.direction = sAB.direction;
	for (int i_tds = 0; i_tds < 4; i_tds++)
	{

		fe_configGen.FillTDSConfig(i_tds+1, feb_config.tdsConfigs.at(i_tds));

	}
	//feb_config.Dump();

	return;


}

sTGCsFEBandConfig& BandsConfigurationGeneration::Upsert(const TString& name)
{
	std::map<TString,sTGCsFEBandConfig>::iterator it = sFEBandConfigLookup.find(name);
	if (it == sFEBandConfigLookup.end()) {
		sTGCsFEBandConfig& result = sFEBandConfigLookup[name];
		result.fe_type = name;
		return result;
	}
	return it->second;
}

std::map<TString, sTGCsFEBandConfig> BandsConfigurationGeneration::GetFEBandLookup()
{
	return sFEBandConfigLookup;
}
