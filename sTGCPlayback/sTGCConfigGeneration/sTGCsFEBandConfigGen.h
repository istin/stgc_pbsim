/*
 * sTGCsFEBandConfig.h
 *
 *  Created on: Feb 26, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_CABLINGMODEL_STGCSFEBANDCONFIG_H_
#define STGCPLAYBACK_CABLINGMODEL_STGCSFEBANDCONFIG_H_

#include "TString.h"
#include <vector>

#include "CablingModel/CablingModelEnums.h"
#include "CablingModel/sTDSConfig.h"

class sTGCsFEBandConfigGen {
public:
	sTGCsFEBandConfigGen();
	sTGCsFEBandConfigGen(TString feb, DIR_WRT_DET dir);
	virtual ~sTGCsFEBandConfigGen();
	void AddBand(int band_id, int firstChannel, int nChannels, double firstChannel_offset_in_strips_units=0);

	void FillTDSConfig(int i_tds, sTDSConfig& tds);
	void BandMergerOffsetTable();

	void Reset();


	DIR_WRT_DET GetDirection() const {
		return direction;
	}

	const TString& GetFEType() const {
		return fe_type;
	}

	void SetFEType(const TString& febType) {
		fe_type = febType;
	}

	void SetDirection(DIR_WRT_DET dir){
		direction = dir;
	}

protected:

	TString fe_type;
	DIR_WRT_DET direction;
	std::vector<int> m_firstChannel;
	std::vector<int> m_nChannels;
	std::vector<int> m_firstChannel_offset_in_nStrips;
	std::vector<int> m_channel_to_band;

};

#endif /* STGCPLAYBACK_CABLINGMODEL_STGCSFEBANDCONFIG_H_ */
