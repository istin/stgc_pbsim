/*
 * sTGCsFEBandConfig.cpp
 *
 *  Created on: Feb 26, 2019
 *      Author: ekajomov
 */

#include <sTGCConfigGeneration/sTGCsFEBandConfigGen.h>
#define N_BAND_IDS 128
#define N_CHANNELS 512
#define N_CHANNELS_IN_TDS 128
//#define  DBG std::cout << "E123: " << __LINE__ << " " << __FILE__ << std::endl;
#include <Utilities/LoggingSvc.h>


sTGCsFEBandConfigGen::sTGCsFEBandConfigGen()
:fe_type(""),
direction(DIR_WRT_DET::UNDEFINED),
m_firstChannel(N_BAND_IDS, 0),
m_nChannels(N_BAND_IDS,0),
m_firstChannel_offset_in_nStrips(N_BAND_IDS,0),
m_channel_to_band(N_CHANNELS,0)
{
}

sTGCsFEBandConfigGen::sTGCsFEBandConfigGen(TString feb, DIR_WRT_DET dir)
:fe_type(feb), direction(dir),
 m_firstChannel(N_BAND_IDS, 0),
 m_nChannels(N_BAND_IDS,0),
 m_firstChannel_offset_in_nStrips(N_BAND_IDS,0),
 m_channel_to_band(N_CHANNELS,0)
{
}


sTGCsFEBandConfigGen::~sTGCsFEBandConfigGen()
{
}

void sTGCsFEBandConfigGen::Reset()
{
	fe_type = "";
	direction = DIR_WRT_DET::UNDEFINED;
	for (int i_b = 0; i_b < N_BAND_IDS; i_b++)
	{
		m_firstChannel[i_b] = 0;
		m_nChannels[i_b] = 0;
		m_firstChannel_offset_in_nStrips[i_b] = 0;
	}
	for (int i_ch = 0; i_ch < N_CHANNELS; i_ch++)
		m_channel_to_band[i_ch] = 0;
}



void sTGCsFEBandConfigGen::AddBand(int band_id, int firstChannel, int nChannels,
                                  double firstChannel_offset_in_strips_units){


	m_firstChannel.at(band_id-1) = firstChannel;
	m_nChannels.at(band_id-1) = nChannels;
	m_firstChannel_offset_in_nStrips.at(band_id-1) = firstChannel_offset_in_strips_units;

	for (int n_ch = 0; n_ch < nChannels; n_ch++)
	{

		int present_band = m_channel_to_band.at(firstChannel + n_ch);
		if (present_band != 0  && present_band != band_id)
		{
			LOG_FATAL("here");
			throw std::runtime_error("Trying to add bands with overlapping strips");
		}

		m_channel_to_band.at(firstChannel + n_ch) = band_id;
	}

}

void sTGCsFEBandConfigGen::FillTDSConfig(int tds_id, sTDSConfig& tds){

	tds.fe_type = fe_type;
	tds.direction = direction;
	tds.tds_id = tds_id;

	int first_tds_physical_channel = (tds_id -1) * N_CHANNELS_IN_TDS; //Channels are zero-based
	int current_band = 0;
	int n_ch =  0;
	while(n_ch < N_CHANNELS_IN_TDS)
	{

		int i_ch = first_tds_physical_channel + n_ch;
		current_band = m_channel_to_band.at(i_ch);

		if (current_band == 0)
		{
			n_ch++;
			continue;
		}
		int n_channels = 0;
		int first_tds_channel_of_band = n_ch;
		while (n_ch < N_CHANNELS_IN_TDS)
		{
			if (m_channel_to_band.at(i_ch) != current_band){
				break;
			}
			n_channels++;
			n_ch++;
			i_ch = first_tds_physical_channel + n_ch;
		}
		BAND_SEGMENT segment = BAND_SEGMENT::UNDEFINED;
		if ( m_nChannels.at(current_band - 1) == n_channels)
		{
			segment = BAND_SEGMENT::COMPLETE;
		}
		else if (n_ch == N_CHANNELS_IN_TDS)
		{
			segment = (direction == DIR_WRT_DET::PARALLEL) ? BAND_SEGMENT::INNER : BAND_SEGMENT::OUTER;
		}
		else
		{
			segment = (direction == DIR_WRT_DET::PARALLEL) ? BAND_SEGMENT::OUTER : BAND_SEGMENT::INNER;
		}

		tds.band_segment.at(current_band-1) = segment;
		tds.firstChannel.at(current_band-1) = first_tds_channel_of_band;
		tds.nChannels.at(current_band-1) = n_channels;
		tds.nBands++;
		tds.firstBand = (tds.firstBand < current_band && tds.firstBand != 0) ? tds.firstBand : current_band;
	}

	//tds.Dump();

}


void sTGCsFEBandConfigGen::BandMergerOffsetTable()
{
	std::vector<int> offsets(256,0); // index is: (BAND_ID - 1) + isOuter * 128
	int previous_tds = 0;
	int previous_band = 0;
	int offset = 0;
	int current_tds = 0;
	int current_band = 0;
	int increment =  (direction == DIR_WRT_DET::PARALLEL) ? 1 : -1;
	int i_startingChannel = (direction == DIR_WRT_DET::PARALLEL) ? 0 : m_channel_to_band.size();
	int i_offset_band_bit  = (direction == DIR_WRT_DET::PARALLEL) ? 128: 0;


	for (unsigned int nCh = 0;  nCh < m_channel_to_band.size(); nCh++)
	{
		int i_ch = i_startingChannel + nCh * increment;
		current_tds = (i_ch/128) + 1;
		current_band =  m_channel_to_band[i_ch];

		if (previous_band != current_band)
		{
			previous_band = current_band;
			previous_tds = current_tds;
			offset = 1;
			continue;
		}


		if (previous_tds == current_tds)
		{
			offset++;
			continue;
		}

		else // this means that we changed tds but we are in the same band
		{
			offsets[current_band - 1 + i_offset_band_bit] = offset;
			continue;
		}

	}

//	for (int i_band =0; i_band < offsets.size() / 2; i_band++)
//	{
//		std::cout << "B: " << i_band + 1 << " I: " << offsets[i_band] << " O: " << offsets[i_band + 128] << std::endl;
//	}
}
