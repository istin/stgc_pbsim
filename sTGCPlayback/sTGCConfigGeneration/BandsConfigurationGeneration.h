/*
 * BandsConfigurationGeneration.h
 *
 *  Created on: Feb 24, 2019
 *      Author: Enrique.Kajomovitz@cern.ch
 */

#ifndef STGCPLAYBACK_STGCCONFIGGENERATION_BANDSCONFIGURATIONGENERATION_H_
#define STGCPLAYBACK_STGCCONFIGGENERATION_BANDSCONFIGURATIONGENERATION_H_

#include "sTGCConfigGeneration/sTGCsFEBandConfigGen.h"
#include "GeometryModel/QuadrupletConfiguration.h"
#include "CablingModel/sTGCsABConfiguration.h"
#include "CablingModel/sTGCsFEConfiguration.h"
#include "CablingModel/sTGCsFEBandConfig.h"



#include <map>

class BandsConfigurationGeneration {
public:
	BandsConfigurationGeneration();
	virtual ~BandsConfigurationGeneration();

	void SetQuadLookup(std::map<TString, STGC_GEOMETRY::QuadrupletConfiguration> config);
	void SetSABLookup(std::map<TString, sTGCsABConfiguration> config);
	void SetSFELookup(std::map<TString, sTGCsFEConfiguration> config);

	std::map<TString, sTGCsFEBandConfig> GetFEBandLookup();

	sTGCsFEBandConfig& Upsert(const TString& name);

	void ComputeBandConfig(TString sfeb);


private:

	std::map<TString, STGC_GEOMETRY::QuadrupletConfiguration> quadLookup;
	std::map<TString, sTGCsABConfiguration> sABLookup;
	std::map<TString, sTGCsFEConfiguration> sFELookup;
	std::map<TString, sTGCsFEBandConfig> sFEBandConfigLookup;



	sTGCsFEBandConfigGen fe_configGen;



};

#endif /* STGCPLAYBACK_STGCCONFIGGENERATION_BANDSCONFIGURATIONGENERATION_H_ */
