#ifndef ComponentHandle_H
#define ComponentHandle_H
#include "Components/ComponentStore.h"
#include <sTGCTriggerSystemComponents/sTGCsAB.h>
#include <sTGCTriggerSystemComponents/sTGCsFE.h>
#include <sTGCTriggerSystemComponents/sTGCRouter.h>
#include <sTGCTriggerSystemComponents/sTGCPadTrigger.h>
#include <sTGCTriggerSystemComponents/sTGCTriggerProcessor.h>
#include <Utilities/LoggingSvc.h>

#include <TString.h>
#include <type_traits>
#include <memory>

//There seems to be  no convention in the xml description on classname+type + name etc..
//The order of the identifying integers seem to be randomly implemented for each component
//Note : Spending some time i understand that sectors are numbered from 1,16 + a size identifying label which is redundant etc ..

template <class T>
class ComponentHandle{
    public:

        //not all componets are layerwise+modulwise but they are all uniquely identified by a physical sector;
        ComponentHandle();
        ComponentHandle(int side,int sector,int module=-1,int layer=-1);        
        T* val(){ return m_val;}
        T* resolve();
    private:
        int m_side;
        int m_sector;
        int m_layer;
        int m_module;
        ComponentStore* m_componentStore;
        
        T* m_val;
};
template <class T>
ComponentHandle<T>::ComponentHandle():m_side(-1),m_sector(-1),m_module(-1),m_layer(-1){
        m_componentStore=ComponentStore::Instance();
}

template <class T>
ComponentHandle<T>::ComponentHandle(int side,int sector,int module,int layer):ComponentHandle(){
                                    m_side=side;
                                    m_sector=sector;
                                    m_layer=layer;
                                    m_module=module; 
}



template <class T>
T* ComponentHandle<T>::resolve(){
    std::string key="";
    std::string component_class="";
    std::string type="";
    std::string name="";    

    std::string sideStr="";
    std::string sizeStr="";
    std::string sectorStr="";
    std::string moduleStr="";
    std::string layerStr="";

    switch(m_side){
        case 0:
            sideStr="C";
            break;
        case 1:
            sideStr="A";
            break;
        default:
            //Throw exception 
            break;        
    }

    if(m_sector % 2==0){
        sizeStr="S";
    }
    else{
        sizeStr="L";
    }

    //sector strings, I guess are alyways two digits long ?

    if(m_sector<10) sectorStr="0"+std::to_string(m_sector);
    if(m_layer>0) {
        if(m_layer<=4){
            layerStr+="P"+std::to_string(m_layer);
        }
        else{
            layerStr+="C"+std::to_string(m_layer-4);
        }
    }
    if(m_module>0){
        moduleStr=std::to_string(m_module);
    }
    
    if( std::is_same<T, sTGCsAB>::value){
        component_class="sAB";
        //sTG1-QL3P4
        type="sTG1-Q"+sizeStr+moduleStr+layerStr;
        name=type+"-"+sideStr+"_"+sectorStr;
    }
    else if(std::is_same<T, sTGCsFE>::value){
        component_class="sAB";
        //sTG1-QL3P4
        type="sTG1-R1-"+sizeStr+layerStr;
        name=type+"-"+sideStr+sectorStr;    
    }
    else if(std::is_same<T, sTGCRouter>::value){
        component_class="Router";
        type="sTG1-R1";
        name=type+"-"+sizeStr+layerStr+"-"+sideStr+"_"+sectorStr;           
    }
    else if(std::is_same<T, sTGCPadTrigger>::value){
        component_class="PadTrigger";
        type="sTG1-"+sideStr+"-"+sizeStr+"-external";
        name="sTG1-PT-"+sideStr+"-"+sizeStr+"-"+sectorStr;    
    }
    else if(std::is_same<T, sTGCTriggerProcessor>::value){
        component_class="TriggerProcessor";
        type="ascii_capture";
        name="sTG1-TP-"+sideStr+"-"+sizeStr+"-"+sectorStr; 

    } 
    else{
        LOG_FATAL("Trying to get an unknown componment");
    }
    //Build the key;
    IComponent* val=m_componentStore->Retrieve(TString(component_class),TString(type), TString(name));
    
    //TODO : At least throw a FATAL message in case of bad cast so that ppl can debug easier

    return dynamic_cast<T*>(val);    
}


#endif
