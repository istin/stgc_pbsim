#ifndef STGC_TRIGGERPROCESSOR_H
#define STGC_TRIGGERPROCESSOR_H
#include <Components/BaseComponent.h>
#include <Trigger/sTGCTrigger.h>
#include <fstream>
#include <iostream>
class sTGCTriggerProcessor : public BaseComponent{

    public:
        sTGCTriggerProcessor();
        ~sTGCTriggerProcessor();
	    int side()const {return m_side;}
	    int sector() const { return m_sector;}        
        void echo(){ std::cout<<"Echo From sTGCTriggerProcessorBase "<<std::endl;}
        void setSector(int s){ m_side=s;}
        void setSide( int s){ m_sector=s;}
        bool setTriggersFromAth(std::vector<sTGCTrigger>);
        virtual void DoProcess(){}
        std::ofstream* fibres()  { return m_fibres;}
        void SetLastEvent(bool b){m_isLastEvent=b;}
    private:

    protected: 
	    int m_side;
	    int m_sector;
        std::vector<sTGCTrigger> m_AthTrgBuffer;//interface to Athena Trigger EDM
        std::ofstream m_fibres[32];
        bool m_isLastEvent;
  
};


#endif
