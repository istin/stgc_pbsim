/*
 * sTGCPadTrigger.cxx
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#include "sTGCTriggerSystemComponents/sTGCPadTrigger.h"

sTGCPadTrigger::sTGCPadTrigger()
{
}

sTGCPadTrigger::~sTGCPadTrigger() {
}

double sTGCPadTrigger::CalcDelay() {
	return 0;
}

void sTGCPadTrigger::DoProcess() {
}

void sTGCPadTrigger::Reset() {
	m_decision = sTGCPadTriggerMsg();
}

void sTGCPadTrigger::ConnectSource(IComponent* source_component,
		const TString& source_port, const TString& target_port) {
	//July-8 2019 :
	//


	return;
}


Link<sTGCPadTriggerMsg>& sTGCPadTrigger::GetLinkToSFE(TString linkname)
{
	std::map<TString, Link<sTGCPadTriggerMsg> >::iterator itr;
	itr = m_outgoing_links.find(linkname);
	if (itr == m_outgoing_links.end())
	{
		Link<sTGCPadTriggerMsg>& result = m_outgoing_links[linkname];
		return result;
	}
	return itr->second;
}
