/*
 * sTGCBCSignal.h
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCBCSIGNAL_H_
#define STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCBCSIGNAL_H_

#include "Components/BaseComponent.h"

class sTGCBCSignal : public BaseComponent {
public:
	sTGCBCSignal();
	virtual ~sTGCBCSignal();

	// TODO @enrique implement
	virtual double CalcDelay();
	virtual void DoProcess();
	virtual void Reset();
	virtual void ConnectSource(IComponent* source_component, const TString& source_port, const TString& target_port);
};

#endif /* STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCBCSIGNAL_H_ */
