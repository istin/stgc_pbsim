/*
 * sTGCRouter.cpp
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#include "sTGCTriggerSystemComponents/sTGCRouter.h"
#include <Utilities/LoggingSvc.h>


sTGCRouter::sTGCRouter()
:m_clock(0),
m_latency(0),
rxChToSerializerBank(12,0),
 rxChToSerializerChannel(12,0),
 rxChDelay(12,0)
 
{

}

sTGCRouter::~sTGCRouter() {
}

double sTGCRouter::CalcDelay() {
	return m_latency;
}

void sTGCRouter::DoProcess() {
}

void sTGCRouter::Reset() {
}

void sTGCRouter::ConnectSource(IComponent* source_component, const TString& source_port, const TString& target_port,
		double propagationDelay, double length, double bandwidth)
{
	LOG_TRACE("sTGCsFE::ConnectSource( " << source_component->GetClass() << " - "<<
	source_component->GetType() << " - " <<  source_component->GetName() << ", "<<
	source_port << ", " << target_port << ", " << propagationDelay << ", "<<
	length << ", " << bandwidth << " )");
	return;
}

void sTGCRouter::SetSerializerForInput(int i_rxch, int bank, int channel_in_bank)
{
	if (i_rxch > 11)
		return;
	rxChToSerializerBank[i_rxch] = bank;
	rxChToSerializerChannel[i_rxch] = channel_in_bank;
}
void sTGCRouter::GetSerializerForInput(int i_rxch, int& bank, int& channel_in_bank)
{
	if (i_rxch > 11)
		return;
	bank = rxChToSerializerBank[i_rxch];
	channel_in_bank = rxChToSerializerChannel[i_rxch];
	return;
}

void sTGCRouter::SetDelayForInput(int i_rxch, double delay)
{
	if (i_rxch > 11)
		return;
	rxChDelay[i_rxch] = delay;

}

double sTGCRouter::GetDelayForInput(int i_rxch)
{
	if (i_rxch > 11)
		return 0;
	return rxChDelay[i_rxch];

}
