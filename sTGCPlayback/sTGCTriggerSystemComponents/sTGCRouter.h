/*
 * sTGCRouter.h
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCROUTER_H_
#define STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCROUTER_H_


#include "Components/BaseComponent.h"

class sTGCRouter : public BaseComponent {
public:
	sTGCRouter();
	virtual ~sTGCRouter();

	virtual double CalcDelay();
	virtual void DoProcess();
	virtual void Reset();
	virtual void ConnectSource(IComponent* source_component,
			                   const TString& source_port,
							   const TString& target_port,
			                   double propagationDelay,
							   double length,
							   double bandwidth);

	void SetLatency(double latency){m_latency = latency;}
	double GetLatency(){return m_latency;}

	void SetSerializerForInput(int rxch, int bank, int channel_in_bank);
	void GetSerializerForInput(int rxch, int& bank, int& channel_in_bank);

	void  SetDelayForInput(int i_rxch, double delay);
	double GetDelayForInput(int i_rxch);

	double GetClockFrequency(){return m_clock;}
	void SetClockFrequency(double frequency) {m_clock = frequency;}

protected:
	double m_clock;
	double m_latency;
	std::vector<int> rxChToSerializerBank;
	std::vector<int> rxChToSerializerChannel;
	std::vector<double> rxChDelay;
};

#endif /* STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCROUTER_H_ */
