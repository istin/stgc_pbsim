/*
 * sTDSDataModel.h
 *
 *  Created on: Apr 10, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSTDS_DATAMODEL_H_
#define STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSTDS_DATAMODEL_H_


struct sTGCsTDS_stripData
{
	unsigned strip_charge:6;
	unsigned bcid_flag:1;
	unsigned bcid:12;
};

struct sTGCsTDS_bcid
{
	unsigned value:12;
};

struct sTGCsTDS_stripCharge
{
	unsigned value:6;
};

#include <vector>

struct sTGCsTDSMsg
{
	int band_id;
	int phi_id;
	int hi_lo;
	int bcid;
	int header;
	std::vector<int> strip_data;
	int msg_size_in_bits;
	int payload_size;

};




#endif /* STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSTDS_DATAMODEL_H_ */
