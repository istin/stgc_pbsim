/*
 * sTGCBCSignalFactory.h
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCBCSIGNALFACTORY_H_
#define STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCBCSIGNALFACTORY_H_

#include "Components/IComponentFactory.h"

class sTGCBCSignalFactory : public IComponentFactory {
public:
	sTGCBCSignalFactory();
	virtual ~sTGCBCSignalFactory();

	IComponent* Create(const TString& componentClass, const TString& componentType, const TString& componentName);


};

#endif /* STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCBCSIGNALFACTORY_H_ */
