/*
 * PadTriggerMsg.h
 *
 *  Created on: Apr 8, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCPADTRIGGERMSG_H_
#define STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCPADTRIGGERMSG_H_
#include <Utilities/LoggingSvc.h>

#define PAD_TRIGGER_UNINITIALIZED -999
struct sTGCPadTriggerMsg
{
	int bandId0;
	int bandId1;
	int bandId2;
	int bandId3;

	int phiId0;
	int phiId1;
	int phiId2;
	int phiId3;

	int bcid;

	sTGCPadTriggerMsg():
	bandId0(PAD_TRIGGER_UNINITIALIZED),
	bandId1(PAD_TRIGGER_UNINITIALIZED),
	bandId2(PAD_TRIGGER_UNINITIALIZED),
	bandId3(PAD_TRIGGER_UNINITIALIZED),
	phiId0(PAD_TRIGGER_UNINITIALIZED),
	phiId1(PAD_TRIGGER_UNINITIALIZED),
	phiId2(PAD_TRIGGER_UNINITIALIZED),
	phiId3(PAD_TRIGGER_UNINITIALIZED),
	bcid(PAD_TRIGGER_UNINITIALIZED)
	{}


	void Dump(){
		LOG_DEBUG(" BCID="<<bcid);
		LOG_DEBUG("bandId0="<<bandId0<<" "<<"bandId1="<<bandId1<<" "<<"bandId2="<<bandId2<<" "<<"bandId3="<<bandId3);	
		LOG_DEBUG("phiId0="<<phiId0<<" "<<"phiId1="<<phiId1<<" "<<"phiId2="<<phiId2<<" "<<"phiId3="<<phiId3);	
	}
};


#endif /* STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCPADTRIGGERMSG_H_ */
