/*
 * sTGCsFE.h
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSFE_H_
#define STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSFE_H_

#include "Components/BaseComponent.h"

#include "Components/Cable.h"

#include "CablingModel/sTGCsFEConfiguration.h"
#include "sTGCTriggerSystemComponents/sTGCsTDS.h"
#include "sTGCTriggerSystemComponents/sTGCsAB.h"
#include "sTGCTriggerSystemComponents/sTGCVMMChannel.h"
#include "sTGCTriggerSystemComponents/sTGCPadTrigger.h"
#include "sTGCTriggerSystemComponents/sTGCPadTriggerMsg.h"
#include "sTGCTriggerSystemComponents/sTGCsFE_PadTriggerInputLinkHandler.h"


#include <vector>



class sTGCsFE : public BaseComponent {
public:
	sTGCsFE();
	virtual ~sTGCsFE();

	virtual void ConnectSource(IComponent* source_component, const TString& source_port, const TString& target_port,
			double propagationDelay, double length, double bandwidth);

	void SetConfiguration(sTGCsFEConfiguration config);
	sTGCsFEConfiguration GetConfiguration(){return m_config;}

	int GetNActiveVMMChannels(){return m_config.nActiveVMMChannels;}
	int GetNActiveVMM(){return m_config.nActiveVMM;}
	int GetNActiveTDS(){return m_config.nActiveTDS;}
	int GetNActiveTDSChannels(){return m_config.nACtiveTDSChannels;}
	int GetLayer(){return m_config.layer;}
	DIR_WRT_DET GetDirection(){return m_config.direction;}
	sTGCsTDS& GetTDS(int tds_number);
	int ChannelConnectedToTDS(int ch){return m_config.ChannelConnectedToTDS(ch);}

protected:
	sTGCsFEConfiguration m_config;
	std::vector<sTGCsTDS> m_tds;
	std::vector<sTGCVMMChannel> m_vmm_channel;
	Link<sTGCPadTriggerMsg> m_padTriggerInputLink;
//	Link<sTGCPadTriggerMsg> m_padTriggerInputLink_toTDS1;
//	Link<sTGCPadTriggerMsg> m_padTriggerInputLink_toTDS2;
//	Link<sTGCPadTriggerMsg> m_padTriggerInputLink_toTDS3;
//	Link<sTGCPadTriggerMsg> m_padTriggerInputLink_toTDS4;

	sTGCsFE_PadTriggerInputLinkHandler m_padTriggerHandler;
	Cable<sTGCPadTriggerMsg> m_padTriggerCable;


	void ConnectVMMstoTDS();

	void ConnectPadTrigger(sTGCPadTrigger* padTrigger, const TString& source_port, const TString& target_port,
			double propagationDelay, double length, double bandwidth);

	void ConnectSAB(sTGCsAB* sab, const TString& source_port, const TString& target_port,
			double propagationDelay, double length, double bandwidth);

};

#endif /* STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSFE_H_ */
