/*
 * sTGCsFEFactory.h
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSFEFACTORY_H_
#define STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSFEFACTORY_H_

#include "Components/IComponentFactory.h"

#include "CablingModel/sTGCsFEConfiguration.h"
#include <map>

class sTGCsFEFactory : public IComponentFactory {
public:
	sTGCsFEFactory();
	virtual ~sTGCsFEFactory();

	IComponent* Create(const TString& componentClass, const TString& componentType, const TString& componentName);

	void SetLookup(std::map<TString, sTGCsFEConfiguration> lookup){m_lookup = lookup; }

protected:

	std::map<TString, sTGCsFEConfiguration> m_lookup;
};

#endif /* STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSFEFACTORY_H_ */
