/*
 * sTGCTriggerProcessorFactory.cxx
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#include "sTGCTriggerSystemComponents/sTGCTriggerProcessorFactory.h"
#include "sTGCTriggerSystemComponents/sTGCTriggerProcessor.h"
#include "sTGCTriggerSystemComponents/sTGCTriggerProcessorASCIIWriter.h"
#include <Utilities/LoggingSvc.h>
#include <regex>
#include <string>

sTGCTriggerProcessorFactory::sTGCTriggerProcessorFactory()
{
}

sTGCTriggerProcessorFactory::~sTGCTriggerProcessorFactory() {
}

IComponent* sTGCTriggerProcessorFactory::Create(const TString& componentClass,
		const TString& componentType, const TString& componentName) {
	if (componentClass != "TriggerProcessor")
		return NULL;

	std::map<TString, TriggerProcessorConfiguration>::iterator itr = m_lookup.find(componentType);
	if (itr == m_lookup.end())
		return NULL;

	sTGCTriggerProcessor * trgp=nullptr;

	
	TriggerProcessorConfiguration config = itr->second;
	if(config.type=="ascii_capture" ){
		trgp= new sTGCTriggerProcessorASCIIWriter();
			for(unsigned int i=0;i<32;i++){
			trgp->fibres()[i].open(std::string(componentName.Data())+"fibre"+std::to_string(i)+".coe", std::ios::out);
		}
	}
	else if(config.type=="algorithm"){
			trgp= new sTGCTriggerProcessor();
	}

	else {
		LOG_FATAL("Unknown TriggerProcessor configuration");
		exit(7);
	}
	trgp->SetClass("TriggerProcessor");
	trgp->SetType(componentType);
	trgp->SetName(componentName);
	 
	std::string sectorExtr = std::regex_replace(
        std::string(componentName),
        std::regex("sTG1-TP-['A'|'C']-['S'|'L']-"),
        std::string("$1")
    );
	assert(sectorExtr.size()==2);
	int sectorNoExtr;
	if(sectorExtr.at(0)=='0'){
	    sectorNoExtr=std::stoi(std::string(1,sectorExtr.at(1)) );
		trgp->setSector(sectorNoExtr);
	}
	else{
		sectorNoExtr=std::stoi(sectorExtr);
		trgp->setSector(sectorNoExtr);
	}
    std::regex rx("sTG1-TP-(\\w+)-['S'|'L'].*");
    std::smatch mtch;
	const std::string nam=std::string(componentName);
    std::regex_search(nam.begin(), nam.end(), mtch, rx);
    assert(mtch.size()>0);
	switch( mtch.str(1).at(0) ){
		case('A'):
			trgp->setSide(1);
			break;
		case('C'):
			trgp->setSide(0);
			break;
		default:
			//throe exception
			break;		

	}
	return trgp;
}
