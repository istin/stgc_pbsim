/*
 * sTGCsFE_PadTriggerInputLinkHandler.cxx
 *
 *  Created on: Apr 9, 2019
 *      Author: ekajomov
 */

#include "sTGCTriggerSystemComponents/sTGCsFE_PadTriggerInputLinkHandler.h"
#include <iostream>
#include <Utilities/LoggingSvc.h>

sTGCsFE_PadTriggerInputLinkHandler::sTGCsFE_PadTriggerInputLinkHandler(Link<sTGCPadTriggerMsg>& link,std::vector<sTGCsTDS>* stdsList):
m_link(link),m_stdsList(stdsList)
{
	m_link.SetNotifiable(this);

}

sTGCsFE_PadTriggerInputLinkHandler::~sTGCsFE_PadTriggerInputLinkHandler()
{
}

void sTGCsFE_PadTriggerInputLinkHandler::Handle()
{

	//TODO @Enrique
	LOG_TRACE("This should be doing something! ");
	for( sTGCsTDS&  stds: *m_stdsList ){
		stds.ProcessPadTriggerMsg(m_link.GetValue());
	}
}


void sTGCsFE_PadTriggerInputLinkHandler::Notify(){
	this->Handle();
}