/*
 * sTGCsTDS_ringBuffer.h
 *
 *  Created on: Apr 10, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */

#ifndef STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSTDS_RINGBUFFER_H_
#define STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSTDS_RINGBUFFER_H_

#define RING_BUFFER_SIZE 4

#include "Components/INotifiable.h"
#include "sTGCTriggerSystemComponents/sTGCsTDS_dataModel.h"

class sTGCsTDS_ringBuffer
{

public:

	sTGCsTDS_ringBuffer();

	virtual ~sTGCsTDS_ringBuffer();

	/********************************************************************
	 * This method returns the strip charge for the latest BCID that
	 * matches the requested bcid
	 * a) if the bcid flag is on, up to a 3 BCID window is acceptable
	 * b) if the bcid flag is off, only exact match is used
	 ********************************************************************/
	sTGCsTDS_stripCharge GetLatestBCIDMatchingStripData(sTGCsTDS_bcid bcid);


	/********************************************************************
	 * Inserts in the next index for the ring buffer
	 ********************************************************************/
	void Insert(sTGCsTDS_stripData in_data);

	void Notify();

protected:
	sTGCsTDS_stripData data[RING_BUFFER_SIZE];
	int index;
};



#endif /* STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSTDS_RINGBUFFER_H_ */
