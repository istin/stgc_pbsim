/*
 * sTGCPadTrigger.h
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCPADTRIGGER_H_
#define STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCPADTRIGGER_H_

#include "Utilities/LoggingSvc.h"
#include <map>

#include "TString.h"

#include "Components/BaseComponent.h"
#include "Components/Link.h"
#include "Components/IScheduler.h"
#include "Components/INotifiable.h"
#include "Components/IResetable.h"
#include "Components/ITransmitter.h"
#include "Components/TransmitCommand.h"

#include "sTGCTriggerSystemComponents/sTGCPadTriggerMsg.h"

class sTGCPadTrigger : public INotifiable, public IResetable, public BaseComponent, public ITransmitter  {
public:
	sTGCPadTrigger();
	virtual ~sTGCPadTrigger();

	virtual double CalcDelay();
	virtual void DoProcess();
	virtual void Reset();
	
	virtual void ConnectSource(IComponent* source_component, const TString& source_port, const TString& target_port);

	const TString& GetCoincidenceTableName() const {
		return m_coincidenceTableName;
	}

	void SetCoincidenceTableName(const TString& coincidenceTableName) {
		m_coincidenceTableName = coincidenceTableName;
	}

	void Transmit(){
		 LOG_DEBUG( this->GetName() + "Transmit Called! ");

		for( auto  &kv : m_outgoing_links){
			kv.second.SetValue(m_decision);
		}

		 return;
	}


	// S.I. and E.K. this is a hack to get this working. In actuallity we should have to implement a different PadTrigger type
	// to just set the athena decision.
	void SetPadTriggerDecision(sTGCPadTriggerMsg decision){
		 m_decision=decision;
		this->Notify();	 
	}
	void Notify(){
		LOG_TRACE(this->GetName());
		ICommand*cmd= new TransmitCommand(this, false);
		m_scheduler->Schedule(cmd,1000,PBSim_Units::TIME_UNIT::ns);
	 	return;
	}

	const TString& GetLatency() const {
		return m_latency;
	}

	void SetLatency(const TString& latency) {
		m_latency = latency;
	}
	//linkname should be the identifier of the TDS and the FEB
	Link<sTGCPadTriggerMsg>& GetLinkToSFE(TString linkname);


protected:
	TString m_latency;
	TString m_coincidenceTableName;

	std::map<TString, Link<sTGCPadTriggerMsg> > m_outgoing_links;

	// Event info
	sTGCPadTriggerMsg m_decision;
};

#endif /* STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCPADTRIGGER_H_ */
