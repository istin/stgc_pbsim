/*
 * sTGCPadTriggerFactory.h
 *
 *  Created on: Mar 21, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */

#ifndef STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCPADTRIGGERFACTORY_H_
#define STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCPADTRIGGERFACTORY_H_

#include "Components/IComponentFactory.h"
#include "CablingModel/sTGCPadTriggerConfiguration.h"
#include <map>

class sTGCPadTriggerFactory : public IComponentFactory {
public:

	sTGCPadTriggerFactory();
	virtual ~sTGCPadTriggerFactory();
	void SetLookup(std::map<TString, sTGCPadTriggerConfiguration> lookup);
	std::map<TString, sTGCPadTriggerConfiguration> GetLookup() {return m_lookup;}


	virtual IComponent* Create(const TString& componentClass, const TString& componentType, const TString& componentName);

protected:
	std::map<TString, sTGCPadTriggerConfiguration> m_lookup;

};

#endif /* STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCPADTRIGGERFACTORY_H_ */
