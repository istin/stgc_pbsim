/*
 * sTGCVMMChannel.cxx
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#include "sTGCTriggerSystemComponents/sTGCVMMChannel.h"

sTGCVMMChannel::sTGCVMMChannel():
m_charge(0), m_maxCharge(0), m_latency(0), m_deadTime(0), m_busy(false),m_scheduler(0)
{
}

sTGCVMMChannel::~sTGCVMMChannel() {
}

void sTGCVMMChannel::Reset(){
	m_busy = false;
	m_charge = 0;
}

