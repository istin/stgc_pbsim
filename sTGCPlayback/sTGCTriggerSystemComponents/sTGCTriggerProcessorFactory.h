/*
 * sTGCTriggerProcessorFactory.h
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */
//S.I can]t we find longer names ????
#ifndef STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCTRIGGERPROCESSORFACTORY_H_
#define STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCTRIGGERPROCESSORFACTORY_H_


#include "Components/IComponentFactory.h"
#include "CablingModel/TriggerProcessorConfiguration.h"
#include <map>

class sTGCTriggerProcessorFactory : public IComponentFactory {
public:
	sTGCTriggerProcessorFactory();
	virtual ~sTGCTriggerProcessorFactory();

	IComponent* Create(const TString& componentClass, const TString& componentType, const TString& componentName);
	void SetLookup(std::map<TString, TriggerProcessorConfiguration> lookup) { m_lookup = lookup;}
	std::map<TString, TriggerProcessorConfiguration> GetLookup() { return m_lookup;}

protected:

	std::map<TString, TriggerProcessorConfiguration> m_lookup;
};

#endif /* STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_sTGCTriggerProcessorFactory_H_ */
