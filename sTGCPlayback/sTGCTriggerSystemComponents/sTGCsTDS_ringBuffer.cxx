/*
 * sTGCsTDS_ringBuffer.cxx
 *
 *  Created on: Apr 10, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */

#include "sTGCTriggerSystemComponents/sTGCsTDS_ringBuffer.h"


sTGCsTDS_ringBuffer::sTGCsTDS_ringBuffer():
index(0)
{
}

sTGCsTDS_ringBuffer::~sTGCsTDS_ringBuffer()
{
}

/********************************************************************
 * This method returns the strip charge for the latest BCID that
 * matches the requested bcid
 * a) if the bcid flag is on, up to a 3 BCID window is acceptable
 * b) if the bcid flag is off, only exact match is used
 ********************************************************************/
sTGCsTDS_stripCharge sTGCsTDS_ringBuffer::GetLatestBCIDMatchingStripData(sTGCsTDS_bcid bcid)
{

	sTGCsTDS_bcid matching_bcid;  matching_bcid.value = 0;
	sTGCsTDS_bcid bcid_1; bcid_1.value = bcid.value + 1;
	sTGCsTDS_bcid bcid_2; bcid_2.value = bcid.value + 2;

	sTGCsTDS_stripCharge charge;
	charge.value = 0;
	int matching_index = -1;
	for (int i = 0; i < RING_BUFFER_SIZE; i++){
		if (data[i].bcid == bcid.value) {
			matching_bcid.value = data[i].bcid;
			matching_index = i;
			continue;
		}
		else if (data[i].bcid == bcid_1.value && data[i].bcid_flag == 1) {
			matching_bcid.value = data[i].bcid;
			matching_index = i;
			continue;
		}
		else if (data[i].bcid == bcid_2.value && data[i].bcid_flag == 1) {
			matching_bcid.value = data[i].bcid;
			matching_index = i;
			continue;
		}
	}

	if ( matching_index == -1 )	{
		return charge;
	}

	charge.value = data[matching_index].strip_charge;
	return charge;
}

/********************************************************************
 * Inserts in the next index for the ring buffer
 ********************************************************************/
void sTGCsTDS_ringBuffer::Insert(sTGCsTDS_stripData in_data)
{
	index %= RING_BUFFER_SIZE;
	data[index++] = in_data;
}
