/*
 * sTGCsTDS.h
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSTDS_H_
#define STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSTDS_H_

#include <vector>

#include "Components/DelayedTransmitter.h"
#include "Components/Link.h"
#include "Components/Cable.h"

#include "CablingModel/sTDSConfig.h"

#include "sTGCTriggerSystemComponents/sTGCsTDS_ringBuffer.h"
#include "sTGCTriggerSystemComponents/sTGCPadTriggerMsg.h"
#include "sTGCTriggerSystemComponents/sTGCVMM_dataModel.h"
#include "sTGCTriggerSystemComponents/sTGCsTDS_dataModel.h"

#include "Components/LinkTransmitter.h"

class sTGCsTDS {
public:

	sTGCsTDS();
	virtual ~sTGCsTDS();
	void ProcessPadTriggerMsg(sTGCPadTriggerMsg msg);

	void SetInputLink_6b(int i_ch, Link<sTGCVMM_6bCharge>& link) { m_6bLinks[i_ch] = &link; }

protected:

	Link<sTGCsTDSMsg> m_toRouter_Link;
	std::vector< Link<sTGCVMM_6bCharge>*> m_6bLinks;
	//std::vector< InputLinkHandler<sTGCVMM_6bCharge> > strip_input_handlers;
	std::vector< sTGCsTDS_ringBuffer> ring_buffers;
	sTGCsTDS_bcid m_local_bcid_counter;
	sTDSConfig m_config;

	// ALL THIS PARAMTERS BELONG TO THE CONFIGURATION
	//int m_bc_matchingWindow_in_6p25ns; // this is in multiples of 6.25 ns
	//int m_resetTimer_in_25ns;
	//std::vector<int> m_firstStripInBand_lut;
	//sTGCsTDS_bcid local_bcid_offset;
	//int local_to_bcid_phase;
	//sTGCsTDS_bcid rollover_value
};

//// PSEUDO CODE
/*
ProcessPadTriggerMsg
 -  Get from the LUT the first channel to be read
 -  From the 17 strips to be read
 -		Ask the ring buffers for a match
 -  	Select set of 14 strips (2+ bottom, 2+ top)
 	 	 	'first top' fired (index 1 in the 17 strip range)-> 0:13
 - 		    else -> 3:16
-       Arrange message from strips, phi id, bcid etc
-       Schedule sending 150ns later
*/




#endif /* STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSTDS_H_ */
