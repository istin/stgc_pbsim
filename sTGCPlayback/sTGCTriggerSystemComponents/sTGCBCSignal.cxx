/*
 * sTGCBCSignal.cxx
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#include "sTGCTriggerSystemComponents/sTGCBCSignal.h"

sTGCBCSignal::sTGCBCSignal() {
}

sTGCBCSignal::~sTGCBCSignal() {
}

double sTGCBCSignal::CalcDelay() {
	return 0;
}

void sTGCBCSignal::DoProcess() {
}

void sTGCBCSignal::Reset() {
}

void sTGCBCSignal::ConnectSource(IComponent* source_component,
		const TString& source_port, const TString& target_port) {
	return;
}
