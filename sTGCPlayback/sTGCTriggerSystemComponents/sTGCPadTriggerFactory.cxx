/*
 * sTGCPadTriggerFactory.cxx
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */
#include "sTGCTriggerSystemComponents/sTGCPadTrigger.h"
#include "sTGCTriggerSystemComponents/sTGCPadTriggerFactory.h"

sTGCPadTriggerFactory::sTGCPadTriggerFactory() {
}

sTGCPadTriggerFactory::~sTGCPadTriggerFactory() {
}

void sTGCPadTriggerFactory::SetLookup(
		std::map<TString, sTGCPadTriggerConfiguration> lookup) {
	m_lookup = lookup;
}

IComponent* sTGCPadTriggerFactory::Create(const TString& componentClass,
		const TString& componentType, const TString& componentName) {
	if (componentClass != "PadTrigger")
		return NULL;

	std::map<TString, sTGCPadTriggerConfiguration>::iterator itr = m_lookup.find(componentType);
	if (itr == m_lookup.end())
		return NULL;

	sTGCPadTrigger* pt = new sTGCPadTrigger();
	pt->SetClass("PadTrigger");
	sTGCPadTriggerConfiguration config = itr->second;
	pt->SetType(componentType);
	pt->SetName(componentName);
	pt->SetLatency(config.latency);
	pt->SetCoincidenceTableName(config.coincidenceTable);
	return pt;

}
