/*
 * sTGCsFEFactory.cxx
 *
 *  Created on: Mar 21, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */

#include "sTGCTriggerSystemComponents/sTGCsFEFactory.h"
#include "sTGCTriggerSystemComponents/sTGCsFE.h"
#include <Utilities/LoggingSvc.h>

sTGCsFEFactory::sTGCsFEFactory() {
}

sTGCsFEFactory::~sTGCsFEFactory() {
}

IComponent* sTGCsFEFactory::Create(const TString& componentClass,
		const TString& componentType, const TString& componentName) {

	LOG_TRACE("sFE::Create " << componentClass << " " << componentType << " " << componentName);

	if (componentClass != "sFE")
		return NULL;
	std::map<TString, sTGCsFEConfiguration>::iterator itr = m_lookup.find(componentType);
	if (itr == m_lookup.end()){
		LOG_FATAL("cannot find component type configuration in lookup: " <<  componentType);
		return NULL;
	}
	sTGCsFE* fe = new sTGCsFE;
	fe->SetClass("sFE");
	fe->SetType(componentType);
	fe->SetName(componentName);
	fe->SetConfiguration(itr->second);
	return fe;
}
