/*
 * sTGCsAB.cxx
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#include "sTGCTriggerSystemComponents/sTGCsAB.h"

sTGCsAB::sTGCsAB()
:m_vmmChannel(512,0)
{
}

sTGCsAB::~sTGCsAB() {
}


void sTGCsAB::ConnectSource(IComponent* source_component,
		const TString& source_port, const TString& target_port) {
}

void sTGCsAB::GetDisconnectedBlock(unsigned int i_block, int& first_channel, int& n_channels)
{
	if( i_block >= m_config.firstChannelDisconnected.size() ){
		return;
	}
	n_channels = m_config.nChannelsDisconnected[i_block];
	first_channel = m_config.firstChannelDisconnected[i_block];
}

INotifiable* sTGCsAB::GetNotifiableForStrip(int stripNumber)
{
	int i_ch = m_config.GetChannelForStrip(stripNumber);
	return m_vmmChannel.at(i_ch);
}


void sTGCsAB::SetVMMChannel(int i_ch, sTGCVMMChannel* channel)
{
	m_vmmChannel.at(i_ch) = channel;
}
