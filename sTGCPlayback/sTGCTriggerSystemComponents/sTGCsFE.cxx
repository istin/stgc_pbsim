/*
 * sTGCsFE.cxx
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#include "sTGCTriggerSystemComponents/sTGCsFE.h"
#include "sTGCTriggerSystemComponents/sTGCPadTrigger.h"
#include "sTGCTriggerSystemComponents/sTGCPadTriggerMsg.h"
#include "sTGCTriggerSystemComponents/sTGCsAB.h"
#include "sTGCTriggerSystemComponents/sTGCsFE_PadTriggerInputLinkHandler.h"
#include "Components/Link.h"
#include <Utilities/LoggingSvc.h>


#define N_TDS 4
#define N_VMM_CH 512
#define N_CH_IN_TDS 128
#define VMM_OUT_PORT "Tx_ADC_6b"

sTGCsFE::sTGCsFE()
:m_tds(N_TDS), m_vmm_channel(N_VMM_CH),
m_padTriggerHandler(m_padTriggerInputLink,&m_tds)
{
}

sTGCsFE::~sTGCsFE(){
}

void sTGCsFE::ConnectSource(IComponent* source_component, const TString& source_port, const TString& target_port,
		double propagationDelay, double length, double bandwidth) {


	LOG_TRACE("sTGCsFE::ConnectSource( " << source_component->GetClass() << " - "<<
		source_component->GetType() << " - " <<  source_component->GetName() << ", "<<
		source_port << ", " << target_port << ", " << propagationDelay << ", "<<
		length << ", " << bandwidth << " )" );

	if (source_component == NULL){
		LOG_FATAL("Cannot connect a NULL component");
		throw std::runtime_error("Trying to connect a NULL component");
	}
	else if (source_component->GetClass() == "PadTrigger")
	{
		sTGCPadTrigger* padTrigger = dynamic_cast<sTGCPadTrigger*>(source_component);
		ConnectPadTrigger(padTrigger, source_port, target_port,
		propagationDelay, length, bandwidth);
		return;
	}
	else if (source_component->GetClass() == "sAB")
	{

		LOG_TRACE("Requestting sAB component connection to sFE");
		sTGCsAB* sab = dynamic_cast<sTGCsAB*>(source_component);
		ConnectSAB(sab, source_port, target_port, propagationDelay, length, bandwidth);
		return;
	}
	else
	{
		std::runtime_error("Trying to connect an unsupported type");
	}

}

void sTGCsFE::ConnectPadTrigger(sTGCPadTrigger* padTrigger, const TString& source_port, const TString& target_port,
		double propagationDelay, double length, double bandwidth) {

		Link<sTGCPadTriggerMsg>& link_from_padTrigger = padTrigger->GetLinkToSFE(source_port);
		m_padTriggerCable.SetInputLink(link_from_padTrigger);
		m_padTriggerCable.SetOutputLink(m_padTriggerInputLink);
		m_padTriggerCable.SetBandwidth(bandwidth);
		m_padTriggerCable.SetLength(length);
		m_padTriggerCable.SetPropagationDelay(propagationDelay);
		m_padTriggerCable.SetScheduler(m_scheduler);
		m_padTriggerCable.SetMessageSize(120);
		LOG_DEBUG(" Bandwidth="<<bandwidth);
}

void sTGCsFE::ConnectSAB(sTGCsAB* sab, const TString& source_port, const TString& target_port,
		double propagationDelay, double length, double bandwidth) {


	for (int i_ch = 0; i_ch < N_VMM_CH; i_ch++)
	{
		sab->SetVMMChannel(i_ch, &m_vmm_channel.at(i_ch));
	}
}

void sTGCsFE::SetConfiguration(sTGCsFEConfiguration config)
{
	m_config = config;
	ConnectVMMstoTDS();
}

void sTGCsFE::ConnectVMMstoTDS()
{
	for (int i_ch = 0; i_ch < N_VMM_CH; i_ch++)
	{
		if (!ChannelConnectedToTDS(i_ch + 1))
			continue;
		int i_tds = i_ch / N_CH_IN_TDS;
		int i_ch_in_tds = i_ch % N_CH_IN_TDS;
		Link<sTGCVMM_6bCharge>& link = m_vmm_channel[i_ch].GetOutputLink_6b();
		m_tds[i_tds].SetInputLink_6b(i_ch_in_tds, link);
	}
}
