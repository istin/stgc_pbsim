/*
 * sTGCsTDSVMM6InputLinkHandler.h
 *
 *  Created on: Apr 11, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSTDSVMM6INPUTLINKHANDLER_H_
#define STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSTDSVMM6INPUTLINKHANDLER_H_

#include "Components/INotifiable.h"
#include "Components/Link.h"
#include "sTGCTriggerSystemComponents/sTGCVMM_dataModel.h"
#include "sTGCTriggerSystemComponents/sTGCsTDS_ringBuffer.h"

class sTGCsTDS_VMM6InputLinkHandler : public INotifiable {
public:
	sTGCsTDS_VMM6InputLinkHandler();
	virtual ~sTGCsTDS_VMM6InputLinkHandler();

	virtual void SetLink(Link<sTGCVMM_6bCharge>* link) {}
	virtual void SetRingBuffer(sTGCsTDS_ringBuffer* buffer){}
	virtual void SetClockPhase(int phase){}
	virtual void SetMatchingWindow(int ){}
	virtual void SetBC(int bc){}

	virtual void Notify(){
		//
	}

protected:

	Link<sTGCVMM_6bCharge>* m_link;
	sTGCsTDS_ringBuffer* m_buffer;
	int m_clockPhase;
	int m_bc;
	//int ;
};

#endif /* STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSTDSVMM6INPUTLINKHANDLER_H_ */
