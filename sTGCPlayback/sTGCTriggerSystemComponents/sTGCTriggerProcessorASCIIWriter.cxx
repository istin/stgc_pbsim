#include <sTGCTriggerSystemComponents/sTGCTriggerProcessorASCIIWriter.h>
#include <Utilities/LoggingSvc.h>

sTGCTriggerProcessorASCIIWriter::sTGCTriggerProcessorASCIIWriter(){
	
	m_isLastEvent=false;


}

sTGCTriggerProcessorASCIIWriter::~sTGCTriggerProcessorASCIIWriter(){

}

void sTGCTriggerProcessorASCIIWriter::Reset(){
	    //PadTrigger_info m_pad_trigger_info;//list of selected triggers in the current BC
    //sTDS_info m_stds_info[32];
	m_pad_trigger_info.band_id[0]=0;
	m_pad_trigger_info.band_id[1]=0;
	m_pad_trigger_info.band_id[2]=0;
	m_pad_trigger_info.band_id[3]=0;
	m_pad_trigger_info.phi_id[0]=0;
	m_pad_trigger_info.phi_id[1]=0;
	m_pad_trigger_info.phi_id[2]=0;
	m_pad_trigger_info.phi_id[3]=0;
	m_pad_trigger_info.bcid=0;
	m_pad_trigger_info.is_null=true;

	for(unsigned int i=0;i<32;i++){
		unsigned int band_id;
		m_stds_info[i].hi_low=0;
		for(unsigned int j=0;j<STRIP_NUMBER;j++){
			m_stds_info[i].strip_data[j]=0;
		}
	
		m_stds_info[i].phi_id=0;
		m_stds_info[i].bcid=0;
		m_stds_info[i].is_null=true;
		m_stds_info[i].router_board=0;
		m_stds_info[i].router_fibre=0;
	}

	return;
}



void sTGCTriggerProcessorASCIIWriter::DoProcess(){
    //Fill 4 triggers at max
	Reset();
    for(unsigned int itrg=0;itrg< m_AthTrgBuffer.size();itrg++){
        auto this_trig=m_AthTrgBuffer.at(itrg);
		if(this_trig.sideID()!=this->side() || this_trig.sectorID()!=this->sector()) continue;
		if(itrg==4) break;
        if(this_trig.isinTransition()){
            LOG_DEBUG("Single Wedge triggers are not allowed. Skip");
            continue;
        }
        int bandId=this_trig.wedgeTriggers().at(0)->selectedBands().at(0);
        int phiId=this_trig.phiId();
        int bcId=this_trig.BCID();
        m_pad_trigger_info.band_id[itrg]=bandId;

        //loop over all selected INNER layers
   
        //for(const auto& innerlayer : this_trig.wedgeTriggers().at(0)->selectedLayers() ){
        for(unsigned int il=0;il<this_trig.wedgeTriggers().at(0)->selectedLayers().size() ; il++){
            std::vector<int> sel_layers=this_trig.wedgeTriggers().at(0)->selectedLayers();
            //because layers are numbered as 1 2 3 4 --> -1 for C array
            int currLayer=sel_layers.at(il)-1;
			int fibreNo=itrg+currLayer*4;
            m_stds_info[fibreNo].band_id=bandId;
	        m_stds_info[fibreNo].hi_low=0;     
	        m_stds_info[fibreNo].phi_id=phiId;
	        m_stds_info[fibreNo].bcid=bcId;
	        m_stds_info[fibreNo].is_null=false;
	        m_stds_info[fibreNo].router_board=currLayer;
	        m_stds_info[fibreNo].router_fibre=fibreNo;
        }
        //loop over all selected OUTER layers
        //for(const auto& outerlayer : this_trig.wedgeTriggers().at(1)->selectedLayers() ){
        for(unsigned int ol=0;ol<this_trig.wedgeTriggers().at(1)->selectedLayers().size() ; ol++){
            std::vector<int> sel_layers=this_trig.wedgeTriggers().at(1)->selectedLayers();
            int currLayer=sel_layers.at(ol)-1+4;
			int fibreNo=itrg+currLayer*4;
            m_stds_info[fibreNo].band_id=bandId;
	        m_stds_info[fibreNo].hi_low=0;	
	        m_stds_info[fibreNo].phi_id=phiId;
	        m_stds_info[fibreNo].bcid=bcId;
	        m_stds_info[fibreNo].is_null=false;
	        m_stds_info[fibreNo].router_board=currLayer;
	        m_stds_info[fibreNo].router_fibre=fibreNo;
        }

       	int wedgeidx=0;
       	int k=0;
       	for(const auto& swt : this_trig.wedgeTriggers() ){
           int layeridx=0;
           for( const auto& layer : swt->selectedLayers() ){
               const std::vector<std::pair<int,float>> strips=swt->selectedStripChannels().at(layeridx);
               for(unsigned int i=0;i<strips.size();i++){
                   int charge=strips.at(i).second;
                   m_stds_info[k].strip_data[i]=charge;
               }
               k++;
               layeridx++;
           }
           wedgeidx++;
       	}
        
        
    }//trg loop


    for(unsigned int i=0;i<32;i++){
        std::bitset<ROUTER_BUS_SIZE> data_bus = fill_router_bus(m_stds_info[i]);
        std::vector< std::bitset<ROUTER_PACKET_SIZE> > packets=package_router_bus(data_bus, !m_stds_info[i].is_null,
										m_stds_info[i].router_board, m_stds_info[i].router_fibre,
										ROUTER_NUMBER_SIZE, ROUTER_FIBRE_SIZE,
										ROUTER_PACKET_DATA_HEADER, ROUTER_PACKET_NULL_HEADER,
										ROUTER_BUS_SIZE, ROUTER_PACKET_HEADER_SIZE, ROUTER_PACKET_SIZE);
       	//bool last_event=false;
        write_to_router_stream(m_fibres[i], ROUTER_PACKETS_PER_LINE,
								packets,
								m_isLastEvent);                                 
    }
return;
}



std::vector< std::bitset<ROUTER_PACKET_SIZE> > sTGCTriggerProcessorASCIIWriter::package_router_bus(std::bitset<ROUTER_BUS_SIZE> router_bus,
												bool has_data, int router_number, int fibre_number,
												int router_number_size, int fiber_number_size,
												int data_header, int null_header,
												int bus_size, int header_size, int packet_size)
{
	/*
	 * Protocol
	 * for data [data-header][data],[data-header][data]... (no padding)
	 * for null [null-header][x], ... [null-header][x][2b fibre num][8b router_board]
	 * the result is FIFO (e.g. the package in index 0 is the first one to be sent
	 */

	int n_packets = (bus_size) / (packet_size - header_size);
	std::vector< std::bitset<ROUTER_PACKET_SIZE> > result;
	if(!has_data)
	{
		for (int i_packet = 0; i_packet < n_packets; i_packet++)
		{
			std::bitset<ROUTER_PACKET_SIZE> packet(null_header);
			packet <<= (packet_size - header_size - router_number_size);
			packet |= fibre_number;
			packet <<= (router_number_size);
			packet |= router_number;
			result.push_back(packet);
		}
		return result;
	}

	// now if it is not null we will copy as follows
	/*
	 * [header][msb bus...]... [header][...lsb bus]
	 */

	int num_bits_to_copy =  (packet_size - header_size);
	unsigned long ulong_mask = (1 << num_bits_to_copy) - 1;
	std::bitset<ROUTER_BUS_SIZE> mask(0);
	std::bitset<ROUTER_BUS_SIZE> copy(0);

	for (int i_packet = 0; i_packet < n_packets; i_packet++)
	{
			int offset = (n_packets - 1 - i_packet) * num_bits_to_copy;
			mask |= ulong_mask;
			mask <<= offset;
			copy = mask & router_bus;
			copy >>= offset;
			unsigned long ulong_payload = copy.to_ulong();
			std::bitset<ROUTER_PACKET_SIZE> packet(data_header);
			packet <<= num_bits_to_copy;
			packet |= ulong_payload;
			result.push_back(packet);
			mask.reset();
			copy.reset();
	}
	return result;
}


std::bitset<ROUTER_BUS_SIZE> sTGCTriggerProcessorASCIIWriter::fill_router_bus(sTDS_info info)
{
	/*
	 * [Band-ID 8b][H/L 1b][Strip 14x6b][Phi-ID 5b][BCID 6b]
	 */
	std::bitset<ROUTER_BUS_SIZE> bus(0);

	if (info.is_null)
		return bus;

	bus |= info.band_id;
	//std::cout << bus << std::endl;
	bus <<= HI_LOW_SIZE;
	bus |= info.hi_low;
	//std::cout << bus << std::endl;
	for (int i_strip = (STRIP_NUMBER - 1); i_strip >= 0; i_strip--)
	{
		bus <<= STRIP_DATA_SIZE;
		//std::cout << info.strip_data[i_strip] << std::endl;
		bus |= (info.strip_data[i_strip] & STRIP_MASK);
		//std::cout << bus << std::endl;
	}

	bus <<= PHI_ID_STRIPS_SIZE;
	bus |= info.phi_id;
	//std::cout << bus << std::endl;

	bus <<= BCID_STRIPS_SIZE;
	bus |= info.bcid;
	//std::cout << bus << std::endl;

	return bus;
}


void sTGCTriggerProcessorASCIIWriter::write_to_router_stream(std::ofstream& output_stream,int packets_per_line,
							std::vector< std::bitset<ROUTER_PACKET_SIZE> > packets,
							bool is_last_event){
	unsigned int i_packets = 0;
	while (i_packets < packets.size())
	{
		for (int i_pl = 0; i_pl < packets_per_line; i_pl++)
		{
			output_stream << packets[i_packets + i_pl].to_string();
			//mif_output_stream << packets[i_packets + i_pl].to_string();
		}
		i_packets += packets_per_line;
		std::string end_char = (is_last_event && (i_packets) == packets.size()) ? ";" : ",";
		output_stream << end_char << std::endl;
		//mif_output_stream << std::endl;
	}
	
	if(is_last_event) LOG_DEBUG("This is the last event");
}
