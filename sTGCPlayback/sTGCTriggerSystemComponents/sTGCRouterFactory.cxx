/*
 * sTGCRouterFactory.cxx
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#include "sTGCTriggerSystemComponents/sTGCRouterFactory.h"
#include "sTGCTriggerSystemComponents/sTGCRouter.h"

sTGCRouterFactory::sTGCRouterFactory()
{
}

sTGCRouterFactory::~sTGCRouterFactory() {
}

IComponent* sTGCRouterFactory::Create(const TString& componentClass,
		const TString& componentType, const TString& componentName) {
	if (componentClass != "Router")
		return NULL;

	std::map<TString, sTGCRouterConfiguration>::iterator itr = m_lookup.find(componentType);
	if (itr == m_lookup.end())
		return NULL;
	sTGCRouter* rtr = new sTGCRouter;
	rtr->SetClass("Router");
	sTGCRouterConfiguration config = itr->second;
	rtr->SetType(componentType);
	rtr->SetName(componentName);
	rtr->SetLatency(config.latency);
	rtr->SetClockFrequency(config.clock);
	for (unsigned int i = 0; i < config.serializerBank.size(); i++){

		rtr->SetSerializerForInput(i,
			config.serializerBank[i],
			config.serializerBankChannel[i]);
		rtr->SetDelayForInput(i, config.delay[i]);
	}

	return rtr;

}
