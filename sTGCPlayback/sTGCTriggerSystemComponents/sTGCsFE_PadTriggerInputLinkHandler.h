/*
 * sTGCsFE_PadTriggerInputLinkHandler.h
 *
 *  Created on: Apr 9, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 *
 */

#ifndef STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSFE_PADTRIGGERINPUTLINKHANDLER_H_
#define STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSFE_PADTRIGGERINPUTLINKHANDLER_H_

#include "Components/INotifiable.h"
#include "Components/Link.h"
#include "sTGCTriggerSystemComponents/sTGCPadTriggerMsg.h"
#include <vector>
#include <sTGCTriggerSystemComponents/sTGCsTDS.h>

class sTGCsFE_PadTriggerInputLinkHandler : public INotifiable
{
public:
	sTGCsFE_PadTriggerInputLinkHandler(Link<sTGCPadTriggerMsg>& link,std::vector<sTGCsTDS>*);
	virtual ~sTGCsFE_PadTriggerInputLinkHandler();
	virtual void SetInputLink(Link<sTGCPadTriggerMsg>& link)
	{
		m_link = link;
		m_link.SetNotifiable(this);
	}

	virtual Link<sTGCPadTriggerMsg>& GetInputLink() {return m_link; }
	std::vector<sTGCsTDS>* GetsTDS()const{ return m_stdsList;}
	virtual void Notify();
	virtual void Handle();
protected:

	//sTGCsFE* m_fe;
	std::vector<sTGCsTDS>* m_stdsList;
	Link<sTGCPadTriggerMsg>& m_link;

};

#endif /* STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSFE_PADTRIGGERINPUTLINKHANDLER_H_ */
