/*
 * sTGCTriggerProcessorASCIIWriter.h
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCTRIGGERPROCESSORASCIIWRITER_H_
#define STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCTRIGGERPROCESSORASCIIWRITER_H_


#include <bitset>
#include <fstream>

#include <sTGCTriggerSystemComponents/sTGCTriggerProcessor.h>



#define LAYERS 8
#define MAX_CANDIDATES 4
#define FIBRES 32


#define BAND_ID_SIZE 8
#define HI_LOW_SIZE 1
#define STRIP_NUMBER 14
#define STRIP_DATA_SIZE 6
#define STRIP_MASK 0b111111

#define PHI_ID_STRIPS_SIZE 5
#define PHI_ID_PADS_SIZE 6
#define PHI_ID_STRIP_MASK 0b111110

#define BCID_STRIPS_SIZE 6
#define BCID_PADS_SIZE 12
#define BCID_STRIP_MASK 0b111111

#define ROUTER_BUS_SIZE 104
#define ROUTER_NUMBER_SIZE 8
#define ROUTER_FIBRE_SIZE 2

#define ROUTER_PACKET_SIZE 30
#define ROUTER_PACKET_HEADER_SIZE 4
#define ROUTER_PACKETS_PER_LINE 2
#define ROUTER_PACKET_DATA_HEADER 0b1010
#define ROUTER_PACKET_NULL_HEADER 0b1100

#define PAD_TRIGGER_BUS_SIZE 80
#define PAD_TRIGGER_BCID_SIZE 12
#define PAD_TRIGGER_LEAD_PADDING 12// alex s request
#define PAD_TRIGGER_TRAIL_PADDING 0
#define PAD_TRIGGER_BAND_ID_SIZE 8
#define PAD_TRIGGER_PHI_ID_SIZE 6
#define PAD_TRIGGER_PACKET_SIZE 17
#define PAD_TRIGGER_PACKET_SYNC_COMMA 0xBCBC;
#define PAD_TRIGGER_HEADER_SIZE 1
#define PAD_TRIGGER_DATA_HEADER 0
#define PAD_TRIGGER_NULL_HEADER 1

struct sTDS_info
{
	unsigned int band_id;
	unsigned int hi_low;
	unsigned int strip_data[STRIP_NUMBER];
	unsigned int phi_id;
	unsigned int bcid;
	bool is_null;
	int router_board;
	int router_fibre;
};

struct PadTrigger_info
{
	unsigned int band_id[4];
	unsigned int phi_id[4];
	unsigned int bcid;
	bool is_null;
	PadTrigger_info()
	{
		band_id[0] = 0;
		band_id[1] = 0;
		band_id[2] = 0;
		band_id[3] = 0;
		phi_id[0] = 0;
		phi_id[1] = 0;
		phi_id[2] = 0;
		phi_id[3] = 0;
		bcid = 0;
		is_null = true;
	}
};



class sTGCTriggerProcessorASCIIWriter : public sTGCTriggerProcessor {
public:
	 sTGCTriggerProcessorASCIIWriter();
	virtual ~sTGCTriggerProcessorASCIIWriter();

	 

	 void DoProcess();



protected:


    
    PadTrigger_info m_pad_trigger_info;//list of selected triggers in the current BC
    sTDS_info m_stds_info[32];

    std::vector< std::bitset<ROUTER_PACKET_SIZE> > package_router_bus(std::bitset<ROUTER_BUS_SIZE> router_bus,
												bool has_data, int router_number, int fibre_number,
												int router_number_size, int fiber_number_size,
												int data_header, int null_header,
												int bus_size, int header_size, int packet_size);
    std::bitset<ROUTER_BUS_SIZE> fill_router_bus(sTDS_info info);                                            
    void write_to_router_stream(std::ofstream& output_stream,int packets_per_line,
    							std::vector< std::bitset<ROUTER_PACKET_SIZE> > packets,
							    bool is_last_event);

	
	void Reset();

	

};
#endif /* STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_sTGCTriggerProcessorASCIIWriter_H_ */
