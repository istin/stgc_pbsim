/*
 * sTGCsABFactory.cxx
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#include "sTGCTriggerSystemComponents/sTGCsABFactory.h"
#include "sTGCTriggerSystemComponents/sTGCsAB.h"
#include <Utilities/LoggingSvc.h>

sTGCsABFactory::sTGCsABFactory() {
}

sTGCsABFactory::~sTGCsABFactory() {
}


IComponent* sTGCsABFactory::Create(const TString& componentClass,
		const TString& componentType, const TString& componentName) {

	LOG_TRACE("sTGCsABFactory::Create " << componentClass << " " << componentType << " " << componentName);

	if (componentClass != "sAB")
		return NULL;
	std::map<TString, sTGCsABConfiguration>::iterator itr = m_lookup.find(componentType);
	if (itr == m_lookup.end())
		return NULL;

	sTGCsABConfiguration config = itr->second;
	sTGCsAB* sAB = new sTGCsAB;

	sAB->SetClass(componentClass);
	sAB->SetType(componentType);
	sAB->SetName(componentName);
	sAB->SetConfiguration(config);
	return sAB;

}
