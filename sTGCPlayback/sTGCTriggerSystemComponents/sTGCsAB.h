/*
 * sTGCsAB.h
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSAB_H_
#define STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSAB_H_

#include <vector>

#include "CablingModel/CablingModelEnums.h"
#include "CablingModel/sTGCsABConfiguration.h"
#include "Components/INotifiable.h"
#include "Components/BaseComponent.h"


#include "sTGCTriggerSystemComponents/sTGCVMMChannel.h"






class sTGCsAB : public BaseComponent {
public:
	sTGCsAB();
	virtual ~sTGCsAB();

	virtual void ConnectSource(IComponent* source_component, const TString& source_port, const TString& target_port);

	void SetConfiguration(sTGCsABConfiguration config) {m_config = config;}
	DIR_WRT_DET GetDirection(){return m_config.direction;}
	int GetNActiveChannels(){return m_config.nActiveChannels;}
	unsigned int GetNDisconnectedBlocks(){return m_config.firstChannelDisconnected.size(); }
	void GetDisconnectedBlock(unsigned int block, int& first_channel, int& n_channels);
	sTGCsABConfiguration GetConfiguration() {return m_config;}
	int GetStripForChannel(int channel) {return m_config.GetStripForChannel(channel);}
	int GetChannelForStrip(int strip) {return m_config.GetChannelForStrip(strip);}
	int GetLayer(){return m_config.layer;}
	INotifiable* GetNotifiableForStrip(int stripNumber);
	void SetVMMChannel(int i_ch, sTGCVMMChannel* channel);

protected:

	sTGCsABConfiguration m_config;
	std::vector<sTGCVMMChannel*> m_vmmChannel;


};

#endif /* STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSAB_H_ */
