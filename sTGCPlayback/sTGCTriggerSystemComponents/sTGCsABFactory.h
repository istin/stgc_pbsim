/*
 * sTGCsABFactory.h
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSABFACTORY_H_
#define STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSABFACTORY_H_

#include "CablingModel/sTGCsABConfiguration.h"
#include "Components/IComponentFactory.h"
#include <map>

class sTGCsABFactory : public IComponentFactory {
public:
	sTGCsABFactory();
	virtual ~sTGCsABFactory();

	IComponent* Create(const TString& componentClass, const TString& componentType, const TString& componentName);

	void SetLookup(std::map<TString, sTGCsABConfiguration> lookup) {m_lookup = lookup;}
	std::map<TString, sTGCsABConfiguration> GetLookup() { return m_lookup;}


protected:

	std::map<TString, sTGCsABConfiguration> m_lookup;

};

#endif /* STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCSABFACTORY_H_ */
