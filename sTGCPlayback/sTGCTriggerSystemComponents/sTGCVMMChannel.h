/*
 * sTGCVMMChannel.h
 *
 *  Created on: Mar 21, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */

#ifndef STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCVMMCHANNEL_H_
#define STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCVMMCHANNEL_H_

#include "Components/Link.h"
#include "Components/INotifiable.h"
#include "Components/DelayedTransmitter.h"
#include "Components/IResetable.h"

#include "Detector/Strip.h"

#include "sTGCTriggerSystemComponents/sTGCVMM_dataModel.h"

#include <memory>

class sTGCVMMChannel : public INotifiable, public IResetable {
public:

	sTGCVMMChannel();
	virtual ~sTGCVMMChannel();
	virtual Link<sTGCVMM_6bCharge>& GetOutputLink_6b() {return m_transmitter.GetOutputLink(); }

	virtual void Notify() {}
	virtual void SetCharge(double charge) {m_charge = charge;}
	virtual void SetMaxCharge(double maxCharge) { m_maxCharge = maxCharge; }
	virtual void SetLatency(double latency) {m_latency = latency; }
	virtual void SetDeadTime(double deadTime) { m_deadTime = deadTime; }
	virtual void SetScheduler(IScheduler* scheduler) {m_scheduler = scheduler;}
	virtual void SetStrip(std::shared_ptr<STGC::Strip> strip)
	{ 	m_strip = strip;
		m_strip->setNotifiable(this);
	}
	virtual void Reset();

protected:

	double m_charge;
	double m_maxCharge;
	double m_latency;
	double m_deadTime;


	bool m_busy;
	sTGCVMM_6bCharge m_digitizedCharge;
	DelayedTransmitter<sTGCVMM_6bCharge> m_transmitter;
	IScheduler* m_scheduler;
	std::shared_ptr<STGC::Strip> m_strip;


};

#endif /* STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCVMMCHANNEL_H_ */
