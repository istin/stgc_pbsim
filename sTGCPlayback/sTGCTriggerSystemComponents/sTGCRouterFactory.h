/*
 * sTGCRouterFactory.h
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCROUTERFACTORY_H_
#define STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCROUTERFACTORY_H_


#include "Components/IComponentFactory.h"
#include "CablingModel/sTGCRouterConfiguration.h"
#include <map>

class sTGCRouterFactory : public IComponentFactory {
public:
	sTGCRouterFactory();
	virtual ~sTGCRouterFactory();

	IComponent* Create(const TString& componentClass, const TString& componentType, const TString& componentName);
	void SetLookup(std::map<TString, sTGCRouterConfiguration> lookup) { m_lookup = lookup;}
	std::map<TString, sTGCRouterConfiguration> GetLookup() { return m_lookup;}

protected:

	std::map<TString, sTGCRouterConfiguration> m_lookup;
};

#endif /* STGCPLAYBACK_STGCTRIGGERSYSTEMCOMPONENTS_STGCROUTERFACTORY_H_ */
