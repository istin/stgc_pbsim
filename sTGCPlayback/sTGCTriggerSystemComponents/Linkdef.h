#include "sTGCTriggerSystemComponents/sTGCBCSignal.h"
#include "sTGCTriggerSystemComponents/sTGCBCSignalFactory.h"
#include "sTGCTriggerSystemComponents/sTGCPadTrigger.h"
#include "sTGCTriggerSystemComponents/sTGCRouter.h"
#include "sTGCTriggerSystemComponents/sTGCRouterFactory.h"
#include "sTGCTriggerSystemComponents/sTGCsAB.h"
#include "sTGCTriggerSystemComponents/sTGCsABFactory.h"
#include "sTGCTriggerSystemComponents/sTGCsFE.h"
#include "sTGCTriggerSystemComponents/sTGCsTDS.h"
#include "sTGCTriggerSystemComponents/sTGCVMMChannel.h"


#ifdef __ROOTCLING__

#pragma link C++ class sTGCBCSignal+;
#pragma link C++ class sTGCBCSignalFactory+;

#pragma link C++ class sTGCPadTrigger+;
#pragma link C++ class sTGCPadTriggerFactory+;

#pragma link C++ class sTGCRouter+;
#pragma link C++ class sTGCRouterFactory+;

#pragma link C++ class sTGCsAB+;
#pragma link C++ class sTGCsABFactory+;

#pragma link C++ class sTGCsFE+;
#pragma link C++ class sTGCsFEFactory+;

#pragma link C++ class sTGCsTDS+;
#pragma link C++ class sTGCVMMChannel+;

#endif
