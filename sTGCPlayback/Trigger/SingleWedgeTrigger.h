#ifndef SingleWedgeTrigger_H
#define SingleWedgeTrigger_H


#include <memory>
#include <vector>
#include <unordered_set>
#include <algorithm>


class SingleWedgeTrigger{
    
    public :
        SingleWedgeTrigger(){};

        

        
        
        int moduleId() const {

            return m_moduleId;
        }
        int wedgeId() const { return m_wedgeId;}
        bool isSmall() const { return m_isSmall;}        
        std::vector<float> bandLocalMinY() const { return m_bandLocalMinY; }
        std::vector<float> bandLocalMaxY() const { return  m_bandLocalMaxY; }
        std::vector<int> selectedLayers() const { return m_selectedLayers;}
        std::vector<int> selectedEtaIndices()const { return  m_padEtaIndices;}
        std::vector<int> selectedPhiIndices() const { return m_padPhiIndices;}
        std::vector<int> selectedBands() const { return m_selectedBands;}
        std::vector<std::vector<std::pair<int,float>>> selectedStripChannels() const { return m_selectedStripChannels;}
        std::vector<std::vector<std::pair<int,float>>> selectedStripCharges() const { return m_selectedStripCharges;}


        
        void addStripChannels(std::vector<std::pair<int,float>> i){m_selectedStripChannels.push_back(i);}
        
        void setSelectedLayers(std::vector<int> i){ m_selectedLayers=i;}
        void setSelectedEtaIndices(std::vector<int> i){m_padEtaIndices=i;}
        void setSelectedPhiIndices(std::vector<int> i){m_padPhiIndices=i;}
        void setBandLocalMinY(std::vector<float> i )  { m_bandLocalMinY=i;}
        void setBandLocalMaxY(std::vector<float> i ) {m_bandLocalMaxY=i;}
        void setBands(std::vector<int> i){m_selectedBands=i;}
        void setModuleId(int i) { m_moduleId=i;}
        void setWedgeId(int i) {m_wedgeId=i;}
        void setIsSmall( bool s){ m_isSmall=s;}
        
        void dump() const;
        
        
    private:
        int m_moduleId;
        int m_wedgeId;
        bool m_isSmall;
        
        //*** all grouped w.r.t selected trigger layers ***
        std::vector<float> m_bandLocalMinY;
        std::vector<float> m_bandLocalMaxY;
        std::vector<int> m_selectedLayers;
        std::vector<int> m_padEtaIndices;
        std::vector<int> m_padPhiIndices;
        std::vector<int> m_selectedBands;
        //use a set-like container to fulter out duplicate strips in the event record.
        //Duplicate strips are present in Strip-TDS Tool and it seems they are coming from digitization. I don't know if this is a feature or a bug
        std::vector<std::vector<std::pair<int,float>>> m_selectedStripChannels;//layer
        std::vector<std::vector<std::pair<int,float>>> m_selectedStripCharges;
        //std::vector<std::unordered_set<int> > m_selectedStripCharges;
};




#endif
