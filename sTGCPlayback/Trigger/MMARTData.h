#include "TString.h"
#include <iostream>
#include <vector>
#include "Trigger/MMHit.h"

#ifndef MMARTData_H
#define MMARTData_H

class TrigTreePRD;

class MMARTData {

    public :     
    
        MMARTData();
        MMARTData& operator += (const MMARTData&);
        bool operator == (const MMARTData&);
        int dump() const;
        int fetchARTData(TrigTreePRD*);
        int fillHits();
        int reset();
        int GetRunNumber()        const { return m_runNumber; }
        int GetEventNumber()      const { return m_eventNumber; }
        int GetNumberOfARTASICs() const { return ARTASIC_PER_LAYER * LAYER_PER_SECTOR; }
        int ARTASICsPerLayer()    const { return ARTASIC_PER_LAYER; }
        int LayersPerSector()     const { return LAYER_PER_SECTOR; }
        int FirstBCID();
        int LastBCID();

        //        struct MMHit {
        //          int index;
        //          int stationEta;
        //          int stationPhi;
        //          int multiplet;
        //          int gas_gap;
        //          int channel;
        //          float time;
        //          float charge;
        //          // --------
        //          int channel_global;
        //          int channel_mmfe8;
        //          int channel_vmm;
        //          int layer;
        //          int bcid;
        //          int vmm;
        //          int mmfe8;
        //          int addc;
        //          int artasic;
        //          int artlink;
        //          int isLarge;
        //          int isAside;
        //          // -----------
        //          TString NameForStore;
        //        };

        int derive_channel_global   (std::shared_ptr<MMHit> hit);
        int derive_channel_mmfe8    (std::shared_ptr<MMHit> hit);
        int derive_channel_vmm      (std::shared_ptr<MMHit> hit);
        int derive_layer            (std::shared_ptr<MMHit> hit);
        int derive_bcid             (std::shared_ptr<MMHit> hit);
        int derive_vmm              (std::shared_ptr<MMHit> hit);
        int derive_mmfe8            (std::shared_ptr<MMHit> hit);
        int derive_addc             (std::shared_ptr<MMHit> hit);
        int derive_artasic          (std::shared_ptr<MMHit> hit);
        int derive_artlink          (std::shared_ptr<MMHit> hit);
        int derive_artvmm           (std::shared_ptr<MMHit> hit);
        int derive_isLarge          (std::shared_ptr<MMHit> hit);
        int derive_isAside          (std::shared_ptr<MMHit> hit);
        TString derive_NameForStore (std::shared_ptr<MMHit> hit);

        std::vector< std::shared_ptr<MMHit> > ARTHits() const { return m_art_hits; }
        std::vector< std::shared_ptr<MMHit> > AllHits() const { return m_all_hits; }

    private :

        // constants
        int GAS_GAP_PER_MULTIPLET = 4;
        int CHANNEL_PER_VMM       = 64;
        int CHANNEL_PER_MMFE8     = 512;
        int VMM_PER_MMFE8         = 8;
        int MMFE8_PER_M1          = 10;
        int MMFE8_PER_M2          = 6;
        int MMFE8_PER_ADDC        = 8;
        int MMFE8_PER_ARTASIC     = 4;
        int ARTASIC_PER_LAYER     = 4;
        int LAYER_PER_SECTOR      = 8;

        // event
        int m_runNumber;
        int m_eventNumber;

        // from the TTree
        std::vector<int>   m_art_stationEta;
        std::vector<int>   m_all_stationEta;
        std::vector<int>   m_art_stationPhi;
        std::vector<int>   m_all_stationPhi;
        std::vector<int>   m_art_multiplet;
        std::vector<int>   m_all_multiplet;
        std::vector<int>   m_art_gas_gap;
        std::vector<int>   m_all_gas_gap;
        std::vector<int>   m_art_channel;
        std::vector<int>   m_all_channel;
        std::vector<float> m_art_time;
        std::vector<float> m_all_time;
        std::vector<float> m_art_charge;
        std::vector<float> m_all_charge;

        // objects
        std::vector< std::shared_ptr<MMHit> > m_art_hits;
        std::vector< std::shared_ptr<MMHit> > m_all_hits;

};

#endif
