//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sun Mar 17 14:47:23 2019 by ROOT version 6.16/00
// from TTree NSWL1SimulationTree/Ntuple of NSWL1Simulation
// found on file: NSWL1Simulation.root
//////////////////////////////////////////////////////////

#ifndef TrigTree_h
#define TrigTree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
using std::vector;

class TrigTree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   UInt_t          runNumber;
   UInt_t          eventNumber;
   UInt_t          nPadHits;
   vector<float>   *padGlobalX;
   vector<float>   *padGlobalY;
   vector<float>   *padGlobalZ;
   vector<float>   *padDelayedTime;
   vector<int>     *padBCHR;
   vector<float>   *padGlobalCornerX;
   vector<float>   *padGlobalCornerY;
   vector<float>   *padGlobalCornerZ;
   vector<float>   *padTruthHitGlobalX;
   vector<float>   *padTruthHitGlobalY;
   vector<float>   *padTruthHitGlobalZ;
   vector<int>     *padEtaIdFromOfflineId;
   vector<int>     *padPhiIdFromOfflineId;
   vector<int>     *padSectorIdFromOfflineId;
   vector<int>     *padSectorTypeFromOfflineId;
   vector<int>     *padGasGapIdFromOfflineId;
   vector<int>     *padModuleIdFromOfflineId;
   vector<int>     *padMultipletIdFromOfflineId;
   vector<int>     *padSideIdFromOfflineId;
   vector<int>     *offlineIdPadEtaIdConverted;
   vector<int>     *offlineIdPadPhiIdConverted;
   vector<int>     *padEtaIdFromOldSimu;
   vector<int>     *padPhiIdFromOldSimu;
   UInt_t          nPadTriggers;
   vector<unsigned int> *padTriggerBCID;
   vector<int>     *padTriggerSectorID;
   vector<int>     *padTriggerModuleIDinner;
   vector<int>     *padTriggerModuleIDouter;
   vector<vector<int> > *padTriggerSelectedLayersInner;
   vector<vector<int> > *padTriggerSelectedLayersOuter;
   vector<vector<int> > *padTriggerSelectedBandsInner;
   vector<vector<int> > *padTriggerSelectedBandsOuter;
   vector<vector<int> > *padTriggerPadEtaIndicesInner;
   vector<vector<int> > *padTriggerPadPhiIndicesInner;
   vector<vector<int> > *padTriggerPadEtaIndicesOuter;
   vector<vector<int> > *padTriggerPadPhiIndicesOuter;
   vector<vector<float> > *padTriggerRCenterMinInner;
   vector<vector<float> > *padTriggerRCenterMaxInner;
   vector<vector<float> > *padTriggerRCenterMinOuter;
   vector<vector<float> > *padTriggerRCenterMaxOuter;
   vector<int>     *padTriggerSectorType;
   vector<int>     *padTriggerSideID;
   vector<int>     *padTriggerIndex;
   vector<unsigned int> *padTriggerBandID;
   vector<float>   *padTriggerEta;
   vector<float>   *padTriggerPhi;
   vector<int>     *padTriggerEtaID;
   vector<int>     *padTriggerPhiID;
   vector<int>     *padTriggerMultipletID;
   vector<float>   *padTriggerEtamin;
   vector<float>   *padTriggerEtamax;
   vector<float>   *padTriggerPhimin;
   vector<float>   *padTriggerPhimax;
   vector<vector<float> > *padTriggerlocalminYInner;
   vector<vector<float> > *padTriggerlocalmaxYInner;
   vector<vector<float> > *padTriggerlocalminYOuter;
   vector<vector<float> > *padTriggerlocalmaxYOuter;
   UInt_t          StripTdsOfflineTool_nStripHits;
   vector<float>   *StripTdsOfflineTool_charge;
   vector<float>   *StripTdsOfflineTool_charge_6bit;
   vector<float>   *StripTdsOfflineTool_charge_10bit;
   vector<float>   *StripTdsOfflineTool_global_X;
   vector<float>   *StripTdsOfflineTool_global_Y;
   vector<float>   *StripTdsOfflineTool_global_Z;
   vector<float>   *StripTdsOfflineTool_local_X;
   vector<float>   *StripTdsOfflineTool_local_Y;
   vector<float>   *StripTdsOfflineTool_layer;
   vector<float>   *StripTdsOfflineTool_isSmall;
   vector<float>   *StripTdsOfflineTool_eta;
   vector<float>   *StripTdsOfflineTool_phi;
   vector<float>   *StripTdsOfflineTool_readStrip;
   vector<int>     *StripTdsOfflineTool_channel;
   vector<int>     *StripTdsOfflineTool_BCID;
   vector<int>     *StripTdsOfflineTool_wedge;
   vector<float>   *StripTdsOfflineTool_time;
   UInt_t          StripClusterTool_cl_n;
   vector<int>     *StripClusterTool_cl_charge;
   vector<float>   *StripClusterTool_cl_x;
   vector<float>   *StripClusterTool_cl_y;
   vector<float>   *StripClusterTool_cl_z;
   vector<float>   *StripClusterTool_cl_lx;
   vector<float>   *StripClusterTool_cl_ly;
   vector<float>   *StripClusterTool_cl_lz;
   vector<float>   *StripClusterTool_cl_ltgx;
   vector<float>   *StripClusterTool_cl_ltgy;
   vector<float>   *StripClusterTool_cl_ltgz;
   vector<int>     *StripClusterTool_cl_size;
   vector<int>     *StripClusterTool_cl_isSmall;
   vector<int>     *StripClusterTool_cl_side;
   vector<int>     *StripClusterTool_cl_wedge;
   vector<int>     *StripClusterTool_cl_sector;
   vector<int>     *StripClusterTool_cl_module;
   vector<int>     *StripClusterTool_cl_layer;
   vector<int>     *StripClusterTool_cl_bandId;
   vector<int>     *StripClusterTool_cl_phiId;
   vector<float>   *StripClusterTool_cl_truth_x;
   vector<float>   *StripClusterTool_cl_truth_y;
   vector<float>   *StripClusterTool_cl_truth_z;
   vector<float>   *StripClusterTool_cl_truth_lx;
   vector<float>   *StripClusterTool_cl_truth_ly;
   vector<float>   *StripClusterTool_cl_truth_lz;
   vector<float>   *StripClusterTool_cl_truth_E;
   vector<int>     *StripClusterTool_cl_truth_n;
   vector<float>   *StripSegmentTool_seg_theta;
   vector<float>   *StripSegmentTool_seg_dtheta;
   vector<unsigned char> *StripSegmentTool_seg_dtheta_int;
   vector<float>   *StripSegmentTool_seg_eta;
   vector<float>   *StripSegmentTool_seg_eta_inf;
   vector<float>   *StripSegmentTool_seg_phi;
   vector<float>   *StripSegmentTool_seg_global_r;
   vector<float>   *StripSegmentTool_seg_global_x;
   vector<float>   *StripSegmentTool_seg_global_y;
   vector<float>   *StripSegmentTool_seg_global_z;
   vector<float>   *StripSegmentTool_seg_dir_r;
   vector<float>   *StripSegmentTool_seg_dir_y;
   vector<float>   *StripSegmentTool_seg_dir_z;
   vector<int>     *StripSegmentTool_seg_bandId;
   vector<int>     *StripSegmentTool_seg_phiId;
   vector<int>     *StripSegmentTool_seg_rIdx;
   vector<int>     *StripSegmentTool_seg_wedge1_size;
   vector<int>     *StripSegmentTool_seg_wedge2_size;

   // List of branches
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_nPadHits;   //!
   TBranch        *b_padGlobalX;   //!
   TBranch        *b_padGlobalY;   //!
   TBranch        *b_padGlobalZ;   //!
   TBranch        *b_padDelayedTime;   //!
   TBranch        *b_padBCHR;   //!
   TBranch        *b_padGlobalCornerX;   //!
   TBranch        *b_padGlobalCornerY;   //!
   TBranch        *b_padGlobalCornerZ;   //!
   TBranch        *b_padTruthHitGlobalX;   //!
   TBranch        *b_padTruthHitGlobalY;   //!
   TBranch        *b_padTruthHitGlobalZ;   //!
   TBranch        *b_padEtaIdFromOfflineId;   //!
   TBranch        *b_padPhiIdFromOfflineId;   //!
   TBranch        *b_padSectorIdFromOfflineId;   //!
   TBranch        *b_padSectorTypeFromOfflineId;   //!
   TBranch        *b_padGasGapIdFromOfflineId;   //!
   TBranch        *b_padModuleIdFromOfflineId;   //!
   TBranch        *b_padMultipletIdFromOfflineId;   //!
   TBranch        *b_padSideIdFromOfflineId;   //!
   TBranch        *b_offlineIdPadEtaIdConverted;   //!
   TBranch        *b_offlineIdPadPhiIdConverted;   //!
   TBranch        *b_padEtaIdFromOldSimu;   //!
   TBranch        *b_padPhiIdFromOldSimu;   //!
   TBranch        *b_nPadTriggers;   //!
   TBranch        *b_padTriggerBCID;   //!
   TBranch        *b_padTriggerSectorID;   //!
   TBranch        *b_padTriggerModuleIDinner;   //!
   TBranch        *b_padTriggerModuleIDouter;   //!
   TBranch        *b_padTriggerSelectedLayersInner;   //!
   TBranch        *b_padTriggerSelectedLayersOuter;   //!
   TBranch        *b_padTriggerSelectedBandsInner;   //!
   TBranch        *b_padTriggerSelectedBandsOuter;   //!
   TBranch        *b_padTriggerPadEtaIndicesInner;   //!
   TBranch        *b_padTriggerPadPhiIndicesInner;   //!
   TBranch        *b_padTriggerPadEtaIndicesOuter;   //!
   TBranch        *b_padTriggerPadPhiIndicesOuter;   //!
   TBranch        *b_padTriggerRCenterMinInner;   //!
   TBranch        *b_padTriggerRCenterMaxInner;   //!
   TBranch        *b_padTriggerRCenterMinOuter;   //!
   TBranch        *b_padTriggerRCenterMaxOuter;   //!
   TBranch        *b_padTriggerSectorType;   //!
   TBranch        *b_padTriggerSideID;   //!
   TBranch        *b_padTriggerIndex;   //!
   TBranch        *b_padTriggerBandID;   //!
   TBranch        *b_padTriggerEta;   //!
   TBranch        *b_padTriggerPhi;   //!
   TBranch        *b_padTriggerEtaID;   //!
   TBranch        *b_padTriggerPhiID;   //!
   TBranch        *b_padTriggerMultipletID;   //!
   TBranch        *b_padTriggerEtamin;   //!
   TBranch        *b_padTriggerEtamax;   //!
   TBranch        *b_padTriggerPhimin;   //!
   TBranch        *b_padTriggerPhimax;   //!
   TBranch        *b_padTriggerlocalminYInner;   //!
   TBranch        *b_padTriggerlocalmaxYInner;   //!
   TBranch        *b_padTriggerlocalminYOuter;   //!
   TBranch        *b_padTriggerlocalmaxYOuter;   //!
   TBranch        *b_StripTdsOfflineTool_nStripHits;   //!
   TBranch        *b_StripTdsOfflineTool_charge;   //!
   TBranch        *b_StripTdsOfflineTool_charge_6bit;   //!
   TBranch        *b_StripTdsOfflineTool_charge_10bit;   //!
   TBranch        *b_StripTdsOfflineTool_global_X;   //!
   TBranch        *b_StripTdsOfflineTool_global_Y;   //!
   TBranch        *b_StripTdsOfflineTool_global_Z;   //!
   TBranch        *b_StripTdsOfflineTool_local_X;   //!
   TBranch        *b_StripTdsOfflineTool_local_Y;   //!
   TBranch        *b_StripTdsOfflineTool_layer;   //!
   TBranch        *b_StripTdsOfflineTool_isSmall;   //!
   TBranch        *b_StripTdsOfflineTool_eta;   //!
   TBranch        *b_StripTdsOfflineTool_phi;   //!
   TBranch        *b_StripTdsOfflineTool_readStrip;   //!
   TBranch        *b_StripTdsOfflineTool_channel;   //!
   TBranch        *b_StripTdsOfflineTool_BCID;   //!
   TBranch        *b_StripTdsOfflineTool_wedge;   //!
   TBranch        *b_StripTdsOfflineTool_time;   //!
   TBranch        *b_StripClusterTool_cl_n;   //!
   TBranch        *b_StripClusterTool_cl_charge;   //!
   TBranch        *b_StripClusterTool_cl_x;   //!
   TBranch        *b_StripClusterTool_cl_y;   //!
   TBranch        *b_StripClusterTool_cl_z;   //!
   TBranch        *b_StripClusterTool_cl_lx;   //!
   TBranch        *b_StripClusterTool_cl_ly;   //!
   TBranch        *b_StripClusterTool_cl_lz;   //!
   TBranch        *b_StripClusterTool_cl_ltgx;   //!
   TBranch        *b_StripClusterTool_cl_ltgy;   //!
   TBranch        *b_StripClusterTool_cl_ltgz;   //!
   TBranch        *b_StripClusterTool_cl_size;   //!
   TBranch        *b_StripClusterTool_cl_isSmall;   //!
   TBranch        *b_StripClusterTool_cl_side;   //!
   TBranch        *b_StripClusterTool_cl_wedge;   //!
   TBranch        *b_StripClusterTool_cl_sector;   //!
   TBranch        *b_StripClusterTool_cl_module;   //!
   TBranch        *b_StripClusterTool_cl_layer;   //!
   TBranch        *b_StripClusterTool_cl_bandId;   //!
   TBranch        *b_StripClusterTool_cl_phiId;   //!
   TBranch        *b_StripClusterTool_cl_truth_x;   //!
   TBranch        *b_StripClusterTool_cl_truth_y;   //!
   TBranch        *b_StripClusterTool_cl_truth_z;   //!
   TBranch        *b_StripClusterTool_cl_truth_lx;   //!
   TBranch        *b_StripClusterTool_cl_truth_ly;   //!
   TBranch        *b_StripClusterTool_cl_truth_lz;   //!
   TBranch        *b_StripClusterTool_cl_truth_E;   //!
   TBranch        *b_StripClusterTool_cl_truth_n;   //!
   TBranch        *b_StripSegmentTool_seg_theta;   //!
   TBranch        *b_StripSegmentTool_seg_dtheta;   //!
   TBranch        *b_StripSegmentTool_seg_dtheta_int;   //!
   TBranch        *b_StripSegmentTool_seg_eta;   //!
   TBranch        *b_StripSegmentTool_seg_eta_inf;   //!
   TBranch        *b_StripSegmentTool_seg_phi;   //!
   TBranch        *b_StripSegmentTool_seg_global_r;   //!
   TBranch        *b_StripSegmentTool_seg_global_x;   //!
   TBranch        *b_StripSegmentTool_seg_global_y;   //!
   TBranch        *b_StripSegmentTool_seg_global_z;   //!
   TBranch        *b_StripSegmentTool_seg_dir_r;   //!
   TBranch        *b_StripSegmentTool_seg_dir_y;   //!
   TBranch        *b_StripSegmentTool_seg_dir_z;   //!
   TBranch        *b_StripSegmentTool_seg_bandId;   //!
   TBranch        *b_StripSegmentTool_seg_phiId;   //!
   TBranch        *b_StripSegmentTool_seg_rIdx;   //!
   TBranch        *b_StripSegmentTool_seg_wedge1_size;   //!
   TBranch        *b_StripSegmentTool_seg_wedge2_size;   //!

   TrigTree(TTree *tree=0);
   virtual ~TrigTree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef TrigTree_cxx
TrigTree::TrigTree(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("NSWL1Simulation.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("NSWL1Simulation.root");
      }
      f->GetObject("NSWL1SimulationTree",tree);

   }
   Init(tree);
}

TrigTree::~TrigTree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t TrigTree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t TrigTree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void TrigTree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   padGlobalX = 0;
   padGlobalY = 0;
   padGlobalZ = 0;
   padDelayedTime = 0;
   padBCHR = 0;
   padGlobalCornerX = 0;
   padGlobalCornerY = 0;
   padGlobalCornerZ = 0;
   padTruthHitGlobalX = 0;
   padTruthHitGlobalY = 0;
   padTruthHitGlobalZ = 0;
   padEtaIdFromOfflineId = 0;
   padPhiIdFromOfflineId = 0;
   padSectorIdFromOfflineId = 0;
   padSectorTypeFromOfflineId = 0;
   padGasGapIdFromOfflineId = 0;
   padModuleIdFromOfflineId = 0;
   padMultipletIdFromOfflineId = 0;
   padSideIdFromOfflineId = 0;
   offlineIdPadEtaIdConverted = 0;
   offlineIdPadPhiIdConverted = 0;
   padEtaIdFromOldSimu = 0;
   padPhiIdFromOldSimu = 0;
   padTriggerBCID = 0;
   padTriggerSectorID = 0;
   padTriggerModuleIDinner = 0;
   padTriggerModuleIDouter = 0;
   padTriggerSelectedLayersInner = 0;
   padTriggerSelectedLayersOuter = 0;
   padTriggerSelectedBandsInner = 0;
   padTriggerSelectedBandsOuter = 0;
   padTriggerPadEtaIndicesInner = 0;
   padTriggerPadPhiIndicesInner = 0;
   padTriggerPadEtaIndicesOuter = 0;
   padTriggerPadPhiIndicesOuter = 0;
   padTriggerRCenterMinInner = 0;
   padTriggerRCenterMaxInner = 0;
   padTriggerRCenterMinOuter = 0;
   padTriggerRCenterMaxOuter = 0;
   padTriggerSectorType = 0;
   padTriggerSideID = 0;
   padTriggerIndex = 0;
   padTriggerBandID = 0;
   padTriggerEta = 0;
   padTriggerPhi = 0;
   padTriggerEtaID = 0;
   padTriggerPhiID = 0;
   padTriggerMultipletID = 0;
   padTriggerEtamin = 0;
   padTriggerEtamax = 0;
   padTriggerPhimin = 0;
   padTriggerPhimax = 0;
   padTriggerlocalminYInner = 0;
   padTriggerlocalmaxYInner = 0;
   padTriggerlocalminYOuter = 0;
   padTriggerlocalmaxYOuter = 0;
   StripTdsOfflineTool_charge = 0;
   StripTdsOfflineTool_charge_6bit = 0;
   StripTdsOfflineTool_charge_10bit = 0;
   StripTdsOfflineTool_global_X = 0;
   StripTdsOfflineTool_global_Y = 0;
   StripTdsOfflineTool_global_Z = 0;
   StripTdsOfflineTool_local_X = 0;
   StripTdsOfflineTool_local_Y = 0;
   StripTdsOfflineTool_layer = 0;
   StripTdsOfflineTool_isSmall = 0;
   StripTdsOfflineTool_eta = 0;
   StripTdsOfflineTool_phi = 0;
   StripTdsOfflineTool_readStrip = 0;
   StripTdsOfflineTool_channel = 0;
   StripTdsOfflineTool_BCID = 0;
   StripTdsOfflineTool_wedge = 0;
   StripTdsOfflineTool_time = 0;
   StripClusterTool_cl_charge = 0;
   StripClusterTool_cl_x = 0;
   StripClusterTool_cl_y = 0;
   StripClusterTool_cl_z = 0;
   StripClusterTool_cl_lx = 0;
   StripClusterTool_cl_ly = 0;
   StripClusterTool_cl_lz = 0;
   StripClusterTool_cl_ltgx = 0;
   StripClusterTool_cl_ltgy = 0;
   StripClusterTool_cl_ltgz = 0;
   StripClusterTool_cl_size = 0;
   StripClusterTool_cl_isSmall = 0;
   StripClusterTool_cl_side = 0;
   StripClusterTool_cl_wedge = 0;
   StripClusterTool_cl_sector = 0;
   StripClusterTool_cl_module = 0;
   StripClusterTool_cl_layer = 0;
   StripClusterTool_cl_bandId = 0;
   StripClusterTool_cl_phiId = 0;
   StripClusterTool_cl_truth_x = 0;
   StripClusterTool_cl_truth_y = 0;
   StripClusterTool_cl_truth_z = 0;
   StripClusterTool_cl_truth_lx = 0;
   StripClusterTool_cl_truth_ly = 0;
   StripClusterTool_cl_truth_lz = 0;
   StripClusterTool_cl_truth_E = 0;
   StripClusterTool_cl_truth_n = 0;
   StripSegmentTool_seg_theta = 0;
   StripSegmentTool_seg_dtheta = 0;
   StripSegmentTool_seg_dtheta_int = 0;
   StripSegmentTool_seg_eta = 0;
   StripSegmentTool_seg_eta_inf = 0;
   StripSegmentTool_seg_phi = 0;
   StripSegmentTool_seg_global_r = 0;
   StripSegmentTool_seg_global_x = 0;
   StripSegmentTool_seg_global_y = 0;
   StripSegmentTool_seg_global_z = 0;
   StripSegmentTool_seg_dir_r = 0;
   StripSegmentTool_seg_dir_y = 0;
   StripSegmentTool_seg_dir_z = 0;
   StripSegmentTool_seg_bandId = 0;
   StripSegmentTool_seg_phiId = 0;
   StripSegmentTool_seg_rIdx = 0;
   StripSegmentTool_seg_wedge1_size = 0;
   StripSegmentTool_seg_wedge2_size = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("nPadHits", &nPadHits, &b_nPadHits);
   fChain->SetBranchAddress("padGlobalX", &padGlobalX, &b_padGlobalX);
   fChain->SetBranchAddress("padGlobalY", &padGlobalY, &b_padGlobalY);
   fChain->SetBranchAddress("padGlobalZ", &padGlobalZ, &b_padGlobalZ);
   fChain->SetBranchAddress("padDelayedTime", &padDelayedTime, &b_padDelayedTime);
   fChain->SetBranchAddress("padBCHR", &padBCHR, &b_padBCHR);
   fChain->SetBranchAddress("padGlobalCornerX", &padGlobalCornerX, &b_padGlobalCornerX);
   fChain->SetBranchAddress("padGlobalCornerY", &padGlobalCornerY, &b_padGlobalCornerY);
   fChain->SetBranchAddress("padGlobalCornerZ", &padGlobalCornerZ, &b_padGlobalCornerZ);
   fChain->SetBranchAddress("padTruthHitGlobalX", &padTruthHitGlobalX, &b_padTruthHitGlobalX);
   fChain->SetBranchAddress("padTruthHitGlobalY", &padTruthHitGlobalY, &b_padTruthHitGlobalY);
   fChain->SetBranchAddress("padTruthHitGlobalZ", &padTruthHitGlobalZ, &b_padTruthHitGlobalZ);
   fChain->SetBranchAddress("padEtaIdFromOfflineId", &padEtaIdFromOfflineId, &b_padEtaIdFromOfflineId);
   fChain->SetBranchAddress("padPhiIdFromOfflineId", &padPhiIdFromOfflineId, &b_padPhiIdFromOfflineId);
   fChain->SetBranchAddress("padSectorIdFromOfflineId", &padSectorIdFromOfflineId, &b_padSectorIdFromOfflineId);
   fChain->SetBranchAddress("padSectorTypeFromOfflineId", &padSectorTypeFromOfflineId, &b_padSectorTypeFromOfflineId);
   fChain->SetBranchAddress("padGasGapIdFromOfflineId", &padGasGapIdFromOfflineId, &b_padGasGapIdFromOfflineId);
   fChain->SetBranchAddress("padModuleIdFromOfflineId", &padModuleIdFromOfflineId, &b_padModuleIdFromOfflineId);
   fChain->SetBranchAddress("padMultipletIdFromOfflineId", &padMultipletIdFromOfflineId, &b_padMultipletIdFromOfflineId);
   fChain->SetBranchAddress("padSideIdFromOfflineId", &padSideIdFromOfflineId, &b_padSideIdFromOfflineId);
   fChain->SetBranchAddress("offlineIdPadEtaIdConverted", &offlineIdPadEtaIdConverted, &b_offlineIdPadEtaIdConverted);
   fChain->SetBranchAddress("offlineIdPadPhiIdConverted", &offlineIdPadPhiIdConverted, &b_offlineIdPadPhiIdConverted);
   fChain->SetBranchAddress("padEtaIdFromOldSimu", &padEtaIdFromOldSimu, &b_padEtaIdFromOldSimu);
   fChain->SetBranchAddress("padPhiIdFromOldSimu", &padPhiIdFromOldSimu, &b_padPhiIdFromOldSimu);
   fChain->SetBranchAddress("nPadTriggers", &nPadTriggers, &b_nPadTriggers);
   fChain->SetBranchAddress("padTriggerBCID", &padTriggerBCID, &b_padTriggerBCID);
   fChain->SetBranchAddress("padTriggerSectorID", &padTriggerSectorID, &b_padTriggerSectorID);
   fChain->SetBranchAddress("padTriggerModuleIDinner", &padTriggerModuleIDinner, &b_padTriggerModuleIDinner);
   fChain->SetBranchAddress("padTriggerModuleIDouter", &padTriggerModuleIDouter, &b_padTriggerModuleIDouter);
   fChain->SetBranchAddress("padTriggerSelectedLayersInner", &padTriggerSelectedLayersInner, &b_padTriggerSelectedLayersInner);
   fChain->SetBranchAddress("padTriggerSelectedLayersOuter", &padTriggerSelectedLayersOuter, &b_padTriggerSelectedLayersOuter);
   fChain->SetBranchAddress("padTriggerSelectedBandsInner", &padTriggerSelectedBandsInner, &b_padTriggerSelectedBandsInner);
   fChain->SetBranchAddress("padTriggerSelectedBandsOuter", &padTriggerSelectedBandsOuter, &b_padTriggerSelectedBandsOuter);
   fChain->SetBranchAddress("padTriggerPadEtaIndicesInner", &padTriggerPadEtaIndicesInner, &b_padTriggerPadEtaIndicesInner);
   fChain->SetBranchAddress("padTriggerPadPhiIndicesInner", &padTriggerPadPhiIndicesInner, &b_padTriggerPadPhiIndicesInner);
   fChain->SetBranchAddress("padTriggerPadEtaIndicesOuter", &padTriggerPadEtaIndicesOuter, &b_padTriggerPadEtaIndicesOuter);
   fChain->SetBranchAddress("padTriggerPadPhiIndicesOuter", &padTriggerPadPhiIndicesOuter, &b_padTriggerPadPhiIndicesOuter);
   fChain->SetBranchAddress("padTriggerRCenterMinInner", &padTriggerRCenterMinInner, &b_padTriggerRCenterMinInner);
   fChain->SetBranchAddress("padTriggerRCenterMaxInner", &padTriggerRCenterMaxInner, &b_padTriggerRCenterMaxInner);
   fChain->SetBranchAddress("padTriggerRCenterMinOuter", &padTriggerRCenterMinOuter, &b_padTriggerRCenterMinOuter);
   fChain->SetBranchAddress("padTriggerRCenterMaxOuter", &padTriggerRCenterMaxOuter, &b_padTriggerRCenterMaxOuter);
   fChain->SetBranchAddress("padTriggerSectorType", &padTriggerSectorType, &b_padTriggerSectorType);
   fChain->SetBranchAddress("padTriggerSideID", &padTriggerSideID, &b_padTriggerSideID);
   fChain->SetBranchAddress("padTriggerIndex", &padTriggerIndex, &b_padTriggerIndex);
   fChain->SetBranchAddress("padTriggerBandID", &padTriggerBandID, &b_padTriggerBandID);
   fChain->SetBranchAddress("padTriggerEta", &padTriggerEta, &b_padTriggerEta);
   fChain->SetBranchAddress("padTriggerPhi", &padTriggerPhi, &b_padTriggerPhi);
   fChain->SetBranchAddress("padTriggerEtaID", &padTriggerEtaID, &b_padTriggerEtaID);
   fChain->SetBranchAddress("padTriggerPhiID", &padTriggerPhiID, &b_padTriggerPhiID);
   fChain->SetBranchAddress("padTriggerMultipletID", &padTriggerMultipletID, &b_padTriggerMultipletID);
   fChain->SetBranchAddress("padTriggerEtamin", &padTriggerEtamin, &b_padTriggerEtamin);
   fChain->SetBranchAddress("padTriggerEtamax", &padTriggerEtamax, &b_padTriggerEtamax);
   fChain->SetBranchAddress("padTriggerPhimin", &padTriggerPhimin, &b_padTriggerPhimin);
   fChain->SetBranchAddress("padTriggerPhimax", &padTriggerPhimax, &b_padTriggerPhimax);
   fChain->SetBranchAddress("padTriggerlocalminYInner", &padTriggerlocalminYInner, &b_padTriggerlocalminYInner);
   fChain->SetBranchAddress("padTriggerlocalmaxYInner", &padTriggerlocalmaxYInner, &b_padTriggerlocalmaxYInner);
   fChain->SetBranchAddress("padTriggerlocalminYOuter", &padTriggerlocalminYOuter, &b_padTriggerlocalminYOuter);
   fChain->SetBranchAddress("padTriggerlocalmaxYOuter", &padTriggerlocalmaxYOuter, &b_padTriggerlocalmaxYOuter);
   fChain->SetBranchAddress("StripTdsOfflineTool_nStripHits", &StripTdsOfflineTool_nStripHits, &b_StripTdsOfflineTool_nStripHits);
   fChain->SetBranchAddress("StripTdsOfflineTool_charge", &StripTdsOfflineTool_charge, &b_StripTdsOfflineTool_charge);
   fChain->SetBranchAddress("StripTdsOfflineTool_charge_6bit", &StripTdsOfflineTool_charge_6bit, &b_StripTdsOfflineTool_charge_6bit);
   fChain->SetBranchAddress("StripTdsOfflineTool_charge_10bit", &StripTdsOfflineTool_charge_10bit, &b_StripTdsOfflineTool_charge_10bit);
   fChain->SetBranchAddress("StripTdsOfflineTool_global_X", &StripTdsOfflineTool_global_X, &b_StripTdsOfflineTool_global_X);
   fChain->SetBranchAddress("StripTdsOfflineTool_global_Y", &StripTdsOfflineTool_global_Y, &b_StripTdsOfflineTool_global_Y);
   fChain->SetBranchAddress("StripTdsOfflineTool_global_Z", &StripTdsOfflineTool_global_Z, &b_StripTdsOfflineTool_global_Z);
   fChain->SetBranchAddress("StripTdsOfflineTool_local_X", &StripTdsOfflineTool_local_X, &b_StripTdsOfflineTool_local_X);
   fChain->SetBranchAddress("StripTdsOfflineTool_local_Y", &StripTdsOfflineTool_local_Y, &b_StripTdsOfflineTool_local_Y);
   fChain->SetBranchAddress("StripTdsOfflineTool_layer", &StripTdsOfflineTool_layer, &b_StripTdsOfflineTool_layer);
   fChain->SetBranchAddress("StripTdsOfflineTool_isSmall", &StripTdsOfflineTool_isSmall, &b_StripTdsOfflineTool_isSmall);
   fChain->SetBranchAddress("StripTdsOfflineTool_eta", &StripTdsOfflineTool_eta, &b_StripTdsOfflineTool_eta);
   fChain->SetBranchAddress("StripTdsOfflineTool_phi", &StripTdsOfflineTool_phi, &b_StripTdsOfflineTool_phi);
   fChain->SetBranchAddress("StripTdsOfflineTool_readStrip", &StripTdsOfflineTool_readStrip, &b_StripTdsOfflineTool_readStrip);
   fChain->SetBranchAddress("StripTdsOfflineTool_channel", &StripTdsOfflineTool_channel, &b_StripTdsOfflineTool_channel);
   fChain->SetBranchAddress("StripTdsOfflineTool_BCID", &StripTdsOfflineTool_BCID, &b_StripTdsOfflineTool_BCID);
   fChain->SetBranchAddress("StripTdsOfflineTool_wedge", &StripTdsOfflineTool_wedge, &b_StripTdsOfflineTool_wedge);
   fChain->SetBranchAddress("StripTdsOfflineTool_time", &StripTdsOfflineTool_time, &b_StripTdsOfflineTool_time);
   fChain->SetBranchAddress("StripClusterTool_cl_n", &StripClusterTool_cl_n, &b_StripClusterTool_cl_n);
   fChain->SetBranchAddress("StripClusterTool_cl_charge", &StripClusterTool_cl_charge, &b_StripClusterTool_cl_charge);
   fChain->SetBranchAddress("StripClusterTool_cl_x", &StripClusterTool_cl_x, &b_StripClusterTool_cl_x);
   fChain->SetBranchAddress("StripClusterTool_cl_y", &StripClusterTool_cl_y, &b_StripClusterTool_cl_y);
   fChain->SetBranchAddress("StripClusterTool_cl_z", &StripClusterTool_cl_z, &b_StripClusterTool_cl_z);
   fChain->SetBranchAddress("StripClusterTool_cl_lx", &StripClusterTool_cl_lx, &b_StripClusterTool_cl_lx);
   fChain->SetBranchAddress("StripClusterTool_cl_ly", &StripClusterTool_cl_ly, &b_StripClusterTool_cl_ly);
   fChain->SetBranchAddress("StripClusterTool_cl_lz", &StripClusterTool_cl_lz, &b_StripClusterTool_cl_lz);
   fChain->SetBranchAddress("StripClusterTool_cl_ltgx", &StripClusterTool_cl_ltgx, &b_StripClusterTool_cl_ltgx);
   fChain->SetBranchAddress("StripClusterTool_cl_ltgy", &StripClusterTool_cl_ltgy, &b_StripClusterTool_cl_ltgy);
   fChain->SetBranchAddress("StripClusterTool_cl_ltgz", &StripClusterTool_cl_ltgz, &b_StripClusterTool_cl_ltgz);
   fChain->SetBranchAddress("StripClusterTool_cl_size", &StripClusterTool_cl_size, &b_StripClusterTool_cl_size);
   fChain->SetBranchAddress("StripClusterTool_cl_isSmall", &StripClusterTool_cl_isSmall, &b_StripClusterTool_cl_isSmall);
   fChain->SetBranchAddress("StripClusterTool_cl_side", &StripClusterTool_cl_side, &b_StripClusterTool_cl_side);
   fChain->SetBranchAddress("StripClusterTool_cl_wedge", &StripClusterTool_cl_wedge, &b_StripClusterTool_cl_wedge);
   fChain->SetBranchAddress("StripClusterTool_cl_sector", &StripClusterTool_cl_sector, &b_StripClusterTool_cl_sector);
   fChain->SetBranchAddress("StripClusterTool_cl_module", &StripClusterTool_cl_module, &b_StripClusterTool_cl_module);
   fChain->SetBranchAddress("StripClusterTool_cl_layer", &StripClusterTool_cl_layer, &b_StripClusterTool_cl_layer);
   fChain->SetBranchAddress("StripClusterTool_cl_bandId", &StripClusterTool_cl_bandId, &b_StripClusterTool_cl_bandId);
   fChain->SetBranchAddress("StripClusterTool_cl_phiId", &StripClusterTool_cl_phiId, &b_StripClusterTool_cl_phiId);
   fChain->SetBranchAddress("StripClusterTool_cl_truth_x", &StripClusterTool_cl_truth_x, &b_StripClusterTool_cl_truth_x);
   fChain->SetBranchAddress("StripClusterTool_cl_truth_y", &StripClusterTool_cl_truth_y, &b_StripClusterTool_cl_truth_y);
   fChain->SetBranchAddress("StripClusterTool_cl_truth_z", &StripClusterTool_cl_truth_z, &b_StripClusterTool_cl_truth_z);
   fChain->SetBranchAddress("StripClusterTool_cl_truth_lx", &StripClusterTool_cl_truth_lx, &b_StripClusterTool_cl_truth_lx);
   fChain->SetBranchAddress("StripClusterTool_cl_truth_ly", &StripClusterTool_cl_truth_ly, &b_StripClusterTool_cl_truth_ly);
   fChain->SetBranchAddress("StripClusterTool_cl_truth_lz", &StripClusterTool_cl_truth_lz, &b_StripClusterTool_cl_truth_lz);
   fChain->SetBranchAddress("StripClusterTool_cl_truth_E", &StripClusterTool_cl_truth_E, &b_StripClusterTool_cl_truth_E);
   fChain->SetBranchAddress("StripClusterTool_cl_truth_n", &StripClusterTool_cl_truth_n, &b_StripClusterTool_cl_truth_n);
   fChain->SetBranchAddress("StripSegmentTool_seg_theta", &StripSegmentTool_seg_theta, &b_StripSegmentTool_seg_theta);
   fChain->SetBranchAddress("StripSegmentTool_seg_dtheta", &StripSegmentTool_seg_dtheta, &b_StripSegmentTool_seg_dtheta);
   fChain->SetBranchAddress("StripSegmentTool_seg_dtheta_int", &StripSegmentTool_seg_dtheta_int, &b_StripSegmentTool_seg_dtheta_int);
   fChain->SetBranchAddress("StripSegmentTool_seg_eta", &StripSegmentTool_seg_eta, &b_StripSegmentTool_seg_eta);
   fChain->SetBranchAddress("StripSegmentTool_seg_eta_inf", &StripSegmentTool_seg_eta_inf, &b_StripSegmentTool_seg_eta_inf);
   fChain->SetBranchAddress("StripSegmentTool_seg_phi", &StripSegmentTool_seg_phi, &b_StripSegmentTool_seg_phi);
   fChain->SetBranchAddress("StripSegmentTool_seg_global_r", &StripSegmentTool_seg_global_r, &b_StripSegmentTool_seg_global_r);
   fChain->SetBranchAddress("StripSegmentTool_seg_global_x", &StripSegmentTool_seg_global_x, &b_StripSegmentTool_seg_global_x);
   fChain->SetBranchAddress("StripSegmentTool_seg_global_y", &StripSegmentTool_seg_global_y, &b_StripSegmentTool_seg_global_y);
   fChain->SetBranchAddress("StripSegmentTool_seg_global_z", &StripSegmentTool_seg_global_z, &b_StripSegmentTool_seg_global_z);
   fChain->SetBranchAddress("StripSegmentTool_seg_dir_r", &StripSegmentTool_seg_dir_r, &b_StripSegmentTool_seg_dir_r);
   fChain->SetBranchAddress("StripSegmentTool_seg_dir_y", &StripSegmentTool_seg_dir_y, &b_StripSegmentTool_seg_dir_y);
   fChain->SetBranchAddress("StripSegmentTool_seg_dir_z", &StripSegmentTool_seg_dir_z, &b_StripSegmentTool_seg_dir_z);
   fChain->SetBranchAddress("StripSegmentTool_seg_bandId", &StripSegmentTool_seg_bandId, &b_StripSegmentTool_seg_bandId);
   fChain->SetBranchAddress("StripSegmentTool_seg_phiId", &StripSegmentTool_seg_phiId, &b_StripSegmentTool_seg_phiId);
   fChain->SetBranchAddress("StripSegmentTool_seg_rIdx", &StripSegmentTool_seg_rIdx, &b_StripSegmentTool_seg_rIdx);
   fChain->SetBranchAddress("StripSegmentTool_seg_wedge1_size", &StripSegmentTool_seg_wedge1_size, &b_StripSegmentTool_seg_wedge1_size);
   fChain->SetBranchAddress("StripSegmentTool_seg_wedge2_size", &StripSegmentTool_seg_wedge2_size, &b_StripSegmentTool_seg_wedge2_size);
   Notify();
}

Bool_t TrigTree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void TrigTree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t TrigTree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef TrigTree_cxx
