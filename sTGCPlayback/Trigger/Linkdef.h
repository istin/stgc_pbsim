#include <Trigger/sTGCTrigger.h>
#include <Trigger/TrigTree.h>
#include <Trigger/TrigTreePRD.h>
#include <Trigger/SingleWedgeTrigger.h>

#include <vector>

#ifdef __ROOTCLING__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;


#pragma link C++ class sTGCTrigger+;
#pragma link C++ class SingleWedgeTrigger+;
#pragma link C++ class MMARTData+;

#pragma link C++ class TrigTree+;
#pragma link C++ class TrigTreePRD+;


#endif
