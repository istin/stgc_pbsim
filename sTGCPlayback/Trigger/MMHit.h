#include "TString.h"
#include <iostream>
#include <vector>

#ifndef MMHit_H
#define MMHit_H

struct MMHit {
  int index;
  int stationEta;
  int stationPhi;
  int multiplet;
  int gas_gap;
  int channel;
  float time;
  float charge;
  // --------
  int channel_global;
  int channel_mmfe8;
  int channel_vmm;
  int layer;
  int bcid;
  int vmm;
  int mmfe8;
  int addc;
  int artasic; // 00-03: ART chip on this layer
  int artlink; // 00-03: MMFE8 on this ART chip
  int artvmm;  // 00-31: VMM on this ART chip
  int isLarge;
  int isAside;
  // -----------
  TString NameForStore;

  MMHit(){
    index = 0;
    stationEta = 0;
    stationPhi = 0;
    multiplet = 0;
    gas_gap = 0;
    channel = 0;
    time = 0;
    charge = 0;
    channel_global = 0;
    channel_mmfe8 = 0;
    channel_vmm = 0;
    layer = 0;
    bcid = 0;
    vmm = 0;
    mmfe8 = 0;
    addc = 0;
    artasic = 0;
    artlink = 0;
    artvmm = 0;
    isLarge = 0;
    isAside = 0;
    NameForStore = "";
  }

};

#endif
