#include <iostream>
#include <vector>
#include <memory>
#include <set>
#include "SingleWedgeTrigger.h"
#include <Detector/DetSvc.h>


#ifndef sTGCTrigger_H
#define sTGCTrigger_H

class TrigTree;






class sTGCTrigger{
  
    public :     
    
        sTGCTrigger();//default ctor
        
        //you can define various fetchPadTrigger(T* t) and use them if you fetch data from different sources
        
        
        sTGCTrigger& operator +=(const sTGCTrigger&);
        bool operator ==( const sTGCTrigger&  );   
        
        void dump() const;
        
        size_t key()const{
            return DetSvc::hashElement({this->side(),this->sector()});
        }

        int bandID()const{ return wedgeTriggers().at(0)->selectedBands().at(0);}

        void fetchPadTrigger(TrigTree* ,int);//fetch data from Default Makefile class of Ntuples
        
        int size()const{
            int size=1?this->isLargeSector():0; 
            return size;
        }
        //Thesere just for ath2offline conversion
        int sector()const{
            if( this->isLargeSector() ){
                return 2*this->sectorID()-1;
            }
            else{
                return 2*this->sectorID();
           }
        }

        int side()const{
            return this->sideID();
        }
        std::vector<std::shared_ptr<SingleWedgeTrigger>> wedgeTriggers() const { return m_wedgeTrigs;}

        int BCID() const { return m_BCID;}
        int sideID() const { return m_sideID;}
        int sectorID() const { return m_sectorID;}
    
        int moduleIDInner() const{

            return m_moduleIdInner;
        }
        int moduleIDOuter() const {
            return m_moduleIdOuter;
        }
        
        bool isLargeSector() const { return m_isLargeSector;}
        bool isinTransition() const {
            if(m_wedgeTrigs.size()==1){
                return true;
            }
            
            else if(m_wedgeTrigs.size()==2){
                if(m_wedgeTrigs.at(0)->moduleId()!=m_wedgeTrigs.at(1)->moduleId()){
                    return true;
                    
                }
            }
            else{
                std::cerr<<"ERROR Trigger has "<<m_wedgeTrigs.size()<<" wedges!"<<std::endl;
                exit(8);
            }
            
            return false;
            
        }
        
        bool isSingle(){
            if(m_wedgeTrigs.size()==0) return true;
            return false;
        }
        

        
        int phiId()const{return m_padTrigPhiId;}
        int Rindex() const { return m_Rindex;}
        int dTheta() const { return m_dTheta;}        
    
    private :
        void FillTriggerStripInfo(TrigTree* );

        //Pad Triggger Related stuff
        int m_BCID;
        int m_sideID;
        int m_sectorID;
        bool m_isLargeSector;
        bool m_isTransition;
        int m_moduleIdInner;
        int m_moduleIdOuter;
        
        std::vector<std::shared_ptr<SingleWedgeTrigger>> m_wedgeTrigs;
        
        float  m_padTrigGlobalEtaCntr;
        float m_padTrigGlobalPhiCntr;
        int m_padTrigPhiId;
        //segment related stuff
        // ***** IMPORTANT ******************************
        //These should match to the number of bits in the HW implementation. Talk to HW people.
        /**** Needed for Trogger EDM ************/
        int m_Rindex;
        float m_dTheta;//exists in ntuples / validate / probably calculated wrong as everything else that was done before
    
    /******* Talk to digi guys to fix SDOs in digitization. I guess we dont have any truth related info inside RDOs yet ***/
};






#endif
