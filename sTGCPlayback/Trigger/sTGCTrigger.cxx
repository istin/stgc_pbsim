#include "sTGCTrigger.h"
#include "TrigTree.h"
#include <algorithm>




sTGCTrigger::sTGCTrigger(){ //default ctor
    
    m_BCID=-1;
    m_sideID=-1;
    m_sectorID=-1;

    m_isLargeSector=false;
    m_isTransition=false;
    
     m_padTrigGlobalEtaCntr=-1;
     m_padTrigGlobalPhiCntr=-1;
        /*** Needed for trigger EDM *****/
     m_padTrigPhiId=-1;
     //segment related stuff
     // ***** IMPORTANT ******************************
     //These should match to the number of bits in the HW implementation. Talk to HW people.
     /**** Needed for Trogger EDM ************/
     m_Rindex=-1;
     m_dTheta=-1;
}




void sTGCTrigger::dump() const{
            
            std::string sector=std::to_string(this->sectorID());
            bool isInTr=false;
            if(this->wedgeTriggers().size()==2){
                if(this->wedgeTriggers().at(0)->moduleId()!=this->wedgeTriggers().at(1)->moduleId()){
                    isInTr=true;
                }
            }
            else{
                isInTr=true;
            }
            std::string isTrDoubleStr="";
            if(isInTr){
                isTrDoubleStr+=" inTransition ";
            }
            std::cout<<"********* Trigger in sector "<<sector<<isTrDoubleStr<<" ********************************"<<std::endl;
            for(const auto& swt : this->wedgeTriggers() ){//inner outer

                int i=0;
                for( const auto& iLayer : swt->selectedLayers() ){
                    int ieta=swt->selectedEtaIndices().at(i);
                    int iphi=swt->selectedPhiIndices().at(i);
                    
                    int bandId=swt->selectedBands().at(i);
                    int wedge=swt->wedgeId();
                    
                    //int firstch_str=nstrips!=0 ? *std::min_element(swt->selectedStripChannels().at(i).begin(),swt->selectedStripChannels().at(i).end()) :0;//v.at(0);
                    //int lastch_str=nstrips!=0 ? *std::max_element(swt->selectedStripChannels().at(i).begin(),swt->selectedStripChannels().at(i).end()) :0;//v.at(v.size()-1);   
                    int n=swt->selectedStripChannels().at(i).size();
                    int  firstch_str=0;
                    int lastch_str=0;
                    int nstrips=0;                   
                    if(n!=0){
                        firstch_str=swt->selectedStripChannels().at(i).at(0).first;
                        lastch_str=swt->selectedStripChannels().at(i).at(n-1).first;
                        nstrips=lastch_str-firstch_str+1;
                    }
                    std::cout<<"    swt ==> "<<" module= "<<swt->moduleId()<<" wedge="<< wedge<<" ieta="<<ieta<<" iphi="<<iphi<<" lyr="<<iLayer<<" band="<<bandId<<" str1="<<firstch_str<<" strlast="<<lastch_str<<" nstrips="<<nstrips<<std::endl;
                    i++;
                }
               
            }//inner outer
             std::cout<<"*************************************************************"<<std::endl;
             
            return ;
}



void sTGCTrigger::fetchPadTrigger(TrigTree* nsw,int iTrigger){
    if(nsw==nullptr){
        std::cerr<<"Error fetching data! The source not initialized"<<std::endl;
        exit(6);
    }
    m_BCID=nsw->padTriggerBCID->at(iTrigger);
    m_sectorID=nsw->padTriggerSectorID->at(iTrigger);
    m_sideID=nsw->padTriggerSideID->at(iTrigger);
    m_moduleIdInner=nsw->padTriggerModuleIDinner->at(iTrigger);
    m_moduleIdOuter=nsw->padTriggerModuleIDouter->at(iTrigger);
    m_isLargeSector=nsw->padTriggerSectorType->at(iTrigger);
    m_padTrigPhiId=nsw->padTriggerPhiID->at(iTrigger);

    if(m_moduleIdInner>0){
        auto inner= std::make_shared<SingleWedgeTrigger>();
        inner->setSelectedLayers(nsw->padTriggerSelectedLayersInner->at(iTrigger));
        inner->setSelectedEtaIndices(nsw->padTriggerPadEtaIndicesInner->at(iTrigger));
        inner->setSelectedPhiIndices(nsw->padTriggerPadPhiIndicesInner->at(iTrigger));
        inner->setBandLocalMinY(nsw->padTriggerlocalminYInner->at(iTrigger));
        inner->setBandLocalMaxY(nsw->padTriggerlocalmaxYInner->at(iTrigger));
        inner->setModuleId(nsw->padTriggerModuleIDinner->at(iTrigger));
        inner->setWedgeId(1);
        inner->setIsSmall(!nsw->padTriggerSectorType->at(iTrigger));
        inner->setBands(nsw->padTriggerSelectedBandsInner->at(iTrigger));

        
        m_wedgeTrigs.push_back(std::move(inner));
    }
    if(m_moduleIdOuter>0){
        auto outer=std::make_shared<SingleWedgeTrigger>();
        outer->setSelectedLayers(nsw->padTriggerSelectedLayersOuter->at(iTrigger));
        outer->setSelectedEtaIndices(nsw->padTriggerPadEtaIndicesOuter->at(iTrigger));
        outer->setSelectedPhiIndices(nsw->padTriggerPadPhiIndicesOuter->at(iTrigger));
        outer->setBandLocalMinY(nsw->padTriggerlocalminYOuter->at(iTrigger));
        outer->setBandLocalMaxY(nsw->padTriggerlocalmaxYOuter->at(iTrigger));
        outer->setModuleId(nsw->padTriggerModuleIDouter->at(iTrigger));
        outer->setWedgeId(2);
        outer->setIsSmall(!nsw->padTriggerSectorType->at(iTrigger));
        outer->setBands(nsw->padTriggerSelectedBandsOuter->at(iTrigger));
        m_wedgeTrigs.push_back(std::move(outer));

    }
    this->FillTriggerStripInfo(nsw);
}




void sTGCTrigger::FillTriggerStripInfo(TrigTree* nsw){
   
    const int nstripHits=nsw->StripTdsOfflineTool_nStripHits;

    //for(const auto & trg : m_trgbuffer ){
        for(const auto & swt : this->wedgeTriggers() ){
            int idx=0; //layer index            
            for( const auto& layer : swt->selectedLayers() ){
                float downY=swt->bandLocalMinY().at(idx);
                float upY=swt->bandLocalMaxY().at(idx);
                std::vector<std::pair<int,float>> strip_channels;
                //****TODO: Strip-trigger matching can be refactored ****/
                // *** Match strips to the trigger ********/
                for(int istrip=0;istrip<nstripHits;istrip++){
                   
                    int strchannel=nsw->StripTdsOfflineTool_channel->at(istrip);
                    float charge6bit=nsw->StripTdsOfflineTool_charge_6bit->at(istrip);
                    int strip_sector=nsw->StripTdsOfflineTool_phi->at(istrip);
                    int strip_module=nsw->StripTdsOfflineTool_eta->at(istrip);
                    int strip_wedge=nsw->StripTdsOfflineTool_wedge->at(istrip);
                    int strip_lyr=nsw->StripTdsOfflineTool_layer->at(istrip);
                    int strip_isSmall=nsw->StripTdsOfflineTool_isSmall->at(istrip);
                    if( strip_wedge!=swt->wedgeId() ||
                        strip_sector!=this->sectorID() 
                        || strip_module!=swt->moduleId()
                        || strip_isSmall==this->isLargeSector()
                        || strip_lyr!=layer
                    ){
                        continue;
                    }
                    float stripLocY=nsw->StripTdsOfflineTool_local_X->at(istrip);
                    if(stripLocY<downY-3.2 || stripLocY > upY+3.2){
                        continue;
                    }
                    
                    bool read=nsw->StripTdsOfflineTool_readStrip->at(istrip);
                    if(!read){
                        continue;
                    }
                    strip_channels.emplace_back(std::make_pair(strchannel,charge6bit));//use a proper container as there are duplicate strip channels
                }//strips
                
                //sort selected channels accordint to channel number
                std::sort(strip_channels.begin(),strip_channels.end(),
                          [&](const std::pair<int,float>&a,const std::pair<int,float>& b){return a.first<b.first;}
                         );
                swt->addStripChannels(strip_channels);
                idx++;
            }//layers
        }//inner-outer
    //}//trigs
    
    return;
}