#include "MMARTData.h"
#include "TrigTreePRD.h"
#include <algorithm>


MMARTData::MMARTData(){
  m_art_stationEta = {};
  m_all_stationEta = {};
  m_art_stationPhi = {};
  m_all_stationPhi = {};
  m_art_multiplet = {};
  m_all_multiplet = {};
  m_art_gas_gap = {};
  m_all_gas_gap = {};
  m_art_channel = {};
  m_all_channel = {};
  m_art_time = {};
  m_all_time = {};
  m_art_charge = {};
  m_all_charge = {};
}

int MMARTData::dump() const{

  std::cout << "Run number   " << GetRunNumber()   << std::endl;
  std::cout << "Event number " << GetEventNumber() << std::endl;
  std::cout << "ART hits:    " << std::endl;
  for (auto hit: ARTHits()){
    std::cout << " - index          " << hit->index          << std::endl;
    std::cout << " - channel        " << hit->channel        << std::endl;
    std::cout << " - gas_gap        " << hit->gas_gap        << std::endl;
    std::cout << " - multiplet      " << hit->multiplet      << std::endl;
    std::cout << " - stationEta     " << hit->stationEta     << std::endl;
    std::cout << " - stationPhi     " << hit->stationPhi     << std::endl;
    std::cout << " - time           " << hit->time           << std::endl;
    std::cout << " - charge         " << hit->charge         << std::endl;
    std::cout << " - channel_global " << hit->channel_global << std::endl;
    std::cout << " - channel_mmfe8  " << hit->channel_mmfe8  << std::endl;
    std::cout << " - channel_vmm    " << hit->channel_vmm    << std::endl;
    std::cout << " - layer          " << hit->layer          << std::endl;
    std::cout << " - vmm            " << hit->vmm            << std::endl;
    std::cout << " - mmfe8          " << hit->mmfe8          << std::endl;
    std::cout << " - artasic        " << hit->artasic        << std::endl;
    std::cout << " - artlink        " << hit->artlink        << std::endl;
    std::cout << " - artvmm         " << hit->artvmm         << std::endl;
    std::cout << " - addc           " << hit->addc           << std::endl;
    std::cout << " - bcid           " << hit->bcid           << std::endl;
    std::cout << " - NameForStore   " << hit->NameForStore   << std::endl;
    std::cout << std::endl;
  }

  return 0;
}

int MMARTData::fetchARTData(TrigTreePRD* tree){

  if(tree==nullptr){
    std::cerr << "Error fetching data! The source not initialized" << std::endl;
    exit(6);
    return 1;
  }

  reset();

  m_runNumber   = tree->runNumber;
  m_eventNumber = tree->eventNumber;

  // hits
  for (size_t i = 0; i < tree->Digits_MM; i++) {

    // ART hits
    for (size_t j = 0; j < tree->Digits_MM_position_trigger->at(i) .size(); j++) {
      m_art_channel    .push_back( (*tree->Digits_MM_position_trigger) [i][j]);
      m_art_time       .push_back( (*tree->Digits_MM_time_trigger)     [i][j]);
      m_art_charge     .push_back( (*tree->Digits_MM_charge_trigger)   [i][j]);
      m_art_stationEta .push_back( (*tree->Digits_MM_stationEta)       [i]);
      m_art_stationPhi .push_back( (*tree->Digits_MM_stationPhi)       [i]);
      m_art_multiplet  .push_back( (*tree->Digits_MM_multiplet)        [i]);
      m_art_gas_gap    .push_back( (*tree->Digits_MM_gas_gap)          [i]);
    }
    
    // All hits
    for (size_t j = 0; j < tree->Digits_MM_stripPosition->at(i) .size(); j++) {
      m_all_channel    .push_back( (*tree->Digits_MM_stripPosition)    [i][j]);
      m_all_time       .push_back( (*tree->Digits_MM_time)             [i][j]);
      m_all_charge     .push_back( (*tree->Digits_MM_charge)           [i][j]);
      m_all_stationEta .push_back( (*tree->Digits_MM_stationEta)       [i]);
      m_all_stationPhi .push_back( (*tree->Digits_MM_stationPhi)       [i]);
      m_all_multiplet  .push_back( (*tree->Digits_MM_multiplet)        [i]);
      m_all_gas_gap    .push_back( (*tree->Digits_MM_gas_gap)          [i]);
    }
    
  }

  fillHits();
  return 0;

}

int MMARTData::fillHits(){

  m_art_hits.clear();
  m_all_hits.clear();

  for (size_t i = 0; i < m_art_channel.size(); i++){
    auto hit = std::make_shared<MMHit>();
    hit->index      = i;
    hit->stationEta = m_art_stationEta[i];
    hit->stationPhi = m_art_stationPhi[i];
    hit->multiplet  = m_art_multiplet [i];
    hit->gas_gap    = m_art_gas_gap   [i];
    hit->channel    = m_art_channel   [i];
    hit->time       = m_art_time      [i];
    hit->charge     = m_art_charge    [i];
    // ---------------
    // Order matters.
    // ---------------
    hit->channel_global = derive_channel_global(hit);
    hit->channel_mmfe8  = derive_channel_mmfe8(hit);
    hit->channel_vmm    = derive_channel_vmm(hit);
    hit->layer          = derive_layer(hit);
    hit->bcid           = derive_bcid(hit);
    hit->vmm            = derive_vmm(hit);
    hit->mmfe8          = derive_mmfe8(hit);
    hit->addc           = derive_addc(hit);
    hit->artasic        = derive_artasic(hit);
    hit->artlink        = derive_artlink(hit);
    hit->artvmm         = derive_artvmm(hit);
    hit->isLarge        = derive_isLarge(hit);
    hit->isAside        = derive_isAside(hit);
    // ---------------
    hit->NameForStore = derive_NameForStore(hit);
    m_art_hits.push_back(hit);
  }

  for (size_t i = 0; i < m_all_channel.size(); i++){
    auto hit = std::make_shared<MMHit>();
    hit->index      = i;
    hit->stationEta = m_all_stationEta[i];
    hit->stationPhi = m_all_stationPhi[i];
    hit->multiplet  = m_all_multiplet [i];
    hit->gas_gap    = m_all_gas_gap   [i];
    hit->channel    = m_all_channel   [i];
    hit->time       = m_all_time      [i];
    hit->charge     = m_all_charge    [i];
    // ---------------
    // Order matters.
    // ---------------
    hit->channel_global = derive_channel_global(hit);
    hit->channel_mmfe8  = derive_channel_mmfe8(hit);
    hit->channel_vmm    = derive_channel_vmm(hit);
    hit->layer          = derive_layer(hit);
    hit->bcid           = derive_bcid(hit);
    hit->vmm            = derive_vmm(hit);
    hit->mmfe8          = derive_mmfe8(hit);
    hit->addc           = derive_addc(hit);
    hit->artasic        = derive_artasic(hit);
    hit->artlink        = derive_artlink(hit);
    hit->artvmm         = derive_artvmm(hit);
    hit->isLarge        = derive_isLarge(hit);
    hit->isAside        = derive_isAside(hit);
    // ---------------
    hit->NameForStore = derive_NameForStore(hit);
    m_all_hits.push_back(hit);
  }

  return 0;
}

int MMARTData::derive_channel_global(std::shared_ptr<MMHit> hit){
  return hit->channel + (hit->stationEta - 1)*MMFE8_PER_M1*CHANNEL_PER_MMFE8;
}

int MMARTData::derive_channel_mmfe8(std::shared_ptr<MMHit> hit){
  return hit->channel % CHANNEL_PER_MMFE8;
}

int MMARTData::derive_channel_vmm(std::shared_ptr<MMHit> hit){
  return hit->channel % CHANNEL_PER_VMM;
}

int MMARTData::derive_layer(std::shared_ptr<MMHit> hit){
  return (hit->gas_gap-1) + (hit->multiplet-1)*GAS_GAP_PER_MULTIPLET;
}

int MMARTData::derive_bcid(std::shared_ptr<MMHit> hit){
  return (int)(hit->time) / 25;
}

int MMARTData::derive_vmm(std::shared_ptr<MMHit> hit){
  return (hit->channel_global / CHANNEL_PER_VMM) % VMM_PER_MMFE8;
}

int MMARTData::derive_mmfe8(std::shared_ptr<MMHit> hit){
  return hit->channel_global / CHANNEL_PER_MMFE8;
}

int MMARTData::derive_addc(std::shared_ptr<MMHit> hit){
  return hit->channel_global / (CHANNEL_PER_MMFE8 * MMFE8_PER_ADDC);
}

int MMARTData::derive_artasic(std::shared_ptr<MMHit> hit){
  return hit->channel_global / (CHANNEL_PER_MMFE8 * MMFE8_PER_ARTASIC);
}

int MMARTData::derive_artlink(std::shared_ptr<MMHit> hit){
  return hit->mmfe8 % 4;
}

int MMARTData::derive_artvmm(std::shared_ptr<MMHit> hit){
  return hit->vmm + hit->artlink * VMM_PER_MMFE8;
}

int MMARTData::derive_isLarge(std::shared_ptr<MMHit> hit){
  // odd Large, even Small
  return (hit->stationPhi % 2 == 1) ? 1 : 0;
}

int MMARTData::derive_isAside(std::shared_ptr<MMHit> hit){
  // FIX ME PLZ
  return 1;
}

TString MMARTData::derive_NameForStore(std::shared_ptr<MMHit> hit){
  return Form("MM_MMFE8_Link%i_layer_%i_%s_%s_%i", 
              hit->artlink, hit->layer, hit->isLarge ? "L":"S", hit->isAside ? "A":"C", hit->stationPhi);
}

int MMARTData::FirstBCID(){
  int obj = 1e6;
  for (auto hit: ARTHits())
    obj = std::min(obj, hit->bcid);
  return obj;
}

int MMARTData::LastBCID(){
  int obj = 0;
  for (auto hit: ARTHits())
    obj = std::max(obj, hit->bcid);
  return obj;
}

int MMARTData::reset(){
  m_art_stationEta.clear();
  m_all_stationEta.clear();
  m_art_stationPhi.clear();
  m_all_stationPhi.clear();
  m_art_multiplet.clear();
  m_all_multiplet.clear();
  m_art_gas_gap.clear();
  m_all_gas_gap.clear();
  m_art_channel.clear();
  m_all_channel.clear();
  m_art_time.clear();
  m_all_time.clear();
  m_art_charge.clear();
  m_all_charge.clear();
  return 0;
}
