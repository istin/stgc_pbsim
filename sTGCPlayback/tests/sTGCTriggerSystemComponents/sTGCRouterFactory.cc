/*
 * sTGCBCSignal.cc
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

// Copyright 2006, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include "gtest/gtest.h"
#include "sTGCTriggerSystemComponents/sTGCRouter.h"
#include "sTGCTriggerSystemComponents/sTGCRouterFactory.h"
#include "CablingModel/sTGCRouterConfiguration.h"


// Tests that we don't have to define main() when we link to
// gtest_main instead of gtest.

namespace {

TEST(sTGCRouterFactory, factory_lookup){

	sTGCRouterConfiguration config;
	config.latency = 100;
	config.type = "dummy";
	config.clock = 160;
	for (int i_ch = 0; i_ch < 12; i_ch++)
	{
		config.delay[i_ch] = (i_ch * 10);
		config.serializerBank[i_ch] = i_ch / 4;
		config.serializerBankChannel[i_ch] = i_ch % 4;
	}

	std::map<TString, sTGCRouterConfiguration> rtrLookup;
	rtrLookup["dummy"] = config;

	sTGCRouterFactory factory;
	factory.SetLookup(rtrLookup);
	std::map<TString, sTGCRouterConfiguration> obtaineLookup = factory.GetLookup();

	ASSERT_EQ(obtaineLookup.size(), 1u);
	std::map<TString, sTGCRouterConfiguration>::iterator itr = obtaineLookup.find("dummy");
	ASSERT_NE(itr,  obtaineLookup.end());
	ASSERT_EQ(itr->second.type, "dummy");
	ASSERT_EQ(itr->second.latency, 100);
	ASSERT_EQ(itr->second.clock, 160);
	for (int i_ch = 0; i_ch < 12; i_ch++)
	{
		ASSERT_EQ(itr->second.delay[i_ch],i_ch * 10);
		ASSERT_EQ(itr->second.serializerBank[i_ch],i_ch / 4);
		ASSERT_EQ(itr->second.serializerBankChannel[i_ch],i_ch % 4);
	}


}


TEST(sTGCRouterFactory, create){

	sTGCRouterConfiguration config;
	config.latency = 100;
	config.type = "dummy";
	config.clock = 160;
	for (int i_ch = 0; i_ch < 12; i_ch++)
	{
		config.delay[i_ch] = (i_ch * 10);
		config.serializerBank[i_ch] = i_ch / 4;
		config.serializerBankChannel[i_ch] = i_ch % 4;
	}

	std::map<TString, sTGCRouterConfiguration> rtrLookup;
	rtrLookup["dummy"] = config;

	sTGCRouterFactory factory;
	factory.SetLookup(rtrLookup);

	IComponent* component = factory.Create("Router", "dummy", "dummy1");

	ASSERT_EQ(component->GetClass(), "Router");
	ASSERT_EQ(component->GetType(), "dummy");
	ASSERT_EQ(component->GetName(), "dummy1");

	sTGCRouter* rtr = dynamic_cast<sTGCRouter*>(component);
	ASSERT_EQ(rtr->GetLatency(), 100);
	ASSERT_EQ(rtr->GetClockFrequency(), 160);
	for (int i_ch = 0; i_ch < 12; i_ch++)
	{
		int bank, channel;
		rtr->GetSerializerForInput(i_ch,bank, channel);
		ASSERT_EQ(bank, i_ch / 4);
		ASSERT_EQ(channel, i_ch % 4);
		ASSERT_EQ(rtr->GetDelayForInput(i_ch), i_ch * 10);
	}
}

}  // namespace





