/*
 * sTGCBCSignal.cc
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

// Copyright 2006, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include "gtest/gtest.h"
#include "CablingModel/sTGCsABConfiguration.h"
#include "sTGCTriggerSystemComponents/sTGCsABFactory.h"
#include "sTGCTriggerSystemComponents/sTGCsAB.h"

// Tests that we don't have to define main() when we link to
// gtest_main instead of gtest.

namespace {

TEST(sTGCsABFactory, lookup){

	sTGCsABConfiguration config;
	config.direction = DIR_WRT_DET::ANTIPARALLEL;
	config.firstChannelDisconnected.push_back(10);
	config.nActiveChannels = 412;
	config.nChannelsDisconnected.push_back(100);
	config.layer = 1;
	config.type = "a_type";

	std::map<TString, sTGCsABConfiguration> lookup;
	lookup["a_type"] = config;

	sTGCsABFactory factory;
	factory.SetLookup(lookup);

	std::map<TString, sTGCsABConfiguration> retrieved = factory.GetLookup();

	std::map<TString, sTGCsABConfiguration>::iterator itr = retrieved.find("a_type");
	ASSERT_EQ(retrieved.size(), 1u);
	ASSERT_NE(itr, retrieved.end());

	sTGCsABConfiguration config2 = itr->second;
	ASSERT_EQ(config2.direction, DIR_WRT_DET::ANTIPARALLEL);
	ASSERT_EQ(config2.firstChannelDisconnected.size(), 1u);
	ASSERT_EQ(config2.nChannelsDisconnected.size(), 1u);
	ASSERT_EQ(config2.firstChannelDisconnected[0], 10);
	ASSERT_EQ(config2.nChannelsDisconnected[0], 100);
	ASSERT_EQ(config2.layer,  1);
	ASSERT_EQ(config2.type, "a_type");
	ASSERT_EQ(config2.nActiveChannels, 412);
}

TEST(sTGCsABFactory, create){

	sTGCsABConfiguration config;
	config.direction = DIR_WRT_DET::ANTIPARALLEL;
	config.firstChannelDisconnected.push_back(10);
	config.nActiveChannels = 412;
	config.nChannelsDisconnected.push_back(100);
	config.layer = 1;
	config.type = "a_type";

	std::map<TString, sTGCsABConfiguration> lookup;
	lookup["a_type"] = config;

	sTGCsABFactory factory;
	factory.SetLookup(lookup);

	IComponent* component = factory.Create("sAB", "a_type", "dummy");

	ASSERT_NE((component != 0), 0);

	ASSERT_EQ(component->GetClass(), "sAB");
	ASSERT_EQ(component->GetType(), "a_type");
	ASSERT_EQ(component->GetName(), "dummy");

	sTGCsAB* sAB = dynamic_cast<sTGCsAB*>(component);
	ASSERT_NE((sAB != 0), 0);
	ASSERT_EQ(sAB->GetDirection(), DIR_WRT_DET::ANTIPARALLEL);
	ASSERT_EQ(sAB->GetNActiveChannels(), 412);
	int firstChannel = config.firstChannelDisconnected[0];
	int nChannels = config.nChannelsDisconnected[0];

	int ob_firstChannel;
	int ob_nChannels;
	sAB->GetDisconnectedBlock(0, ob_firstChannel, ob_nChannels);
	ASSERT_EQ(sAB->GetNDisconnectedBlocks(), 1u);
	ASSERT_EQ(ob_firstChannel, 10);
	ASSERT_EQ(ob_nChannels, 100);
	ASSERT_EQ(sAB->GetLayer(), 1);

}

}  // namespace


