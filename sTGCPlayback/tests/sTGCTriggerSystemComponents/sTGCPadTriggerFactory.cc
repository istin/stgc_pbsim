// Copyright 2006, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include "gtest/gtest.h"
#include "sTGCTriggerSystemComponents/sTGCPadTriggerFactory.h"
#include "sTGCTriggerSystemComponents/sTGCPadTrigger.h"


// Tests that we don't have to define main() when we link to
// gtest_main instead of gtest.

namespace {

TEST(sTGCPadTriggerFactory, set_lookup){
	sTGCPadTriggerConfiguration config;
	config.coincidenceTable = "external";
	config.latency = 1000.0;
	config.type = "external_type";
	std::map<TString, sTGCPadTriggerConfiguration> lookup;
	lookup[config.type] = config;

	sTGCPadTriggerFactory factory;
	factory.SetLookup(lookup);

	std::map<TString, sTGCPadTriggerConfiguration> obtainedLookup;
	obtainedLookup = factory.GetLookup();

	ASSERT_EQ(obtainedLookup.size(),1u);
	sTGCPadTriggerConfiguration obtainedConfig = obtainedLookup["external_type"];

	ASSERT_EQ(obtainedConfig.coincidenceTable, "external");
	ASSERT_EQ(obtainedConfig.latency, 1000.0);
	ASSERT_EQ(obtainedConfig.type, "external_type");

}

TEST(sTGCPadTriggerFactory, create){
	sTGCPadTriggerConfiguration config;
	config.coincidenceTable = "external";
	config.latency = 1000.0;
	config.type = "external_type";
	std::map<TString, sTGCPadTriggerConfiguration> lookup;
	lookup[config.type] = config;

	sTGCPadTriggerFactory factory;
	factory.SetLookup(lookup);

	std::map<TString, sTGCPadTriggerConfiguration> obtainedLookup;
	IComponent* ic = factory.Create("PadTrigger", "external_type", "instance1");
	ASSERT_EQ((ic != NULL), 1);
	ASSERT_EQ(ic->GetName(), "instance1");
	ASSERT_EQ(ic->GetType(), "external_type");
	ASSERT_EQ(ic->GetClass(), "PadTrigger");
	sTGCPadTrigger* pt = dynamic_cast<sTGCPadTrigger*>(ic);
	ASSERT_EQ((pt != NULL), 1);
	ASSERT_EQ(pt->GetLatency(), 1000.0);
	ASSERT_EQ(pt->GetCoincidenceTableName(),  "external");
}

}  // namespace



