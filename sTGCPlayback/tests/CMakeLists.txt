 
######
file(GLOB TEST_SRC_FILES ${CMAKE_CURRENT_SOURCE_DIR}/*.cc)
include_directories("${GOOGLE_TEST_SOURCES}/googletest/include")
include_directories("${PROJECT_SOURCE_DIR}/sTGCPlayback")
link_directories(${PLAYBACK_LIBS})
foreach(_test_file ${TEST_SRC_FILES})
    get_filename_component(_test_name ${_test_file} NAME_WE)
    #link_directories( "${PROJECT_BINARY_DIR}/external/googletest-build/lib/")
    link_directories( "${GOOGLE_TEST_LIBS}")
    add_executable(${_test_name} ${_test_file})
    add_dependencies(${_test_name} googletest)
    target_link_libraries(${_test_name} gtest gtest_main   CablingModel Components FrameWork GeoModel Simulation XMLUtils Detector sTGCConfigGen)
    add_test(${_test_name} ${_test_name})
    set_tests_properties(${_test_name} PROPERTIES TIMEOUT 5)
    install(TARGETS ${_test_name}
        DESTINATION "${INSTALLATION}/bin")
endforeach()

add_subdirectory(CablingModel)
add_subdirectory(sTGCConfigGeneration)
add_subdirectory(Components)
add_subdirectory(sTGCTriggerSystemComponents)
#######
