/*
 * CablingConfigurationParserTest.cxx
 *
 *  Created on: Feb 24, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 *  CHANGES:
 *
 */

#include "gtest/gtest.h"
#include "CablingModel/CablingConfigurationParser.h"
#include "TString.h"

TEST(CablingConfigurationParserTest, ParseSAB){

	const char* trp = std::getenv("TESTRESOURCESPATH");
	TString configFilePath = TString::Format("%s/CablingModel/sAB_test1.xml", trp);

	CablingConfigurationParser ccp;

	ccp.SetConfigurationFile(configFilePath);
	ccp.Parse();
	std::map<TString, sTGCsABConfiguration> sablkp = ccp.GetSABLookup();

	ASSERT_EQ(sablkp.size(),1u);

	std::map<TString, sTGCsABConfiguration>::iterator  itr = sablkp.find("sTG1-QL1P");

	ASSERT_EQ( (itr == sablkp.end()), false);

	sTGCsABConfiguration config = itr->second;
	/*
	 * <sAB type="sTG1-QL1P" layer="1" direction="ANTIPARALLEL"
		nActiveChannels="400" nChannelsDisconnected="50;62"
		firstChannelDisconnected="300;400"></sAB>
	 */
	ASSERT_EQ(config.type, "sTG1-QL1P");
	ASSERT_EQ(config.layer, 1);
	ASSERT_EQ(config.direction, DIR_WRT_DET::ANTIPARALLEL);
	ASSERT_EQ(config.nActiveChannels, 400);
	ASSERT_EQ(config.nChannelsDisconnected.size(), 2u);
	ASSERT_EQ(config.nChannelsDisconnected[0], 50);
	ASSERT_EQ(config.nChannelsDisconnected[1], 62);
	ASSERT_EQ(config.firstChannelDisconnected.size(), 2u);
	ASSERT_EQ(config.firstChannelDisconnected[0], 300);
	ASSERT_EQ(config.firstChannelDisconnected[1], 400);

}

TEST(CablingConfigurationParserTest, ParseSFE){

	const char* trp = std::getenv("TESTRESOURCESPATH");
	TString configFilePath = TString::Format("%s/CablingModel/sFE_test1.xml", trp);

	CablingConfigurationParser ccp;

	ccp.SetConfigurationFile(configFilePath);
	ccp.Parse();
	std::map<TString, sTGCsFEConfiguration> sasfelkp = ccp.GetSFELookup();



	ASSERT_EQ(sasfelkp.size(),1u);

	std::map<TString, sTGCsFEConfiguration>::iterator  itr = sasfelkp.find("sTG1-QL1P1");

	ASSERT_EQ( (itr == sasfelkp.end()), false);

	/*
	 * 	<sFE type="sTG1-QL1P1" layer="1" direction="ANTIPARALLEL"
		nActiveVMMChannels="408" nActiveVMM="8" nACtiveTDSChannels="336"
		nActiveTDS="3" firstTDSChannelDisconnected="336"
		nTDSChannelDisconnected="176">
	    </sFE>
	 */

	sTGCsFEConfiguration config = itr->second;
	ASSERT_EQ(config.type, "sTG1-QL1P1");
	ASSERT_EQ(config.layer, 1);
	ASSERT_EQ(config.direction, DIR_WRT_DET::ANTIPARALLEL);
	ASSERT_EQ(config.nActiveVMMChannels, 408);
	ASSERT_EQ(config.nActiveVMM, 8);
	ASSERT_EQ(config.nACtiveTDSChannels, 336);
	ASSERT_EQ(config.nActiveTDS, 3);
	ASSERT_EQ(config.firstTDSChannelDisconnected.size(),1u);
	ASSERT_EQ(config.firstTDSChannelDisconnected[0],336);
	ASSERT_EQ(config.nTDSChannelDisconnected[0], 176);

}

TEST(CablingConfigurationParserTest, PadTrigger){

	const char* trp = std::getenv("TESTRESOURCESPATH");
	TString configFilePath = TString::Format("%s/CablingModel/padTrigger_test1.xml", trp);

	CablingConfigurationParser ccp;

	ccp.SetConfigurationFile(configFilePath);
	ccp.Parse();

	std::map<TString, sTGCPadTriggerConfiguration> padLookup =  ccp.GetPadTriggerLookup();
	ASSERT_EQ(padLookup.size(), 1u);

	std::map<TString, sTGCPadTriggerConfiguration>::iterator itr = padLookup.find("sTG1-A-L-external");

	ASSERT_NE(itr, padLookup.end());

	sTGCPadTriggerConfiguration ptc = itr->second;
	ASSERT_EQ(ptc.type, "sTG1-A-L-external");
	ASSERT_EQ(ptc.coincidenceTable, "external");
	ASSERT_EQ(ptc.latency, 100);

}

TEST(CablingConfigurationParserTest, Router){

	const char* trp = std::getenv("TESTRESOURCESPATH");
	TString configFilePath = TString::Format("%s/CablingModel/router_test1.xml", trp);

	CablingConfigurationParser ccp;

	ccp.SetConfigurationFile(configFilePath);
	ccp.Parse();


	std::map<TString, sTGCRouterConfiguration> rtlkp = ccp.GetRouterLookup();

	ASSERT_EQ(rtlkp.size(), 1u);

	TString type = "dummy";
	std::map<TString, sTGCRouterConfiguration>::iterator itr = rtlkp.find(type);

	ASSERT_NE(itr, rtlkp.end());

	sTGCRouterConfiguration rtr = itr->second;


	ASSERT_EQ(rtr.type, type);
	ASSERT_EQ(rtr.serializerBank.size(), 12u);
	int i = 0;
	ASSERT_EQ(rtr.serializerBank[i++], 1);
	ASSERT_EQ(rtr.serializerBank[i++], 1);
	ASSERT_EQ(rtr.serializerBank[i++], 1);
	ASSERT_EQ(rtr.serializerBank[i++], 1);

	ASSERT_EQ(rtr.serializerBank[i++], 2);
	ASSERT_EQ(rtr.serializerBank[i++], 2);
	ASSERT_EQ(rtr.serializerBank[i++], 2);
	ASSERT_EQ(rtr.serializerBank[i++], 2);

	ASSERT_EQ(rtr.serializerBank[i++], 3);
	ASSERT_EQ(rtr.serializerBank[i++], 3);
	ASSERT_EQ(rtr.serializerBank[i++], 3);
	ASSERT_EQ(rtr.serializerBank[i++], 3);

	i = 0;
	ASSERT_EQ(rtr.serializerBankChannel.size(), 12u);

	ASSERT_EQ(rtr.serializerBankChannel[i++], 1);
	ASSERT_EQ(rtr.serializerBankChannel[i++], 2);
	ASSERT_EQ(rtr.serializerBankChannel[i++], 3);
	ASSERT_EQ(rtr.serializerBankChannel[i++], 4);

	ASSERT_EQ(rtr.serializerBankChannel[i++], 4);
	ASSERT_EQ(rtr.serializerBankChannel[i++], 3);
	ASSERT_EQ(rtr.serializerBankChannel[i++], 2);
	ASSERT_EQ(rtr.serializerBankChannel[i++], 1);

	ASSERT_EQ(rtr.serializerBankChannel[i++], 1);
	ASSERT_EQ(rtr.serializerBankChannel[i++], 2);
	ASSERT_EQ(rtr.serializerBankChannel[i++], 3);
	ASSERT_EQ(rtr.serializerBankChannel[i++], 4);


	ASSERT_EQ(rtr.delay.size(), 12u);
	for (int i = 0; i < 12; i++)
	ASSERT_EQ(rtr.delay[i], 0);

	ASSERT_EQ(rtr.latency, 100);
	ASSERT_EQ(rtr.clock, 160);

}

TEST(CablingConfigurationParserTest, TriggerProcessor){

	const char* trp = std::getenv("TESTRESOURCESPATH");
	TString configFilePath = TString::Format("%s/CablingModel/triggerProcessor_test1.xml", trp);

	CablingConfigurationParser ccp;

	ccp.SetConfigurationFile(configFilePath);
	ccp.Parse();

	std::map<TString, TriggerProcessorConfiguration> tplkp = ccp.GetTriggerProcessorLookup();

	ASSERT_EQ(tplkp.size(), 1u);

	std::map<TString, TriggerProcessorConfiguration>::iterator itr = tplkp.find("ascii-capture");

	ASSERT_NE(itr, tplkp.end());

	TriggerProcessorConfiguration tpc = itr->second;
	ASSERT_EQ(tpc.type, "ascii-capture");

	/*<TriggerProcessor type="ascii-capture"></TriggerProcessor>*/

}

TEST(CablingConfigurationParserTest, Composition){

	const char* trp = std::getenv("TESTRESOURCESPATH");
	TString configFilePath = TString::Format("%s/CablingModel/composition_test1.xml", trp);

	CablingConfigurationParser ccp;

	ccp.SetConfigurationFile(configFilePath);
	ccp.Parse();
	std::map<TString, CablingComposition> lookup = ccp.GetCablingCompositionLookup();

	ASSERT_EQ(lookup.size(), 1u);

	std::map<TString, CablingComposition>::iterator itr = lookup.find("TEST1");
	ASSERT_NE(itr, lookup.end());


	CablingComposition comp_a = itr->second;
	ASSERT_EQ(comp_a.compositionName, "TEST1");

	ASSERT_EQ(comp_a.simpleComponents.size(), 1u);
	ASSERT_EQ(comp_a.simpleComponents[0].componentClass, "sc_class");
	ASSERT_EQ(comp_a.simpleComponents[0].componentType, "sc_type");
	ASSERT_EQ(comp_a.simpleComponents[0].componentName, "sc_name");

	ASSERT_EQ(comp_a.compoundComponents[0].compositionName, "cc_name");
	ASSERT_EQ(comp_a.compoundComponents[0].dictionary.size(), 1u);
	ASSERT_EQ(comp_a.compoundComponents[0].dictionary["D1"], "d1");

	ASSERT_EQ(comp_a.dictionary.size(), 1u);
	ASSERT_EQ(comp_a.dictionary["A1"], "a1");

	ASSERT_EQ(comp_a.connections.size(), 1u);
	ASSERT_EQ(comp_a.connections[0].sourceClass, "s_class");
	ASSERT_EQ(comp_a.connections[0].sourceType, "s_type");
	ASSERT_EQ(comp_a.connections[0].sourceName, "s_name");
	ASSERT_EQ(comp_a.connections[0].sourcePort, "s_port");

	ASSERT_EQ(comp_a.connections[0].destinationClass, "d_class");
	ASSERT_EQ(comp_a.connections[0].destinationType, "d_type");
	ASSERT_EQ(comp_a.connections[0].destinationName, "d_name");
	ASSERT_EQ(comp_a.connections[0].destinationPort, "d_port");

	ASSERT_EQ(comp_a.connections[0].cableLength, "c_length");
	ASSERT_EQ(comp_a.connections[0].propagationDelay, "p_delay");
	ASSERT_EQ(comp_a.connections[0].connectionDetails, "c_details");


}
