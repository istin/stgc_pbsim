/*
 * TemplatedCablingComposition.cc
 *
 *  Created on: Mar 19, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */



#include <CablingModel/CablingComposition.h>
#include "gtest/gtest.h"


namespace {

TEST(CablingComposition, null_constructor){
	CablingComposition tcc;
	ASSERT_EQ(tcc.compositionName,"");
	ASSERT_EQ(tcc.simpleComponents.size(), 0u);
	ASSERT_EQ(tcc.connections.size(), 0u);
}

TEST(CablingComposition, incomplete_resolution_compositionName){
	CablingComposition tcc;

	tcc.compositionName = "name_test_${TEMPLATE_VALUE}";
	ASSERT_EQ(tcc.compositionName,"name_test_${TEMPLATE_VALUE}");
	ASSERT_EQ(tcc.simpleComponents.size(), 0u);
	ASSERT_EQ(tcc.connections.size(), 0u);

	std::map<TString, TString> dictionary;
	EXPECT_ANY_THROW(CablingComposition cc = tcc.GetResolved(dictionary));
}

TEST(CablingComposition, incomplete_resolution_dictionary_key){
	CablingComposition tcc;

	tcc.compositionName = "name_test";
	tcc.dictionary["${KEY}"] = "target";
	ASSERT_EQ(tcc.compositionName,"name_test");
	ASSERT_EQ(tcc.simpleComponents.size(), 0u);
	ASSERT_EQ(tcc.connections.size(), 0u);
	ASSERT_EQ(tcc.dictionary.size(), 1u);
	std::map<TString, TString> dictionary;
	EXPECT_ANY_THROW(CablingComposition cc = tcc.GetResolved(dictionary));
}

TEST(CablingComposition, incomplete_resolution_dictionary_target){
	CablingComposition tcc;

	tcc.compositionName = "name_test";
	tcc.dictionary["key"] = "${TARGET}";
	ASSERT_EQ(tcc.compositionName,"name_test");
	ASSERT_EQ(tcc.simpleComponents.size(), 0u);
	ASSERT_EQ(tcc.connections.size(), 0u);
	ASSERT_EQ(tcc.dictionary.size(), 1u);
	std::map<TString, TString> dictionary;
	EXPECT_ANY_THROW(CablingComposition cc = tcc.GetResolved(dictionary));
}

TEST(CablingComposition, resolution_dictionary_key){
	CablingComposition tcc;

	tcc.compositionName = "name_test";
	tcc.dictionary["${key}"] = "target";
	ASSERT_EQ(tcc.compositionName,"name_test");
	ASSERT_EQ(tcc.simpleComponents.size(), 0u);
	ASSERT_EQ(tcc.connections.size(), 0u);
	ASSERT_EQ(tcc.dictionary.size(), 1u);

	std::map<TString, TString> dictionary;
	dictionary["key"] = "alpha";

	CablingComposition cc = tcc.GetResolved(dictionary);
	std::map<TString, TString>::iterator itr = cc.dictionary.find("alpha");
	ASSERT_NE(itr,cc.dictionary.end());
}

TEST(CablingComposition, resolution_dictionary_target){

	CablingComposition tcc;
	tcc.compositionName = "name_test";
	tcc.dictionary["key"] = "${TARGET}";
	ASSERT_EQ(tcc.compositionName,"name_test");
	ASSERT_EQ(tcc.simpleComponents.size(), 0u);
	ASSERT_EQ(tcc.connections.size(), 0u);
	ASSERT_EQ(tcc.dictionary.size(), 1u);

	std::map<TString, TString> dictionary;
	dictionary["TARGET"] = "beta";

	CablingComposition cc = tcc.GetResolved(dictionary);
	std::map<TString, TString>::iterator itr = cc.dictionary.find("key");
	ASSERT_NE(itr,cc.dictionary.end());
	ASSERT_EQ(itr->second, "beta");
}

TEST(CablingComposition, resolution_dictionary_extension){

	CablingComposition tcc;
	tcc.compositionName = "name_test";

	ASSERT_EQ(tcc.compositionName,"name_test");
	ASSERT_EQ(tcc.simpleComponents.size(), 0u);
	ASSERT_EQ(tcc.connections.size(), 0u);
	ASSERT_EQ(tcc.dictionary.size(), 0u);

	std::map<TString, TString> dictionary;
	dictionary["TARGET"] = "beta";

	CablingComposition cc = tcc.GetResolved(dictionary);

	ASSERT_EQ(cc.dictionary.size(), 1u);

	std::map<TString, TString>::iterator itr = cc.dictionary.find("TARGET");
	ASSERT_NE(itr,cc.dictionary.end());
	ASSERT_EQ(itr->first, "TARGET");
	ASSERT_EQ(itr->second, "beta");
}


TEST(CablingComposition, template_resolution_simpleComponent){
	CablingComposition tcc;
	tcc.compositionName = "name_test";
	ASSERT_EQ(tcc.compositionName,"name_test");
	ASSERT_EQ(tcc.simpleComponents.size(), 0u);
	ASSERT_EQ(tcc.connections.size(), 0u);

	SimpleComponent sc;
	sc.componentClass="${CLASS}";
	sc.componentType="${TYPE}";
	sc.componentName="${NAME}";

	std::map<TString, TString> dict;
	dict["CLASS"]="class";
	dict["TYPE"]="type";
	dict["NAME"]="name";

	tcc.simpleComponents.push_back(sc);

	ASSERT_EQ(tcc.simpleComponents.size(),1u);

	CablingComposition cc = tcc.GetResolved(dict);
	ASSERT_EQ(cc.simpleComponents.size(),1u);
	ASSERT_EQ(cc.simpleComponents[0].componentClass, "class");
	ASSERT_EQ(cc.simpleComponents[0].componentType, "type");
	ASSERT_EQ(cc.simpleComponents[0].componentName, "name");

}


TEST(CablingComposition, template_resolution_many_SimpleComponent){
	CablingComposition tcc;
	tcc.compositionName = "name_test";
	ASSERT_EQ(tcc.compositionName,"name_test");
	ASSERT_EQ(tcc.simpleComponents.size(), 0u);
	ASSERT_EQ(tcc.connections.size(), 0u);

	SimpleComponent sc1;
	sc1.componentClass="${CLASS}";
	sc1.componentType="${TYPE}";
	sc1.componentName="${NAME}";

	SimpleComponent sc2;
	sc2.componentClass="${CLASS}a";
	sc2.componentType="${TYPE}a";
	sc2.componentName="${NAME}a";

	std::map<TString, TString> dict;
	dict["CLASS"]="class";
	dict["TYPE"]="type";
	dict["NAME"]="name";

	tcc.simpleComponents.push_back(sc1);
	tcc.simpleComponents.push_back(sc2);

	ASSERT_EQ(tcc.simpleComponents.size(),2u);

	CablingComposition cc = tcc.GetResolved(dict);
	ASSERT_EQ(cc.simpleComponents.size(),2u);
	ASSERT_EQ(cc.simpleComponents[0].componentClass, "class");
	ASSERT_EQ(cc.simpleComponents[0].componentType, "type");
	ASSERT_EQ(cc.simpleComponents[0].componentName, "name");
	ASSERT_EQ(cc.simpleComponents[1].componentClass, "classa");
	ASSERT_EQ(cc.simpleComponents[1].componentType, "typea");
	ASSERT_EQ(cc.simpleComponents[1].componentName, "namea");

}


TEST(CablingComposition, template_resolution_compoundComponent){
	CablingComposition tcc;
	tcc.compositionName = "name_test";
	ASSERT_EQ(tcc.compositionName,"name_test");
	ASSERT_EQ(tcc.simpleComponents.size(), 0u);
	ASSERT_EQ(tcc.connections.size(), 0u);

	CompoundComponent compComp;
	compComp.compositionName="${COMPOSITION_NAME}";
	compComp.dictionary["A_KEY"] = "${A_KEY}";

	std::map<TString, TString> dict;
	dict["COMPOSITION_NAME"]="name";
	dict["A_KEY"]="a_target";

	tcc.compoundComponents.push_back(compComp);

	ASSERT_EQ(tcc.compoundComponents.size(),1u);

	CablingComposition cc = tcc.GetResolved(dict);
	ASSERT_EQ(cc.compoundComponents.size(),1u);
	ASSERT_EQ(cc.compoundComponents[0].compositionName, "name");
	ASSERT_EQ(cc.compoundComponents[0].dictionary.size(), 2u);
	ASSERT_EQ(cc.compoundComponents[0].dictionary["A_KEY"], "a_target");

}

TEST(CablingComposition, template_resolution_connection){

	CablingComposition tcc;
	ComponentConnection connection;
	connection.sourceClass = "${S_CLASS}";
	connection.sourceType = "${S_TYPE}";
	connection.sourceName = "${S_NAME}";
	connection.sourcePort = "${S_PORT}";

	connection.destinationClass = "${D_CLASS}";
	connection.destinationType = "${D_TYPE}";
	connection.destinationName = "${D_NAME}";
	connection.destinationPort = "${D_PORT}";
	connection.cableLength = "${C_LENGTH}";
	connection.propagationDelay = "${P_DELAY}";

	tcc.connections.push_back(connection);

	std::map<TString, TString> dict;
	dict["S_CLASS"]="s_class";
	dict["S_TYPE"]="s_type";
	dict["S_NAME"]="s_name";
	dict["S_PORT"]="s_port";
	dict["D_CLASS"]="d_class";
	dict["D_TYPE"]="d_type";
	dict["D_NAME"]="d_name";
	dict["D_PORT"]="d_port";
	dict["C_LENGTH"]="100";
	dict["P_DELAY"]="50";

	CablingComposition cc = tcc.GetResolved(dict);
	ASSERT_EQ(cc.connections.size(), 1u);
	ASSERT_EQ(cc.connections[0].sourceClass, "s_class");
	ASSERT_EQ(cc.connections[0].sourceType, "s_type");
	ASSERT_EQ(cc.connections[0].sourceName, "s_name");
	ASSERT_EQ(cc.connections[0].sourcePort, "s_port");

	ASSERT_EQ(cc.connections[0].destinationClass, "d_class");
	ASSERT_EQ(cc.connections[0].destinationType, "d_type");
	ASSERT_EQ(cc.connections[0].destinationName, "d_name");
	ASSERT_EQ(cc.connections[0].destinationPort, "d_port");

	ASSERT_EQ(cc.connections[0].cableLength, "100");
	ASSERT_EQ(cc.connections[0].propagationDelay, "50");

}

}// namespace
