// Copyright 2006, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include "gtest/gtest.h"
#include "sTGCConfigGeneration/sTGCsFEBandConfigGen.h"

// Tests that we don't have to define main() when we link to
// gtest_main instead of gtest.

namespace {

TEST(sTGCsFEBandConfigGen, test_band_number){

	sTGCsFEBandConfigGen configGen;
	//configGen.SetFEType("TEST1");
	configGen.AddBand(1, 1,10,0);
	sTDSConfig tdsConfig;
	configGen.FillTDSConfig(1,tdsConfig);

	ASSERT_EQ(tdsConfig.firstBand, 1);
}

TEST(sTGCsFEBandConfigGen, test_bands_cannot_have_overlapping_strips){

	sTGCsFEBandConfigGen configGen;
	//configGen.SetFEType("TEST1");
	configGen.AddBand(1, 1,10,0);
	EXPECT_ANY_THROW(configGen.AddBand(2, 1,10,0));
}

TEST(sTGCsFEBandConfigGen, test_name_of_fe_in_configuration){

	sTGCsFEBandConfigGen configGen;
	configGen.SetFEType("TEST1");
	sTDSConfig tdsConfig;
	configGen.FillTDSConfig(1,tdsConfig);
	ASSERT_EQ(tdsConfig.fe_type, "TEST1");
}

TEST(sTGCsFEBandConfigGen, test_of_direction){

	sTGCsFEBandConfigGen configGen;
	ASSERT_EQ(configGen.GetDirection(), DIR_WRT_DET::UNDEFINED);
	configGen.SetDirection(DIR_WRT_DET::PARALLEL);
	ASSERT_EQ(configGen.GetDirection(), DIR_WRT_DET::PARALLEL);
	configGen.SetDirection(DIR_WRT_DET::ANTIPARALLEL);
	ASSERT_EQ(configGen.GetDirection(), DIR_WRT_DET::ANTIPARALLEL);
}


TEST(sTGCsFEBandConfigGen, test_of_segments){

	sTGCsFEBandConfigGen configGen;
	configGen.SetDirection(DIR_WRT_DET::PARALLEL);
	configGen.AddBand(1, 128,2,0);
	configGen.AddBand(2, 270,10,0);

	sTDSConfig tdsConfig1;
	sTDSConfig tdsConfig2;
	sTDSConfig tdsConfig3;
	configGen.FillTDSConfig(1,tdsConfig1);
	configGen.FillTDSConfig(2,tdsConfig2);
	configGen.FillTDSConfig(3,tdsConfig3);
	ASSERT_EQ(tdsConfig1.direction, DIR_WRT_DET::PARALLEL);
	ASSERT_EQ(tdsConfig2.direction, DIR_WRT_DET::PARALLEL);
	ASSERT_EQ(tdsConfig3.direction, DIR_WRT_DET::PARALLEL);

	ASSERT_EQ(tdsConfig1.band_segment.size(), 128u);
	ASSERT_EQ(tdsConfig2.band_segment.size(), 128u);
	ASSERT_EQ(tdsConfig3.band_segment.size(), 128u);

	ASSERT_EQ(tdsConfig1.band_segment[0], BAND_SEGMENT::INNER);
	ASSERT_EQ(tdsConfig2.band_segment[0], BAND_SEGMENT::OUTER);
	ASSERT_EQ(tdsConfig3.band_segment[1], BAND_SEGMENT::COMPLETE);
	ASSERT_EQ(tdsConfig3.band_segment[0], BAND_SEGMENT::UNDEFINED);


	configGen.SetDirection(DIR_WRT_DET::ANTIPARALLEL);
	sTDSConfig tdsConfig1a;
	sTDSConfig tdsConfig2a;
	sTDSConfig tdsConfig3a;

	configGen.FillTDSConfig(1,tdsConfig1a);
	configGen.FillTDSConfig(2,tdsConfig2a);
	configGen.FillTDSConfig(3,tdsConfig3a);

	ASSERT_EQ(tdsConfig1a.band_segment[0], BAND_SEGMENT::OUTER);
	ASSERT_EQ(tdsConfig2a.band_segment[0], BAND_SEGMENT::INNER);
	ASSERT_EQ(tdsConfig3a.band_segment[1], BAND_SEGMENT::COMPLETE);
	ASSERT_EQ(tdsConfig3a.band_segment[0], BAND_SEGMENT::UNDEFINED);

}

}  // namespace

// We are using the main() function defined in gtest_main.cc, so we
// don't define it here.
