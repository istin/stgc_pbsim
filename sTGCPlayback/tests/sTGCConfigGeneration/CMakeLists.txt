 
######
file(GLOB TEST_SRC_FILES ${CMAKE_CURRENT_SOURCE_DIR}/*.cc)
include_directories("${GOOGLE_TEST_SOURCES}/googletest/include")
include_directories("${PROJECT_SOURCE_DIR}/sTGCPlayback")
get_filename_component(CURRDIRNAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)
set(_test_name gtest_${CURRDIRNAME})
link_directories(${PLAYBACK_LIBS})
add_executable(${_test_name} ${TEST_SRC_FILES})
add_dependencies(${_test_name} googletest)
target_link_libraries(${_test_name} gtest gtest_main CablingModel Components FrameWork GeoModel Simulation XMLUtils Detector sTGCConfigGen)
add_test(${_test_name} ${_test_name})
set_tests_properties(${_test_name} PROPERTIES TIMEOUT 5)
install(TARGETS ${_test_name} DESTINATION "${INSTALLATION}/bin")
add_subdirectory(Resources)
#######
