// Copyright 2006, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include "gtest/gtest.h"
#include "Components/Clock.h"
#include "tests/Components/MockScheduler.h"
// Tests that we don't have to define main() when we link to
// gtest_main instead of gtest.

namespace {

TEST(Clock, NullInitialization){

	Clock clock;
	ASSERT_EQ(clock.IsActive(),false);
	ASSERT_EQ(clock.GetDutyCycle(), 0);
	ASSERT_EQ(clock.GetClockState(), 0);
	ASSERT_EQ(clock.GetPeriod(PBSim_Units::TIME_UNIT::ns), 0);

}

TEST(Clock, SetPeriod){

	Clock clock;
	clock.SetPeriodAndDutyCycle(3125, PBSim_Units::TIME_UNIT::ps, 0.5);
	ASSERT_EQ(clock.GetPeriod(PBSim_Units::TIME_UNIT::ps), 3125);
	ASSERT_EQ(clock.GetDutyCycle(), 0.5);

}


TEST(Clock, Activate){
//	Clock clock;
//	MockScheduler scheduler;
//	clock.Activate();
//	ASSERT_EQ(clock.GetPeriod(PBSim_Units::TIME_UNIT::ps), 3125);
//	ASSERT_EQ(clock.GetDutyCycle(), 0.5);
}


}  // namespace

// We are using the main() function defined in gtest_main.cc, so we
// don't define it here.
