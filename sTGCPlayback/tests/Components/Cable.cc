// Copyright 2006, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include "Components/Cable.h"
#include "Components/IScheduler.h"
#include "Components/ICommand.h"
#include "Components/ConstantsAndUnits.h"
#include "tests/Components/MockScheduler.h"

#include "gtest/gtest.h"



// Tests that we don't have to define main() when we link to
// gtest_main instead of gtest.


namespace {

int is_a_CableReleaseCommandPtr(ICommand* cmd)
{
	return (dynamic_cast< Cable<double>::CableReleaseCommand *>(cmd) != NULL);
}

int is_a_CableTrasnmissionCommandPtr(ICommand* cmd)
{
	return (dynamic_cast< Cable<double>::CableTransmissionCommand *>(cmd) != NULL);
}

TEST(Cable, ScheduleCommands){

	Link<double> input;
	Link<double> output;
	Cable<double> cable;

	IScheduler * scheduler;
	//MockScheduler scheduler;

//	EXPECT_CALL(*scheduler, Schedule(::testing::Truly(is_a_CableReleaseCommandPtr), ( 2.0 * PBSim_Units::s) ))
//	.Times(1);
//
//	double totalTime = 1 * PBSim_Units::ns_over_m * 6e3 * PBSim_Units::m + 2.0 * PBSim_Units::s;
//
//	EXPECT_CALL(*scheduler, Schedule(::testing::Truly(is_a_CableTrasnmissionCommandPtr),  ( totalTime ) ))
//	.Times(1);
//
//	cable.SetInputLink(input);
//	cable.SetOutputLink(input);
//	cable.SetPropagationDelay(1 * PBSim_Units::ns_over_m);
//	cable.SetLength(6e3 * PBSim_Units::m);
//	cable.SetMessageSize(100 * PBSim_Units::Gb);
//	cable.SetBandwidth(50 * PBSim_Units::Gb_over_s);
//	cable.SetScheduler(scheduler);
//	cable.Release();
//
//	input.SetValue(10);
//	ASSERT_EQ(0,0);
}

}  // namespace
