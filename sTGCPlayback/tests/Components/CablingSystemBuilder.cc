// Copyright 2006, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include "gtest/gtest.h"
#include "Components/CablingSystemBuilder.h"
#include "tests/Components/FakeComponentFactory.h"
#include "tests/Components/FakeComponentToTestConnection.h"
#include "Components/ComponentStore.h"
#include "Components/BaseComponent.h"


// Tests that we don't have to define main() when we link to
// gtest_main instead of gtest.

namespace {

TEST(CablingSystemBuilder, BuildSimpleComponentsOnly){

	CablingComposition composition;
	IComponentStore* store = ComponentStore::Instance();
	composition.compositionName = "dummy";
	SimpleComponent component_a, component_b;
	component_a.componentClass = "component_a_class_BuildSimpleComponentsOnly";
	component_a.componentType  = "component_a_type_BuildSimpleComponentsOnly";
	component_a.componentName  = "component_a_name_BuildSimpleComponentsOnly";

	component_b.componentClass = "component_b_class_BuildSimpleComponentsOnly";
	component_b.componentType  = "component_b_type_BuildSimpleComponentsOnly";
	component_b.componentName  = "component_b_name_BuildSimpleComponentsOnly";

	composition.simpleComponents.push_back(component_a);
	composition.simpleComponents.push_back(component_b);

	std::map<TString, CablingComposition> lookup;
	lookup[composition.compositionName] = composition;


	FakeFactory fakeFactory;

	BaseComponent* bc_a = new BaseComponent;
	bc_a->SetClass(component_a.componentClass);
	bc_a->SetType (component_a.componentType);
	bc_a->SetName (component_a.componentName);
	fakeFactory.m_components[fakeFactory.Key(component_a.componentClass,
			                                 component_a.componentType,
											 component_a.componentName)] = bc_a;

	BaseComponent* bc_b = new BaseComponent;
	bc_b->SetClass(component_b.componentClass);
	bc_b->SetType (component_b.componentType);
	bc_b->SetName (component_b.componentName);
	fakeFactory.m_components[fakeFactory.Key(component_b.componentClass,
			                                 component_b.componentType,
											 component_b.componentName)] = bc_b;

	ASSERT_EQ(bc_a, fakeFactory.Create(component_a.componentClass,
			                           component_a.componentType,
									   component_a.componentName ));

	ASSERT_EQ(bc_b, fakeFactory.Create(component_b.componentClass,
			                           component_b.componentType,
									   component_b.componentName ));

	CablingSystemBuilder builder;
	builder.WithCompositionLookup(lookup)
		   .WithComponentStore(store)
		   .WithFactoryRegistry(&fakeFactory)
		   .Build("dummy");

	IComponent* ic_a = store->Retrieve(component_a.componentClass,
                                       component_a.componentType,
			                           component_a.componentName);


	IComponent* ic_b = store->Retrieve(component_b.componentClass,
                                       component_b.componentType,
			                           component_b.componentName);

	ASSERT_EQ(ic_a, bc_a);
	ASSERT_EQ(ic_b, bc_b);

}



TEST(CablingSystemBuilder, BuildNestedComposition){

	CablingComposition composition_a, composition_b, enclosing_composition;
	IComponentStore* store = ComponentStore::Instance();
	composition_a.compositionName = "composition_a";
	composition_b.compositionName = "composition_b";
	enclosing_composition.compositionName = "enclosing_composition";

	SimpleComponent component_a, component_b;
	component_a.componentClass = "component_a_class_BuildNestedComposition";
	component_a.componentType  = "component_a_type_BuildNestedComposition";
	component_a.componentName  = "component_a_name_BuildNestedComposition";

	component_b.componentClass = "component_b_class_BuildNestedComposition";
	component_b.componentType  = "component_b_type_BuildNestedComposition";
	component_b.componentName  = "component_b_name_BuildNestedComposition";

	composition_a.simpleComponents.push_back(component_a);
	composition_b.simpleComponents.push_back(component_b);

	CompoundComponent cc_a, cc_b;
	cc_a.compositionName = composition_a.compositionName;
	cc_b.compositionName = composition_b.compositionName;
	enclosing_composition.compoundComponents.push_back(cc_a);
	enclosing_composition.compoundComponents.push_back(cc_b);



	std::map<TString, CablingComposition> lookup;
	lookup[composition_a.compositionName] = composition_a;
	lookup[composition_b.compositionName] = composition_b;
	lookup[enclosing_composition.compositionName] = enclosing_composition;

	FakeFactory fakeFactory;

	BaseComponent* bc_a = new BaseComponent;
	bc_a->SetClass(component_a.componentClass);
	bc_a->SetType (component_a.componentType);
	bc_a->SetName (component_a.componentName);
	fakeFactory.m_components[fakeFactory.Key(component_a.componentClass,
			                                 component_a.componentType,
											 component_a.componentName)] = bc_a;

	BaseComponent* bc_b = new BaseComponent;
	bc_b->SetClass(component_b.componentClass);
	bc_b->SetType (component_b.componentType);
	bc_b->SetName (component_b.componentName);
	fakeFactory.m_components[fakeFactory.Key(component_b.componentClass,
			                                 component_b.componentType,
											 component_b.componentName)] = bc_b;

	ASSERT_EQ(bc_a, fakeFactory.Create(component_a.componentClass,
			                           component_a.componentType,
									   component_a.componentName ));

	ASSERT_EQ(bc_b, fakeFactory.Create(component_b.componentClass,
			                           component_b.componentType,
									   component_b.componentName ));

	CablingSystemBuilder builder;
	builder.WithCompositionLookup(lookup)
		   .WithComponentStore(store)
		   .WithFactoryRegistry(&fakeFactory)
		   .Build(enclosing_composition.compositionName);

	IComponent* ic_a = store->Retrieve(component_a.componentClass,
                                       component_a.componentType,
			                           component_a.componentName);


	IComponent* ic_b = store->Retrieve(component_b.componentClass,
                                       component_b.componentType,
			                           component_b.componentName);

	ASSERT_EQ(ic_a, bc_a);
	ASSERT_EQ(ic_b, bc_b);

}

TEST(CablingSystemBuilder, BuildNestedCompositionWithTemplates){

	CablingComposition composition_a, composition_b, enclosing_composition;
	IComponentStore* store = ComponentStore::Instance();
	composition_a.compositionName = "composition_a";
	composition_b.compositionName = "composition_b";
	enclosing_composition.compositionName = "enclosing_composition";

	SimpleComponent component_a, component_b;
	component_a.componentClass = "component_${T_a}_class_BuildNestedCompositionWithTemplates";
	component_a.componentType  = "component_${T_a}_type_BuildNestedCompositionWithTemplates";
	component_a.componentName  = "component_${T_a}_name_BuildNestedCompositionWithTemplates";

	component_b.componentClass = "component_${T_b}_class_BuildNestedCompositionWithTemplates";
	component_b.componentType  = "component_${T_b}_type_BuildNestedCompositionWithTemplates";
	component_b.componentName  = "component_${T_b}_name_BuildNestedCompositionWithTemplates";

	composition_a.simpleComponents.push_back(component_a);
	composition_b.simpleComponents.push_back(component_b);

	CompoundComponent cc_a, cc_b;
	cc_a.compositionName = composition_a.compositionName;
	cc_b.compositionName = composition_b.compositionName;
	enclosing_composition.compoundComponents.push_back(cc_a);
	enclosing_composition.compoundComponents.push_back(cc_b);
	enclosing_composition.dictionary["T_a"] = "a";
	enclosing_composition.dictionary["T_b"] = "b";

	std::map<TString, CablingComposition> lookup;
	lookup[composition_a.compositionName] = composition_a;
	lookup[composition_b.compositionName] = composition_b;
	lookup[enclosing_composition.compositionName] = enclosing_composition;

	FakeFactory fakeFactory;

	BaseComponent* bc_a = new BaseComponent;
	bc_a->SetClass("component_a_class_BuildNestedCompositionWithTemplates");
	bc_a->SetType ("component_a_type_BuildNestedCompositionWithTemplates");
	bc_a->SetName ("component_a_name_BuildNestedCompositionWithTemplates");
	fakeFactory.m_components[fakeFactory.Key(bc_a->GetClass(), bc_a->GetType(), bc_a->GetName())] = bc_a;

	BaseComponent* bc_b = new BaseComponent;
	bc_b->SetClass("component_b_class_BuildNestedCompositionWithTemplates");
	bc_b->SetType ("component_b_type_BuildNestedCompositionWithTemplates");
	bc_b->SetName ("component_b_name_BuildNestedCompositionWithTemplates");
	fakeFactory.m_components[fakeFactory.Key(bc_b->GetClass(), bc_b->GetType(), bc_b->GetName())] = bc_b;


	CablingSystemBuilder builder;
	builder.WithCompositionLookup(lookup)
		   .WithComponentStore(store)
		   .WithFactoryRegistry(&fakeFactory)
		   .Build(enclosing_composition.compositionName);

	IComponent* ic_a = store->Retrieve(bc_a->GetClass(), bc_a->GetType(), bc_a->GetName());


	IComponent* ic_b = store->Retrieve(bc_b->GetClass(), bc_b->GetType(), bc_b->GetName());

	ASSERT_EQ(ic_a, bc_a);
	ASSERT_EQ(ic_b, bc_b);

}


TEST(CablingSystemBuilder, TestConnection){

	CablingComposition composition;
	IComponentStore* store = ComponentStore::Instance();
	composition.compositionName = "connection_dummy";
	SimpleComponent component_a, component_b;
	component_a.componentClass = "component_a_class_TestConnection";
	component_a.componentType  = "component_a_type_TestConnection";
	component_a.componentName  = "component_a_name_TestConnection";

	component_b.componentClass = "component_b_class_TestConnection";
	component_b.componentType  = "component_b_type_TestConnection";
	component_b.componentName  = "component_b_name_TestConnection";

	composition.simpleComponents.push_back(component_a);
	composition.simpleComponents.push_back(component_b);

	std::map<TString, CablingComposition> lookup;
	lookup[composition.compositionName] = composition;


	FakeFactory fakeFactory;

	FakeComponentToTestConnection* bc_a = new FakeComponentToTestConnection;
	bc_a->SetClass(component_a.componentClass);
	bc_a->SetType (component_a.componentType);
	bc_a->SetName (component_a.componentName);
	fakeFactory.m_components[fakeFactory.Key(component_a.componentClass,
			                                 component_a.componentType,
											 component_a.componentName)] = bc_a;

	BaseComponent* bc_b = new BaseComponent;
	bc_b->SetClass(component_b.componentClass);
	bc_b->SetType (component_b.componentType);
	bc_b->SetName (component_b.componentName);
	fakeFactory.m_components[fakeFactory.Key(component_b.componentClass,
			                                 component_b.componentType,
											 component_b.componentName)] = bc_b;

	ASSERT_EQ(bc_a, fakeFactory.Create(component_a.componentClass,
			                           component_a.componentType,
									   component_a.componentName ));

	ASSERT_EQ(bc_b, fakeFactory.Create(component_b.componentClass,
			                           component_b.componentType,
									   component_b.componentName ));

	double bandwidth = 30;
	double propagationDelay = 100;
	double length = 50;
	bc_a->AddExpectedConnectionRequest(bc_b, "b_port", "a_port", propagationDelay, length, bandwidth);

	ComponentConnection connection;
	connection.bandwidth = "30";
	connection.cableLength = "50";
	connection.propagationDelay = "100";
	connection.sourceClass = component_b.componentClass;
	connection.sourceType = component_b.componentType;
	connection.sourceName = component_b.componentName;
	connection.destinationClass = component_a.componentClass;
	connection.destinationType = component_a.componentType;
	connection.destinationName = component_a.componentName;


	composition.connections.push_back(connection);

	CablingSystemBuilder builder;
	builder.WithCompositionLookup(lookup)
		   .WithComponentStore(store)
		   .WithFactoryRegistry(&fakeFactory)
		   .Build("connection_dummy");

	IComponent* ic_a = store->Retrieve(component_a.componentClass,
                                       component_a.componentType,
			                           component_a.componentName);


	IComponent* ic_b = store->Retrieve(component_b.componentClass,
                                       component_b.componentType,
			                           component_b.componentName);

	ASSERT_EQ(ic_a, bc_a);
	ASSERT_EQ(ic_b, bc_b);
	ASSERT_EQ(bc_a->AllExpectedWereRequested(), true);
}

}  // namespace

// We are using
