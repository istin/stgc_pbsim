/*
 * MockScheduler.cc
 *
 *  Created on: Apr 8, 2019
 *      Author: ekajomov
 */


#include "gmock/gmock.h"
#include "Components/IScheduler.h"
#include "Components/ICommand.h"


namespace {

class MockScheduler //: public IScheduler
{
public:
	MOCK_METHOD2(Schedule, void( ICommand* command, double time_from_now) );
	MOCK_METHOD0(Notify, void() );
};

}
