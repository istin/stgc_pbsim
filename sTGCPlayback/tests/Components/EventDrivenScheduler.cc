//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include <tests/Components/MockCommand.h>
#include "gtest/gtest.h"
#include "Components/EventDrivenScheduler.h"



using ::testing::AtLeast;

// Tests that we don't have to define main() when we link to
// gtest_main instead of gtest.

namespace {

TEST(EventDrivenScheduler, test){

	MockCommand* command1 = new MockCommand;
	MockCommand* command11 = new MockCommand;
	MockCommand* command2 = new MockCommand;

	EXPECT_CALL(*command1, Execute())
		.Times(1);
	EXPECT_CALL(*command11, Execute())
		.Times(1);
	EXPECT_CALL(*command2, Execute())
		.Times(1);
	EventDrivenScheduler scheduler;
	scheduler.Schedule(command2,10, PBSim_Units::TIME_UNIT::ns);
	scheduler.Schedule(command1,5, PBSim_Units::TIME_UNIT::ns);
	scheduler.Schedule(command11,5, PBSim_Units::TIME_UNIT::ns);
	scheduler.Initialize();
	scheduler.Next();
	ASSERT_EQ(scheduler.GetElapsedTime(PBSim_Units::TIME_UNIT::ns), 5);
	scheduler.Next();
	ASSERT_EQ(scheduler.GetElapsedTime(PBSim_Units::TIME_UNIT::ns),10);

}

}  // namespace
