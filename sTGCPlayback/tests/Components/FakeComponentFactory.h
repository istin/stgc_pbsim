/*
 * FakeComponentFactory.h
 *
 *  Created on: Apr 22, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_TESTS_COMPONENTS_FAKECOMPONENTFACTORY_H_
#define STGCPLAYBACK_TESTS_COMPONENTS_FAKECOMPONENTFACTORY_H_

#include "Components/IComponentFactory.h"
#include <map>

class FakeFactory : public IComponentFactory
{
public:

	FakeFactory() {}
	virtual ~FakeFactory() {}
	virtual IComponent* Create(const TString& componentClass, const TString& componentType, const TString& componentName)
	{
		return m_components[Key(componentClass, componentType,componentName)];
	}

	TString Key(const TString& componentClass, const TString& componentType, const TString& componentName)
	{
		return componentClass + "_" + componentType + "_" + componentName;
	}

	std::map<TString, IComponent*> m_components;
};



#endif /* STGCPLAYBACK_TESTS_COMPONENTS_FAKECOMPONENTFACTORY_H_ */
