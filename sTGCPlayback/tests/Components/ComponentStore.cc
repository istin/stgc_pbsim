// Copyright 2006, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include "gtest/gtest.h"
#include "Components/ComponentStore.h"
#include "Components/BaseComponent.h"
#include "TRandom3.h"


// Tests that we don't have to define main() when we link to
// gtest_main instead of gtest.

namespace {

TEST(ComponentStore, AddRetrieveDelete){

	TRandom3 random;
	ComponentStore* store = ComponentStore::Instance();
	std::vector<BaseComponent*> components;
	TString cClass, cType, cName;

	for (int i = 0; i < 1e6; i++) {
		BaseComponent* component = new BaseComponent;
		component->SetClass(cClass = TString::Format("%1.6lf", random.Uniform(1)));
		component->SetType(cType = TString::Format("%1.6lf", random.Uniform(1)));
		component->SetName(cName = TString::Format("%1.6lf", random.Uniform(1)));
		IComponent* rc = store->Retrieve(cClass, cType, cName);
		if (rc != NULL)
			continue;
		components.push_back(component);
		store->Add(component);
	}

	for (int i = 0; i < 1e6; i++) {
		IComponent* retrievedComponent = store->Retrieve(components[i]->GetClass(),
				                                           components[i]->GetType(),
					                                       components[i]->GetName());
		ASSERT_EQ(components[i], retrievedComponent);
	}
}

TEST(ComponentStore, ReturnNullOnNonExistent){

	TRandom3 random;
	ComponentStore* store = ComponentStore::Instance();
	IComponent* retrievedComponent = store->Retrieve("aaa", "bbb", "ccc");
	ASSERT_EQ((retrievedComponent == NULL), true);
}

TEST(ComponentStore, FailOnAddingAnExistentComponent){

	TRandom3 random; // Since we have the same seed and the seed is the same we should fail here right away
	ComponentStore* store = ComponentStore::Instance();
	std::vector<BaseComponent*> components;
	BaseComponent* component = new BaseComponent;
	component->SetClass(TString::Format("%1.6lf", random.Uniform(1)));
	component->SetType(TString::Format("%1.6lf", random.Uniform(1)));
	component->SetName(TString::Format("%1.6lf", random.Uniform(1)));
	components.push_back(component);
	EXPECT_ANY_THROW(store->Add(component));
}

}  // namespace

