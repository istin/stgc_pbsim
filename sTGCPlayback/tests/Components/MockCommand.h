/*
 * MockCommand.cc
 *
 *  Created on: Apr 16, 2019
 *      Author: ekajomov
 */

#include "gmock/gmock.h"
#include "Components/ICommand.h"


namespace {

class MockCommand : public ICommand
{
public:
	MOCK_METHOD0(Execute, void() );
};

}
