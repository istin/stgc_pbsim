/*
 * FakeComponentToTestConnection.h
 *
 *  Created on: Apr 22, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_TESTS_CABLINGMODEL_FAKECOMPONENTTOTESTCONNECTION_H_
#define STGCPLAYBACK_TESTS_CABLINGMODEL_FAKECOMPONENTTOTESTCONNECTION_H_

#include "Components/BaseComponent.h"
#include <map>

struct ConectionBundle
{
	IComponent* source_component;
	TString source_port;
	TString target_port;
	double propagationDelay;
	double length;
	double bandwidth;
};


class FakeComponentToTestConnection : public BaseComponent
{
public:
	FakeComponentToTestConnection(){}
	virtual ~FakeComponentToTestConnection(){}


	virtual void AddExpectedConnectionRequest(IComponent* source_component, const TString& source_port, const TString& target_port,
			double propagationDelay, double length, double bandwidth)
	{
		ConectionBundle bundle;
		bundle.source_component = source_component;
		bundle.source_port      = source_port;
		bundle.target_port      = target_port;
		bundle.propagationDelay = propagationDelay;
		bundle.length           = length;
		bundle.bandwidth        = bandwidth;

		m_expectedConnections[source_component] = bundle;


	}


	virtual void ConnectSource(IComponent* source_component, const TString& source_port, const TString& target_port,
			double propagationDelay, double length, double bandwidth)
	{
		ConectionBundle bundle;
		bundle.source_component = source_component;
		bundle.source_port      = source_port;
		bundle.target_port      = target_port;
		bundle.propagationDelay = propagationDelay;
		bundle.length           = length;
		bundle.bandwidth        = bandwidth;

		m_requestedConnections[source_component] = bundle;
	}

	virtual bool AllExpectedWereRequested()
	{
		unsigned int totalRequested = 0;
		std::map<IComponent*, ConectionBundle>::iterator itr = m_expectedConnections.begin();
		for (; itr != m_expectedConnections.end(); ++itr)
		{
			ConectionBundle expected_bundle = itr->second;
			if (m_expectedConnections.find(itr->first) == m_expectedConnections.end())
				return false;
			ConectionBundle requested_bundle = m_expectedConnections[itr->first];

			bool ok = true;
			ok &= (requested_bundle.source_component == expected_bundle.source_component);
			ok &= (requested_bundle.source_port      == expected_bundle.source_port);
			ok &= (requested_bundle.target_port      == expected_bundle.target_port);
			ok &= (requested_bundle.propagationDelay == expected_bundle.propagationDelay);
			ok &= (requested_bundle.length           == expected_bundle.length);
			ok &= (requested_bundle.bandwidth        == expected_bundle.bandwidth);

			if (!ok)
				return false;
			totalRequested++;
		}
		if (totalRequested !=  m_expectedConnections.size())
			return false;
		return true;
	}

	std::map<IComponent*, ConectionBundle> m_expectedConnections;
	std::map<IComponent*, ConectionBundle> m_requestedConnections;





};



#endif /* STGCPLAYBACK_TESTS_CABLINGMODEL_FAKECOMPONENTTOTESTCONNECTION_H_ */
