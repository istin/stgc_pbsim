// Copyright 2006, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include "gtest/gtest.h"
#include "Detector/DetSvc.h"
#include <Detector/Sector.h>
#include <Detector/Side.h>
#include <Framework/PlaybackLoop.h>
#include <Detector/SectorBands.h>

#include <iostream>
#include <fstream>

#include <random>

// Tests that we don't have to define main() when we link to
// gtest_main instead of gtest.

namespace {

    const char* pbdatapath = std::getenv("PBDATAPATH");    
        
    TEST(EmptyTest, ShouldSucceed) {
    
        
    }

    TEST(EnvironmentTest, ShouldSucceed) {
        EXPECT_TRUE(pbdatapath!=nullptr);
    }
    //
    std::string xmlgeo=std::string(pbdatapath)+"/NSW_sTGC.xml";
    //
    //
    TEST(GeoXmlFileExists,ShouldSucceed){
        std::ifstream ifs(xmlgeo);
        EXPECT_TRUE(ifs.good());
    }
    //
    //


    TEST(DetSvcInit, ShouldSucceed) {
        auto detsvc=DetSvc::instance();
        detsvc->DumpConfig();
        //detsvc->dumpStripTable();
        EXPECT_TRUE(true);
    }
    
    
    TEST(SideWiseBuildTearDown,ShouldSucceed){
        
        auto sideA=Side(0);
        auto sideC=Side(1);
        
        int a=0;
        int b=0;
        for(const auto& sector : sideA){
            a+=sector->size();
        }
        
        for(const auto& sector : sideC){
            b+=sector->size();
        }        
        
        EXPECT_TRUE( a==b && a>0 && b>0 );
    }
    
    
    TEST(SectorWiseBuildTeardown,ShouldSucceed){
        auto ActiveSides={0,1};
        auto ActiveSectors={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
        std::vector<Sector> AllSectors;
        for(const auto& sideId : ActiveSides){
            for(const auto& secId : ActiveSectors){
                AllSectors.emplace_back(Sector(sideId,secId));
            }
        }
    
        int nsec_check=0;
        int nquad_check=0;
        for(const auto& sec : AllSectors){
            nsec_check++;
            nquad_check+=sec.size();
        }
        EXPECT_TRUE(nsec_check>0 && nquad_check>0);
    }    
    

    TEST(DetSvcRandomLookup, ShouldSucceed) {
        auto detsvc=DetSvc::instance();
        auto mastercfg=detsvc->getConfig();
        auto activeSectorsSideA=mastercfg.ActiveSectorsSideA;
        auto activeSectorsSideC=mastercfg.ActiveSectorsSideC;
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<> sector_distributionA(0,activeSectorsSideA.size()-1);
        std::uniform_int_distribution<> sector_distributionC(0,activeSectorsSideC.size()-1);
        std::uniform_int_distribution<> module_distribution(1,3);
        std::uniform_int_distribution<> multilayer_distribution(1,2);
        std::uniform_int_distribution<> layer_distribution(1,4);
        
        std::uniform_int_distribution<> strip_distribution(1,306);
        
        
        int ntrials=1E7;
         enum{SIDEA,SIDEC};
        for(int i=0;i<ntrials;i++){
            int randomSectorNo=activeSectorsSideA.at(sector_distributionA(gen));
            int randomModuleNo=module_distribution(gen);
            int randomMultilayerNo=multilayer_distribution(gen);
            int randomLayerNo=layer_distribution(gen);
            int randomStripIdNo=strip_distribution(gen);
            detsvc->getStrip(SIDEA,randomSectorNo,randomModuleNo,randomMultilayerNo,randomLayerNo,randomStripIdNo);
        }
        for(int i=0;i<ntrials;i++){
            int randomSectorNo=activeSectorsSideC.at(sector_distributionC(gen));
            int randomModuleNo=module_distribution(gen);
            int randomMultilayerNo=multilayer_distribution(gen);
            int randomLayerNo=layer_distribution(gen);
            int randomStripIdNo=strip_distribution(gen);
            detsvc->getStrip(SIDEC,randomSectorNo,randomModuleNo,randomMultilayerNo,randomLayerNo,randomStripIdNo);
        }        
        
        EXPECT_TRUE(true);
    }
    
    TEST(PlaybackLoop, ShouldSucceed) {
    
        PlaybackLoop pbl;

        //pbl.Loop();
    }    
    
    TEST(BandBuildingFromAGDD,ShouldSucceed){
        SectorBands sb;
        sb.buildBandsFromAGDD();
    }


    
}  // namespace

// We are using the main() function defined in gtest_main.cc, so we
// don't define it here.
