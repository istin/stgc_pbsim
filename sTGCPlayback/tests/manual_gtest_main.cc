/*
 * manual_gtest_main.cc
 *
 *  Created on: Feb 24, 2019
 *      Author: ekajomov
 */

#include "gtest/gtest.h"

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

