include_directories(${CMAKE_CURRENT_SOURCE_DIR}/..)

FILE(GLOB  UtilSources *.cxx)
FILE(GLOB UtilHeaders *.h)
list(FILTER UtilHeaders EXCLUDE REGEX "Linkdef.h$")

set(LIBNAME Utils)
ROOT_GENERATE_DICTIONARY(G__${LIBNAME} ${UtilHeaders} LINKDEF Linkdef.h MODULE)

add_library(${LIBNAME}  SHARED ${UtilHeaders} ${UtilSources}  G__${LIBNAME}.cxx)


target_link_libraries(${LIBNAME}  ${Boost_LIBRARIES} )

install(TARGETS ${LIBNAME}
    LIBRARY DESTINATION ${PLAYBACK_LIBS}
    )

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/lib${LIBNAME}_rdict.pcm DESTINATION ${PLAYBACK_LIBS}) 
