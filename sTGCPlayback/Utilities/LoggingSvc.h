#ifndef LOGGER_H
#define LOGGER_H

#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/expressions.hpp>
#include "boost/log/utility/setup.hpp"

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)


#define LOG_INFO(x)    BOOST_LOG_TRIVIAL(info)<<" "<<__FILENAME__<<"::"<<__func__<<" "<<x
#define LOG_TRACE(x)   BOOST_LOG_TRIVIAL(trace)<<" "<<__FILENAME__<<"::"<<__func__<<" "<<x
#define LOG_DEBUG(x)   BOOST_LOG_TRIVIAL(debug)<<" "<<__FILENAME__<<"::"<<__func__<<" "<<x
#define LOG_WARNING(x) BOOST_LOG_TRIVIAL(warning)<<" "<<__FILENAME__<<"::"<<__func__<<" "<<x
#define LOG_ERROR(x)   BOOST_LOG_TRIVIAL(error)<<" "<<__FILENAME__<<"::"<<__func__<<" "<<x
#define LOG_FATAL(x)   BOOST_LOG_TRIVIAL(fatal)<<" "<<__FILENAME__<<"::"<<__func__<<" "<<x



class LoggingSvc{
     
    LoggingSvc(const LoggingSvc& )=delete;
    LoggingSvc& operator=(const LoggingSvc&) = delete;
    LoggingSvc(LoggingSvc&&) = delete;
    LoggingSvc& operator=(LoggingSvc&&) = delete;

public:
    static LoggingSvc* instance(bool consoleout=false);
    void SetMsgLvl(int);
    ~LoggingSvc();
private:    
    LoggingSvc();
    static LoggingSvc* m_inst; 

};










#endif