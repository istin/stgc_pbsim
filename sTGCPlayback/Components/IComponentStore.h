/*
 * IComponentStore.h
 *
 *  Created on: Mar 12, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 *
 */

#ifndef STGCPLAYBACK_COMPONENTS_ICOMPONENTSTORE_H_
#define STGCPLAYBACK_COMPONENTS_ICOMPONENTSTORE_H_

#include "Components/IComponent.h"
#include "Components/IScheduler.h"

class IComponentStore
{
public:
	virtual ~IComponentStore(){};
	virtual void Add(IComponent* ) = 0;
	virtual IComponent* Retrieve(const TString& component_class, const TString& type,  const TString& name) = 0;
	virtual void DeleteComponents() = 0;
	virtual void SetScheduler(IScheduler* ) = 0;

};



#endif /* STGCPLAYBACK_COMPONENTS_ICOMPONENTSTORE_H_ */
