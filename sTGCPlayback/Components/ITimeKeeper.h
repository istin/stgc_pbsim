/*
 * ITimeKeeper.h
 *
 *  Created on: Apr 14, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_COMPONENTS_ITIMEKEEPER_H_
#define STGCPLAYBACK_COMPONENTS_ITIMEKEEPER_H_


#include "Components/ConstantsAndUnits.h"

class ITimeKeeper
{
public:
	ITimeKeeper(){}
	virtual ~ITimeKeeper()=default;

	virtual void SetStartingTime(long time_from_now, PBSim_Units::TIME_UNIT time_unit) = 0;
	virtual long GetCurrentTime(PBSim_Units::TIME_UNIT time_unit) = 0;
	virtual long GetElapsedTime(PBSim_Units::TIME_UNIT time_unit) = 0;

};



#endif /* STGCPLAYBACK_COMPONENTS_ITIMEKEEPER_H_ */
