/*
 * ConstantsAndUnits.h
 *
 *  Created on: Nov 30, 2017
 *      Author: ekajomov
 */

#ifndef CONSTANTSANDUNITS_H_
#define CONSTANTSANDUNITS_H_

namespace PBSim_Units
{
extern const double ns;
extern const double us;
extern const double ms;
extern const double s;


enum class TIME_UNIT
{
	ps = 0,
	ns = 3,
	us = 6,
	ms = 3,
	s = 9,
	UNDEFINED
};

double ConvertTimeUnit(PBSim_Units::TIME_UNIT from, PBSim_Units::TIME_UNIT to);

extern const double KHz;
extern const double MHz;
extern const double GHz;

extern const double m;
extern const double mm;
extern const double cm;

extern const double Pb_over_s;
extern const double Tb_over_s;
extern const double Gb_over_s;
extern const double Mb_over_s;
extern const double Kb_over_s;

extern const double ns_over_mm;
extern const double ns_over_cm;
extern const double ns_over_m;

extern const double bit;
extern const double byte;
extern const double Kb;
extern const double Mb;
extern const double Gb;
extern const double Tb;
extern const double KB;
extern const double MB;
extern const double GB;
extern const double TB;


}

#endif /* CONSTANTSANDUNITS_H_ */
