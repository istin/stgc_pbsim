/*
 * Clock.h
 *
 *  Created on: Apr 17, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */

#ifndef STGCPLAYBACK_COMPONENTS_CLOCK_H_
#define STGCPLAYBACK_COMPONENTS_CLOCK_H_

#include "Components/INotifiable.h"
#include "Components/IScheduler.h"
#include <vector>

class Clock {
public:


	Clock();
	virtual ~Clock();

	virtual void Activate();
	virtual void Stop();
	virtual bool IsActive();
	virtual void SetScheduler(IScheduler* scheduler);
	virtual void SetPeriodAndDutyCycle(long period, PBSim_Units::TIME_UNIT unit, double dutyCycle);
	virtual double GetDutyCycle();
	virtual long GetPeriod(PBSim_Units::TIME_UNIT unit);
	virtual const int & GetClockState();
	virtual void AddToNotificationList(INotifiable* notifiable);

protected:

	virtual void FlipAndScheduleNext();
	friend class ClockCommand;

	IScheduler* m_scheduler;

	int m_clockState;
	bool m_isActive;
	long m_period;
	PBSim_Units::TIME_UNIT m_periodUnit;
	double m_dutyCycle;
	long m_timeToNextFlipFromActive;
	long m_timeToNextFlipFromInnactive;
	std::vector<INotifiable*>m_notificationList;

};


class ClockCommand : public ICommand
{
public:
	ClockCommand(Clock* clock);
	virtual ~ClockCommand();
	virtual void Execute();

private:
	Clock* m_clock;

};
#endif /* STGCPLAYBACK_COMPONENTS_CLOCK_H_ */
