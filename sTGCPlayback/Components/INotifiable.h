/*
 * INotifiable.h
 *
 *  Created on: Apr 7, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_COMPONENTS_INOTIFIABLE_H_
#define STGCPLAYBACK_COMPONENTS_INOTIFIABLE_H_

class INotifiable
{
public:

	INotifiable(){}
	virtual ~INotifiable(){}
	virtual void Notify()=0;
};



#endif /* STGCPLAYBACK_CABLINGMODEL_INOTIFIABLE_H_ */
