/*
 * IComponentScheduler.h
 *
 *  Created on: Nov 29, 2017
 *      Author: Enrique.Kajomovitz@cern.ch
 */

#ifndef ICOMPONENTSCHEDULER_H_
#define ICOMPONENTSCHEDULER_H_

#include "Components/ICommand.h"
#include "Components/ConstantsAndUnits.h"


class IScheduler
{
public:

	IScheduler(){};
	virtual ~IScheduler(){};
	/************
	 *  Minimum time in that can be resolved by the scheduler
	 ************/
	virtual void SetTimeResolution(double minimum_value, PBSim_Units::TIME_UNIT time_unit) = 0;
	virtual double GetTimeResolution(PBSim_Units::TIME_UNIT time_unit) = 0;

	/************
	 * Schedule a command to be run in time_from_now
	 ************/
	virtual void Schedule(ICommand* command, long time_from_now, PBSim_Units::TIME_UNIT time_unit) = 0;

	virtual void Initialize() = 0;
	virtual bool Next() = 0;

};



#endif /* ICOMPONENTSCHEDULER_H_ */
