/*
 * CablingSystemBuilder.cxx
 *
 *  Created on: Apr 21, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */

#include <Components/CablingSystemBuilder.h>
#include <iostream>
#include <map>
#include <Utilities/LoggingSvc.h>
#include <Utilities/LoggingSvc.h>

CablingSystemBuilder::CablingSystemBuilder():
m_factoryRegistry(0), m_componentStore(0),m_scheduler(nullptr){
}

CablingSystemBuilder::~CablingSystemBuilder() {
}


void CablingSystemBuilder::Build(TString systemName)
{
	std::map<TString, CablingComposition>::iterator compItr = m_compositionLookup.find(systemName);
	if (compItr == m_compositionLookup.end())
	{
		LOG_FATAL("Cannot build this composition!");
		throw std::runtime_error("composition component not found in lookup");
		return;
	}

	CablingComposition composition = compItr->second;

	BuildComposition(composition.GetResolved(composition.dictionary)); // Will this add to the components the w
	HandleConnections();
}


void CablingSystemBuilder::BuildComposition(CablingComposition composition)
{

	m_componentStore->SetScheduler(m_scheduler);

	for (unsigned int i_compoundComponent = 0;
		i_compoundComponent < composition.compoundComponents.size();
		i_compoundComponent++) {
		CompoundComponent cc = composition.compoundComponents[i_compoundComponent];
		std::map<TString, CablingComposition>::iterator compItr = m_compositionLookup.find(cc.compositionName);
		if (compItr == m_compositionLookup.end()) {
			LOG_FATAL("Cannot build this composition!");
			throw std::runtime_error("composition component not found in lookup");
			return;
		}
		CablingComposition daughterComposition = compItr->second.GetResolved(cc.dictionary);
		BuildComposition(daughterComposition);
	}

	for (unsigned int i_simpleComponent = 0;i_simpleComponent  < composition.simpleComponents.size();i_simpleComponent++) {
		TString cClass = composition.simpleComponents[i_simpleComponent].componentClass;
		TString cType  = composition.simpleComponents[i_simpleComponent].componentType;
		TString cName  = composition.simpleComponents[i_simpleComponent].componentName;
		m_componentStore->Add( m_factoryRegistry->Create(cClass, cType, cName));
	}

	for (unsigned int i_connection = 0;
			 i_connection  < composition.connections.size();
			 i_connection++) {
		AddConnection(composition.connections[i_connection]);
	}

}


void CablingSystemBuilder::HandleConnections() {
	if (m_componentStore == NULL)
		return;
	LOG_INFO("Handling connections");
	for (auto & description : m_connections) {

		LOG_TRACE("Retrieving source: " << description.sourceClass << " " <<  description.sourceType << " " << description.sourceName);
		IComponent* source = m_componentStore->Retrieve(description.sourceClass,
				                                        description.sourceType,
														description.sourceName);

		LOG_TRACE("Retrieving destination: " << description.destinationClass << " " <<  description.destinationType << " " << description.destinationName);
		IComponent* dest = m_componentStore->Retrieve(description.destinationClass,
				                                      description.destinationType,
													  description.destinationName);

		
		
 
		double clength   = description.cableLength.Atof();
		double pdelay    = description.propagationDelay.Atof();
		double bandwidth = description.bandwidth.Atof();
	
		
		if (dest == NULL){
			LOG_FATAL("FATAL: could not find  dest: "<<   description.destinationClass << " " <<  description.destinationType << " " << description.destinationName << " (" << dest << ") ");
		}
		if (source  == NULL){
			LOG_FATAL("could not find source: "<<   description.sourceClass << " " <<  description.sourceType << " " << description.sourceName << " (" << source << ") ");
		}

 
		if(dest == NULL || source == NULL){
			return;
		}


		dest->ConnectSource(source,
				            description.destinationPort,
				            description.sourcePort,
							pdelay,
							clength,
							bandwidth);




	}
}

void CablingSystemBuilder::AddConnection(
		ComponentConnection description) {
	m_connections.push_back(description);
}

CablingSystemBuilder& CablingSystemBuilder::WithScheduler( IScheduler* sch) {
	m_scheduler = sch;
	return *this;
}


CablingSystemBuilder& CablingSystemBuilder::WithCompositionLookup(std::map<TString, CablingComposition> lookup) {
	m_compositionLookup = lookup;
	return *this;
}

CablingSystemBuilder& CablingSystemBuilder::WithComponentStore(IComponentStore* store) {
	m_componentStore = store;
	return *this;
}

CablingSystemBuilder& CablingSystemBuilder::WithFactoryRegistry(IComponentFactory* factory) {
	m_factoryRegistry = factory;
	return *this;
}
