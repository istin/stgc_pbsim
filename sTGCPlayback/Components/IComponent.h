/*
 * IComponent.h
 *
 *  Created on: Nov 29, 2017
 *      Author: Enrique.Kajomovitz@CERN.CH
 */

#ifndef ICOMPONENT_H_
#define ICOMPONENT_H_

#include <vector>
#include <TString.h>

#include "Components/IScheduler.h"


class IComponent {
public:

	virtual ~IComponent(){};

	virtual TString GetName() = 0;
	virtual TString GetClass() = 0;
	virtual TString GetType() = 0;

	virtual void SetScheduler(IScheduler* scheduler) = 0;

	virtual void ConnectSource(IComponent* source_component,
			                   const TString& source_port,
							   const TString& target_port,
			                   double propagationDelay,
							   double length, double bandwith) = 0;

};

#endif /* ICOMPONENT_H_ */

