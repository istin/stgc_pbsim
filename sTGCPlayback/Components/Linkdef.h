#include <Components/Link.h>
#include <Components/BaseComponent.h>
#include <Components/Cable.h>
#include <Components/Clock.h>
#include <Components/CablingSystemBuilder.h>
#include <Components/ComponentFactoryRegistry.h>
#include <Components/ComponentStore.h>
#include <Components/ConstantsAndUnits.h>
#include <Components/DelayedTransmitter.h>
#include <Components/EventDrivenScheduler.h>
#include <Components/ICommand.h>
#include <Components/IComponentFactory.h>
#include <Components/IComponent.h>
#include <Components/IComponentStore.h>
#include <Components/IComponent.h>
#include <Components/INotifiable.h>
#include <Components/IResetable.h>
#include <Components/IScheduler.h>
#include <Components/ITimeKeeper.h>
#include <Components/ITransmitter.h>
#include <Components/LinkTransmitter.h>
#include <Components/ResetCommand.h>
#include <Components/TransmitCommand.h>

#include "Components/ComponentFactoryRegistry.h"

#include <vector>






#ifdef __ROOTCLING__


#pragma link C++ class BaseComponent+;
#pragma link C++ class Cable+;
#pragma link C++ class CablingSystemBuilder+;
#pragma link C++ class ComponentStore+;
#pragma link C++ class Clock+;
#pragma link C++ class ConstantsAndUnits+;
#pragma link C++ class DelayedTransmitter+;
#pragma link C++ class EventDrivenScheduler+;
#pragma link C++ class ICommand+;
#pragma link C++ class IComponentFactory+;
#pragma link C++ class IComponent+;
#pragma link C++ class IComponentStore+;
#pragma link C++ class IComponent+;
#pragma link C++ class INotifiable+;
#pragma link C++ class IResetable+;
#pragma link C++ class IScheduler+;
#pragma link C++ class ITimeKeeper+;
#pragma link C++ class ITransmitter+;
#pragma link C++ class LinkTransmitter+;
#pragma link C++ class ResetCommand+;
#pragma link C++ class TransmitCommand+;

#pragma link C++ class Link<double>+;
#pragma link C++ class Cable<double>+;
#endif
