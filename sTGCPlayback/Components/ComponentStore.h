/*
 * ComponentStore.h
 *
 *  Created on: Mar 13, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */


#ifndef STGCPLAYBACK_COMPONENTS_COMPONENTSTORE_H_
#define STGCPLAYBACK_COMPONENTS_COMPONENTSTORE_H_



#include <unordered_map>
#include "Components/IComponentStore.h"
#include "Components/IScheduler.h"
#include <string>
class ComponentStore : public IComponentStore
{
public:



	static ComponentStore* Instance();

    virtual void Add(IComponent*); // Once added the components are owned by the Store

    virtual IComponent* Retrieve(const TString& component_class, const TString& type,  const TString& name);
	virtual void DeleteComponents();
	virtual void SetScheduler(IScheduler* sch);
	IScheduler* GetScheduler() { return m_scheduler;}
	virtual ~ComponentStore();

protected:

	ComponentStore();
	std::string Key(const TString& component_class, const TString& type,  const TString& name);

	static ComponentStore* m_instance;
	std::unordered_map<std::string, IComponent*> m_storage;
	IScheduler* m_scheduler;

};

#endif /* STGCPLAYBACK_COMPONENTS_COMPONENTSTORE_H_ */
