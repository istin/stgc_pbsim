/*
 * IComponentFactory.h
 *
 *  Created on: Mar 12, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */

#ifndef STGCPLAYBACK_COMPONENTS_ICOMPONENTFACTORY_H_
#define STGCPLAYBACK_COMPONENTS_ICOMPONENTFACTORY_H_

#include "Components/IComponent.h"
#include "Components/IScheduler.h"

class IComponentFactory
{
public:
	virtual ~IComponentFactory(){};
	virtual IComponent* Create(const TString& componentClass, const TString& componentType, const TString& componentName) = 0;
};



#endif /* STGCPLAYBACK_COMPONENTS_ICOMPONENTFACTORY_H_ */
