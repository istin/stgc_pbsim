/*
 * Link.h
 *
 *  Created on: Nov 29, 2017
 *      Author: ekajomov
 */

#ifndef LINK_H_
#define LINK_H_

#include "Components/INotifiable.h"
#include <iostream>


template <typename T>
class Link
{

public:

Link():m_notifiable(0) {}


   virtual ~Link(){};

   T GetValue()  {
      return m_value;
   }

   void SetValue(T value) {
       this->m_value = value;
       if (m_notifiable != 0){
    	   m_notifiable->Notify();
       }
   }

   void SetNotifiable(INotifiable* notifiable){
	   m_notifiable = notifiable;
   }

protected:
   INotifiable* m_notifiable;
   T m_value;

};

#endif /* LINK_H_ */
