/*
 * Scheduler.h
 *
 *  Created on: Apr 13, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_COMPONENTS_EVENTDRIVENSCHEDULER_H_
#define STGCPLAYBACK_COMPONENTS_EVENTDRIVENSCHEDULER_H_


#include <map>
#include <vector>

#include "Components/IScheduler.h"
#include "Components/ITimeKeeper.h"


class EventDrivenScheduler : public IScheduler,  public ITimeKeeper {
public:

	EventDrivenScheduler();
	virtual ~EventDrivenScheduler();

	virtual void SetTimeResolution(double tr, PBSim_Units::TIME_UNIT time_unit);
	virtual double GetTimeResolution(PBSim_Units::TIME_UNIT time_unit);


	virtual void Schedule(ICommand* command, long time_from_now, PBSim_Units::TIME_UNIT time_unit);

	virtual void SetStartingTime(long time, PBSim_Units::TIME_UNIT time_unit);
	virtual long GetCurrentTime(PBSim_Units::TIME_UNIT time_unit);
	virtual long GetElapsedTime(PBSim_Units::TIME_UNIT time_unit);

	virtual void SetRandomizerSeed(long randomizer_seed);
	virtual long GetRandomizerSeed();
	virtual void Initialize();
	virtual bool Next();
protected:
	long m_startingTime;
	PBSim_Units::TIME_UNIT m_startingTime_unit;
	double m_timeResolution;
	PBSim_Units::TIME_UNIT m_timeResolution_unit;
	long m_elapsedTime;
	long m_randomizerSeed;
	std::vector<ICommand*> m_executing;
	
	
	std::map<long, std::vector <ICommand*> > m_commandRegistry;
	std::map<long, std::vector <ICommand*> >::iterator m_nextCommands;
	bool m_triggerExitFromNext;





};

#endif /* STGCPLAYBACK_COMPONENTS_EVENTDRIVENSCHEDULER_H_ */
