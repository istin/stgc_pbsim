/*
 * ICommand.h
 *
 *  Created on: Apr 7, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_COMPONENTS_ICOMMAND_H_
#define STGCPLAYBACK_COMPONENTS_ICOMMAND_H_

class ICommand
{
public:
	ICommand(){}
	virtual ~ICommand(){}
	virtual void Execute() = 0;
};


#endif /* STGCPLAYBACK_COMPONENTS_ICOMMAND_H_ */
