/*
 * DelegatingComponentFactory.cxx
 *
 *  Created on: Mar 12, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */

#include <Components/ComponentFactoryRegistry.h>
#include <iostream>
#include <Utilities/LoggingSvc.h>


ComponentFactoryRegistry::ComponentFactoryRegistry(){
}

ComponentFactoryRegistry::~ComponentFactoryRegistry() {
}

IComponent* ComponentFactoryRegistry::Create(const TString& componentClass,
		const TString& componentType, const TString& componentName) {

	LOG_TRACE("ComponentFactoryRegistry: Create( " << componentClass << ", " << componentType << ", " << componentName << ")");
	std::map<TString, IComponentFactory*>::iterator itr = factoryLookup.find(componentClass);
	if (itr == factoryLookup.end() )
	{
		LOG_ERROR("cannot find the factory for: " << componentClass);
		return NULL;
	}
	LOG_TRACE("calling the factory for " << componentClass);
	IComponent* cmp = itr->second->Create(componentClass,componentType,componentName);
	return cmp;
}




void ComponentFactoryRegistry::RegisterFactory(TString factory_class, IComponentFactory* factory) {
	if (factoryLookup.find(factory_class) != factoryLookup.end() )
	{
		LOG_WARNING("WARNING: Trying to add more than one factory of the same class");
	}
	factoryLookup[factory_class] = factory;
	return;
}

