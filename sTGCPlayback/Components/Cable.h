/*
 * Cable.h
 *
 *  Created on: Apr 7, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */

#ifndef STGCPLAYBACK_COMPONENTS_CABLE_H_
#define STGCPLAYBACK_COMPONENTS_CABLE_H_

#include "Components/INotifiable.h"
#include "Components/IScheduler.h"
#include "Components/Link.h"
#include "Components/ConstantsAndUnits.h"
#include "Utilities/LoggingSvc.h"
#include <iostream>

template <class T>
class Cable : public INotifiable {
public:

	Cable(): m_inputLink(NULL), m_outputLink(NULL), m_free(true){}
	virtual ~Cable() {}

	void SetPropagationDelay(double pDelay_in_ns_per_meter) {m_pDelay = pDelay_in_ns_per_meter;}
	double GetPropagationDelay() const {return m_pDelay;}

	void SetLength(double length) {m_length = length;}
	double GetLength() const {return m_length;}

	void SetMessageSize(long messageSize_in_bits) {m_messageSize = messageSize_in_bits;}
	int GetMessageSize() const {return m_messageSize;}

	void SetBandwidth(double bandwidth) { m_bandwidth = bandwidth; }
	double GetBandwidth() const {return m_bandwidth; }

	void SetScheduler(IScheduler* scheduler) {m_scheduler = scheduler;}
	IScheduler* GetScheduler() const {return m_scheduler;}

	void SetInputLink(Link<T>& in) { 
		LOG_TRACE(" being called");
		m_inputLink = &in; in.SetNotifiable(this); 
	}
	Link<T>& GetInputLink() {return *m_inputLink; }

	void SetOutputLink(Link<T>& out) {
		LOG_TRACE(" being called");
		m_outputLink = &out;
	}
	Link<T>& GetOutputLink() {return *m_outputLink;}

	void Release() { 
		LOG_TRACE(" being called");
		m_free = true;
	}
	bool IsFree() {return m_free;}


	//////////////// NOTIFY //////////

	virtual void Notify()
	{
		LOG_TRACE(" being called");
		if (!m_free)
		{
			LOG_ERROR("Trying to transmit on a busy cable!");
			throw std::runtime_error("Trying to transmit on a busy cable");
		}
		
		m_free = false;
		//double second = PBSim_Units::s;
		if(m_bandwidth == 0){
			LOG_ERROR("Division by zero");
			throw std::runtime_error("Division by zero!");
		}
		double busyTime = (double) (m_messageSize) / ((m_bandwidth)) ;

		ICommand* release = CreateReleaseCommand();
		LOG_DEBUG(" scheduling Release command with busyTime="<<busyTime);
		m_scheduler->Schedule(release,
				busyTime * PBSim_Units::ConvertTimeUnit(PBSim_Units::TIME_UNIT::ns,PBSim_Units::TIME_UNIT::ps),
				PBSim_Units::TIME_UNIT::ps);


		double total_propagation_delay = m_length  *  m_pDelay;
		ICommand* transmit = CreateTransmissionCommand();
		LOG_DEBUG(" scheduling Transmit command at total_propagatin_delay="<<total_propagation_delay);
		m_scheduler->Schedule(transmit,
				(total_propagation_delay + busyTime) * PBSim_Units::ConvertTimeUnit(PBSim_Units::TIME_UNIT::ns,PBSim_Units::TIME_UNIT::ps)
						, PBSim_Units::TIME_UNIT::ps);
		return;
	}


	class CableTransmissionCommand : public ICommand {
	public:
		CableTransmissionCommand(T content, Link<T>& outgoingLink)
	:m_capturedContent(content), m_outgoingLink(outgoingLink)
	{

	}

		virtual ~CableTransmissionCommand(){}

		void Execute()
		{
			LOG_TRACE("Being Called");
			m_outgoingLink.SetValue(m_capturedContent);
		}
	protected:

		T m_capturedContent;
		Link<T>& m_outgoingLink;

	};

	class CableReleaseCommand : public ICommand {
	public:
		CableReleaseCommand(Cable<T>& cable)
	: m_cable(cable)
	{

	}

		virtual ~CableReleaseCommand(){}

		void Execute()
		{
			m_cable.Release();
		}
	protected:

		Cable<T>& m_cable;
	};

protected:

	ICommand* CreateTransmissionCommand() {
		ICommand* co = new CableTransmissionCommand(m_inputLink->GetValue(), *m_outputLink);
	    return co;
	    }
ICommand* CreateReleaseCommand(){ return new CableReleaseCommand(*this);}

	bool m_free;
	IScheduler* m_scheduler;
	double m_pDelay;
	double m_length;
	long m_messageSize;
	double m_bandwidth;
	Link<T>* m_inputLink;
	Link<T>* m_outputLink;
};



#endif /* STGCPLAYBACK_COMPONENTS_CABLE_H_ */
