/*
 * TransmitCommand.cxx
 *
 *  Created on: Apr 11, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */

#include "Components/TransmitCommand.h"
#include <Utilities/LoggingSvc.h>
TransmitCommand::TransmitCommand(ITransmitter* transmitter, bool ownTransmitter)
:m_transmitter(transmitter), m_ownTransmitter(ownTransmitter)
{
}

TransmitCommand::~TransmitCommand() {
	if (m_ownTransmitter)
	{
		delete m_transmitter;
	}
}

void TransmitCommand::Execute()
{
	if (m_transmitter == 0){
		LOG_FATAL("here");
		throw std::runtime_error("Cannot execute for a null transmitter");
	}
	else
		m_transmitter->Transmit();
}
