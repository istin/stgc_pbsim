/*
 * ResetCommand.cxx
 *
 *  Created on: Apr 10, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */

#include "Components/ResetCommand.h"

ResetCommand::ResetCommand(IResetable* resetable):
m_resetable(resetable){
}

ResetCommand::~ResetCommand() {
}

void ResetCommand::Execute() {
	m_resetable->Reset();
}
