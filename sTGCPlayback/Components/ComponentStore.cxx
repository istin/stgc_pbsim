/*
 * ComponentStore.cxx
 *
 *  Created on: Mar 13, 2019
 *      Author: ekajomov
 */

#include <Components/ComponentStore.h>
#include <iostream>
#include <assert.h>
#include <Utilities/LoggingSvc.h>
ComponentStore* ComponentStore::m_instance = 0;
ComponentStore* ComponentStore::Instance() {

	if (m_instance)
		return m_instance;
	else
	{
		m_instance = new ComponentStore();
		return m_instance;
	}
}

void ComponentStore::Add(IComponent* component) {
	if (component == NULL)
		return;
	std::string key = Key(component->GetClass(),
			component->GetType(),
			component->GetName());
	std::unordered_map<std::string, IComponent*>::iterator itr= m_storage.find(key);
	if (itr != m_storage.end()){
		LOG_FATAL("here");
		throw std::runtime_error("FATAL: attempting to add a component twice");
	}
	m_storage[key] = component;
	component->SetScheduler(m_scheduler);
}

IComponent* ComponentStore::Retrieve(const TString& component_class,
		const TString& type, const TString& name) {
	std::string key = Key(component_class, type, name);
	std::unordered_map<std::string, IComponent*>::iterator itr= m_storage.find(key);
	if (itr == m_storage.end())
		return NULL;
	return itr->second;

}

void ComponentStore::SetScheduler(IScheduler* scheduler)
{
	m_scheduler = scheduler;
}

void ComponentStore::DeleteComponents() {
	auto it = m_storage.begin();

	while (it != m_storage.end()) {
		IComponent* component = it->second;
	        assert(m_storage.size() >= 1);
	        {
	        	delete component;
	        	it = m_storage.erase(it);  // <-- Return value should be a valid iterator.
	        }
	    }
}

ComponentStore::ComponentStore() {
}

ComponentStore::~ComponentStore() {
}

std::string ComponentStore::Key(const TString& component_class,
		const TString& type, const TString& name) {

	std::string key = TString::Format("C:%s_T:%s_N:%s", component_class.Data(), type.Data(), name.Data()).Data();
	return key;
}
