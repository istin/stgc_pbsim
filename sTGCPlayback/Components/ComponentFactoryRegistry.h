/*
 * AggregateComponentFactory.h
 *
 *  Created on: Mar 12, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 *
 */

#ifndef STGCPLAYBACK_COMPONENTS_COMPONENTFACTORYREGISTRY_H_
#define STGCPLAYBACK_COMPONENTS_COMPONENTFACTORYREGISTRY_H_

#include "Components/IComponentFactory.h"
#include <map>

class ComponentFactoryRegistry: public IComponentFactory
{
public:
	ComponentFactoryRegistry();
	virtual ~ComponentFactoryRegistry();

	virtual IComponent* Create(const TString& componentClass, const TString& componentType, const TString& componentName);
	virtual void RegisterFactory(TString factory_class, IComponentFactory* factory);

protected:

	std::map<TString, IComponentFactory*> factoryLookup;

};

#endif /* STGCPLAYBACK_COMPONENTS_COMPONENTFACTORYREGISTRY_H_ */
