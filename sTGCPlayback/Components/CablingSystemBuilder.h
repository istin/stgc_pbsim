/*
 * CablingSystemBuilder.h
 *
 *  Created on: Apr 21, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_COMPONENTS_CABLINGSYSTEMBUILDER_H_
#define STGCPLAYBACK_COMPONENTS_CABLINGSYSTEMBUILDER_H_

#include "Components/IComponentFactory.h"
#include "Components/IComponentStore.h"
#include "CablingModel/CompoundComponent.h"
#include "CablingModel/CablingComposition.h"

#include "TString.h"
#include <map>

class CablingSystemBuilder {
public:


	CablingSystemBuilder();
	virtual ~CablingSystemBuilder();


	virtual CablingSystemBuilder& WithCompositionLookup(std::map<TString, CablingComposition> lookup);
	virtual CablingSystemBuilder& WithComponentStore(IComponentStore* store);
	virtual CablingSystemBuilder& WithFactoryRegistry(IComponentFactory* factory);
	virtual void Build(TString systemName);
	virtual CablingSystemBuilder& WithScheduler( IScheduler * ); 

protected:


	IComponentFactory* m_factoryRegistry;
	IComponentStore* m_componentStore;
	std::map<TString, CablingComposition> m_compositionLookup;
	std::vector<ComponentConnection> m_connections;
	IScheduler* m_scheduler;

	virtual void BuildComposition(CablingComposition composition);
	virtual void HandleConnections();
	virtual void AddConnection(ComponentConnection description);
};

#endif /* STGCPLAYBACK_COMPONENTS_CABLINGSYSTEMBUILDER_H_ */
