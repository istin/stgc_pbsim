/*
 * LinkTransmitter.h
 *
 *  Created on: Apr 11, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_COMPONENTS_LINKTRANSMITTER_H_
#define STGCPLAYBACK_COMPONENTS_LINKTRANSMITTER_H_

#include "Components/TransmitCommand.h"
#include "Components/ITransmitter.h"
#include "Components/ICommand.h"
#include "Components/Link.h"


template <class T>
class LinkTransmitter : public ITransmitter
{
public:
	LinkTransmitter(Link<T>& link, T value);
	virtual ~LinkTransmitter();

	virtual void Transmit()
	{
		m_link->SetValue(value);
	}

	static ICommand* TransmitCommand(Link<T>& link, T value)
	{
		LinkTransmitter<T>* transmitter = new  LinkTransmitter<T>(link, value);
		ICommand* command = new class TransmitCommand(transmitter, true);
		return command;
	}

protected:
	Link<T>& m_link;
	T value;
};


#endif /* STGCPLAYBACK_COMPONENTS_LINKTRANSMITTER_H_ */
