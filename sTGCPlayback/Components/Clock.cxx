/*
 * Clock.cxx
 *
 *  Created on: Apr 17, 2019
 *      Author: ekajomov
 */

#include "Components/Clock.h"
#include "Components/ConstantsAndUnits.h"
#include "TMath.h"
#include <iostream>
#include <Utilities/LoggingSvc.h>
Clock::Clock()
:m_scheduler(0),
 m_clockState(0),
 m_isActive(false),
 m_period(0),
 m_periodUnit(PBSim_Units::TIME_UNIT::UNDEFINED),
 m_dutyCycle(0),
 m_timeToNextFlipFromActive(0),
 m_timeToNextFlipFromInnactive(0)
{

}



Clock::~Clock() {
}

void Clock::Activate()
{
	if (m_isActive == true)
		return;
	m_isActive = true;
	FlipAndScheduleNext();
}

void Clock::Stop()
{
	m_isActive = false;
}


bool Clock::IsActive()
{
	return m_isActive;
}

double Clock::GetDutyCycle()
{
	return m_dutyCycle;
}

long Clock::GetPeriod(PBSim_Units::TIME_UNIT unit)
{
	return m_period * PBSim_Units::ConvertTimeUnit(m_periodUnit, unit);
}
void Clock::SetScheduler(IScheduler* scheduler)
{
	m_scheduler = scheduler;
}

void Clock::SetPeriodAndDutyCycle(long period, PBSim_Units::TIME_UNIT unit, double dutyCycle)
{
	m_period = period;
	m_periodUnit = unit;
	m_dutyCycle = dutyCycle;
	m_timeToNextFlipFromActive = TMath::Nint(period * dutyCycle);
	m_timeToNextFlipFromInnactive = period - m_timeToNextFlipFromActive;
}

const int & Clock::GetClockState()
{
	return m_clockState;
}

void Clock::AddToNotificationList(INotifiable* notifiable)
{
	if (notifiable == 0)
	{
		std::cout << "FATAL: Clock trying to add a null notifiable to list" << std::endl;
		LOG_FATAL("here");
		throw std::runtime_error("Cannot add a null notifiable!");
	}
	m_notificationList.push_back(notifiable);
}

void Clock::FlipAndScheduleNext()
{

	if (!m_isActive)
		return;

	long timeToNextFlip;
	if (m_clockState == 0)
	{
		timeToNextFlip = m_timeToNextFlipFromInnactive;
		m_clockState = 1;
	}
	else
	{
		timeToNextFlip = m_timeToNextFlipFromActive;
		m_clockState = 0;
	}

	m_scheduler->Schedule(new ClockCommand(this),
			timeToNextFlip,
			m_periodUnit);

	for (unsigned int i_notify = 0; i_notify < m_notificationList.size(); i_notify++)
	{
		m_notificationList[i_notify]->Notify();
	}
}

ClockCommand::ClockCommand(Clock* clock)
:m_clock(clock)
{
}

ClockCommand::~ClockCommand()
{
}

void ClockCommand::Execute()
{
	m_clock->FlipAndScheduleNext();
}
