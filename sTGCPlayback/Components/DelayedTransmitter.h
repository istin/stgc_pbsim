/*
 * DelayedTransmitter.h
 *
 *  Created on: Apr 10, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */

#ifndef STGCPLAYBACK_COMPONENTS_DELAYEDTRANSMITTER_H_
#define STGCPLAYBACK_COMPONENTS_DELAYEDTRANSMITTER_H_


#include "Components/INotifiable.h"
#include "Components/IScheduler.h"
#include "Components/Link.h"
#include "Components/ConstantsAndUnits.h"

#include <iostream>

template <class T>
class DelayedTransmitter {
public:

	DelayedTransmitter(): m_outputLink(NULL), m_scheduler(NULL) {}
	virtual ~DelayedTransmitter() {}

	void SetScheduler(IScheduler* scheduler) {m_scheduler = scheduler;}
	IScheduler* GetScheduler() const {return m_scheduler;}

	void SetOutputLink(Link<T>& out) {m_outputLink = &out;}
	Link<T>& GetOutputLink() {return *m_outputLink;}


	//////////////// NOTIFY //////////

	virtual void Transmit(T value, double time_from_now)
	{
		ICommand* transmit = CreateTransmissionCommand(value);
		m_scheduler->Schedule(transmit,
				time_from_now * PBSim_Units::ConvertTimeUnit(PBSim_Units::TIME_UNIT::ns,PBSim_Units::TIME_UNIT::ps),
				PBSim_Units::TIME_UNIT::ps);
		return;
	}


	class DelayedTrasnmitterCommand : public ICommand {
	public:
		DelayedTrasnmitterCommand(T content, Link<T>& outgoingLink)
	:m_capturedContent(content), m_outgoingLink(outgoingLink)
	{

	}

		virtual ~DelayedTrasnmitterCommand(){}

		void Execute()
		{
			m_outgoingLink.SetValue(m_capturedContent);
		}
	protected:

		T m_capturedContent;
		Link<T>& m_outgoingLink;

	};


protected:

	ICommand* CreateTransmissionCommand(T content) {
		ICommand* co = new DelayedTrasnmitterCommand(content, *m_outputLink);
	    return co;
	    }

	Link<T>* m_outputLink;
	IScheduler* m_scheduler;
};

#endif /* STGCPLAYBACK_COMPONENTS_DELAYEDTRANSMITTER_H_ */
