/*
 * BaseComponent.h
 *
 *  Created on: Nov 29, 2017
 *      Author: Enrique.Kajomovitz@CERN.CH
 */

#ifndef BASECOMPONENT_H_
#define BASECOMPONENT_H_


#include <Components/IScheduler.h>
#include "IComponent.h"
#include "TString.h"
#include <iostream>

class BaseComponent: public IComponent
{
public:
	BaseComponent();
	virtual ~BaseComponent();


	virtual void SetScheduler(IScheduler* scheduler)
	{
		m_scheduler = scheduler;
	}

	virtual IScheduler* GetScheduler()
	{
		return m_scheduler;
	}

	virtual TString GetName() {return component_name;}
	virtual TString GetClass() {return component_class;}
	virtual TString GetType() {return component_type;}

	virtual void SetName(const TString& name) {component_name = name;}
	virtual void SetClass(const TString& co_class) {component_class = co_class;}
	virtual void SetType(const TString& type) {component_type = type;}

	virtual void ConnectSource(IComponent* source_component, const TString& source_port, const TString& target_port,
			double propagationDelay, double length, double bandwidth);

protected:

	bool m_scheduled;
	IScheduler* m_scheduler;

	TString component_class;
	TString component_type;
	TString component_name;

};

#endif /* BASECOMPONENT_H_ */
