/*
 * ResetCommand.h
 *
 *  Created on: Apr 10, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_COMPONENTS_RESETCOMMAND_H_
#define STGCPLAYBACK_COMPONENTS_RESETCOMMAND_H_

#include "Components/ICommand.h"
#include "Components/IResetable.h"

class ResetCommand : public ICommand {
public:
	ResetCommand(IResetable* resetable);
	virtual ~ResetCommand();
	virtual void Execute();
protected:
	IResetable* m_resetable;
};

#endif /* STGCPLAYBACK_COMPONENTS_RESETCOMMAND_H_ */
