/*
 * BaseComponent.cpp
 *
 *  Created on: Nov 29, 2017
 *      Author: Enrique.Kajomovitz@CERN.Ch
 */

#include "Components/BaseComponent.h"
#include <iostream>

BaseComponent::BaseComponent():
m_scheduled(false), m_scheduler(0)
{
}

BaseComponent::~BaseComponent()
{

}


void BaseComponent::ConnectSource(IComponent* source_component,
		const TString& source_port, const TString& target_port,
		double propagationDelay, double length, double bandwith) {
	//throw "Can't connect source to base component";
	//S.I Jul8/19 : throws like above wuite dangerous. They throw unknown exception which are quite painful to debug.
	//Some of the Components inherited from BaseComponent do not have ConnectSource implemented yet thus calling the implementation above with unknown exception.
	// Principally we should remove the imlpelemtation turning BaseComponent pure virtual to avoid nasty runtime errors

	return;
}