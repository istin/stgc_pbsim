/*
 * TransmitCommand.h
 *
 *  Created on: Apr 11, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_COMPONENTS_TRANSMITCOMMAND_H_
#define STGCPLAYBACK_COMPONENTS_TRANSMITCOMMAND_H_

#include "Components/ITransmitter.h"
#include "Components/ICommand.h"

class TransmitCommand : public ICommand {
public:
	TransmitCommand(ITransmitter* transmitter, bool ownTransmitter = false);
	virtual ~TransmitCommand();
	virtual void Execute();

protected:

	ITransmitter* m_transmitter;
	bool m_ownTransmitter;

};

#endif /* STGCPLAYBACK_COMPONENTS_TRANSMITCOMMAND_H_ */
