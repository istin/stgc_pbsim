/*
 * ConstantsAndUnits.cpp
 *
 *  Created on: Nov 30, 2017
 *      Author: ekajomov
 */

#include "ConstantsAndUnits.h"
#include "TMath.h"

namespace PBSim_Units
{

const double ns = 1;
const double us = ns * 1e3;
const double ms = ns * 1e6;
const double s = ns * 1e9;

const double mm = 1;
const double cm = mm * 1e2;
const double m = mm * 1e3;

const double KHz = 1 / ms;
const double MHz = 1 / us;
const double GHz = 1 / ns;

const double bits_over_ns = 1;
const double Pb_over_s = bits_over_ns * 1.0e15 / (s);
const double Tb_over_s = bits_over_ns * 1.0e12 / (s);
const double Gb_over_s = bits_over_ns * 1.0e9 / (s);
const double Mb_over_s = bits_over_ns * 1.0e6 / (s);
const double Kb_over_s = bits_over_ns * 1.0e3 / (s);

const double ns_over_mm = 1;
const double ns_over_cm =  ns_over_mm / (cm / mm);
const double ns_over_m = ns_over_mm / (m / mm);

const double bit = 1;
const double byte = 8;
const double Kb = 1e3;
const double Mb =  1e6;
const double Gb = 1e9;
const double Tb = 1e12;
const double KB = byte * 1e3;
const double MB = byte * 1e6;
const double GB = byte * 1e9;
const double TB = byte * 1e12;

double ConvertTimeUnit(PBSim_Units::TIME_UNIT from, PBSim_Units::TIME_UNIT to)
{
	if (from == PBSim_Units::TIME_UNIT::UNDEFINED ||
		to == PBSim_Units::TIME_UNIT::UNDEFINED)
		return 0;

	int diff = (int) to - (int) from;

	return TMath::Power(10, diff);

}


}


