/*
 * IResetable.h
 *
 *  Created on: Apr 10, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */

#ifndef STGCPLAYBACK_COMPONENTS_IRESETABLE_H_
#define STGCPLAYBACK_COMPONENTS_IRESETABLE_H_

class IResetable
{
public:
	IResetable(){};
	virtual ~IResetable(){};
	virtual void Reset() = 0;
};



#endif /* STGCPLAYBACK_COMPONENTS_IRESETABLE_H_ */
