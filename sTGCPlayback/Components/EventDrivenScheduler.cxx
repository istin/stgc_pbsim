/*
 * Scheduler.cxx
 *
 *  Created on: Apr 13, 2019
 *      Author: ekajomov
 */

#include <Components/EventDrivenScheduler.h>
#include <algorithm>
#include <cstdlib>
#include <Utilities/LoggingSvc.h>
#include <iostream>
#define DBG LOG_TRACE( "E123: " << __LINE__ << " " << __FILE__ );


EventDrivenScheduler::EventDrivenScheduler()
:m_startingTime(0), m_startingTime_unit(PBSim_Units::TIME_UNIT::ns),
 m_timeResolution(0.1), m_timeResolution_unit(PBSim_Units::TIME_UNIT::ns),
 m_elapsedTime(0), m_randomizerSeed(12345678910){
}

EventDrivenScheduler::~EventDrivenScheduler() {

}

void EventDrivenScheduler::SetTimeResolution(double tr,
		PBSim_Units::TIME_UNIT time_unit) {
	m_timeResolution = tr;
	m_timeResolution_unit = time_unit;
}

double EventDrivenScheduler::GetTimeResolution(
		PBSim_Units::TIME_UNIT time_unit) {
	return m_timeResolution * PBSim_Units::ConvertTimeUnit(m_timeResolution_unit, time_unit);
}


void EventDrivenScheduler::Schedule(ICommand* command, long time_from_now,
		PBSim_Units::TIME_UNIT
		 time_unit) {

	 double time_in_resolution_units = time_from_now * ConvertTimeUnit(m_timeResolution_unit, time_unit);
	 long key_time = m_elapsedTime + time_in_resolution_units / m_timeResolution;
	 LOG_TRACE(" KEY inside "<<key_time);	
	 if (key_time == m_elapsedTime)
	 {
		 LOG_DEBUG(" Adding to the current execution list at time: "<<m_elapsedTime);
		 m_executing.push_back(command);
		 std::random_shuffle(m_executing.begin(), m_executing.end());
		 return;
	 }
	 std::map<long, std::vector<ICommand*> >::iterator itr = m_commandRegistry.find(key_time);
	 if (itr != m_commandRegistry.end())
	 {
	 	itr->second.push_back(command);
	 	return;
	 }
	 std::vector<ICommand*>&  v_command = m_commandRegistry[key_time];
	 v_command.push_back(command);
	 m_nextCommands = m_commandRegistry.begin();
	return;
}

void EventDrivenScheduler::SetStartingTime(long time,
		PBSim_Units::TIME_UNIT time_unit) {
	m_startingTime = time;
	m_startingTime_unit = time_unit;
}

long EventDrivenScheduler::GetCurrentTime(PBSim_Units::TIME_UNIT time_unit) {
	double offset = m_startingTime * ConvertTimeUnit(m_startingTime_unit, time_unit);
	double elapsed_time = m_elapsedTime * m_timeResolution * ConvertTimeUnit(m_timeResolution_unit, time_unit);
	return (long) offset + elapsed_time;
}

long EventDrivenScheduler::GetElapsedTime(PBSim_Units::TIME_UNIT time_unit) {
	double elapsed_time = m_elapsedTime * m_timeResolution * ConvertTimeUnit(m_timeResolution_unit, time_unit);
	return (long) elapsed_time;
}

void EventDrivenScheduler::SetRandomizerSeed(long randomizer_seed) {
	m_randomizerSeed = randomizer_seed;
}

long EventDrivenScheduler::GetRandomizerSeed() {
	return m_randomizerSeed;
}

void EventDrivenScheduler::Initialize() {

	if(m_commandRegistry.size()!=0){
		LOG_WARNING("Command Registry not empty");
	}
	m_elapsedTime=m_startingTime;
	m_nextCommands = m_commandRegistry.begin();
	std::srand(m_randomizerSeed);
}

bool EventDrivenScheduler::Next() {
	LOG_TRACE(" being called");
	LOG_TRACE(" CommandRegistry size_top="<<m_commandRegistry.size());
    //if (m_nextCommands == m_commandRegistry.end())
	if ( m_commandRegistry.size()==0)
	{
		LOG_DEBUG(" empty command list"<<" : Elapsed time="<<m_elapsedTime);
		//S.I
		return false;
	}
	m_elapsedTime = m_nextCommands->first;
	m_executing = m_nextCommands->second;
	LOG_TRACE("Found<< "<<m_executing.size()<<" commands :  Elapsed Time="<<m_elapsedTime);
	std::random_shuffle(m_executing.begin(), m_executing.end());
	std::vector<ICommand*>::iterator itr = m_executing.begin();
	while(itr != m_executing.end()){
		ICommand* c = (*itr);
		itr = m_executing.erase(itr);
		c->Execute();
		delete c;
	}
	std::map<long, std::vector<ICommand*> >::iterator previous = m_nextCommands;
	m_commandRegistry.erase(previous);
	LOG_TRACE(" CommandRegistry size_end="<<m_commandRegistry.size());
	if (m_commandRegistry.size() == 0)
	{
		m_nextCommands = m_commandRegistry.end();
		return false;
	}

	m_nextCommands = m_commandRegistry.begin();
	return true;
}
