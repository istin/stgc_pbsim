/*
 * ITransmitter.h
 *
 *  Created on: Apr 11, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_COMPONENTS_ITRANSMITTER_H_
#define STGCPLAYBACK_COMPONENTS_ITRANSMITTER_H_

class ITransmitter
{
public:
	ITransmitter(){}
	virtual ~ITransmitter(){}
	virtual void Transmit() = 0;
};



#endif /* STGCPLAYBACK_COMPONENTS_ITRANSMITTER_H_ */
