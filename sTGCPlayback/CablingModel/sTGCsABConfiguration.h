/*
 * sTGCABConfiguration.h
 *
 *  Created on: Feb 23, 2019
 *      Author: Enrique.Kajomovitz@cern.ch
 */

#ifndef CABLINGMODEL_STGCSABCONFIGURATION_H_
#define CABLINGMODEL_STGCSABCONFIGURATION_H_

#include "TString.h"
#include <vector>
#include "CablingModel/CablingModelEnums.h"
#include <iostream>

struct sTGCsABConfiguration
{

	TString type;
	int layer;
	DIR_WRT_DET direction;
	int nActiveChannels; // this is should be the same as the number of strips
	std::vector<int> firstChannelDisconnected;
	std::vector<int> nChannelsDisconnected; // channels connecting to VMM that are disconnected
	// Sanity - nActiveChannels + Sum(nChannelsDisconnected) = 512

	sTGCsABConfiguration():
	type(""), layer(0), direction(DIR_WRT_DET::UNDEFINED),
	nActiveChannels(0){}

	void Dump()
	{
		std::cout << "Type: " << type << " layer: " << layer;
		std::cout << " nActiveChannels " << nActiveChannels << std::endl;
		std::cout << " direction: ";
		if (direction == DIR_WRT_DET::UNDEFINED)
			std::cout << " UNDEFINED";
		else if (direction == DIR_WRT_DET::PARALLEL)
			std::cout << " PARALLEL";
		else
			std::cout << " ANTIPARALLEL";
		std::cout << std::endl;
		for (unsigned int i_dch = 0; i_dch < firstChannelDisconnected.size(); i_dch++)
		{
			std::cout << " firstChannelDisconnected: " << firstChannelDisconnected.at(i_dch);
			std::cout << " nActiveChannels: " << nChannelsDisconnected.at(i_dch) << std::endl;
		}
		std::cout << std::endl;
	}

	////////// Basic intelligence stuff /////
	void InitializeMappings()
	{
		channels_to_strips = std::vector<int>(512,0);
		strips_to_channels = std::vector<int>(512,-1);

		int disconnected = -1;
		for (unsigned int i_block = 0;
				i_block < firstChannelDisconnected.size(); i_block++) {

			int first_channel = firstChannelDisconnected.at(i_block);
			int nChD = nChannelsDisconnected.at(i_block);
			for (int i_dch = 0; i_dch < nChD; i_dch++)
			{
				channels_to_strips.at(i_dch + first_channel) = disconnected;
			}
		}

		// map channels to strips
		int strip = (direction == DIR_WRT_DET::PARALLEL) ? 1 : nActiveChannels;
		int strip_step = (direction == DIR_WRT_DET::PARALLEL) ? 1 : -1;
		for (unsigned int i_ch = 0; i_ch < channels_to_strips.size(); i_ch++) {
			if (channels_to_strips.at(i_ch) == disconnected)
			{
				continue;
			}
			channels_to_strips.at(i_ch) = strip;
			strips_to_channels.at(strip-1) = i_ch;
			strip += strip_step;
		}

	}

	int GetStripForChannel(int channel)
	{
		if (channels_to_strips.size() == 0)
			InitializeMappings();
		if (channel > 511)
			return -1;
		return channels_to_strips.at(channel);

	}

	int GetChannelForStrip(int strip)
	{
		if (strips_to_channels.size() == 0)
			InitializeMappings();
		if (strip > nActiveChannels)
			return -1;
		return strips_to_channels.at(strip-1);
	}

protected:

	std::vector<int> channels_to_strips;
	std::vector<int> strips_to_channels;



};



#endif /* CABLINGMODEL_STGCSABCONFIGURATION_H_ */
