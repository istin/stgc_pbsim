/*
 * sTGCsFEBandConfig.h
 *
 *  Created on: Feb 27, 2019
 *      Author: ekajomov
 */



#ifndef STGCPLAYBACK_CABLINGMODEL_FEBANDCONFIG_H_
#define STGCPLAYBACK_CABLINGMODEL_FEBANDCONFIG_H_


#include "TString.h"
#include <vector>

#include "CablingModel/CablingModelEnums.h"
#include "CablingModel/sTDSConfig.h"

#include <iostream>

#define N_TDS_FE 4

struct sTGCsFEBandConfig
{
	// FE Identifier, includes layer
	TString fe_type;
	DIR_WRT_DET direction;
	std::vector<sTDSConfig> tdsConfigs;

	sTGCsFEBandConfig()
	:fe_type(""),
	 direction(DIR_WRT_DET::UNDEFINED)
	{
		 for (int i = 0; i < N_TDS_FE; i++)
			 tdsConfigs.push_back(sTDSConfig());
	}

	void Dump()
	{
		std::cout << "FE type: " << fe_type;
		std::cout << " direction: ";
		if (direction == DIR_WRT_DET::UNDEFINED)
			std::cout << " UNDEFINED";
		else if (direction == DIR_WRT_DET::PARALLEL)
			std::cout << " PARALLEL";
		else
			std::cout << " ANTIPARALLEL";
		std::cout << std::endl;


		int i_first_tds = (direction == DIR_WRT_DET::ANTIPARALLEL) ? 0 : tdsConfigs.size()-1;
		int increment = (direction == DIR_WRT_DET::ANTIPARALLEL) ? 1 : -1;
		for (unsigned int ntds = 0; ntds < tdsConfigs.size(); ntds++)
		{
				int i_tds = i_first_tds + ntds * increment;
				tdsConfigs.at(i_tds).Dump();
		}
	}
};

#endif


