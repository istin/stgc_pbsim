/*
 * CompoundComponent.cxx
 *
 *  Created on: Mar 30, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */
#include <iostream>

#include "CablingModel/CompoundComponent.h"
#include <Utilities/LoggingSvc.h>


CompoundComponent::CompoundComponent() {
}

CompoundComponent::~CompoundComponent() {
}


CompoundComponent CompoundComponent::GetResolved(const std::map<TString, TString>& call_dict)
{

	bool unresolved = false;
	CompoundComponent result;
	TString resolvedCompositionName = compositionName;

	////// Resolve all current entries in the dictionary
	std::map<TString, TString>::const_iterator compDicItr = dictionary.begin();
	for (; compDicItr != dictionary.end(); compDicItr++)
	{
		TString key = compDicItr->first.Data();
		TString target = compDicItr->second.Data();
		std::map<TString, TString>::const_iterator callDicItr = call_dict.begin();
		for (; callDicItr != call_dict.end(); callDicItr++)
		{
			TString call_key = TString::Format("${%s}",callDicItr->first.Data());
			key.ReplaceAll(call_key, callDicItr->second);
			target.ReplaceAll(call_key, callDicItr->second);
		}

		unresolved |= key.Contains("${");
		unresolved |= target.Contains("${");
		if (unresolved)
		{
			LOG_FATAL("Unresolved CablingComposition" << key<<" : " <<target);
			throw std::runtime_error("Found unresolved items in template when trying to resolve");
		}
		result.dictionary[key] = target;
	}
	// Add non-existing items + override with call dictionary
	std::map<TString, TString>::const_iterator callDicItr = call_dict.begin();
	for (; callDicItr != call_dict.end(); callDicItr++)
	{
		result.dictionary[callDicItr->first] =  callDicItr->second;
	}


	// RESOLVE COMPOSITION NAME
	std::map<TString, TString>::const_iterator itr = result.dictionary.begin();
	for (; itr != result.dictionary.end(); itr++)
	{
		TString key = TString::Format("${%s}",itr->first.Data());
		resolvedCompositionName.ReplaceAll(key, itr->second);
	}

	result.compositionName = resolvedCompositionName;

	unresolved |= resolvedCompositionName.Contains("${");
	if (unresolved)
	{
		LOG_FATAL("Unresolved TemplateConnectionDescription");
		throw std::runtime_error("Found unresolved items in template when trying to resolve");
	}

	return result;
}


