/*
 * sTGCGeometryParser.cpp
 *
 *  Created on: Feb 3, 2019
 *      Author: Enrique.Kajomovitz@cern.ch
 *
 */
#include "TDOMParser.h"
#include "CablingModel/CablingConfigurationParser.h"
#include "TObjString.h"
#include <Utilities/LoggingSvc.h>

CablingConfigurationParser::CablingConfigurationParser(TXMLNode* in_docRoot):
m_docRoot(in_docRoot),
m_configFile(""),
m_ownDocRoot(false){

}

CablingConfigurationParser::~CablingConfigurationParser() {	
	if (m_ownDocRoot){
		delete m_docRoot;
	}
	
}

void CablingConfigurationParser::ProcessDefaultNode(TXMLNode* xmlNode) {
	// assumption: xmlNode is NOT a named node for which we have a specific handling
	// therefore, we don't care about its attributes, but we do have to go over all its children
	TXMLNode* child = xmlNode->GetChildren();
	while (child != NULL) {
		m_parser.Clear();
		TString nodeName = child->GetNodeName();
		if (nodeName.EqualTo("sAB")) {
			ProcessSABNode(child);
		} else if (nodeName.EqualTo("sFE")) {
			ProcessSFENode(child);
		} else if (nodeName.EqualTo("PadTrigger")) {
			ProcessPadTriggerNode(child);
		} else if (nodeName.EqualTo("Router")) {
			ProcessRouterNode(child);
		} else if (nodeName.EqualTo("MMFE8")) {
			ProcessMMFE8Node(child);
		} else if (nodeName.EqualTo("ARTASIC")) {
			ProcessARTASICNode(child);
		} else if (nodeName.EqualTo("TriggerProcessor")) {
			ProcessTriggerProcessor(child);
		} else if (nodeName.EqualTo("CablingComposition")) {
			ProcessComposition(child);
		}
		else {
			ProcessDefaultNode(child);
		}
		child = child->GetNextNode();
	}
}


void CablingConfigurationParser::ProcessSABNode(TXMLNode* xmlNode) {
	if (!m_parser.ParseNode(xmlNode, "sAB", "type:layer:direction:nActiveChannels:firstChannelDisconnected:nChannelsDisconnected")) {
		xmlNode->Dump();
		LOG_FATAL("here");
		throw std::runtime_error("Not all attributes present in sAB xmlNode");
	}

	sTGCsABConfiguration& sABConfig = UpsertSAB(m_parser.ValueOf("type"));

	sABConfig.type = m_parser.ValueOf("type");
	sABConfig.layer = m_parser.IntAttributeValue("layer");
	sABConfig.direction = DirValueOf("direction");
	sABConfig.nActiveChannels =  m_parser.IntAttributeValue("nActiveChannels");
	sABConfig.firstChannelDisconnected  = ValueAsIntList("firstChannelDisconnected");
	sABConfig.nChannelsDisconnected = ValueAsIntList("nChannelsDisconnected");
	sABConfig.InitializeMappings();
	m_parser.Clear();

}

void CablingConfigurationParser::ProcessSFENode(TXMLNode* xmlNode) {

	TString atts = "type:layer:direction:nActiveVMMChannels:nActiveVMM:";
		atts += "nACtiveTDSChannels:nActiveTDS:firstTDSChannelDisconnected:nTDSChannelDisconnected";

	if (!m_parser.ParseNode(xmlNode, "sFE", atts)) {
		xmlNode->Dump();
		LOG_FATAL("here");
		throw std::runtime_error("Not all attributes present in sFE xmlNode");
	}

	sTGCsFEConfiguration& sFEConfig = UpsertSFE(m_parser.ValueOf("type"));

	sFEConfig.type = m_parser.ValueOf("type");
	sFEConfig.layer = m_parser.IntAttributeValue("layer");
	sFEConfig.direction = DirValueOf("direction");
	sFEConfig.nActiveVMMChannels =  m_parser.IntAttributeValue("nActiveVMMChannels");
	sFEConfig.nActiveVMM =  m_parser.IntAttributeValue("nActiveVMM");
	sFEConfig.nACtiveTDSChannels  = m_parser.IntAttributeValue("nACtiveTDSChannels");
	sFEConfig.nActiveTDS  = m_parser.IntAttributeValue("nActiveTDS");
	sFEConfig.firstTDSChannelDisconnected = ValueAsIntList("firstTDSChannelDisconnected");
	sFEConfig.nTDSChannelDisconnected = ValueAsIntList("nTDSChannelDisconnected");
	sFEConfig.InitializeMappings();
	m_parser.Clear();
}


std::vector<int> CablingConfigurationParser::ValueAsIntList(const TString& attribute) {
	std::vector<TString>* tokens = XMLNodeParser::Tokenize(m_parser.ValueOf(attribute), ";");
	std::vector<int> result;
	for (std::vector<TString>::iterator it = tokens->begin(); it != tokens->end(); ++it) {
		TString& value = *it;
		int trimIdx = 0;
		while (trimIdx < value.Length()) {
			char c = value(trimIdx);
			if (c == ' ' || c == '\t' || c == '\r' || c == '\n')
				trimIdx++;
			else break;
		}
		result.push_back(TString(value(trimIdx, value.Length() - trimIdx)).Atoi());
	}
	delete tokens;
	return result;
}

DIR_WRT_DET CablingConfigurationParser::DirValueOf(const TString& attribute){

	TString s_direction = m_parser.ValueOf(attribute);
	if (s_direction.EqualTo("PARALLEL", TString::kIgnoreCase))
		return DIR_WRT_DET::PARALLEL;
	else if (s_direction.EqualTo("ANTIPARALLEL", TString::kIgnoreCase))
		return DIR_WRT_DET::ANTIPARALLEL;
	else
		return DIR_WRT_DET::UNDEFINED;
}

std::vector<double> CablingConfigurationParser::ValueAsDoubleList(const TString& attribute) {
	std::vector<TString>* tokens = XMLNodeParser::Tokenize(m_parser.ValueOf(attribute), ";");

	std::vector<double> result;
	for (std::vector<TString>::iterator it = tokens->begin(); it != tokens->end(); ++it) {
		TString& value = *it;
		int trimIdx = 0;
		while (trimIdx < value.Length()) {
			char c = value(trimIdx);
			if (c == ' ' || c == '\t' || c == '\r' || c == '\n')
				trimIdx++;
			else break;
		}
		result.push_back(TString(value(trimIdx, value.Length() - trimIdx)).Atof());
	}
	delete tokens;
	return result;
}

std::vector<TString> CablingConfigurationParser::ValueAsStringList(const TString& attribute) {
	std::vector<TString>* tokens = XMLNodeParser::Tokenize(m_parser.ValueOf(attribute), ";");

	std::vector<TString> result;
	for (std::vector<TString>::iterator it = tokens->begin(); it != tokens->end(); ++it) {
		TString& value = *it;
		int trimIdx = 0;
		while (trimIdx < value.Length()) {
			char c = value(trimIdx);
			if (c == ' ' || c == '\t' || c == '\r' || c == '\n')
				trimIdx++;
			else break;
		}
		result.push_back(value);
	}
	delete tokens;
	return result;
}

void  CablingConfigurationParser::Parse(){
	ProcessDefaultNode(m_docRoot);
}

void CablingConfigurationParser::SetConfigurationFile(TString in_configFile){
	m_configFile = in_configFile;
	SetXMLRoot();
}

void CablingConfigurationParser::SetXMLRoot(TXMLNode* in_docRoot){
	if (in_docRoot != NULL)
		m_docRoot = in_docRoot;
	m_domParser=new TDOMParser();
	if (m_configFile == ""){
		LOG_FATAL("here");
		throw std::runtime_error("Trying to setup an empty XML file for configuration");
	}
	m_domParser->SetValidate(false); // do not validate with DTD
	int parsecode = m_domParser->ParseFile(m_configFile);

	if (parsecode < 0) {
		LOG_FATAL("here");
		throw std::runtime_error(m_domParser->GetParseCodeMessage(parsecode));
	}

	m_docRoot = m_domParser->GetXMLDocument()->GetRootNode();
	m_ownDocRoot = true;
}

sTGCsABConfiguration& CablingConfigurationParser::UpsertSAB(const TString& name) {
    std::map<TString,sTGCsABConfiguration>::iterator it = sABLookup.find(name);
    if (it == sABLookup.end()) {
    	sTGCsABConfiguration& result = sABLookup[name];
        result.type = name;
        return result;
    }
    return it->second;
}

sTGCsFEConfiguration& CablingConfigurationParser::UpsertSFE(const TString& name) {
    std::map<TString,sTGCsFEConfiguration>::iterator it = sFELookup.find(name);
    if (it == sFELookup.end()) {
    	sTGCsFEConfiguration& result = sFELookup[name];
        result.type = name;
        return result;
    }
    return it->second;
}

void CablingConfigurationParser::ProcessPadTriggerNode(TXMLNode* xmlNode) {
	TString atts = "type:coincidenceTable:latency";

		if (!m_parser.ParseNode(xmlNode, "PadTrigger", atts)) {
			xmlNode->Dump();
			LOG_FATAL("here");
			throw std::runtime_error("Not all attributes present in sFE xmlNode");
		}

		sTGCPadTriggerConfiguration& pTConfig = UpsertPT(m_parser.ValueOf("type"));
		pTConfig.type = m_parser.ValueOf("type");
		pTConfig.coincidenceTable = m_parser.ValueOf("coincidenceTable");
		pTConfig.latency = m_parser.DoubleAttributeValue("latency");
		m_parser.Clear();
}

void CablingConfigurationParser::ProcessRouterNode(TXMLNode* xmlNode) {

	TString atts = "type:serializerBank:serializerBankChannel:delay:latency:clock";

	if (!m_parser.ParseNode(xmlNode, "Router", atts)) {
		xmlNode->Dump();
		LOG_FATAL("here");
		throw std::runtime_error("Not all attributes present in Router xmlNode");
	}

	sTGCRouterConfiguration& routerConfig = UpsertRouter(m_parser.ValueOf("type"));

	routerConfig.type = m_parser.ValueOf("type");
	routerConfig.serializerBank = ValueAsIntList("serializerBank");
	routerConfig.serializerBankChannel = ValueAsIntList("serializerBankChannel");
	routerConfig.delay = ValueAsDoubleList("delay");
	routerConfig.latency = m_parser.DoubleAttributeValue("latency");
	routerConfig.clock = m_parser.DoubleAttributeValue("clock");
	m_parser.Clear();


}

void CablingConfigurationParser::ProcessMMFE8Node(TXMLNode* xmlNode) {

    TString atts = "type";
    if (!m_parser.ParseNode(xmlNode, "MMFE8", atts)) {
        xmlNode->Dump();
		LOG_FATAL("here");
        throw std::runtime_error("Not all attributes present in MMFE8 xmlNode");
    }

    MMFE8Configuration& MMFE8Config = UpsertMMFE8(m_parser.ValueOf("type"));
    MMFE8Config.type = m_parser.ValueOf("type");
    m_parser.Clear();

}

void CablingConfigurationParser::ProcessARTASICNode(TXMLNode* xmlNode) {

    TString atts = "type";
    if (!m_parser.ParseNode(xmlNode, "ARTASIC", atts)) {
        xmlNode->Dump();
		LOG_FATAL("here");
        throw std::runtime_error("Not all attributes present in ARTASIC xmlNode");
    }

    ARTASICConfiguration& ARTASICConfig = UpsertARTASIC(m_parser.ValueOf("type"));
    ARTASICConfig.type = m_parser.ValueOf("type");
    m_parser.Clear();

}

sTGCPadTriggerConfiguration& CablingConfigurationParser::UpsertPT(
		const TString& name) {
	std::map<TString,sTGCPadTriggerConfiguration>::iterator it = pTLookup.find(name);
	if (it == pTLookup.end()) {
		sTGCPadTriggerConfiguration& result = pTLookup[name];
		result.type = name;
		return result;
	}
	return it->second;
}

sTGCRouterConfiguration& CablingConfigurationParser::UpsertRouter(
		const TString& name) {
	std::map<TString,sTGCRouterConfiguration>::iterator it = routerLookup.find(name);
	if (it == routerLookup.end()) {
		sTGCRouterConfiguration& result = routerLookup[name];
		result.type = name;
		return result;
	}
	return it->second;
}

MMFE8Configuration& CablingConfigurationParser::UpsertMMFE8(
		const TString& name) {
	std::map<TString,MMFE8Configuration>::iterator it = MMFE8Lookup.find(name);
	if (it == MMFE8Lookup.end()) {
		MMFE8Configuration& result = MMFE8Lookup[name];
		result.type = name;
		return result;
	}
	return it->second;
}


ARTASICConfiguration& CablingConfigurationParser::UpsertARTASIC(
		const TString& name) {
	std::map<TString,ARTASICConfiguration>::iterator it = ARTASICLookup.find(name);
	if (it == ARTASICLookup.end()) {
		ARTASICConfiguration& result = ARTASICLookup[name];
		result.type = name;
		return result;
	}
	return it->second;
}

void CablingConfigurationParser::ProcessTriggerProcessor(
		TXMLNode* xmlNode) {

	TString atts = "type";

	if (!m_parser.ParseNode(xmlNode, "TriggerProcessor", atts)) {
		xmlNode->Dump();
		LOG_FATAL("here");
		throw std::runtime_error("Not all attributes present in TriggerProcessor xmlNode");
	}

	TriggerProcessorConfiguration& tp = UpsertTP(m_parser.ValueOf("type"));

	tp.type = m_parser.ValueOf("type");

	m_parser.Clear();
}

TriggerProcessorConfiguration& CablingConfigurationParser::UpsertTP(
		const TString& type) {

	std::map<TString,TriggerProcessorConfiguration>::iterator it = tPLookup.find(type);
	if (it == tPLookup.end()) {
		TriggerProcessorConfiguration& result = tPLookup[type];
		result.type = type;
		return result;
	}
	return it->second;
}

void CablingConfigurationParser::ProcessComposition(TXMLNode* xmlNode) {

	TString atts = "compositionName";
	if (!m_parser.ParseNode(xmlNode, "CablingComposition", atts)) {
		xmlNode->Dump();
		LOG_FATAL("here");
		throw std::runtime_error("Not all attributes present in CablingComposition xmlNode");
	}

	CablingComposition& tcc = UpsertComposition(m_parser.ValueOf("compositionName"));
	tcc.compositionName = m_parser.ValueOf("compositionName");

	m_parser.Clear();

	TXMLNode* child = xmlNode->GetChildren();
	while (child != NULL) {
		m_parser.Clear();
		TString nodeName = child->GetNodeName();
		if (nodeName.EqualTo("SimpleComponent")){
			tcc.simpleComponents.push_back(ProcessSimpleComponent(child));
		}
		else if (nodeName.EqualTo("CompoundComponent")){
			tcc.compoundComponents.push_back(ProcessCompoundComponent(child));
		}
		else if (nodeName.EqualTo("ComponentConnection")){
			tcc.connections.push_back(ProcessComponentConnection(child));
		}
		else if (nodeName.EqualTo("Dictionary"))
		{
			if (tcc.dictionary.size() != 0){
				LOG_FATAL("Trying to fill a dictionary twice");
				throw std::runtime_error("Trying to fill a dictionary twice");
			}
			tcc.dictionary = ProcessDictionary(child);
		}
		else{
			ProcessDefaultNode(child);
		}
		child = child->GetNextNode();
	}
}

ComponentConnection CablingConfigurationParser::ProcessComponentConnection(TXMLNode* xmlNode)
{
	TString atts = "destinationClass:destinationType:destinationName:destinationPort:";
	atts += "sourceClass:sourceType:sourceName:sourcePort:connectionDetails:";
	atts += "cableLength:propagationDelay:bandwidth";

	if (!m_parser.ParseNode(xmlNode, "ComponentConnection", atts)) {
		xmlNode->Dump();
		LOG_FATAL("here");
		throw std::runtime_error("Not all attributes present in ComponentConnection xmlNode");
	}

	ComponentConnection tccd;
	tccd.destinationClass = m_parser.ValueOf("destinationClass");
	tccd.destinationType = m_parser.ValueOf("destinationType");
	tccd.destinationName = m_parser.ValueOf("destinationName");
	tccd.destinationPort = m_parser.ValueOf("destinationPort");
	tccd.sourceClass = m_parser.ValueOf("sourceClass");
	tccd.sourceType = m_parser.ValueOf("sourceType");
	tccd.sourceName = m_parser.ValueOf("sourceName");
	tccd.sourcePort = m_parser.ValueOf("sourcePort");
	tccd.connectionDetails = m_parser.ValueOf("connectionDetails");
	tccd.propagationDelay =  m_parser.ValueOf("propagationDelay");
	tccd.cableLength = m_parser.ValueOf("cableLength");
	tccd.bandwidth=m_parser.ValueOf("bandwidth");
	m_parser.Clear();
	return tccd;
}

CompoundComponent CablingConfigurationParser::ProcessCompoundComponent(TXMLNode* xmlNode)
{
	TString atts = "compositionName:componentDictionary";

	if (!m_parser.ParseNode(xmlNode, "CompoundComponent", atts)) {
		xmlNode->Dump();
		LOG_FATAL("here");
		throw std::runtime_error("Not all attributes present in CompoundComponent xmlNode");
	}

	CompoundComponent cct;
	cct.compositionName = m_parser.ValueOf("compositionName");
	TString s_componentDictionary = m_parser.ValueOf("componentDictionary");
	std::map<TString, TString> dict =  StringToDictionary(s_componentDictionary);

	cct.dictionary = StringToDictionary(s_componentDictionary);
	m_parser.Clear();
	return cct;

}

CablingComposition& CablingConfigurationParser::UpsertComposition(
		const TString& name) {
	std::map<TString,CablingComposition>::iterator it = ccLookup.find(name);
	if (it == ccLookup.end()) {
		CablingComposition& result = ccLookup[name];
		result.compositionName = name;
		return result;
	}
	return it->second;
}

SimpleComponent CablingConfigurationParser::ProcessSimpleComponent(TXMLNode* xmlNode)
{
	TString atts = "componentClass:componentType:componentName";

	if (!m_parser.ParseNode(xmlNode, "SimpleComponent", atts)) {
		xmlNode->Dump();
		LOG_FATAL("here");
		throw std::runtime_error("Not all attributes present in SimpleComponent xmlNode");
	}

	SimpleComponent sc;
	sc.componentClass = m_parser.ValueOf("componentClass");
	sc.componentType = m_parser.ValueOf("componentType");
	sc.componentName = m_parser.ValueOf("componentName");
	m_parser.Clear();
	return sc;

}

std::map<TString, TString> CablingConfigurationParser::ProcessDictionary(TXMLNode* xmlNode)
{
	TString atts = "content";

	if (!m_parser.ParseNode(xmlNode, "Dictionary", atts)) {
		xmlNode->Dump();
		LOG_FATAL("here");
		throw std::runtime_error("Not all attributes present in Dictionary xmlNode");
	}

	TString s_dict = m_parser.ValueOf("content");
	m_parser.Clear();

	return StringToDictionary(s_dict);
}


std::map<TString, TString> CablingConfigurationParser::StringToDictionary(TString dictionary)
{
	TString pair;
	TString tok;
	TString delims_pair = ";";
	TString delims_dic = ":";
	Ssiz_t from = 0;
	int num = 0;
	TString key;
	TString target;
	std::map<TString, TString> result;
	while (dictionary.Tokenize(pair, from, delims_pair)) {
		Ssiz_t from_pair = 0;
		num = 0;
		while (pair.Tokenize(tok, from_pair, delims_dic)) {
			if (num == 0)
				key = tok;
			else
				target = tok;
			num++;
		}
		key    =    key.Remove(TString::kBoth, 'n').
				        Remove(TString::kBoth, '\r').
				        Remove(TString::kBoth, '\t').
				        Remove(TString::kBoth, ' ');

		target = target.Remove(TString::kBoth, '\n').
				        Remove(TString::kBoth, '\r').
				        Remove(TString::kBoth, '\t').
				        Remove(TString::kBoth, ' ');

		result[key] = target;

	}
	return result;
}
