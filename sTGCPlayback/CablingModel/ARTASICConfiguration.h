/*
 * ARTASICConfiguration.h
 *
 *  Created on: Mar 6, 2019
 *      Author: Enrique.Kajomovitz@cern.ch
 */

#ifndef STGCPLAYBACK_CABLINGMODEL_ARTASICCONFIGURATION_H_
#define STGCPLAYBACK_CABLINGMODEL_ARTASICCONFIGURATION_H_

#include "TString.h"
#include <iostream>

struct ARTASICConfiguration
{
	TString type;
	double latency;

	void Dump()
	{
		std::cout << "=================>>> ARTASIC <<<=================" << std::endl;
		std::cout << "Type: " << type << " latency:  "<< latency <<  std::endl;
	}

    ARTASICConfiguration():
	type(""), latency(50)
    {}

};


#endif /* STGCPLAYBACK_CABLINGMODEL_ARTASICCONFIGURATION_H_ */
