/*
 * TemplatedCablingComposition.cxx
 *
 *  Created on: Mar 18, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */

#include <iostream>
#include <Utilities/LoggingSvc.h>
#include "CablingModel/CablingComposition.h"


CablingComposition::CablingComposition() {

}

CablingComposition::~CablingComposition() {
}

CablingComposition CablingComposition::GetResolved(
		const std::map<TString, TString>& call_dict) {

	// First resolve dictionary

	bool unresolved = false;
	CablingComposition result;
	TString resolvedCompositionName = compositionName;

	////// Resolve all current entries in the dictionary
	std::map<TString, TString>::const_iterator compDicItr = dictionary.begin();
	for (; compDicItr != dictionary.end(); compDicItr++)
	{
		TString key = compDicItr->first.Data();
		TString target = compDicItr->second.Data();
		std::map<TString, TString>::const_iterator callDicItr = call_dict.begin();
		for (; callDicItr != call_dict.end(); callDicItr++)
		{
			TString call_key = TString::Format("${%s}",callDicItr->first.Data());
			key.ReplaceAll(call_key, callDicItr->second);
			target.ReplaceAll(call_key, callDicItr->second);
		}

		unresolved |= key.Contains("${");
		unresolved |= target.Contains("${");
		if (unresolved)
		{
			LOG_WARNING("Unresolved CablingComposition");
			throw std::runtime_error("Found unresolved items in template when trying to resolve");
		}
		result.dictionary[key] = target;
	}
	// Add non-existing items + override with call dictionary
	std::map<TString, TString>::const_iterator callDicItr = call_dict.begin();
	for (; callDicItr != call_dict.end(); callDicItr++)
	{
		result.dictionary[callDicItr->first] =  callDicItr->second;
	}


	// RESOLVE COMPOSITION NAME
	std::map<TString, TString>::const_iterator itr = result.dictionary.begin();
	for (; itr != result.dictionary.end(); itr++)
	{
		TString key = TString::Format("${%s}",itr->first.Data());
		resolvedCompositionName.ReplaceAll(key, itr->second);
	}

	result.compositionName = resolvedCompositionName;

	unresolved |= resolvedCompositionName.Contains("${");
	if (unresolved)
	{
		LOG_WARNING("Unresolved TemplateConnectionDescription");
		throw std::runtime_error("Found unresolved items in template when trying to resolve");
	}

	for (unsigned int i_component = 0; i_component  < simpleComponents.size(); i_component++)
	{
		result.simpleComponents.push_back(simpleComponents[i_component].GetResolved(result.dictionary));
	}

	for (unsigned int i_compoundComponent = 0; i_compoundComponent  < compoundComponents.size(); i_compoundComponent++)
	{
		result.compoundComponents.push_back(compoundComponents[i_compoundComponent].GetResolved(result.dictionary));
	}

	for (unsigned int i_connection = 0; i_connection  < connections.size(); i_connection++)
	{
		result.connections.push_back(connections[i_connection].GetResolved(result.dictionary));
	}

	return result;
}
