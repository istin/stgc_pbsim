/*
 * CablingComposition.h
 *
 *  Created on: Mar 18, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */

#ifndef STGCPLAYBACK_CABLINGMODEL_CABLINGCOMPOSITION_H_
#define STGCPLAYBACK_CABLINGMODEL_CABLINGCOMPOSITION_H_

#include "CablingModel/ComponentConnection.h"
#include "CablingModel/SimpleComponent.h"
#include "CablingModel/CompoundComponent.h"
#include "CablingModel/CablingComposition.h"
#include <map>

class CablingComposition {
public:


	TString compositionName;

	CablingComposition();
	virtual ~CablingComposition();

	CablingComposition GetResolved(const std::map<TString,TString>& dict);

	std::vector<SimpleComponent> simpleComponents;
	std::vector<CompoundComponent> compoundComponents;
	std::vector<ComponentConnection> connections;
	std::map<TString, TString> dictionary; // this is the global dictionary of the composition

	bool isResolved;


};

#endif /* STGCPLAYBACK_CABLINGMODEL_CABLINGCOMPOSITION_H_ */
