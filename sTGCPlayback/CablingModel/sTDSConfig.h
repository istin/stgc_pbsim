/*
 * TDSConfigEntry.h
 *
 *  Created on: Feb 24, 2019
 *      Author: Enrique.Kajomovitz@cern.ch
 */

#ifndef STGCPLAYBACK_CABLINGMODEL_STDSCONFIG_H_
#define STGCPLAYBACK_CABLINGMODEL_STDSCONFIG_H_

/*
 * This struct describes which channels need to be read on a TDS for a band
 */

#include "TString.h"
#include "CablingModel/CablingModelEnums.h"
#include <iostream>

struct sTDSConfig
{
	// FE Identifier, includes layer
	TString fe_type;
	int tds_id;
	DIR_WRT_DET direction;

	int nBands;
	int firstBand;
	// band particulars

	std::vector<BAND_SEGMENT> band_segment;
	std::vector<int> firstChannel;
	std::vector<int> nChannels;


	sTDSConfig()
	: fe_type(""),
	  tds_id(0),
	  direction(DIR_WRT_DET::UNDEFINED),
	  nBands(0),
	  firstBand(0),
	  band_segment(128,BAND_SEGMENT::UNDEFINED),
	  firstChannel(128,0),
	  nChannels(128,0)
	{

	}

	void Dump()
	{
		TString s_direc;
		if (direction == DIR_WRT_DET::UNDEFINED )
			s_direc = "UNDEFINED";
		else if (direction == DIR_WRT_DET::PARALLEL )
			s_direc = "PARALLEL";
		else
			s_direc = "ANTIPARALLEL";

		std::cout << "FE: " << fe_type << " direction: " << s_direc << " tds_id: " <<  tds_id << std::endl;
		int i_first_band = (direction == DIR_WRT_DET::PARALLEL ) ?  nBands -1 : nBands -1;
		int step = (direction == DIR_WRT_DET::PARALLEL ) ? -1 : -1;
		for (int nBand = 0; nBand < nBands; nBand++)
		{
			int i_band = i_first_band + nBand * step;
			std::cout << "Band: " << i_band + firstBand;
			std::cout << " First channel: " << firstChannel[i_band + firstBand - 1];
			std::cout << " N channels: " << nChannels[i_band + firstBand -1];
			std::cout << " Band segment: ";
			if (band_segment[i_band + firstBand -1] == BAND_SEGMENT::UNDEFINED)
				std::cout << " UNDEFINED";
			else if (band_segment[i_band + firstBand -1] == BAND_SEGMENT::COMPLETE)
				std::cout << " COMPLETE";
			else if (band_segment[i_band + firstBand -1] == BAND_SEGMENT::INNER)
				std::cout << " INNER";
			else
				std::cout << " OUTER";
			std::cout << std::endl;
		}
	}
};


#endif /* STGCPLAYBACK_CABLINGMODEL_STDSCONFIG_H_ */
