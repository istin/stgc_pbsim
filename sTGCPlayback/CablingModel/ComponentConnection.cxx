/*
 * ComponentConnection.cxx
 *
 *  Created on: Mar 18, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */

#include <Utilities/LoggingSvc.h>

#include "CablingModel/ComponentConnection.h"

ComponentConnection::ComponentConnection() {
}

ComponentConnection::~ComponentConnection() {
}

ComponentConnection ComponentConnection::GetResolved(
		const std::map<TString, TString>& dictionary) {

	TString resolvedDestinationClass = destinationClass;
	TString resolvedDestinationType = destinationType;
	TString resolvedDestinationName = destinationName;
	TString resolvedDestinationPort = destinationPort;

	TString resolvedSourceClass = sourceClass;
	TString resolvedSourceType = sourceType;
	TString resolvedSourceName = sourceName;
	TString resolvedSourcePort = sourcePort;
	TString resolvedCableLength = cableLength;
	TString resolvedPropagationDelay = propagationDelay;
	TString resolvedBandwidth = bandwidth;

	TString resolvedConnectionDetails = connectionDetails;

	std::map<TString, TString>::const_iterator itr = dictionary.begin();
	for (; itr != dictionary.end(); itr++)
	{
		TString key = TString::Format("${%s}",itr->first.Data());
		resolvedDestinationClass.ReplaceAll(key, itr->second);
		resolvedDestinationType.ReplaceAll(key, itr->second);
		resolvedDestinationName.ReplaceAll(key, itr->second);
		resolvedDestinationPort.ReplaceAll(key, itr->second);
		resolvedSourceClass.ReplaceAll(key, itr->second);
		resolvedSourceType.ReplaceAll(key, itr->second);
		resolvedSourceName.ReplaceAll(key, itr->second);
		resolvedSourcePort.ReplaceAll(key, itr->second);
		resolvedConnectionDetails.ReplaceAll(key, itr->second);
		resolvedCableLength.ReplaceAll(key, itr->second);
		resolvedPropagationDelay.ReplaceAll(key, itr->second);
		resolvedBandwidth.ReplaceAll(key, itr->second);

	}

	/// now check that all is resolved
	bool unresolved = false;
	unresolved |= resolvedDestinationClass.Contains("${");
	unresolved |= resolvedDestinationType.Contains("${");
	unresolved |= resolvedDestinationName.Contains("${");
	unresolved |= resolvedDestinationPort.Contains("${");
	unresolved |= resolvedSourceClass.Contains("${");
	unresolved |= resolvedSourceType.Contains("${");
	unresolved |= resolvedSourceName.Contains("${");
	unresolved |= resolvedSourcePort.Contains("${");
	unresolved |= resolvedConnectionDetails.Contains("${");
	unresolved |= resolvedCableLength.Contains("${");
	unresolved |= resolvedPropagationDelay.Contains("${");
	unresolved |= resolvedBandwidth.Contains("${");

	if (unresolved)
	{
		LOG_FATAL("Unresolved TemplateConnectionDescription");
		throw std::runtime_error("Found unresolved items in template when trying to resolve");
	}


	ComponentConnection result;
	result.destinationClass = resolvedDestinationClass;
	result.destinationType = resolvedDestinationType;
	result.destinationName = resolvedDestinationName;
	result.destinationPort = resolvedDestinationPort;

	result.sourceClass = resolvedSourceClass;
	result.sourceType = resolvedSourceType;
	result.sourceName = resolvedSourceName;
	result.sourcePort = resolvedSourcePort;

	result.connectionDetails = resolvedConnectionDetails;

	result.cableLength = resolvedCableLength;
	result.propagationDelay = resolvedPropagationDelay;
	result.bandwidth = resolvedBandwidth;

	return result;
}
