/*
 * sTGCPadTriggerConfiguration.h
 *
 *  Created on: Mar 6, 2019
 *      Author: Enrique.Kajomovitz@cern.ch
 */

#ifndef STGCPLAYBACK_CABLINGMODEL_STGCPADTRIGGERCONFIGURATION_H_
#define STGCPLAYBACK_CABLINGMODEL_STGCPADTRIGGERCONFIGURATION_H_

#include "TString.h"
#include <iostream>

struct sTGCPadTriggerConfiguration
{
	TString type;
	TString coincidenceTable;
	double latency;

	void Dump()
	{
		std::cout << "=================>>> PadTrigger <<<=================" << std::endl;
		std::cout << "Type: " << type << " latency:  "<< latency <<  std::endl;
		std::cout << "Coincidence Table: " << coincidenceTable << std::endl;
	}

	sTGCPadTriggerConfiguration()
	:type(""), coincidenceTable("external"), latency(100)
	{

	}

};




#endif /* STGCPLAYBACK_CABLINGMODEL_STGCPADTRIGGERCONFIGURATION_H_ */
