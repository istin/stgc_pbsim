
#include "CablingModel/CablingModelEnums.h"
#include "CablingModel/sTGCsABConfiguration.h"
#include "CablingModel/sTGCsFEConfiguration.h"
#include "CablingModel/CablingConfigurationParser.h"
#include "CablingModel/sTGCPadTriggerConfiguration.h"
#include "CablingModel/TriggerProcessorConfiguration.h"

#include "CablingModel/CablingComposition.h"
#include "CablingModel/ComponentConnection.h"
#include "CablingModel/CompoundComponent.h"
#include "CablingModel/SimpleComponent.h"

#include "CablingModel/sTGCsFEBandConfig.h"
#include "CablingModel/sTDSConfig.h"

#ifdef __ROOTCLING__

#pragma link C++ enum class DIR_WRT_DET+;
#pragma link C++ enum class BAND_SEGMENT+;

#pragma link C++ class sTGCsFEConfiguration+;
#pragma link C++ class sTGCsABConfiguration+;
#pragma link C++ class sTGCRouterConfiguration+;
#pragma link C++ class sTGCPadTriggerConfiguration+;
#pragma link C++ class TriggerProcessorConfiguration+;
#pragma link C++ class MMFE8Configuration+;
#pragma link C++ class ARTASICConfiguration+;

#pragma link C++ class sTDSConfig+;
#pragma link C++ class sTGCsFEBandConfig+;
#pragma link C++ class CablingConfigurationParser+;
#pragma link C++ class sTGCPadTriggerConfiguration+;

#pragma link C++ class CablingComposition+;
#pragma link C++ class SimpleComponent+;
#pragma link C++ class CompoundComponent+;
#pragma link C++ class ComponentConnection+;

#endif
