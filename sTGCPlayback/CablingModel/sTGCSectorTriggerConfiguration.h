/*
 * sTGCSectorTriggerConfiguration.h
 *
 *  Created on: Mar 6, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_CABLINGMODEL_STGCSECTORTRIGGERCONFIGURATION_H_
#define STGCPLAYBACK_CABLINGMODEL_STGCSECTORTRIGGERCONFIGURATION_H_

#include "TString.h"
#include <iostream>
#include <vector>
struct sTGCSectorTriggerConfiguration
{
	TString type;
	TString padTrigger;
	TString triggerProcessor;
	std::vector<int> stgcTriggerProcessorRouterInput;
	std::vector<TString> routerBoards;
	std::vector<int> routerTdsInput;
	std::vector<double> routerTdsCableLength;
	std::vector<TString> sFEBoards;
	std::vector<TString> sABoards;
	std::vector<TString> sQuadLayer;


	void Dump()
	{
		std::cout << "==============>>> Sector <<<============== " << std::endl;
		std::cout << "Type: " << type << std::endl;
		std::cout << "padTrigger: " << padTrigger << std::endl;
		for (unsigned int i_rt = 0; i_rt < routerBoards.size(); i_rt++)
		{
			std::cout << "Router board: " << routerBoards[i_rt] << std::endl;
			std::cout << "No details on Router boards and connections are dumped for now" << std::endl;
		}
		for (unsigned int i_fe = 0; i_fe < sFEBoards.size(); i_fe++)
		{
			std::cout << "FE board: " << sFEBoards[i_fe] << std::endl;
		}
	}
};



#endif /* STGCPLAYBACK_CABLINGMODEL_STGCSECTORTRIGGERCONFIGURATION_H_ */
