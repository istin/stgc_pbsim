/*
 * TriggerProcessor.h
 *
 *  Created on: Mar 10, 2019
 *      Author: Enrique.Kajomovitz@cern.ch
 */

#ifndef STGCPLAYBACK_CABLINGMODEL_TRIGGERPROCESSORCONFIGURATION_H_
#define STGCPLAYBACK_CABLINGMODEL_TRIGGERPROCESSORCONFIGURATION_H_

#include <TString.h>

struct TriggerProcessorConfiguration
{
	TString type;
};


#endif /* STGCPLAYBACK_CABLINGMODEL_TRIGGERPROCESSORCONFIGURATION_H_ */
