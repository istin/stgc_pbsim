/*
 * sTGCRouterConfig.h
 *
 *  Created on: Mar 6, 2019
 *      Author: Enrique.Kajomovitz@cern.ch
 */

#ifndef STGCPLAYBACK_CABLINGMODEL_STGCROUTERCONFIGURATION_H_
#define STGCPLAYBACK_CABLINGMODEL_STGCROUTERCONFIGURATION_H_

#include "TString.h"
#include <iostream>
#include <vector>

struct sTGCRouterConfiguration
{
	TString type;
	double latency;
	double clock;
	std::vector<int> serializerBankChannel;
	std::vector<int> serializerBank;
	std::vector<double> delay;



	void Dump()
	{
		std::cout << "============>> Router board <<============" << std::endl;
		std::cout << "type: " << type << "latency: " << latency << " clock: " << clock << std::endl;
		for (unsigned int i_ch = 0; i_ch < serializerBankChannel.size(); i_ch++ )
		{
			std::cout << "port: " << i_ch << "serializerBlock: " << serializerBank[i_ch] << " serializerBlockChannel: " << serializerBankChannel[i_ch] << " delay: " << delay[i_ch] << std::endl;
		}

	}

	sTGCRouterConfiguration():
		type(""), latency(100), clock(160), serializerBankChannel(12,0),serializerBank(12,0), delay(12,0)
	{

	}
};



#endif /* STGCPLAYBACK_CABLINGMODEL_STGCROUTERCONFIGURATION_H_ */
