/*
 * MMFE8Configuration.h
 *
 *  Created on: Mar 6, 2019
 *      Author: Enrique.Kajomovitz@cern.ch
 */

#ifndef STGCPLAYBACK_CABLINGMODEL_MMFE8CONFIGURATION_H_
#define STGCPLAYBACK_CABLINGMODEL_MMFE8CONFIGURATION_H_

#include "TString.h"
#include <iostream>

struct MMFE8Configuration
{
	TString type;
	double latency;

	void Dump()
	{
		std::cout << "=================>>> MMFE8 <<<=================" << std::endl;
		std::cout << "Type: " << type << " latency:  "<< latency <<  std::endl;
	}

	MMFE8Configuration():
    type(""), latency(50)
    {}

};


#endif /* STGCPLAYBACK_CABLINGMODEL_MMFE8CONFIGURATION_H_ */
