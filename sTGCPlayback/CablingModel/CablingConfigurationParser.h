/*
 * sTGCGeometryParser.h
 *
 *  Created on: Feb 3, 2019
 *      Author: Enrique.Kajomovitz@cern.ch
 */

#ifndef CablingConfigurationParser_H_
#define CablingConfigurationParser_H_


#include <CablingModel/CablingComposition.h>
#include <CablingModel/ComponentConnection.h>
#include <map>
#include <vector>

#include "TXMLNode.h"
#include "TDOMParser.h"

#include "XMLUtils/XMLNodeParser.h"

#include "CablingModel/sTGCsABConfiguration.h"
#include "CablingModel/sTGCsFEConfiguration.h"
#include "CablingModel/sTGCPadTriggerConfiguration.h"
#include "CablingModel/sTGCRouterConfiguration.h"
#include "CablingModel/TriggerProcessorConfiguration.h"
#include "CablingModel/MMFE8Configuration.h"
#include "CablingModel/ARTASICConfiguration.h"

#include "CablingModel/CablingComposition.h"
#include "CablingModel/ComponentConnection.h"
#include "CablingModel/CompoundComponent.h"
#include "CablingModel/SimpleComponent.h"


using namespace XML_UTILS;


/**
 * This is used to parse an XML document and extract all the cabling configuration information
 * It doesn't recreate exactly the XML structure, but rather flattens the results. For example,
 * it does not care where variables are declared, nor what section a cabling configuration is defined in.
 */
class CablingConfigurationParser {
public:
	CablingConfigurationParser(TXMLNode* in_docRoot = NULL);
    
    void SetXMLRoot(TXMLNode* in_docRoot = NULL);
	void SetConfigurationFile(TString in_configFile);

	void Parse();

	virtual ~CablingConfigurationParser();

	std::map<TString, sTGCsABConfiguration> GetSABLookup() const {return sABLookup;}
	std::map<TString, sTGCsFEConfiguration> GetSFELookup() const {return sFELookup;}
	std::map<TString, sTGCPadTriggerConfiguration> GetPadTriggerLookup() const {return pTLookup;}
	std::map<TString, sTGCRouterConfiguration> GetRouterLookup() const { return routerLookup;}
	std::map<TString, TriggerProcessorConfiguration> GetTriggerProcessorLookup() const { return tPLookup; }
	std::map<TString, MMFE8Configuration> GetMMFE8Lookup() const { return MMFE8Lookup; }
	std::map<TString, ARTASICConfiguration> GetARTASICLookup() const { return ARTASICLookup; }
	std::map<TString, CablingComposition>  GetCablingCompositionLookup() const { return ccLookup;}
	


private:

	void ProcessDefaultNode(TXMLNode* xmlNode);
	void ProcessSFENode(TXMLNode* xmlNode);
	void ProcessSABNode(TXMLNode* xmlNode);
	void ProcessPadTriggerNode(TXMLNode* xmlNode);
	void ProcessRouterNode(TXMLNode* xmlNode);
	void ProcessTriggerProcessor(TXMLNode* xmlNode);
	void ProcessMMFE8Node(TXMLNode* xmlNode);
	void ProcessARTASICNode(TXMLNode* xmlNode);
	void ProcessComposition(TXMLNode* xmlNode);

	std::map<TString, TString> ProcessDictionary(TXMLNode* xmlNode);
	SimpleComponent ProcessSimpleComponent(TXMLNode* xmlNode);
	CompoundComponent ProcessCompoundComponent(TXMLNode* xmlNode);
	ComponentConnection ProcessComponentConnection(TXMLNode* xmlNode);


	std::map<TString, TString> StringToDictionary(TString dictionary);
	std::vector<int> ValueAsIntList(const TString& attribute);
	std::vector<double> ValueAsDoubleList(const TString& attribute);
	std::vector<TString> ValueAsStringList(const TString& attribute);

	DIR_WRT_DET DirValueOf(const TString& attribute);

	sTGCsFEConfiguration& UpsertSFE(const TString& name);
	sTGCsABConfiguration& UpsertSAB(const TString& name);
	sTGCPadTriggerConfiguration& UpsertPT(const TString& name);
	sTGCRouterConfiguration& UpsertRouter(const TString& name);
	MMFE8Configuration& UpsertMMFE8(const TString& name);
	ARTASICConfiguration& UpsertARTASIC(const TString& name);

	TriggerProcessorConfiguration& UpsertTP(const TString& name);
	CablingComposition& UpsertComposition(const TString& name);

	TXMLNode* m_docRoot;
    
    TDOMParser *m_domParser;
    
	TString m_configFile;
	bool m_ownDocRoot;
	XMLNodeParser m_parser;

	std::map<TString, sTGCsABConfiguration> sABLookup;
	std::map<TString, sTGCsFEConfiguration> sFELookup;
	std::map<TString, sTGCPadTriggerConfiguration> pTLookup;
	std::map<TString, sTGCRouterConfiguration> routerLookup;
	std::map<TString, MMFE8Configuration> MMFE8Lookup;
	std::map<TString, ARTASICConfiguration> ARTASICLookup;
	std::map<TString, TriggerProcessorConfiguration> tPLookup;

	std::map<TString, CablingComposition>  ccLookup;
	std::map<TString, TString> varLookup; // variable types will be resolved by specific attributes; wasteful, but simplest
	std::map<TString, TString> arrayLookup; // variable types will be resolved by specific attributes; wasteful, but simplest

};


#endif /* CablingConfigurationParser_H_ */
