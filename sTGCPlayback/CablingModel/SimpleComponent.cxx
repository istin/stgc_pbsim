/*
 * TemplatedCablingComponent.cxx
 *
 *  Created on: Mar 27, 2019
 *      Author: ekajomov
 */

#include <CablingModel/SimpleComponent.h>
#include <iostream>
#include <Utilities/LoggingSvc.h>


SimpleComponent::SimpleComponent() {

}

SimpleComponent::~SimpleComponent() {
}

SimpleComponent SimpleComponent::GetResolved(const std::map<TString,TString>& dictionary)
{

	SimpleComponent result;
	TString resolvedComponentClass = componentClass;
	TString resolvedComponentType = componentType;
	TString resolvedCompositionName = componentName;
	std::map<TString, TString>::const_iterator itr = dictionary.begin();
	for (; itr != dictionary.end(); itr++)
	{
		TString key = TString::Format("${%s}",itr->first.Data());
		resolvedComponentClass.ReplaceAll(key, itr->second);
		resolvedComponentType.ReplaceAll(key, itr->second);
		resolvedCompositionName.ReplaceAll(key, itr->second);
	}

	result.componentClass = resolvedComponentClass;
	result.componentType = resolvedComponentType;
	result.componentName = resolvedCompositionName;

	bool unresolved = false;
	unresolved |= resolvedComponentClass.Contains("${");
	unresolved |= resolvedComponentType.Contains("${");
	unresolved |= resolvedCompositionName.Contains("${");

	if (unresolved)
	{
		LOG_FATAL("WARNING: Unresolved TemplateConnectionDescription");
		throw std::runtime_error("Found unresolved items in template when trying to resolve");
	}

	return result;
}

