/*
 * ComposedComponentTemplate.h
 *
 *  Created on: Mar 30, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */

#ifndef STGCPLAYBACK_CABLINGMODEL_COMPOUNDCOMPONENT_H_
#define STGCPLAYBACK_CABLINGMODEL_COMPOUNDCOMPONENT_H_

#include <map>

#include "TString.h"

#include "CablingModel/CompoundComponent.h"

class CompoundComponent {
public:

	CompoundComponent();
	virtual ~CompoundComponent();

	CompoundComponent GetResolved(const std::map<TString, TString>& globalDictionary);
	TString compositionName;
	std::map<TString, TString> dictionary;
};

#endif /* STGCPLAYBACK_CABLINGMODEL_COMPOUNDCOMPONENT_H_ */
