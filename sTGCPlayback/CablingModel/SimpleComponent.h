  /*
 * TemplatedCablingComponent.h
 *
 *  Created on: Mar 27, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_CABLINGMODEL_SIMPLECOMPONENT_H_
#define STGCPLAYBACK_CABLINGMODEL_SIMPLECOMPONENT_H_

#include "TString.h"
#include <map>


class SimpleComponent {
public:
	SimpleComponent();
	virtual ~SimpleComponent();

	TString componentClass;
	TString componentType;
	TString componentName;

	SimpleComponent GetResolved(const std::map<TString, TString>& dict);

	bool isResolved;


};

#endif /* STGCPLAYBACK_CABLINGMODEL_SIMPLECOMPONENT_H_ */
