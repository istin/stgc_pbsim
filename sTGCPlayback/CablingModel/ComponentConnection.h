/*
 * TemplatedComponentConnectionDescription.h
 *
 *  Created on: Mar 18, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */

#ifndef STGCPLAYBACK_CABLINGMODEL_COMPONENTCONNECTION_H_
#define STGCPLAYBACK_CABLINGMODEL_COMPONENTCONNECTION_H_

#include "CablingModel/ComponentConnection.h"
#include "TString.h"
#include <map>


class ComponentConnection {
public:
	ComponentConnection();
	virtual ~ComponentConnection();

	ComponentConnection GetResolved(const std::map<TString,TString>& dictionary);

	TString destinationClass;
	TString destinationType;
	TString destinationName;
	TString destinationPort;

	TString sourceClass;
	TString sourceType;
	TString sourceName;
	TString sourcePort;

	TString connectionDetails;
	TString propagationDelay;
	TString cableLength;

	TString bandwidth;

};

#endif /* STGCPLAYBACK_CABLINGMODEL_COMPONENTCONNECTION_H_ */
