/*
 * sTGCsFEConfiguration.h
 *
 *  Created on: Feb 23, 2019
 *      Author: ekajomov
 */

#ifndef CABLINGMODEL_STGCSFECONFIGURATION_H_
#define CABLINGMODEL_STGCSFECONFIGURATION_H_
#include "TString.h"

#include "CablingModel/CablingModelEnums.h"
#include <iostream>

struct sTGCsFEConfiguration
{

	// we use FE-global numbering scheme where the first channel is the board is 1 and the last
	// possible channel in the board is 512 (VMM1 1-64, VMM2: 65-128 ...)
	TString type;
	int layer;
	DIR_WRT_DET direction;

	int nActiveVMMChannels; // this is should be the same as the number of strips
	int nActiveVMM;

	int nACtiveTDSChannels;
	int nActiveTDS;
	std::vector<int> firstTDSChannelDisconnected;
	std::vector<int> nTDSChannelDisconnected;

	// Sanity - nActiveChannels + Sum(nChannelsDisconnected) = 512


	sTGCsFEConfiguration():
	type(""), layer(0), direction(DIR_WRT_DET::UNDEFINED),
	nActiveVMMChannels(0), nActiveVMM(0),
	nACtiveTDSChannels(0), nActiveTDS(0){}


	void InitializeMappings()
	{
		channels_connected_to_tds = std::vector<int>(512,1);

		int disconnected = -1;
		for (unsigned int i_block = 0;
				i_block < firstTDSChannelDisconnected.size(); i_block++) {

			int first_channel = firstTDSChannelDisconnected.at(i_block);
			int nChD = nTDSChannelDisconnected.at(i_block);
			for (int i_dch = 0; i_dch < nChD; i_dch++)
			{
				channels_connected_to_tds.at(i_dch + first_channel) = disconnected;
			}
		}
	}

	int ChannelConnectedToTDS(int ch)
	{
		if (channels_connected_to_tds.size() == 0)
			InitializeMappings();
		if (ch > 511 || ch < 0)
			return -1;
		int result = channels_connected_to_tds.at(ch);
		return result;
	}

	void Dump()
	{
		std::cout << "Type: " << type << " layer: " << layer;
		std::cout << " nActiveVMMChannels " << nActiveVMMChannels;
		std::cout << " nActiveVMM: " << nActiveVMM << std::endl;

		std::cout << "Type: " << type << " layer: " << layer;
		std::cout << " nACtiveTDSChannels: " << nACtiveTDSChannels;
		std::cout << " nActiveTDS: " << nActiveTDS << std::endl;

		std::cout << " direction: ";
		if (direction == DIR_WRT_DET::UNDEFINED)
			std::cout << " UNDEFINED";
		else if (direction == DIR_WRT_DET::PARALLEL)
			std::cout << " PARALLEL";
		else
			std::cout << " ANTIPARALLEL";
		std::cout << std::endl;
		for (unsigned int i_dch = 0; i_dch < firstTDSChannelDisconnected.size(); i_dch++)
		{
			std::cout << " firstTDSChannelDisconnected: " << firstTDSChannelDisconnected.at(i_dch);
			std::cout << " nTDSChannelDisconnected: " << nTDSChannelDisconnected.at(i_dch) << std::endl;
		}
		std::cout << std::endl;
	}

protected:
	 std::vector<int> channels_connected_to_tds;
};



#endif /* CABLINGMODEL_STGCSFECONFIGURATION_H_ */
