#!/usr/bin/env python

#an example class to prepare the band configuration
#author : Enrique.Kajomovitz@cern.ch

from PlaybackLoader import Load as pbl
pbl()
from ROOT.STGC_GEOMETRY import QuadrupletConfigurationParser as QuadCfgParser


from ROOT import CablingConfigurationParser as CablingCfgParser
from ROOT import BandsConfigurationGeneration as BandsCfgGen


import os

class QuadParser(QuadCfgParser):
    
    def __init__(self, xmlfile):
        QuadCfgParser.__init__(self)
        #includes both Large and Small quadruplets:
        self.AllowedNames=['sTG1-QL1P','sTG1-QL2P','sTG1-QL3P','sTG1-QL1C','sTG1-QL2C','sTG1-QL3C',
       'sTG1-QS1P','sTG1-QS2P','sTG1-QS3P','sTG1-QS1C','sTG1-QS2C','sTG1-QS3C']
        self.SetConfigurationFile(xmlfile)
        self.SetXMLRoot()
        self.Parse()
        
class CablingParser(CablingCfgParser):
    
    def __init__(self, xmlfile):
        CablingCfgParser.__init__(self)
        self.AllowedNames=['sTG1-QL1P1','sTG1-QL2P1','sTG1-QL3P1','sTG1-QL1C1','sTG1-QL2C1','sTG1-QL3C1',
                           'sTG1-QL1P2','sTG1-QL2P2','sTG1-QL3P2','sTG1-QL1C2','sTG1-QL2C2','sTG1-QL3C2',
                           'sTG1-QL1P3','sTG1-QL2P3','sTG1-QL3P3','sTG1-QL1C3','sTG1-QL2C3','sTG1-QL3C3',
                           'sTG1-QL1P4','sTG1-QL2P4','sTG1-QL3P4','sTG1-QL1C4','sTG1-QL2C4','sTG1-QL3C4',
                           'sTG1-QS1P1','sTG1-QS2P1','sTG1-QS3P1','sTG1-QS1C1','sTG1-QS2C1','sTG1-QS3C1',
                           'sTG1-QS1P2','sTG1-QS2P2','sTG1-QS3P2','sTG1-QS1C2','sTG1-QS2C2','sTG1-QS3C2',
                           'sTG1-QS1P3','sTG1-QS2P3','sTG1-QS3P3','sTG1-QS1C3','sTG1-QS2C3','sTG1-QS3C3',
                           'sTG1-QS1P4','sTG1-QS2P4','sTG1-QS3P4','sTG1-QS1C4','sTG1-QS2C4','sTG1-QS3C4',
                           ]      
        self.SetConfigurationFile(xmlfile)
        self.SetXMLRoot()
        self.Parse()    

    
def main():
    from optparse import OptionParser
    parser = OptionParser(usage="usage: %prog [options] filename")
    parser.add_option("-g", "--geo_xml",
                      action="store",
                      dest="GEO_XMLFILE",
                      default=None,
                      help="GEO XML File to read (MANDATORY)")
    
    parser.add_option("-c", "--cabling_xml",
                      action="store",
                      dest="CABLING_XMLFILE",
                      default=None,
                      help="CABLING XML File to read (MANDATORY)")
    
    parser.add_option("-f", "--fename",
                      action="store",
                      dest="FE",
                      default=None,
                      help="Name of the quad to print (OPTIONAL)\n Default is ALL")
    parser.add_option("-p", "--pt",
                      action="store",
                      dest="padTrigger_filePrefix",
                      default="padTriggerTDSConf",
                      help="Prefix for the padTrigger file")

    (options, args) = parser.parse_args()
    
    if options.GEO_XMLFILE  is None:
        print("an geo xml file must be supplied!")
        parser.print_help()
        os.sys.exit()
    if options.CABLING_XMLFILE  is None:
        print("a cabling xml file must be supplied!")
        parser.print_help()
        os.sys.exit()
    
    if not os.path.isfile(options.GEO_XMLFILE) :
        print( "File"+options.GEO_XMLFILE+" does not exist!" )
        os.sys.exit()    
    
    
    if not os.path.isfile(options.CABLING_XMLFILE) :
        print( "File"+options.CABLING_XMLFILE+" does not exist!" )
        os.sys.exit()
    
      
    fe_names_Large =["sTG1-QL1P1",'sTG1-QL2P1','sTG1-QL3P1','sTG1-QL1C1','sTG1-QL2C1','sTG1-QL3C1',
               'sTG1-QL1P2','sTG1-QL2P2','sTG1-QL3P2','sTG1-QL1C2','sTG1-QL2C2','sTG1-QL3C2',
               'sTG1-QL1P3','sTG1-QL2P3','sTG1-QL3P3','sTG1-QL1C3','sTG1-QL2C3','sTG1-QL3C3',
                'sTG1-QL1P4','sTG1-QL2P4','sTG1-QL3P4','sTG1-QL1C4','sTG1-QL2C4','sTG1-QL3C4']

    fe_names_Small =["sTG1-QS1P1",'sTG1-QS2P1', 'sTG1-QS3P1', 'sTG1-QS1C1', 'sTG1-QS2C1','sTG1-QS3C1',
                           'sTG1-QS1P2','sTG1-QS2P2','sTG1-QS3P2', 'sTG1-QS1C2', 'sTG1-QS2C2','sTG1-QS3C2',
                           'sTG1-QS1P3','sTG1-QS2P3','sTG1-QS3P3','sTG1-QS1C3','sTG1-QS2C3','sTG1-QS3C3',
                           'sTG1-QS1P4','sTG1-QS2P4','sTG1-QS3P4','sTG1-QS1C4','sTG1-QS2C4','sTG1-QS3C4']  
        
    qp=QuadParser(options.GEO_XMLFILE)
    
    cp=CablingParser(options.CABLING_XMLFILE)
    
    quad_lookup=qp.GetQuadLookup()
    
    sab_lookup=cp.GetSABLookup()
    #sab_lookup["sTG1-QS1P1"].Dump()
    
    sef_lookup=cp.GetSFELookup()
    #sef_lookup["sTG1-QS1P1"].Dump()
    
    bcg=BandsCfgGen()
    bcg.SetQuadLookup(quad_lookup)
    bcg.SetSABLookup(sab_lookup)
    bcg.SetSFELookup(sef_lookup)
    
    for fe_name in fe_names_Large:
        bcg.ComputeBandConfig(fe_name)
       
    for fe_name in fe_names_Small:
        #print fe_name
        bcg.ComputeBandConfig(fe_name)

    
    feband_lookup=bcg.GetFEBandLookup()
    #print "-----------xxxxxx-----------"
    #feband_lookup["sTG1-QS1P1"].tdsConfigs[0].Dump()
    #band_to_tds[BAND_ID][LAYER] = TDS_NUMBER
    band_to_tds = []
    split_bands = []
    for band in range(0,127):
        tds_in_layer = []
        split_bands.append("")
        for layer in range(0,8):
            tds_in_layer.append(0)
            tds_in_layer.append(0)
            
        band_to_tds.append(tds_in_layer)
    
    
    
    fe_name_to_tds_offset_Large = {'sTG1-QL1P1':0,'sTG1-QL2P1':4,'sTG1-QL3P1':8,
                             'sTG1-QL1C1':0,'sTG1-QL2C1':4,'sTG1-QL3C1':8,
                             'sTG1-QL1P2':0,'sTG1-QL2P2':4,'sTG1-QL3P2':8,
                             'sTG1-QL1C2':0,'sTG1-QL2C2':4,'sTG1-QL3C2':8,
                             'sTG1-QL1P3':0,'sTG1-QL2P3':4,'sTG1-QL3P3':8,
                             'sTG1-QL1C3':0,'sTG1-QL2C3':4,'sTG1-QL3C3':8,
                             'sTG1-QL1P4':0,'sTG1-QL2P4':4,'sTG1-QL3P4':8,
                             'sTG1-QL1C4':0,'sTG1-QL2C4':4,'sTG1-QL3C4':8}
    
    fe_name_to_layer_Large = {'sTG1-QL1P1':0,'sTG1-QL2P1':0,'sTG1-QL3P1':0,
                        'sTG1-QL1C1':4,'sTG1-QL2C1':4,'sTG1-QL3C1':4,
                        'sTG1-QL1P2':1,'sTG1-QL2P2':1,'sTG1-QL3P2':1,
                        'sTG1-QL1C2':5,'sTG1-QL2C2':5,'sTG1-QL3C2':5,
                        'sTG1-QL1P3':2,'sTG1-QL2P3':2,'sTG1-QL3P3':2,
                        'sTG1-QL1C3':6,'sTG1-QL2C3':6,'sTG1-QL3C3':6,
                        'sTG1-QL1P4':3,'sTG1-QL2P4':3,'sTG1-QL3P4':3,
                        'sTG1-QL1C4':7,'sTG1-QL2C4':7,'sTG1-QL3C4':7}




    fe_name_to_tds_offset_Small = {'sTG1-QS1P1':0,'sTG1-QS2P1':4,'sTG1-QS3P1':8,
                        'sTG1-QS1C1':0,'sTG1-QS2C1':4,'sTG1-QS3C1':8,
                        'sTG1-QS1P2':0,'sTG1-QS2P2':4,'sTG1-QS3P2':8,
                        'sTG1-QS1C2':0,'sTG1-QS2C2':4,'sTG1-QS3C2':8,
                        'sTG1-QS1P3':0,'sTG1-QS2P3':4,'sTG1-QS3P3':8,
                        'sTG1-QS1C3':0,'sTG1-QS2C3':4,'sTG1-QS3C3':8,
                        'sTG1-QS1P4':0,'sTG1-QS2P4':4,'sTG1-QS3P4':8,
                        'sTG1-QS1C4':0,'sTG1-QS2C4':4,'sTG1-QS3C4':8}

    fe_name_to_layer_Small = {'sTG1-QS1P1':4,'sTG1-QS2P1':4,'sTG1-QS3P1':4,
                        'sTG1-QS1C1':0,'sTG1-QS2C1':0,'sTG1-QS3C1':0,
                        'sTG1-QS1P2':5,'sTG1-QS2P2':5,'sTG1-QS3P2':5,
                        'sTG1-QS1C2':1,'sTG1-QS2C2':1,'sTG1-QS3C2':1,
                        'sTG1-QS1P3':6,'sTG1-QS2P3':6,'sTG1-QS3P3':6,
                        'sTG1-QS1C3':2,'sTG1-QS2C3':2,'sTG1-QS3C3':2,
                        'sTG1-QS1P4':7,'sTG1-QS2P4':7,'sTG1-QS3P4':7,
                        'sTG1-QS1C4':3,'sTG1-QS2C4':3,'sTG1-QS3C4':3}


    
    #Configuring Large quadruplets only
    
    for fe_name in fe_names_Large:
        #print fe_name
        for i_tds in range(0,4):
            tdsConfig=feband_lookup[fe_name].tdsConfigs[i_tds]
#            outF_L = open("OutFile_{}_{}_conf.txt".format(fe_name, i_tds), "w") # Create file due for first channel in TDS table
            for b in range(0, tdsConfig.nBands):
                actual_band = tdsConfig.firstBand + b
                secondary_tds = 0
                layer = fe_name_to_layer_Large[fe_name]
                 
                if  band_to_tds[actual_band][2*layer] != 0:
                    secondary_tds = 1
                band_to_tds[actual_band][2*layer + secondary_tds] = tdsConfig.tds_id + fe_name_to_tds_offset_Large[fe_name] 
                if secondary_tds != 0:
                    split_bands[actual_band] = "split"


                First_Channel = tdsConfig.firstChannel[actual_band -1]
                #Write into file
 #               outF_L.write(str(actual_band))
 #               outF_L.write(" ")
 #               outF_L.write(str(First_Channel))
#                outF_L.write("\n")
                    
                    
    f_padTriggerLarge = open(options.padTrigger_filePrefix + "_large.conf", "w+")                    
    for band in range(0,127):
        f_padTriggerLarge.write("%i %i %i %i %i %i %i %i %i %i\n" % ( 0, band, band_to_tds[band][0], band_to_tds[band][2], band_to_tds[band][4], band_to_tds[band][6], band_to_tds[band][8], band_to_tds[band][10], band_to_tds[band][12], band_to_tds[band][14]) )
        #print 0, band, band_to_tds[band][0], band_to_tds[band][2], band_to_tds[band][4], band_to_tds[band][6], band_to_tds[band][8], band_to_tds[band][10], band_to_tds[band][12], band_to_tds[band][14]
        if (band_to_tds[band][1] + band_to_tds[band][3] + band_to_tds[band][5] + band_to_tds[band][7] + band_to_tds[band][9] + band_to_tds[band][11] + band_to_tds[band][13] + band_to_tds[band][15] != 0):
            #print 1, band, band_to_tds[band][1], band_to_tds[band][3], band_to_tds[band][5], band_to_tds[band][7], band_to_tds[band][9], band_to_tds[band][11], band_to_tds[band][13], band_to_tds[band][15]
            f_padTriggerLarge.write("%i %i %i %i %i %i %i %i %i %i\n" % (1, band, band_to_tds[band][1], band_to_tds[band][3], band_to_tds[band][5], band_to_tds[band][7], band_to_tds[band][9], band_to_tds[band][11], band_to_tds[band][13], band_to_tds[band][15] ))
    f_padTriggerLarge.close()
######### Onwards the Small are configured


    #Reinitialisation:
    band_to_tds = []
    split_bands = []
    i_tds = 0
    feband_lookup=bcg.GetFEBandLookup()
    #feband_lookup["sTG1-QS1P1"].tdsConfigs[0].Dump()


    for band in range(0,127):
        tds_in_layer = []
        split_bands.append("")
        for layer in range(0,8):
            tds_in_layer.append(0)
            tds_in_layer.append(0)
            
        band_to_tds.append(tds_in_layer)


    fe_name, band = 0, 0 #Reset dummy indeces
#################################################

    #Configuring Small quadruplets:
    print ("Finished Large quadruplets now configuring Small quadruplets: ")
    for fe_name in fe_names_Small:
        #print fe_name
        for i_tds in range(0,4):
            tdsConfig=feband_lookup[fe_name].tdsConfigs[i_tds]
            outF_S = open("OutFile_{}_{}_conf.txt".format(fe_name, i_tds), "w") # Create file due for first channel in TDS table

            #feband_lookup[fe_name].Dump()

            #print tdsConfig

            #print tdsConfig.nBands
            for a in range(0, tdsConfig.nBands):
                actual_band = tdsConfig.firstBand + a
                secondary_tds = 0
                layer = fe_name_to_layer_Small[fe_name]
                #print fe_name, tdsConfig.tds_id, layer, actual_band 
                if  band_to_tds[actual_band][2*layer] != 0:
                    secondary_tds = 1
                band_to_tds[actual_band][2*layer + secondary_tds] = tdsConfig.tds_id + fe_name_to_tds_offset_Small[fe_name]
                if secondary_tds != 0:
                    split_bands[actual_band] = "split"
                        
                First_Channel = tdsConfig.firstChannel[actual_band -1]
                #Write into file
                outF_S.write(str(actual_band))
                outF_S.write(" ")
                outF_S.write(str(First_Channel))
                outF_S.write("\n")



    f_padTriggerSmall = open(options.padTrigger_filePrefix + "_small.conf", "w+")                    
    for band in range(0,127):
        f_padTriggerSmall.write("%i %i %i %i %i %i %i %i %i %i\n" % ( 0, band, band_to_tds[band][0], band_to_tds[band][2], band_to_tds[band][4], band_to_tds[band][6], band_to_tds[band][8], band_to_tds[band][10], band_to_tds[band][12], band_to_tds[band][14]) )
        #print 0, band, band_to_tds[band][0], band_to_tds[band][2], band_to_tds[band][4], band_to_tds[band][6], band_to_tds[band][8], band_to_tds[band][10], band_to_tds[band][12], band_to_tds[band][14]
        if (band_to_tds[band][1] + band_to_tds[band][3] + band_to_tds[band][5] + band_to_tds[band][7] + band_to_tds[band][9] + band_to_tds[band][11] + band_to_tds[band][13] + band_to_tds[band][15] != 0):
            #print 1, band, band_to_tds[band][1], band_to_tds[band][3], band_to_tds[band][5], band_to_tds[band][7], band_to_tds[band][9], band_to_tds[band][11], band_to_tds[band][13], band_to_tds[band][15]
            f_padTriggerSmall.write("%i %i %i %i %i %i %i %i %i %i\n" % (1, band, band_to_tds[band][1], band_to_tds[band][3], band_to_tds[band][5], band_to_tds[band][7], band_to_tds[band][9], band_to_tds[band][11], band_to_tds[band][13], band_to_tds[band][15] ))
    f_padTriggerSmall.close()

if __name__ == "__main__":
    main()
