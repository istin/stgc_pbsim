#!/usr/bin/env python

#an example class to implement quadruplet QuadrupletConfigurationParser in python
#author : Serhat Istin

from PlaybackLoader import Load as pbl
pbl()
from ROOT.STGC_GEOMETRY import QuadrupletConfigurationParser as QuadCfgParser

import os

class QuadDumper(QuadCfgParser):
    
    def __init__(self, xmlfile):
        QuadCfgParser.__init__(self)
        self.AllowedNames=['sTG1-QL1P','sTG1-QL2P','sTG1-QL3P','sTG1-QL1C','sTG1-QL2C','sTG1-QL3C',
       'sTG1-QS1P','sTG1-QS2P','sTG1-QS3P','sTG1-QS1C','sTG1-QS2C','sTG1-QS3C']        
        self.SetConfigurationFile(xmlfile)
        self.SetXMLRoot()
        self.Parse()
        self.configmap=self.GetQuadLookup()
    def Print(self,name):
        if name not in self.AllowedNames:
            print("Error in name :"+name)
            print("AllowedNames are : ")
            print (self.AllowedNames)
            os.sys.exit()
        self.configmap[name].Dump()
    def Dump(self):
        for q in self.AllowedNames:
            self.Print(q)
        

    
def main():
    from optparse import OptionParser
    parser = OptionParser(usage="usage: %prog [options] filename")
    parser.add_option("-x", "--xml",
                      action="store",
                      dest="XMLFILE",
                      default=None,
                      help="XML File to read (MANDATORY)")


    parser.add_option("-q", "--quadname",
                      action="store",
                      dest="QUAD",
                      default=None,
                      help="Name of the quad to print (OPTIONAL)\n Default is ALL")

    (options, args) = parser.parse_args()
    
    if options.XMLFILE  is None:
        print("an xml file must be supplied!")
        parser.print_help()
        os.sys.exit()
    
    
    
    
    if not os.path.isfile(options.XMLFILE) :
        print( "File"+options.XMLFILE+" does not exist!" )
        os.sys.exit()
        
    qd=QuadDumper(options.XMLFILE)
    if options.QUAD is not None:
        qd.Print(options.QUAD)
    else:
        qd.Dump()


if __name__ == "__main__":
    main()
