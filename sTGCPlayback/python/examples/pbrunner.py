#!/usr/bin/env python

from NSW.playback import PlaybackRunnerWrapper

from NSW.cabling import NSWCablingSystem


        
from ROOT import sTGCTrgAlg,MMTriggerAlg

def main():
    #define some flags if you want to handle them in some subprocesses later on depending on your taste
    CLERRFLAG=1
    FOLDERERRFLAG=2
    RUNBOTHERRFLAG=3
    
    
    from optparse import OptionParser
    import os
    parser = OptionParser(usage="usage: %prog [options] dirname")
    parser.add_option("-d", "--datafolder",
                      action="store",
                      dest="FOLDER",
                      default=None,
                      help="Folder that contains trigger ntuples (OPTIONAL)")

    parser.add_option("-t", "--treename",
                      action="store",
                      dest="TREE",
                      default=None,
                      help="Name of the input Tree (OPTIONAL)")

    parser.add_option("-s","--sTGC",
                      action="store_true",
                      dest="DO_STGC",
                      default=False,
                      help="Run the sTGC trigger alg")

    parser.add_option("-m","--MM",
                      action="store_true",
                      dest="DO_MM",
                      default=False,
                      help="Run the MM trigger alg")
    parser.add_option("-c","--consoleOut",
                      action="store_true",
                      dest="CONSOLEOUT",
                      default=False,
                      help="Stream Messages to console too")
   

    (options, args) = parser.parse_args()

    
    cablingXmlFile=None
    if options.DO_STGC:
        cablingXmlFile=os.environ['PBDATAPATH']+"/sTGC_CablingModel.xml"
    elif options.DO_MM:
         cablingXmlFile=os.environ['PBDATAPATH']+"/MM_CablingModel.xml"   
    else:
        print("No technology is set")
        os.sys.exit()

    runner=PlaybackRunnerWrapper("PlaybackSimu",options.CONSOLEOUT)
    cablingSystem=NSWCablingSystem(cablingXmlFile,runner.GetScheduler())
    if(options.DO_STGC and options.DO_MM):
        print("Error: Currently cannot run the sTGC and MM algorithms simultaneously.")
        parser.print_help()
        os.sys.exit(RUNBOTHERRFLAG)
    elif(options.DO_STGC):
        runner.AddAlg( sTGCTrgAlg() )
    elif(options.DO_MM):
        runner.AddAlg( MMTriggerAlg() )

    #all the CL arguments are optional
    
    if(options.FOLDER is not None):
        inputfolder=os.path.abspath(options.FOLDER)    
        if(not os.path.isdir(inputfolder)):
            print("Error : Folder "+inputfolder+" does not exist")
            os.sys.exit(FOLDERERRFLAG) 
        runner.SetFolder(inputfolder)
    
    if(options.TREE is not None):
        runner.SetTree(options.TREE)
    
    runner.Run()



if __name__ == "__main__":
    main()
