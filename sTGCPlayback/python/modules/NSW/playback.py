from ROOT import DetSvc,AlgorithmManager,PlaybackLoop,LoggingSvc,EventDrivenScheduler

class PlaybackRunnerWrapper:
    def __init__(self, name,consoleout):
        self.name=name
        self.lgSvc=LoggingSvc.instance(consoleout)#.SetMsgLvl(1)
        self.detSvc=DetSvc.instance() #this is reading the geometry xml
        self.algMgr=AlgorithmManager.Instance()
        self.looper=PlaybackLoop()
        self.EventDrivenScheduler=EventDrivenScheduler()
        
    def AddAlg(self,algo):
        self.algMgr.Add(algo)
    
    def SetFolder(self,d):
        self.looper.SetDirectory(d)
    def SetTree(self,t):
        self.looper.SetTree(t)
    
    def Run(self):
        self.looper.Configure()
        self.looper.Run()
        self.looper.Finalize()
        
    def GetScheduler(self):
        return self.EventDrivenScheduler    