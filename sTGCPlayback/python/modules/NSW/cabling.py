
from ROOT import CablingConfigurationParser,sTGCPadTriggerFactory,sTGCBCSignalFactory,sTGCRouterFactory,sTGCsABFactory,sTGCsFEFactory,sTGCTriggerProcessorFactory
from ROOT import ComponentFactoryRegistry,CablingSystemBuilder,ComponentStore,EventDrivenScheduler


class NSWCablingSystem(CablingConfigurationParser):
    def __init__(self, xmlfile,scheduler):
        CablingConfigurationParser.__init__(self)
        
        self.SetConfigurationFile(xmlfile)
        self.Parse()
        self.activeFactories=[]
        self.EventDrivenScheduler=scheduler
        self.SetupAndRegisterFactories()
        self.AssembleSystem()
        
    def SetupAndRegisterFactories(self):
        self.componentFactoryRegistry=ComponentFactoryRegistry()

        self.sTGCPadTriggerFactory=sTGCPadTriggerFactory()
        self.sTGCPadTriggerFactory.SetLookup(self.GetPadTriggerLookup())
        self.componentFactoryRegistry.RegisterFactory("PadTrigger",self.sTGCPadTriggerFactory)
        self.activeFactories.append(self.sTGCPadTriggerFactory)
        
        self.sTGCRouterFactory=sTGCRouterFactory()
        self.sTGCRouterFactory.SetLookup(self.GetRouterLookup())
        self.componentFactoryRegistry.RegisterFactory("Router",self.sTGCRouterFactory)
        self.activeFactories.append(self.sTGCRouterFactory)
        
        self.sTGCsABFactory=sTGCsABFactory()
        self.sTGCsABFactory.SetLookup(self.GetSABLookup())
        self.componentFactoryRegistry.RegisterFactory("sAB",self.sTGCsABFactory)
        self.activeFactories.append(self.sTGCsABFactory)
        
        self.sTGCsFEFactory=sTGCsFEFactory()
        self.sTGCsFEFactory.SetLookup(self.GetSFELookup())
        self.componentFactoryRegistry.RegisterFactory("sFE",self.sTGCsFEFactory)
        self.activeFactories.append(self.sTGCsFEFactory)

        self.sTGCTriggerProcessorFactory=sTGCTriggerProcessorFactory()
        self.sTGCTriggerProcessorFactory.SetLookup(self.GetTriggerProcessorLookup())
        self.componentFactoryRegistry.RegisterFactory("TriggerProcessor",self.sTGCTriggerProcessorFactory)
        self.activeFactories.append(self.sTGCTriggerProcessorFactory)

    def AssembleSystem(self):     
        self.componentStore=ComponentStore.Instance()
        self.cablingSystemBuilder=CablingSystemBuilder()
        self.cablingSystemBuilder.WithCompositionLookup(self.GetCablingCompositionLookup());
        self.cablingSystemBuilder.WithComponentStore(self.componentStore)
        self.cablingSystemBuilder.WithFactoryRegistry(self.componentFactoryRegistry)
        self.cablingSystemBuilder.WithScheduler(self.EventDrivenScheduler)
        self.cablingSystemBuilder.Build("System")

        
    def GetScheduler(self):
        return self.EventDrivenScheduler