/*
 * MMFE8Factory.cxx
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#include "MMTriggerSystemComponents/MMFE8.h"
#include "MMTriggerSystemComponents/MMFE8Factory.h"

MMFE8Factory::MMFE8Factory() {
}

MMFE8Factory::~MMFE8Factory() {
}

void MMFE8Factory::SetLookup(std::map<TString, MMFE8Configuration> lookup) {
  m_lookup = lookup;
}

IComponent* MMFE8Factory::Create(const TString& componentClass,
                                     const TString& componentType, 
                                     const TString& componentName) {
  
  if (componentClass != "MMFE8")
    return NULL;

  std::map<TString, MMFE8Configuration>::iterator itr = m_lookup.find(componentType);
  if (itr == m_lookup.end())
    return NULL;

  MMFE8* obj = new MMFE8();
  obj->SetClass("MMFE8");
  MMFE8Configuration config = itr->second;
  obj->SetType(componentType);
  obj->SetName(componentName);
  obj->SetLatency(config.latency);
  return obj;

}
