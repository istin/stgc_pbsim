/*
 * MMARTASICFactory.h
 *
 *  Created on: Mar 21, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */

#ifndef STGCPLAYBACK_MMTRIGGERSYSTEMCOMPONENTS_MMARTASICFACTORY_H_
#define STGCPLAYBACK_MMTRIGGERSYSTEMCOMPONENTS_MMARTASICFACTORY_H_

#include "Components/IComponentFactory.h"
#include "CablingModel/ARTASICConfiguration.h"
#include <map>

class MMARTASICFactory : public IComponentFactory {
public:

	MMARTASICFactory();
	virtual ~MMARTASICFactory();
	void SetLookup(std::map<TString, ARTASICConfiguration> lookup);
	std::map<TString, ARTASICConfiguration> GetLookup() { return m_lookup; }

	virtual IComponent* Create(const TString& componentClass, const TString& componentType, const TString& componentName);

protected:
	std::map<TString, ARTASICConfiguration> m_lookup;

};

#endif
