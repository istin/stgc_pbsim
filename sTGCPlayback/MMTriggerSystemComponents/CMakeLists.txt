include_directories(${CMAKE_CURRENT_SOURCE_DIR}/..)
FILE(GLOB CompSources *.cxx)
FILE(GLOB CompHeaders *.h)
list(FILTER CompHeaders EXCLUDE REGEX "Linkdef.h$")
set(LIBNAME MMTriggerSystemComponents)
ROOT_GENERATE_DICTIONARY(G__${LIBNAME} ${CompHeaders} LINKDEF Linkdef.h MODULE)



add_library( ${LIBNAME} SHARED ${CompSources} G__${LIBNAME}.cxx)
target_link_libraries(${LIBNAME} ${ROOT_LIBRARIES} Components)

install(TARGETS ${LIBNAME}
    LIBRARY DESTINATION ${PLAYBACK_LIBS}
    )

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/lib${LIBNAME}_rdict.pcm DESTINATION ${PLAYBACK_LIBS}) 

