#include "MMTriggerSystemComponents/MMARTASIC.h"
#include "MMTriggerSystemComponents/MMARTASICFactory.h"
#include "MMTriggerSystemComponents/MMFE8.h"
#include "MMTriggerSystemComponents/MMFE8Factory.h"

#ifdef __ROOTCLING__

#pragma link C++ class MMARTASIC+;
#pragma link C++ class MMARTASICFactory+;
#pragma link C++ class MMFE8+;
#pragma link C++ class MMFE8Factory+;

#endif
