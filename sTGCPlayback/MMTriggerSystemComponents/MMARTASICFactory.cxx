/*
 * MMARTASICFactory.cxx
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#include "MMTriggerSystemComponents/MMARTASIC.h"
#include "MMTriggerSystemComponents/MMARTASICFactory.h"

MMARTASICFactory::MMARTASICFactory() {
}

MMARTASICFactory::~MMARTASICFactory() {
}

void MMARTASICFactory::SetLookup(std::map<TString, ARTASICConfiguration> lookup) {
  m_lookup = lookup;
}

IComponent* MMARTASICFactory::Create(const TString& componentClass,
                                     const TString& componentType, 
                                     const TString& componentName) {
  
  if (componentClass != "ARTASIC")
    return NULL;

  std::map<TString, ARTASICConfiguration>::iterator itr = m_lookup.find(componentType);
  if (itr == m_lookup.end())
    return NULL;

  MMARTASIC* obj = new MMARTASIC();
  obj->SetClass("ARTASIC");
  ARTASICConfiguration config = itr->second;
  obj->SetType(componentType);
  obj->SetName(componentName);
  // obj->SetLatency(config.latency);
  return obj;

}
