/*
 * MMFE8Factory.h
 *
 *  Created on: Mar 21, 2019
 *      Author: Enrique.Kajomovitz@CERN.CH
 */

#ifndef STGCPLAYBACK_MMTRIGGERSYSTEMCOMPONENTS_MMFE8FACTORY_H_
#define STGCPLAYBACK_MMTRIGGERSYSTEMCOMPONENTS_MMFE8FACTORY_H_

#include "Components/IComponentFactory.h"
#include "CablingModel/MMFE8Configuration.h"
#include <map>

class MMFE8Factory : public IComponentFactory {
public:

	MMFE8Factory();
	virtual ~MMFE8Factory();
	void SetLookup(std::map<TString, MMFE8Configuration> lookup);
	std::map<TString, MMFE8Configuration> GetLookup() { return m_lookup; }

	virtual IComponent* Create(const TString& componentClass, const TString& componentType, const TString& componentName);

protected:
	std::map<TString, MMFE8Configuration> m_lookup;

};

#endif
