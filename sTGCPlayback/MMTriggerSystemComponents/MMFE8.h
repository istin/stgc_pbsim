/*
 * MMFE8.h
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_MMTRIGGERSYSTEMCOMPONENTS_MMFE8_H_
#define STGCPLAYBACK_MMTRIGGERSYSTEMCOMPONENTS_MMFE8_H_

#include <map>

#include "TString.h"

#include "Components/BaseComponent.h"
#include "Components/Link.h"
#include "Components/IScheduler.h"

class MMFE8 : public BaseComponent, public INotifiable  {
 public:

  MMFE8();
  virtual ~MMFE8();
  
  virtual void Process();
  virtual void Notify() { this->Process(); }

  virtual void ConnectSource(IComponent* source_component,
                             const TString& source_port,
                             const TString& target_port,
                             double propagationDelay,
                             double length,
                             double bandwith);

  const long GetLatency() const       { return m_latency; }
  void SetLatency(const long latency) { m_latency = latency; }

  // Link<MMFE8Msg>& GetLinkToMMTP();


 protected:

  ICommand* CreateTransmissionCommand();
  long m_latency;

};

#endif
