/*
 * MMFE8.cxx
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#include "MMTriggerSystemComponents/MMFE8.h"

MMFE8::MMFE8() {
}

MMFE8::~MMFE8() {
}

void MMFE8::Process() {
}

void MMFE8::ConnectSource(IComponent* source_component,
                          const TString& source_port,
                          const TString& target_port,
                          double propagationDelay,
                          double length,
                          double bandwith) {
  return;
}

