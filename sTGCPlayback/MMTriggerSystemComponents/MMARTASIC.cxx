/*
 * MMARTASIC.cxx
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#include <sstream>
#include <iomanip>
#include "MMTriggerSystemComponents/MMARTASIC.h"
#include "Components/LinkTransmitter.h"

MMARTASIC::MMARTASIC() {
  m_MMHits = new std::vector< std::shared_ptr<MMHit> >;
  m_MMHits->clear();
}

MMARTASIC::~MMARTASIC() {
}

void MMARTASIC::Process() {

  //for (auto mmfe8: MMFE8s()){
  //  // collect input hits
  //}

  // organize according to BC
  // choose those which should pass
  // build a message to the MMTP
  // send the message

  MMARTASICMsg message;
  // auto cmd = LinkTransmitter<MMARTASICMsg>::TransmitCommand(m_outgoing_link, message);
  // m_scheduler->Schedule(cmd, m_latency, m_latency_unit);

}

void MMARTASIC::ConnectSource(IComponent* source_component,
                              const TString& source_port, 
                              const TString& target_port,
                              double propagationDelay, 
                              double length, 
                              double bandwith) {
  return;
}

bool MMARTASIC::HasHit(int bcid){
  for (auto hit: *GetHits())
    if (hit->bcid == bcid)
      return 1;
  return 0;
}

TString MMARTASIC::GBTMessage(int bcid, int event_number){

  int N_VMM_TOTAL = 32;
  int N_ART_MAX   = 8;

  // sort hits by VMM
  // its fine if this sort is called many times
  std::sort(GetHits()->begin(), GetHits()->end(), 
            [](std::shared_ptr<MMHit> a, std::shared_ptr<MMHit> b) 
            { return a->artvmm < b->artvmm; });

  // empty VMM map
  std::vector<int> vmm_map = {};
  for (int i = 0; i < N_VMM_TOTAL; i++)
    vmm_map.push_back(0);

  // empty ART map
  std::vector<int> art_map = {};

  // fill the data 
  for (auto hit: *GetHits()){
    if (hit->bcid != bcid)
      continue;
    vmm_map[hit->artvmm] = 1;
    if (art_map.size() < N_ART_MAX)
      art_map.push_back(hit->channel_vmm);
  }

  // pad the ART map
  while (art_map.size() < N_ART_MAX)
    art_map.push_back(0);

  auto x = MakeMessage(bcid, vmm_map, art_map);

  return Form("GBTBCID = %4i on fiber %i", bcid, GetFiber());
}

std::bitset<128> MMARTASIC::MakeMessage(int bcid, std::vector<int> vmm_map, std::vector<int> art_map){
  //
  // convert from human-readable to bitset
  // it would be nice if this were less stupidly hardcoded
  //
  int N_LINES = 4;

  // debug
  if (true){
    std::cout << Form("BCID: %i", bcid) << " "
              << Form("ASIC: %i", GetASIC()) << " "
              << Form("Layer: %i", GetLayer()) << " "
              << std::endl;
    std::cout << "VMM MAP: ";
    for (auto obj = vmm_map.rbegin(); obj != vmm_map.rend(); obj++)
      std::cout << *obj;
    std::cout << std::endl;
    std::cout << "ART MAP: ";
    for (auto obj = art_map.rbegin(); obj != art_map.rend(); obj++)
      std::cout << Form("%02i", *obj) << " ";
    std::cout << std::endl;
    std::cout << std::endl;
  }

  // header  :: 16 bits
  // dummy   :: 04 bits
  // bcid    :: 12 bits
  // errors  :: 08 bits
  // vmm map :: 32 bits
  // parity  :: 08 bits
  // art map :: 48 bitss

  std::string header  = "0000000000001100"; // "0x000C"
  std::string dummy   = "0000"; // "0x0"
  std::string bcidstr = std::bitset<12>(bcid).to_string();
  std::string parity  = std::bitset<8>(0)    .to_string();
  std::string errors  = std::bitset<8>(0)    .to_string();
  std::stringstream ss_vmm;
  std::stringstream ss_art;
  for (auto obj = vmm_map.rbegin(); obj != vmm_map.rend(); obj++)
    ss_vmm << *obj;
  for (auto obj = art_map.rbegin(); obj != art_map.rend(); obj++)
    ss_art << std::bitset<6>(*obj).to_string();

  // the word
  auto gbtstr = header + dummy + bcidstr + errors + ss_vmm.str() + parity + ss_art.str();
  std::bitset<128> gbtword(gbtstr);

  // split into hex
  std::vector<std::string> hex_lines = {};
  for (int i = 0; i < N_LINES; i++){
    hex_lines.push_back( std::string() );
    for (int j = 127-32*(i); j > 127-32*(i+1); j--)
      hex_lines.back() += gbtword[j] ? "1" : "0";
  }
  for (auto line: hex_lines)
    std::cout << "Test  " << line << std::endl;

  // debug
  std::cout << "HEAD    " << std::stoi(header,  nullptr, 2) << std::endl;
  std::cout << "BCID    " << std::stoi(bcidstr, nullptr, 2) << std::endl;
  std::cout << "PARITY  " << std::stoi(parity,  nullptr, 2) << std::endl;
  std::cout << "ERRORS  " << std::stoi(errors,  nullptr, 2) << std::endl;
  std::cout << "VMMMAP  " << ss_vmm.str() << std::endl;
  std::cout << "ARTMAP  " << ss_art.str() << std::endl;
  std::cout << "GBTBITS " << gbtword      << std::endl;
  std::cout << "GBTHEX  " << std::endl;
  for (auto line: hex_lines){
    std::stringstream sformatter;
    sformatter << std::hex
               << std::uppercase
               << std::setfill('0')
               << std::setw(8)
               << std::stoi(line, nullptr, 2);
    std::cout << sformatter.str() << " " << GetFiber() << std::endl;
  }

}
