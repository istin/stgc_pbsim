/*
 * MMARTASICMsg.h
 *
 *  Created on: Apr 8, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_MMTRIGGERSYSTEMCOMPONENTS_MMARTASICMSG_H_
#define STGCPLAYBACK_MMTRIGGERSYSTEMCOMPONENTS_MMARTASICMSG_H_

struct MMARTASICMsg
{
  MMARTASICMsg():
    hits(8, 0), vmms(8, 0){}
  std::vector<int> hits;
  std::vector<int> vmms;
};


#endif
