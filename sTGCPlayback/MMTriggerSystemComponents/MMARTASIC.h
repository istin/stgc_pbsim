/*
 * MMARTASIC.h
 *
 *  Created on: Mar 21, 2019
 *      Author: ekajomov
 */

#ifndef STGCPLAYBACK_MMTRIGGERSYSTEMCOMPONENTS_MMARTASIC_H_
#define STGCPLAYBACK_MMTRIGGERSYSTEMCOMPONENTS_MMARTASIC_H_

#include <algorithm>
#include <bitset>
#include <map>

#include "TString.h"

#include "Components/BaseComponent.h"
#include "Components/Link.h"
#include "Components/IScheduler.h"
#include "Components/ICommand.h"
#include "Components/ConstantsAndUnits.h"

#include "MMTriggerSystemComponents/MMARTASICMsg.h"
#include "MMTriggerSystemComponents/MMFE8.h"

#include "Trigger/MMHit.h"

class MMARTASIC : public BaseComponent, public INotifiable  {
 public:

  MMARTASIC();
  virtual ~MMARTASIC();

  virtual void Process();
  virtual void Notify() { this->Process(); }

  virtual void ConnectSource(IComponent* source_component,
                             const TString& source_port, 
                             const TString& target_port,
                             double propagationDelay, 
                             double length, 
                             double bandwith);

  const double GetLatency(PBSim_Units::TIME_UNIT unit) const       { return m_latency; }
  void SetLatency(const long latency, PBSim_Units::TIME_UNIT unit) { m_latency = latency; }

  //std::vector<MMFE8> MMFE8s()                    { return m_MMFE8s;         }
  //std::vector< std::shared_ptr<MMHit> > MMHits() { return m_MMHits;         }
  void ClearHits()                               {        m_MMHits->clear(); }
  int size()                                     { return m_MMHits->size();  }

  bool HasHit(int bcid);
  void AddHit(std::shared_ptr<MMHit> hit) { m_MMHits->push_back(hit); }
  std::vector< std::shared_ptr<MMHit> >* GetHits() { return m_MMHits; }

  void SetFiber(int obj) { m_fiber = obj; }
  void SetLayer(int obj) { m_layer = obj; }
  void SetASIC (int obj) { m_asic  = obj; }
  int GetFiber() { return m_fiber; }
  int GetLayer() { return m_layer; }
  int GetASIC () { return m_asic;  }

  // bool SortByARTVMM(MMHit a, MMHit b) { return a.artvmm < b.artvmm; }
  TString GBTMessage(int bcid, int event_number);
  std::bitset<128> MakeMessage(int bcid, std::vector<int> vmm_map, std::vector<int> art_map);
  
  // Link<MMARTASICMsg>& GetLinkToMMTP();


 protected:

  ICommand* CreateTransmissionCommand();
  long m_latency;
  PBSim_Units::TIME_UNIT m_latency_unit;

  int m_fiber;
  int m_layer;
  int m_asic;
  std::vector<MMFE8>  m_MMFE8s;
  std::vector< std::shared_ptr<MMHit> >* m_MMHits;
  Link<MMARTASICMsg>  m_outgoing_link;

};

#endif
